<%-- 
    Document   : bLogin
    Created on : Aug 9, 2019, 3:45:55 PM
    Author     : Administrator
--%>

<%@page import="ccm.logic.bean.Util"%>
<%@page import="ccm.dao.bean.connectivity"%>

<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page import="ccm.logic.ctlr.appLander"%>
<%@page import="ccm.logic.model.loginAuthentication"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page  session="true"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1></h1>
        <%
            connectivity conn = new connectivity();
            Connection con = null;
            String userid = request.getParameter("userid");
            try {
                con = conn.getLDBConnection();
                PreparedStatement ps = con.prepareStatement("select * from CClogin where usr = ? and pwd = ? and isactive = 1");
                ps.setString(1, userid);
                ps.setString(2, "pharmacy");
                ResultSet rs = ps.executeQuery();
                if (rs.next()) {
                    session.setAttribute("loginid", rs.getString(1));
                    session.setAttribute("usr", rs.getString(2));
                    session.setAttribute("username", rs.getString(3));
                    session.setAttribute("reg_id", rs.getString(5));
                    session.setAttribute("reg_name", rs.getString(6));
                    session.setAttribute("usr_group", rs.getString(7));
                    session.setAttribute("isactive", rs.getString(9));
                    response.sendRedirect("bLander.jsf");
                } else {

                }
            } catch (Exception e) {
                out.println(e.getLocalizedMessage());

            }


        %>
    </body>
</html>
