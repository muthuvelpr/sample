<%-- 
    Document   : samplejsp
    Created on : 21 Sep, 2019, 10:52:35 AM
    Author     : MUTHUVEL
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.util.List"%>
<%@page import="java.lang.reflect.Type"%>
<%@page import="com.google.gson.reflect.TypeToken"%>
<%@page import="java.util.Collection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="ccm.dao.bean.connectivity"%>
<%@page import="ccm.bean.Lookup"%>
<%@page import="ccm.bean.RequestPayload"%>
<%@page import="ccm.bean.SiteItemDetail"%>
<%@page import="ccm.utils.ItemListGenerator"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="com.google.gson.Gson" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <table border="1">
            <tr>
                <th> OrderID</th>
                <th>Pincode </th>
                <th>Requested ArtCode </th>
                <th>Requested QTY </th>
                <th>Available ARTCode </th>
                <th>Available QTY </th>
                <th>SiteID </th>
                <th>Allocated SiteID </th>
            </tr>
            <%
                Connection conn = null;
                String ordId = "", pincode = "", respayload = "", site = "";
                int totalSize = 0;
                int qt = 0;
                List<String> pincodeList = new ArrayList<>();
                pincodeList.add("500001");
                pincodeList.add("500002");
                pincodeList.add("500003");
                pincodeList.add("500004");
                pincodeList.add("500005");
                pincodeList.add("500006");
                pincodeList.add("500007");
                pincodeList.add("500008");
                pincodeList.add("500009");
                pincodeList.add("500010");
                pincodeList.add("500011");
                pincodeList.add("500012");
                pincodeList.add("500013");
                pincodeList.add("500014");
                pincodeList.add("500015");
                pincodeList.add("500016");
                pincodeList.add("500017");
                pincodeList.add("500018");
                pincodeList.add("500019");
                pincodeList.add("500020");
                pincodeList.add("500022");
                pincodeList.add("500023");
                pincodeList.add("500024");
                pincodeList.add("500025");
                pincodeList.add("500026");
                pincodeList.add("500027");
                pincodeList.add("500028");
                pincodeList.add("500029");
                pincodeList.add("500030");
                pincodeList.add("500031");
                pincodeList.add("500032");
                pincodeList.add("500033");
                pincodeList.add("500034");
                pincodeList.add("500035");
                pincodeList.add("500036");
                pincodeList.add("500037");
                pincodeList.add("500038");
                pincodeList.add("500039");
                pincodeList.add("500040");
                pincodeList.add("500041");
                pincodeList.add("500042");
                pincodeList.add("500044");
                pincodeList.add("500045");
                pincodeList.add("500046");
                pincodeList.add("500047");
                pincodeList.add("500048");
                pincodeList.add("500049");
                pincodeList.add("500050");
                pincodeList.add("500051");
                pincodeList.add("500052");
                pincodeList.add("500053");
                pincodeList.add("500054");
                pincodeList.add("500055");
                pincodeList.add("500056");
                pincodeList.add("500057");
                pincodeList.add("500058");
                pincodeList.add("500059");
                pincodeList.add("500060");
                pincodeList.add("500061");
                pincodeList.add("500062");
                pincodeList.add("500063");
                pincodeList.add("500064");
                pincodeList.add("500065");
                pincodeList.add("500066");
                pincodeList.add("500067");
                pincodeList.add("500068");
                pincodeList.add("500069");
                pincodeList.add("500070");
                pincodeList.add("500071");
                pincodeList.add("500072");
                pincodeList.add("500073");
                pincodeList.add("500074");
                pincodeList.add("500075");
                pincodeList.add("500076");
                pincodeList.add("500077");
                pincodeList.add("500079");
                pincodeList.add("500080");
                pincodeList.add("500081");
                pincodeList.add("500082");
                pincodeList.add("500083");
                pincodeList.add("500084");
                pincodeList.add("500085");
                pincodeList.add("500086");
                pincodeList.add("500087");
                pincodeList.add("500088");
                pincodeList.add("500089");
                pincodeList.add("500090");
                pincodeList.add("500091");
                pincodeList.add("500092");
                pincodeList.add("500093");
                pincodeList.add("500094");
                pincodeList.add("500095");
                pincodeList.add("500096");
                pincodeList.add("500097");
                pincodeList.add("500098");
                pincodeList.add("500100");
                pincodeList.add("500107");
                pincodeList.add("501218");
                pincodeList.add("502032");
                pincodeList.add("502325");
                pincodeList.add("600001");
                pincodeList.add("600002");
                pincodeList.add("600003");
                pincodeList.add("600004");
                pincodeList.add("600005");
                pincodeList.add("600006");
                pincodeList.add("600007");
                pincodeList.add("600008");
                pincodeList.add("600009");
                pincodeList.add("600010");
                pincodeList.add("600011");
                pincodeList.add("600012");
                pincodeList.add("600013");
                pincodeList.add("600014");
                pincodeList.add("600015");
                pincodeList.add("600016");
                pincodeList.add("600017");
                pincodeList.add("600018");
                pincodeList.add("600019");
                pincodeList.add("600020");
                pincodeList.add("600021");
                pincodeList.add("600022");
                pincodeList.add("600023");
                pincodeList.add("600024");
                pincodeList.add("600025");
                pincodeList.add("600026");
                pincodeList.add("600027");
                pincodeList.add("600028");
                pincodeList.add("600032");
                pincodeList.add("600033");
                pincodeList.add("600034");
                pincodeList.add("600035");
                pincodeList.add("600036");
                pincodeList.add("600037");
                pincodeList.add("600038");
                pincodeList.add("600039");
                pincodeList.add("600040");
                pincodeList.add("600041");
                pincodeList.add("600042");
                pincodeList.add("600043");
                pincodeList.add("600044");
                pincodeList.add("600045");
                pincodeList.add("600046");
                pincodeList.add("600047");
                pincodeList.add("600048");
                pincodeList.add("600049");
                pincodeList.add("600051");
                pincodeList.add("600053");
                pincodeList.add("600055");
                pincodeList.add("600056");
                pincodeList.add("600057");
                pincodeList.add("600058");
                pincodeList.add("600059");
                pincodeList.add("600060");
                pincodeList.add("600061");
                pincodeList.add("600062");
                pincodeList.add("600063");
                pincodeList.add("600064");
                pincodeList.add("600065");
                pincodeList.add("600066");
                pincodeList.add("600067");
                pincodeList.add("600068");
                pincodeList.add("600069");
                pincodeList.add("600070");
                pincodeList.add("600071");
                pincodeList.add("600072");
                pincodeList.add("600073");
                pincodeList.add("600074");
                pincodeList.add("600075");
                pincodeList.add("600076");
                pincodeList.add("600077");
                pincodeList.add("600078");
                pincodeList.add("600079");
                pincodeList.add("600080");
                pincodeList.add("600081");
                pincodeList.add("600082");
                pincodeList.add("600083");
                pincodeList.add("600084");
                pincodeList.add("600085");
                pincodeList.add("600086");
                pincodeList.add("600087");
                pincodeList.add("600088");
                pincodeList.add("600089");
                pincodeList.add("600090");
                pincodeList.add("600091");
                pincodeList.add("600092");
                pincodeList.add("600093");
                pincodeList.add("600094");
                pincodeList.add("600095");
                pincodeList.add("600096");
                pincodeList.add("600097");
                pincodeList.add("600098");
                pincodeList.add("600099");
                pincodeList.add("600100");
                pincodeList.add("600101");
                pincodeList.add("600102");
                pincodeList.add("600103");
                pincodeList.add("600104");
                pincodeList.add("600106");
                pincodeList.add("600107");
                pincodeList.add("600110");
                pincodeList.add("600113");
                pincodeList.add("600115");
                pincodeList.add("600116");
                pincodeList.add("600117");
                pincodeList.add("600118");
                pincodeList.add("600119");
                pincodeList.add("600122");
                pincodeList.add("600123");
                pincodeList.add("600125");
                pincodeList.add("600126");
                pincodeList.add("600129");
                pincodeList.add("600130");
                pincodeList.add("603103");
                pincodeList.add("603202");
                pincodeList.add("603211");

                ItemListGenerator ig = new ItemListGenerator();
                try {
                    Gson gson = new Gson();
                    String reqPayloadStr = "", resPayloadStr = "";
                    conn = connectivity.getLDBConnection();
                    PreparedStatement ps = conn.prepareStatement("select distinct(oal.orderid),oal.vendorName,t.reqpayload,t.siteitemdetail,t.resppayload from CC_OrderAllocationLog oal, tat_log t where oal.vendorName in ('Askapollo','Online') and CONVERT (DATE, reqDateTime)>=CONVERT (DATE, '17-Oct-2019')AND CONVERT (DATE,reqDateTime) <=CONVERT (DATE, '17-Oct-2019') and oal.reqPayLoad = t.reqpayload and ( siteitemdetail is not null and siteitemdetail <>'No sites are available with the requested Items')");

                    ResultSet rs = ps.executeQuery();
                    while (rs.next()) {
                        ordId = rs.getString(1);

                        reqPayloadStr = rs.getString(3);
                        resPayloadStr = rs.getString(4);
                        respayload = rs.getString(5);

                        RequestPayload reqpay = gson.fromJson(reqPayloadStr, RequestPayload.class);
                        String allotsiteStr = respayload.replaceAll("[^0-9]", "");
                        String allotSite = allotsiteStr.substring(allotsiteStr.length() - 5);
                        boolean required = false;
                        String postalcode = reqpay.getPostalcode();
//                        for (int j = 0; j < pincodeList.size(); j++) {
//
//                            if (pincodeList.get(j).equals(postalcode)) {
//                                required = true;
//                                break;
//                            }
//
//                        }

//                        if (required) {
                        List<SiteItemDetail> siteItemList = Arrays.asList(gson.fromJson(resPayloadStr, SiteItemDetail[].class));
                        pincode = reqpay.getPostalcode();
                        totalSize = siteItemList.size() * reqpay.getLookup().size();
                        for (int i = 0; i < reqpay.getLookup().size(); i++) {
                            List<SiteItemDetail> siteList = ig.generateListBySku(reqpay.getLookup().get(i).getSku(), siteItemList);
            %>


            <%
                if (siteList.size() > 0) {
            %>

            <tr>
                <td rowspan="<%= siteList.size()%>">'<%= ordId%></td>
                <td rowspan="<%= siteList.size()%>"><%= pincode%></td>
                <td rowspan="<%= siteList.size()%>"><%= reqpay.getLookup().get(i).getSku()%></td>
                <td rowspan="<%= siteList.size()%>"><%= reqpay.getLookup().get(i).getQty()%></td>
                <%
                    for (int j = 0; j < siteList.size(); j++) {
                %>

                <td>
                    <table>

                        <tr>

                            <td>
                                <%= siteList.get(j).getListItem().getArtCode()%>

                        </tr>
                    </table>
                </td>
                <td>
                    <table>
                        <tr>
                            <td>
                                <%
                                    if (siteList.get(j).getListItem().getQty() <= 0) {
                                        qt = 0;
                                    } else {
                                        qt = siteList.get(j).getListItem().getQty();
                                    }
                                %>

                                <%= qt%></td>
                        </tr>
                    </table>
                </td>
                <td>
                    <table>
                        <tr>
                            <td>   <%= siteList.get(j).getSiteId()%></td>
                        </tr>
                    </table>
                </td>
                <td>
                    <table>
                        <tr>
                            <td ><%= allotSite%></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <%
                }
            } else {
            %>
            <tr>
                <td >'<%= ordId%></td>
                <td ><%= pincode%></td>
                <td ><%= reqpay.getLookup().get(i).getSku()%></td>
                <td >
                    <%= reqpay.getLookup().get(i).getQty()%>
                </td>
                <td></td>
                <td></td>
                <td></td>
                <td>
                    <table>
                        <tr>
                            <td><%= allotSite%></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <%
                            }
                        }

//                        }
                    }
                } catch (Exception ex) {
                    System.out.println(ex.getLocalizedMessage());
                }

            %>
        </table>
    </body>
</html>
