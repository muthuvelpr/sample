/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ccm.report.model;

/**
 *
 * @author Pitchu
 * @created 31 Oct, 2014 5:23:40 PM
 *
 */
public class summaryReport {

    private String ordno, siteid, sitename, Regname, patname, patmobileno, gender, status, statusVal, createdby, ordersource, CancelRemarks, paymentmode, PatAddr, invvalue, invno, tpordid, availability, delaySMSBTN, commentBTN, Cust_city, Cust_postalcode;
    Long count;
    Double salevalue, cash, credit;
    String verdate, initdate, deldate, canceldate, stock, orddate, ordamount, returnDate, retamount;
//ordno	siteid,sitename,orddate,initdate,deldate,Regname,patname,patmobileno,gender,status,statusVal,createdby,ordersource,canceldate,CancelRemarks,paymentmode,PatAddr,invvalue,invno

    public summaryReport(String ordno, String siteid, String sitename, String verdate, String initdate, String deldate, String Regname, String patname, String patmobileno, String gender, String status, String statusVal, String createdby, String ordersource, String canceldate, String CancelRemarks, String paymentmode, String invvalue, String invno, String tpordid, String availability, String delaySMSBTN, Double cash, Double credit, String commentBTN, String orddate, String ordamount, String returnDate, String retamount, String Cust_city, String Cust_postalcode) {
        this.ordno = ordno;
        this.siteid = siteid;
        this.sitename = sitename;
        this.verdate = verdate;
        this.initdate = initdate;
        this.deldate = deldate;
        this.Regname = Regname;
        this.patname = patname;
        this.patmobileno = patmobileno;
        this.gender = gender;
        this.status = status;
        this.statusVal = statusVal;
        this.createdby = createdby;
        this.ordersource = ordersource;
        this.canceldate = canceldate;
        this.CancelRemarks = CancelRemarks;
        this.paymentmode = paymentmode;
        this.invvalue = invvalue;
        this.invno = invno;
        this.tpordid = tpordid;
        this.availability = availability;
        this.delaySMSBTN = delaySMSBTN;
        this.cash = cash;
        this.credit = credit;
        this.commentBTN = commentBTN;
        this.orddate = orddate;
        this.ordamount = ordamount;
        this.returnDate = returnDate;
        this.retamount = retamount;
        this.Cust_city = Cust_city;
        this.Cust_postalcode = Cust_postalcode;
    }

    public String getVerdate() {
        return verdate;
    }

    public void setVerdate(String verdate) {
        this.verdate = verdate;
    }

    public String getOrdamount() {
        return ordamount;
    }

    public void setOrdamount(String ordamount) {
        this.ordamount = ordamount;
    }

    public Double getCash() {
        return cash;
    }

    public void setCash(Double cash) {
        this.cash = cash;
    }

    public Double getCredit() {
        return credit;
    }

    public void setCredit(Double credit) {
        this.credit = credit;
    }

    public String getDelaySMSBTN() {
        return delaySMSBTN;
    }

    public void setDelaySMSBTN(String delaySMSBTN) {
        this.delaySMSBTN = delaySMSBTN;
    }

    public String getAvailability() {
        return availability;
    }

    public void setAvailability(String availability) {
        this.availability = availability;
    }

    public summaryReport(String ordersource, Long count) {
        this.ordersource = ordersource;
        this.count = count;
    }

    public summaryReport(String ordersource, String Regname, Double salevalue) {
        this.ordersource = ordersource;
        this.Regname = Regname;
        this.salevalue = salevalue;
    }

    public String getTpordid() {
        return tpordid;
    }

    public void setTpordid(String tpordid) {
        this.tpordid = tpordid;
    }

    public Double getSalevalue() {
        return salevalue;
    }

    public void setSalevalue(Double salevalue) {
        this.salevalue = salevalue;
    }

    @Override
    public String toString() {
        return ordersource + "," + count;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public String getOrdno() {
        return ordno;
    }

    public void setOrdno(String ordno) {
        this.ordno = ordno;
    }

    public String getSiteid() {
        return siteid;
    }

    public void setSiteid(String siteid) {
        this.siteid = siteid;
    }

    public String getSitename() {
        return sitename;
    }

    public void setSitename(String sitename) {
        this.sitename = sitename;
    }

    public String getRegname() {
        return Regname;
    }

    public void setRegname(String Regname) {
        this.Regname = Regname;
    }

    public String getPatname() {
        return patname;
    }

    public void setPatname(String patname) {
        this.patname = patname;
    }

    public String getPatmobileno() {
        return patmobileno;
    }

    public void setPatmobileno(String patmobileno) {
        this.patmobileno = patmobileno;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusVal() {
        return statusVal;
    }

    public void setStatusVal(String statusVal) {
        this.statusVal = statusVal;
    }

    public String getCreatedby() {
        return createdby;
    }

    public void setCreatedby(String createdby) {
        this.createdby = createdby;
    }

    public String getOrdersource() {
        return ordersource;
    }

    public void setOrdersource(String ordersource) {
        this.ordersource = ordersource;
    }

    public String getOrddate() {
        return orddate;
    }

    public void setOrddate(String orddate) {
        this.orddate = orddate;
    }

    public String getInitdate() {
        return initdate;
    }

    public void setInitdate(String initdate) {
        this.initdate = initdate;
    }

    public String getDeldate() {
        return deldate;
    }

    public void setDeldate(String deldate) {
        this.deldate = deldate;
    }

    public String getCanceldate() {
        return canceldate;
    }

    public void setCanceldate(String canceldate) {
        this.canceldate = canceldate;
    }

    public String getCancelRemarks() {
        return CancelRemarks;
    }

    public void setCancelRemarks(String CancelRemarks) {
        this.CancelRemarks = CancelRemarks;
    }

    public String getPaymentmode() {
        return paymentmode;
    }

    public void setPaymentmode(String paymentmode) {
        this.paymentmode = paymentmode;
    }

    public String getPatAddr() {
        return PatAddr;
    }

    public void setPatAddr(String PatAddr) {
        this.PatAddr = PatAddr;
    }

    public String getInvvalue() {
        return invvalue;
    }

    public void setInvvalue(String invvalue) {
        this.invvalue = invvalue;
    }

    public String getInvno() {
        return invno;
    }

    public void setInvno(String invno) {
        this.invno = invno;
    }

    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }

    public String getCommentBTN() {
        return commentBTN;
    }

    public void setCommentBTN(String commentBTN) {
        this.commentBTN = commentBTN;
    }

    public String getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(String returnDate) {
        this.returnDate = returnDate;
    }

    public String getRetamount() {
        return retamount;
    }

    public void setRetamount(String retamount) {
        this.retamount = retamount;
    }

    public String getCust_city() {
        return Cust_city;
    }

    public void setCust_city(String Cust_city) {
        this.Cust_city = Cust_city;
    }

    public String getCust_postalcode() {
        return Cust_postalcode;
    }

    public void setCust_postalcode(String Cust_postalcode) {
        this.Cust_postalcode = Cust_postalcode;
    }

}
