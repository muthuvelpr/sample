/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ccm.report.model;

/**
 *
 * @author Lenovo
 */
public class otpLog {

    private String siteid, contactno, otp, processby, loggedon;
    Long count;

    public otpLog(String processby, Long count) {
        this.processby = processby;
        this.count = count;
    }

    public otpLog(String siteid, String contactno, String otp, String processby, String loggedon) {
        this.siteid = siteid;
        this.contactno = contactno;
        this.otp = otp;
        this.processby = processby;
        this.loggedon = loggedon;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public String getSiteid() {
        return siteid;
    }

    public void setSiteid(String siteid) {
        this.siteid = siteid;
    }

    public String getContactno() {
        return contactno;
    }

    public void setContactno(String contactno) {
        this.contactno = contactno;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getProcessby() {
        return processby;
    }

    public void setProcessby(String processby) {
        this.processby = processby;
    }

    public String getLoggedon() {
        return loggedon;
    }

    public void setLoggedon(String loggedon) {
        this.loggedon = loggedon;
    }

}
