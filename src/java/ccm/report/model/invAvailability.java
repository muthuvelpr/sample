/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ccm.report.model;

/**
 *
 * @author Lenovo
 */
public class invAvailability {

    String artCode, expiry;
    int qty;
    double mrp;

    public invAvailability(String artCode, int qty) {
        this.artCode = artCode;
        this.qty = qty;
    }

    public double getMrp() {
        return mrp;
    }

    public void setMrp(double mrp) {
        this.mrp = mrp;
    }

    public invAvailability(String artCode, int qty, double mrp, String expiry) {
        this.artCode = artCode;
        this.qty = qty;
        this.mrp = mrp;
        this.expiry = expiry;
    }

    public String getExpiry() {
        return expiry;
    }

    public void setExpiry(String expiry) {
        this.expiry = expiry;
    }

    public String getArtCode() {
        return artCode;
    }

    public void setArtCode(String artCode) {
        this.artCode = artCode;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

}
