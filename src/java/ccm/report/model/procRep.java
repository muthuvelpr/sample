/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ccm.report.model;

/**
 *
 * @author Pitchu
 * @created 31 Oct, 2014 5:23:40 PM
 *
 */
public class procRep {

    private int ord, siteid;
    private String mobileno, region, patname, orderby, gender, orddate, statusurl;

    public procRep(int ord, String orddate, int siteid, String region, String patname, String mobileno, String gender, String statusurl, String orderby) {
        this.ord = ord;
        this.orddate = orddate;
        this.siteid = siteid;
        this.mobileno = mobileno;
        this.statusurl = statusurl;
        this.region = region;
        this.patname = patname;
        this.orderby = orderby;
        this.gender = gender;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getMobileno() {
        return mobileno;
    }

    public void setMobileno(String mobileno) {
        this.mobileno = mobileno;
    }

    public int getOrd() {
        return ord;
    }

    public void setOrd(int ord) {
        this.ord = ord;
    }

    public String getOrddate() {
        return orddate;
    }

    public void setOrddate(String orddate) {
        this.orddate = orddate;
    }

    public String getOrderby() {
        return orderby;
    }

    public void setOrderby(String orderby) {
        this.orderby = orderby;
    }

    public String getPatname() {
        return patname;
    }

    public void setPatname(String patname) {
        this.patname = patname;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public int getSiteid() {
        return siteid;
    }

    public void setSiteid(int siteid) {
        this.siteid = siteid;
    }

    public String getStatusurl() {
        return statusurl;
    }

    public void setStatusurl(String statusurl) {
        this.statusurl = statusurl;
    }
}
