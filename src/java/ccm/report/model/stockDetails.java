/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ccm.report.model;

/**
 *
 * @author Lenovo
 */
public class stockDetails {

    String ITEMID, ITEMNAME, SITEID, NAME, BATCHID, LOCATIONID, EXPDATE;
    int QTY;
    double MRP;

    public stockDetails(String ITEMID, String ITEMNAME, String SITEID, String NAME, String BATCHID, String LOCATIONID, String EXPDATE, int QTY, double MRP) {
        this.ITEMID = ITEMID;
        this.ITEMNAME = ITEMNAME;
        this.SITEID = SITEID;
        this.NAME = NAME;
        this.BATCHID = BATCHID;
        this.LOCATIONID = LOCATIONID;
        this.EXPDATE = EXPDATE;
        this.QTY = QTY;
        this.MRP = MRP;
    }

    public String getITEMID() {
        return ITEMID;
    }

    public void setITEMID(String ITEMID) {
        this.ITEMID = ITEMID;
    }

    public String getITEMNAME() {
        return ITEMNAME;
    }

    public void setITEMNAME(String ITEMNAME) {
        this.ITEMNAME = ITEMNAME;
    }

    public String getSITEID() {
        return SITEID;
    }

    public void setSITEID(String SITEID) {
        this.SITEID = SITEID;
    }

    public String getNAME() {
        return NAME;
    }

    public void setNAME(String NAME) {
        this.NAME = NAME;
    }

    public String getBATCHID() {
        return BATCHID;
    }

    public void setBATCHID(String BATCHID) {
        this.BATCHID = BATCHID;
    }

    public String getLOCATIONID() {
        return LOCATIONID;
    }

    public void setLOCATIONID(String LOCATIONID) {
        this.LOCATIONID = LOCATIONID;
    }

    public String getEXPDATE() {
        return EXPDATE;
    }

    public void setEXPDATE(String EXPDATE) {
        this.EXPDATE = EXPDATE;
    }

    public int getQTY() {
        return QTY;
    }

    public void setQTY(int QTY) {
        this.QTY = QTY;
    }

    public double getMRP() {
        return MRP;
    }

    public void setMRP(double MRP) {
        this.MRP = MRP;
    }

}
