/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ccm.report.model;

/**
 * @created for APOLLO PHARMACY DPAPP
 * @author PITCHU
 * @created Aug 3, 2017 8:17:19 PM
 */
public class tpOrderClose {

    private String ord;
    private String siteid;
    private String tpord;
//    private String mobileno;
//    private String region;
    private String patname;
//    private String orderby;
//    private String gender;
    private String orddate;
    private String initdate;
//    private String deldate;
//    private String statusurl;
    private String statusval;
    private String orderSource;
//    private String cancelDate;
//    private String cancelRemarks;
//    private String paymentMode;
    private String commAddr1;

    /*public tpOrderClose(String ord, int siteid, String orddate, String initdate, String deldate, String region, String patname, String mobileno, String gender, String statusurl, String statusval, String orderby, String orderSource, String cancelDate, String cancelRemarks, String paymentMode, String commAddr1, int tpord) {
        this.ord = ord;
        this.siteid = siteid;
        this.mobileno = mobileno;
        this.region = region;
        this.patname = patname;
        this.orderby = orderby;
        this.gender = gender;
        this.orddate = orddate;
        this.initdate = initdate;
        this.deldate = deldate;
        this.statusurl = statusurl;
        this.statusval = statusval;
        this.orderSource = orderSource;
        this.cancelDate = cancelDate;
        this.cancelRemarks = cancelRemarks;
        this.paymentMode = paymentMode;
        this.commAddr1 = commAddr1;
        this.tpord = tpord;
    }*/
    public tpOrderClose(String siteid, String ord, String tpord, String orddate, String initdate, String orderSource, String statusval, String patname, String commAddr1) {
        this.ord = ord;
        this.siteid = siteid;
        this.tpord = tpord;
        this.orddate = orddate;
        this.initdate = initdate;
        this.orderSource = orderSource;
        this.statusval = statusval;
        this.patname = patname;
        this.commAddr1 = commAddr1;
    }

    public String getOrd() {
        return ord;
    }

    public void setOrd(String ord) {
        this.ord = ord;
    }

    public String getSiteid() {
        return siteid;
    }

    public void setSiteid(String siteid) {
        this.siteid = siteid;
    }

    public String getTpord() {
        return tpord;
    }

    public void setTpord(String tpord) {
        this.tpord = tpord;
    }

    public String getOrddate() {
        return orddate;
    }

    public void setOrddate(String orddate) {
        this.orddate = orddate;
    }

    public String getInitdate() {
        return initdate;
    }

    public void setInitdate(String initdate) {
        this.initdate = initdate;
    }

    public String getOrderSource() {
        return orderSource;
    }

    public void setOrderSource(String orderSource) {
        this.orderSource = orderSource;
    }

    public String getStatusval() {
        return statusval;
    }

    public void setStatusval(String statusval) {
        this.statusval = statusval;
    }

    public String getPatname() {
        return patname;
    }

    public void setPatname(String patname) {
        this.patname = patname;
    }

    public String getCommAddr1() {
        return commAddr1;
    }

    public void setCommAddr1(String commAddr1) {
        this.commAddr1 = commAddr1;
    }

}
