/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ccm.report.model;

import ccm.logic.bean.SiteChangeReportBeanLive;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class SiteChangeReportDetail {

    List<SiteChangeReportBeanLive> scrb = new ArrayList<>();

    public List<SiteChangeReportBeanLive> generateDetail(Connection _con, String fDate, String tDate) {
        scrb.clear();
        try {
            PreparedStatement ps = _con.prepareStatement("select sc.ORDNO,ti.TPORDID,h.orderSource,sc.ORIGINSITEID,c.username,h.ordDate,sc.DESTINATIONSITEID,c1.username,sc.CHANGEDON from CC_SITECHANGE sc, CC_Ordhead h, CClogin c,CClogin c1, CC_TPOrderInfo ti  where sc.ORDNO = h.ordNo  and h.loginId = c.id and sc.CHANGEDBY = c1.id and cast(h.ordNo as nvarchar) = ti.APORDID and sc.ORIGINSITEID is not null and cast(sc.CHANGEDON as date) between ? and ?  order by sc.changedon asc");
            ps.setString(1, fDate);
            ps.setString(2, tDate);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                
                scrb.add(new SiteChangeReportBeanLive(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6).substring(0, 16), rs.getString(7), rs.getString(8), rs.getString(9).substring(0, 16)));
            }
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
        }
        return scrb;
    }
}
