/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ccm.report.model;

/**
 * @created for APOLLO PHARMACY CRM
 * @author PITCHU
 * @created Mar 30, 2017 3:09:03 PM
 */
public class billingInfo {

    public String apOrdId, siteId, tId, invNum, invAmt, cash, credit;

    public billingInfo(String apOrdId, String siteId, String tId, String invNum, String invAmt, String cash, String credit) {
        this.apOrdId = apOrdId;
        this.siteId = siteId;
        this.tId = tId;
        this.invNum = invNum;
        this.invAmt = invAmt;
        this.cash = cash;
        this.credit = credit;
    }

    public String getApOrdId() {
        return apOrdId;
    }

    public void setApOrdId(String apOrdId) {
        this.apOrdId = apOrdId;
    }

    public String getSiteId() {
        return siteId;
    }

    public void setSiteId(String siteId) {
        this.siteId = siteId;
    }

    public String gettId() {
        return tId;
    }

    public void settId(String tId) {
        this.tId = tId;
    }

    public String getInvNum() {
        return invNum;
    }

    public void setInvNum(String invNum) {
        this.invNum = invNum;
    }

    public String getInvAmt() {
        return invAmt;
    }

    public void setInvAmt(String invAmt) {
        this.invAmt = invAmt;
    }

    public String getCash() {
        return cash;
    }

    public void setCash(String cash) {
        this.cash = cash;
    }

    public String getCredit() {
        return credit;
    }

    public void setCredit(String credit) {
        this.credit = credit;
    }

}
