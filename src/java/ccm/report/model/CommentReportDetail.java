/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ccm.report.model;

import ccm.logic.bean.CommentReportBean;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class CommentReportDetail implements Serializable {

    List<CommentReportBean> crl = new ArrayList<>();

    public List<CommentReportBean> generateCommentReport(Connection _con, String fDate, String tDate) {
        crl.clear();
        try {
            PreparedStatement ps = _con.prepareStatement("select c.ordno,c.commentdata,cl.username,c.CommentOn,c.OrderSource from CC_OrderComments c, cclogin cl where c.AgentID = cl.id and cast(commenton as date) between ? and ? order by ordno,CommentOn desc");
            ps.setString(1, fDate);
            ps.setString(2, tDate);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                crl.add(new CommentReportBean(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4).substring(0, 16), rs.getString(5)));
            }
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
        }
        return crl;
    }
}
