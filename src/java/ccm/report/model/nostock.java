/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ccm.report.model;

/**
 *
 * @author Lenovo
 */
public class nostock {

    private String ordno, ordersource, siteid, Regname, artcode, artname, reqqoh, onhandupdate, orddate, onhand, status, tpordno, branchname;

    public nostock(String ordno, String ordersource, String siteid, String Regname, String artcode, String artname, String reqqoh, String onhand, String onhandupdate, String orddate, String status, String tpordno, String branchname) {
        this.ordno = ordno;
        this.ordersource = ordersource;
        this.siteid = siteid;
        this.Regname = Regname;
        this.artcode = artcode;
        this.artname = artname;
        this.reqqoh = reqqoh;
        this.onhand = onhand;
        this.onhandupdate = onhandupdate;
        this.orddate = orddate;
        this.status = status;
        this.tpordno = tpordno;
        this.branchname = branchname;
    }

    public String getTpordno() {
        return tpordno;
    }

    public void setTpordno(String tpordno) {
        this.tpordno = tpordno;
    }

    public String getBranchname() {
        return branchname;
    }

    public void setBranchname(String branchname) {
        this.branchname = branchname;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getOrddate() {
        return orddate;
    }

    public void setOrddate(String orddate) {
        this.orddate = orddate;
    }

    public String getOrdno() {
        return ordno;
    }

    public void setOrdno(String ordno) {
        this.ordno = ordno;
    }

    public String getOrdersource() {
        return ordersource;
    }

    public void setOrdersource(String ordersource) {
        this.ordersource = ordersource;
    }

    public String getSiteid() {
        return siteid;
    }

    public void setSiteid(String siteid) {
        this.siteid = siteid;
    }

    public String getRegname() {
        return Regname;
    }

    public void setRegname(String Regname) {
        this.Regname = Regname;
    }

    public String getArtcode() {
        return artcode;
    }

    public void setArtcode(String artcode) {
        this.artcode = artcode;
    }

    public String getArtname() {
        return artname;
    }

    public void setArtname(String artname) {
        this.artname = artname;
    }

    public String getReqqoh() {
        return reqqoh;
    }

    public void setReqqoh(String reqqoh) {
        this.reqqoh = reqqoh;
    }

    public String getOnhand() {
        return onhand;
    }

    public void setOnhand(String onhand) {
        this.onhand = onhand;
    }

    public String getOnhandupdate() {
        return onhandupdate;
    }

    public void setOnhandupdate(String onhandupdate) {
        this.onhandupdate = onhandupdate;
    }

}
