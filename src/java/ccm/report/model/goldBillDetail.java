/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ccm.report.model;

/**
 *
 * @author Pitchu
 * @created 3 Nov, 2014 3:14:25 PM
 *
 */
public class goldBillDetail {

    private String bDate, siteid, artcode, artname, cat, qty, price, discamt;

    public goldBillDetail(String bDate, String siteid, String artcode, String artname, String cat, String qty, String price, String discamt) {
        this.bDate = bDate;
        this.siteid = siteid;
        this.artcode = artcode;
        this.artname = artname;
        this.cat = cat;
        this.qty = qty;
        this.price = price;
        this.discamt = discamt;
    }

    public String getArtcode() {
        return artcode;
    }

    public void setArtcode(String artcode) {
        this.artcode = artcode;
    }

    public String getArtname() {
        return artname;
    }

    public void setArtname(String artname) {
        this.artname = artname;
    }

    public String getbDate() {
        return bDate;
    }

    public void setbDate(String bDate) {
        this.bDate = bDate;
    }

    public String getCat() {
        return cat;
    }

    public void setCat(String cat) {
        this.cat = cat;
    }

    public String getDiscamt() {
        return discamt;
    }

    public void setDiscamt(String discamt) {
        this.discamt = discamt;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getSiteid() {
        return siteid;
    }

    public void setSiteid(String siteid) {
        this.siteid = siteid;
    }
}
