/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ccm.report.model;

/**
 *
 * @author Pitchu
 * @created 24 Nov, 2014 12:30:54 PM
 *
 */
public class categorywiseRep {

    private String category, mrpval;

    public categorywiseRep(String category, String mrpval) {
        this.category = category;
        this.mrpval = mrpval;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getMrpval() {
        return mrpval;
    }

    public void setMrpval(String mrpval) {
        this.mrpval = mrpval;
    }
}
