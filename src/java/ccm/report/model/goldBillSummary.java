/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ccm.report.model;

/**
 *
 * @author Pitchu
 * @created 3 Nov, 2014 12:05:01 PM
 *
 */
public class goldBillSummary {

    private String trxid, billdate, site, sitename, phone, name, age, sex, total, empid, billno;

    public goldBillSummary(String trxid, String billdate, String site, String sitename, String phone, String name, String age, String sex, String total, String empid, String billno) {
        this.trxid = trxid;
        this.billdate = billdate;
        this.site = site;
        this.sitename = sitename;
        this.phone = phone;
        this.name = name;
        this.age = age;
        this.sex = sex;
        this.total = total;
        this.empid = empid;
        this.billno = billno;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getBilldate() {
        return billdate;
    }

    public void setBilldate(String billdate) {
        this.billdate = billdate;
    }

    public String getBillno() {
        return billno;
    }

    public void setBillno(String billno) {
        this.billno = billno;
    }

    public String getEmpid() {
        return empid;
    }

    public void setEmpid(String empid) {
        this.empid = empid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getSitename() {
        return sitename;
    }

    public void setSitename(String sitename) {
        this.sitename = sitename;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getTrxid() {
        return trxid;
    }

    public void setTrxid(String trxid) {
        this.trxid = trxid;
    }
}
