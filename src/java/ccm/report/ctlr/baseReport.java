/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ccm.report.ctlr;

import ccm.dao.bean.connectivity;
import static ccm.dao.bean.connectivity.getLDBConnection;
import ccm.logic.bean.UploadedPrescDocs;
import ccm.logic.ctlr.orderCreation;
import ccm.logic.bean.CancelRemarks;
import com.logic.order.bean.comments;
import ccm.logic.bean.itemDetails;
import ccm.logic.bean.returnItemDetails;
import ccm.report.model.SaleBean;
import ccm.report.model.billingInfo;
import ccm.report.model.categorywiseRep;
import ccm.report.model.delRep;
import ccm.report.model.invAvailability;
import ccm.report.model.newRep;
import ccm.report.model.otpLog;
import ccm.report.model.procRep;
import ccm.report.model.summaryReport;
import ccm.report.model.tpOrderClose;
import com.logic.branch.ctlr.initClose;
import static com.logic.branch.ctlr.initValidator.checkOrderStatusbeforeclosureInit;
import com.logic.order.model.OrderComment;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import com.sun.jersey.api.client.Client;
import static com.sun.jersey.api.client.Client.create;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import ccm.logic.ctlr.sms;
import static java.lang.System.err;
import static java.lang.System.out;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.ZoneId;
import static java.util.Calendar.getInstance;
import java.util.function.Function;
import java.util.stream.Collectors;
import static javax.faces.application.FacesMessage.SEVERITY_WARN;
import javax.faces.bean.ViewScoped;
import static javax.faces.context.FacesContext.getCurrentInstance;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.primefaces.event.RowEditEvent;

/**
 *
 * @author Pitchu
 * @created 31 Oct, 2014 5:08:58 PM
 *
 */
@ManagedBean(name = "baseRep")
@ViewScoped
public class baseReport implements Serializable {

    List<summaryReport> sumRep = new ArrayList<>();
    List<summaryReport> sumRepVendorwise = new ArrayList();
    List<summaryReport> sumRepStatuswise = new ArrayList();
    List<summaryReport> sumRepDatewise = new ArrayList();
    List<summaryReport> sumRepSitewise = new ArrayList();
    List<comments> AgentComments = new ArrayList();

    protected List<SaleBean> sales;

    List<summaryReport> filteredSumReport;
    List<CancelRemarks> cancelRem = new ArrayList<>();
    List<newRep> nrep = new ArrayList<>();
    List<procRep> prep = new ArrayList<>();
    List<delRep> drep = new ArrayList<>();
    List<categorywiseRep> cate = new ArrayList<>();
    List<itemDetails> itemDet = new ArrayList<>();
    List<returnItemDetails> retitemDet = new ArrayList<>();
    List<itemDetails> BilleditemDet = new ArrayList<>();
    List<billingInfo> billInfo = new ArrayList<>();
    List<tpOrderClose> tpSum = new ArrayList();
    List<otpLog> otplog = new ArrayList();
    List<otpLog> otplogpersonwise = new ArrayList();
    List<otpLog> otplogFilter;

    List<String> pb = new ArrayList();
    List<String> ordStatusVendorwise = new ArrayList();
    List<String> ordStatus = new ArrayList();
    List<String> ordStatusDatewise = new ArrayList();
    List<String> ordStatusSitewise = new ArrayList();

    List<String> ordStatusPracto = new ArrayList();

    private tpOrderClose tpcloseselectedOrder;
    private summaryReport selectedOrder;
    private String VendorName;
    private String delaySMSBTN;
    private String commentBTN;

    Date maxDATE = new java.util.Date();

    public String getVendorName() {
        return VendorName;
    }

    public void setVendorName(String VendorName) {
        this.VendorName = VendorName;
    }

    public List<summaryReport> getFilteredSumReport() {
        return filteredSumReport;
    }

    public void setFilteredSumReport(List<summaryReport> filteredSumReport) {
        this.filteredSumReport = filteredSumReport;
    }

    public List<otpLog> getOtplogFilter() {
        return otplogFilter;
    }

    public void setOtplogFilter(List<otpLog> otplogFilter) {
        this.otplogFilter = otplogFilter;
    }

    public tpOrderClose getTpcloseselectedOrder() {
        return tpcloseselectedOrder;
    }

    public void setTpcloseselectedOrder(tpOrderClose tpcloseselectedOrder) {
        this.tpcloseselectedOrder = tpcloseselectedOrder;
    }

    public summaryReport getSelectedOrder() {
        return selectedOrder;
    }

    public String getDelaySMSBTN() {
        return delaySMSBTN;
    }

    public void setDelaySMSBTN(String delaySMSBTN) {
        this.delaySMSBTN = delaySMSBTN;
    }

    public String getCommentBTN() {
        return commentBTN;
    }

    public void setCommentBTN(String commentBTN) {
        this.commentBTN = commentBTN;
    }

    public void setSelectedOrder(summaryReport selectedOrder) {
        this.selectedOrder = selectedOrder;
        String ordNo = selectedOrder.getTpordid();
        String vendorName = selectedOrder.getOrdersource();
        orderCreation ac = new orderCreation();
        AgentComments = ac.listComments(ordNo, vendorName);
    }

    public List<comments> getAgentComments() {
        return AgentComments;
    }

    Calendar c = getInstance();
    SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd");
    SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
    SimpleDateFormat sdfd = new SimpleDateFormat("dd-MMM-yyyy HH:mm.ss");
    Date ordFDate = new java.util.Date();
    Date ordTDate = new java.util.Date();
    Date maxDate = new java.util.Date();
    DecimalFormat dff = new DecimalFormat("##,###.##");
    String _regID = "";

//    public void handleDateSelect(SelectEvent event) {
//        date1 = mdate1;
//        Calendar c = Calendar.getInstance();
//        c.setTime(mdate1);
//        c.add(Calendar.DATE, +7);
//        date2 = c.getTime();
//        stdate = date2;
//
//        Calendar inddf = Calendar.getInstance();
//        inddf.setTime(date1);
//        inddf.add(Calendar.DATE, -180);
//        sfdate = inddf.getTime();
//        genCategory();
//        genClassification();
//    }
    public String getRegID() {
        return _regID;
    }

    public void setRegID(String _regID) {
        this._regID = _regID;
    }

    public Date getOrdFDate() {
        return ordFDate;
    }

    public void setOrdFDate(Date ordFDate) {
        this.ordFDate = ordFDate;
    }

    public Date getOrdTDate() {
        return ordTDate;
    }

    public void setOrdTDate(Date ordTDate) {
        this.ordTDate = ordTDate;
    }

    public Date getMaxDate() {
        return maxDate;
    }

    public void setMaxDate(Date maxDate) {
        this.maxDate = maxDate;
    }

    public Date onFromDateSelect() {

//        System.out.println("contorller.ReportController.onFromDateSelect()" + getDate1());
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

        String d1 = sdf.format(ordFDate);

        int day = Integer.parseInt(new SimpleDateFormat("dd").format(ordFDate));
        int month = Integer.parseInt(new SimpleDateFormat("MM").format(ordFDate));
        int year = Integer.parseInt(new SimpleDateFormat("yyyy").format(ordFDate));

        System.out.println("contorller.ReportController.onFromDateSelect()" + d1);
        LocalDate ldate = LocalDate.of(year, month, day).plusDays(30);
        ordTDate = Date.from(ldate.atStartOfDay(ZoneId.systemDefault()).toInstant());

//        date2 = java.sql.Timestamp.valueOf(ldate.atStartOfDay());
        if (ordTDate.after(maxDATE)) {
            ordTDate = maxDATE;
        }
        return ordTDate;
    }

    public Date onToDateSelect() {

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        String d1 = sdf.format(ordTDate);

        int day = Integer.parseInt(new SimpleDateFormat("dd").format(ordTDate));
        int month = Integer.parseInt(new SimpleDateFormat("MM").format(ordTDate));
        int year = Integer.parseInt(new SimpleDateFormat("yyyy").format(ordTDate));

        System.out.println("contorller.ReportController.onFromDateSelect()" + d1);
        LocalDate ldate = LocalDate.of(year, month, day).minusDays(30);
        ordFDate = java.sql.Timestamp.valueOf(ldate.atStartOfDay());
        return ordFDate;

    }

    public List showInitiatedOrders() {
        Connection con = null;
        this.tpSum.clear();
        try {
            con = getLDBConnection();
            //PreparedStatement ps = con.prepareStatement("select o.ordno,o.siteid,ISNULL(CONVERT(varchar, o.orddate),'-') ,ISNULL(CONVERT(varchar, o.initdate),'-') ,ISNULL(CONVERT(varchar, o.deldate),'-'),(select reg_name from dbo.CClogin where loginid=id) as regname,(select FirstName + ' ' + LastName from dbo.CC_Patient_Registration where patientid=patid) as Patname,(select mobileno from dbo.CC_Patient_Registration where patientid=patid) as Patmobileno,(select gender from dbo.CC_Patient_Registration where patientid=patid) as Gender, status,(select username from dbo.CClogin where loginid=id) as loginusername,ordersource,ISNULL(CONVERT(varchar, cancelDate),'-'),ISNULL(CONVERT(varchar, cancelRemarks),'-'),paymentmode,(select Addr1 from dbo.CC_Patient_Registration where patientid=patid) as PatAddr,(select maorderid from orderupdate where ordno = REPLACE (ap_orderid, 'V' , '')) as MAOrdID from cc_ordhead o where o.status = 1 and ordersource = 'MediAssist' and  CONVERT (DATE, ORDDATE)>=CONVERT (DATE, ?) AND CONVERT (DATE, ORDDATE) <=CONVERT (DATE, ?)");
            PreparedStatement ps = con.prepareStatement("select ci.siteid,ci.APORDID,ci.TPORDID,h.ordDate,h.initDate,h.orderSource,h.status,TPD.patname,TPD.del_add from CC_TPOrderInfo ci, CC_Ordhead h, TPDELADDUPDATE TPD where cast(h.ordNo as nvarchar) = ci.APORDID AND ci.TPORDID = tpd.orderid and h.orderSource = 'MediAssist' and h.status = 1 and cast(h.orddate as date)  between ? and ?");
            ps.setString(1, this.df.format(this.ordFDate));
            ps.setString(2, this.df.format(this.ordTDate));
            ResultSet rs = ps.executeQuery();
            String delCanDate = "";
            while (rs.next()) {
//                delCanDate = rs.getString(5);
//                String ord = rs.getString(1);
//                String statusurl = "";
                String statusval = rs.getString(7);
//                if (statusval.equals("0")) {
//                    statusurl = "./yellow.jpg";
//                } else if (statusval.equals("1")) {
//                    statusurl = "./green.jpg";
//                } else if (statusval.equals("2")) {
//                    statusurl = "./redcan.jpg";
//                } else if (statusval.equals("3")) {
//                    statusurl = "./red.jpg";
//                }
//                if (delCanDate.equals("-")) {
//                    delCanDate = rs.getString(13);
//                }
//                tpSum.add(new tpOrderClose(rs.getString(1), rs.getInt(2), rs.getString(3), rs.getString(4), delCanDate, rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9), statusurl, rs.getString(10), rs.getString(11), rs.getString(12), rs.getString(13), rs.getString(14), rs.getString(15), rs.getString(16), rs.getInt(17)));

                tpSum.add(new tpOrderClose(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9)));
            }
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
        } finally {
        }
        return tpSum;
    }

    public List<tpOrderClose> getTpSum() {
        return tpSum;
    }

    public void showSummaryReport() throws SQLException {
        HttpSession appSession = (HttpSession) getCurrentInstance().getExternalContext().getSession(false);
        String usr_group = appSession.getAttribute("usr_group").toString();
        String region = appSession.getAttribute("reg_id").toString();
        String siteid = appSession.getAttribute("usr").toString();
        out.println(df.format(ordFDate));
        out.println(df.format(ordTDate));
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        Connection con = null;
        Map<String, Long> result = null;
        sumRep.clear();
        sumRepVendorwise.clear();
        sumRepStatuswise.clear();
        sumRepSitewise.clear();
        ordStatus.clear();
        ordStatusVendorwise.clear();
        ordStatusSitewise.clear();
        ResultSet rs = null;
        try {
            con = getLDBConnection();
            String qry = "";
            if (usr_group.equals("3")) {
                if (VendorName.equals("All")) {
                    qry = "select h.ordno,h.siteid,l.name, ISNULL(CONVERT(varchar, h.orddate,120),'-') orddate ,ISNULL(CONVERT(varchar, h.initdate,120),'-') initdate, ISNULL(CONVERT(varchar, h.deldate,120),'-')deldate,l.city,r.firstname +' '+r.lastname AS PATNAME,r.mobileno, r.gender,h.status,log.username,h.ordersource, ISNULL(CONVERT(varchar, cancelDate,120),'-') canceldate,(CASE WHEN (orderSource in ('MediAssist','Online','AskApollo','PRACTO')) THEN (select reason_desc from dbo.CC_OrderCancelRemarks where reason_code = h.cancelRemarks) ELSE ISNULL(CONVERT(varchar,h.cancelRemarks),'-') END) as CancelRemarks,h.paymentmode,h.invvalue,h.invno,i.tpordid,i.inventoryStatus,i.CASHVALUE,i.CREDITVALUE,ISNULL(CONVERT(varchar, th.createddatetime,120),'-'),tp.ORDERAMOUNT,ISNULL(CONVERT(varchar, h.returnDate,120),'-'),ISNULL(CONVERT(nvarchar,(select sum(billqty*mrp) from cc_returnitemdet where ordno = h.ordno)),'-'),tp.city,tp.postal_code from cc_ordhead h , cc_sun_locationdetails l, CC_Patient_Registration r, cclogin log, cc_tporderinfo i, ThirdPartyOrderHeader th,TPDELADDUPDATE tp  where i.tpordid = th.orderid and i.tpvname = th.vendorname and th.OrderId = tp.orderid and th.VendorName = tp.vendorname and h.siteid = l.siteid and h.patid = r.patientid and h.loginid = log.id and cast(h.ordno as nvarchar)= i.apordid and h.siteid IN (select userid from openquery(CMS,'select  userid from web_login_master where dc in(''" + region + "'') and dept  = ''br''')) and CONVERT (DATE, h.ORDDATE)>=CONVERT (DATE, ?) AND CONVERT (DATE, h.ORDDATE) <=CONVERT (DATE, ?)and h.status in (0,1,2,3,4,5,9,10)";
                    PreparedStatement ps = con.prepareStatement(qry);
                    ps.setString(1, df.format(ordFDate));
                    ps.setString(2, df.format(ordTDate));
                    rs = ps.executeQuery();

                } else {
                    qry = "select h.ordno,h.siteid,l.name, ISNULL(CONVERT(varchar, h.orddate,120),'-') orddate ,ISNULL(CONVERT(varchar, h.initdate,120),'-') initdate, ISNULL(CONVERT(varchar, h.deldate,120),'-')deldate,l.city,r.firstname +' '+r.lastname AS PATNAME,r.mobileno, r.gender,h.status,log.username,h.ordersource, ISNULL(CONVERT(varchar, cancelDate,120),'-') canceldate,(CASE WHEN (orderSource in ('MediAssist','Online','AskApollo','PRACTO')) THEN (select reason_desc from dbo.CC_OrderCancelRemarks where reason_code = h.cancelRemarks) ELSE ISNULL(CONVERT(varchar,h.cancelRemarks),'-') END) as CancelRemarks,h.paymentmode,h.invvalue,h.invno,i.tpordid,i.inventoryStatus,i.CASHVALUE,i.CREDITVALUE,ISNULL(CONVERT(varchar, th.createddatetime,120),'-'),tp.ORDERAMOUNT,ISNULL(CONVERT(varchar, h.returnDate,120),'-'),ISNULL(CONVERT(nvarchar,(select sum(billqty*mrp) from cc_returnitemdet where ordno = h.ordno)),'-'),tp.city,tp.postal_code from cc_ordhead h , cc_sun_locationdetails l, CC_Patient_Registration r, cclogin log, cc_tporderinfo i, ThirdPartyOrderHeader th,TPDELADDUPDATE tp where i.tpordid = th.orderid and i.tpvname = th.vendorname and th.OrderId = tp.orderid and th.VendorName = tp.vendorname and h.siteid = l.siteid and h.patid = r.patientid and h.loginid = log.id and cast(h.ordno as nvarchar)= i.apordid and h.siteid IN (select userid from openquery (CMS,'select  userid from WEB..web_login_master where dc in(''" + region + "'') and dept  = ''br'''))and CONVERT (DATE, h.ORDDATE)>=CONVERT (DATE, ?) AND CONVERT (DATE, h.ORDDATE) <=CONVERT (DATE, ?) and h.orderSource = ? and h.status in (0,1,2,3,4,5,9,10)";
                    PreparedStatement ps = con.prepareStatement(qry);
                    ps.setString(1, df.format(ordFDate));
                    ps.setString(2, df.format(ordTDate));
                    ps.setString(3, VendorName);
                    rs = ps.executeQuery();

                }
            } else if (usr_group.equals("1")) {
                if (VendorName.equals("All")) {
                    qry = "select h.ordno,h.siteid,l.name,ISNULL(CONVERT(varchar, h.orddate,120),'-') orddate ,ISNULL(CONVERT(varchar, h.initdate,120),'-') initdate, ISNULL(CONVERT(varchar, h.deldate,120),'-')deldate,l.city,r.firstname +' '+r.lastname AS PATNAME,r.mobileno, r.gender,h.status,log.username,h.ordersource,ISNULL(CONVERT(varchar, cancelDate,120),'-') canceldate,(CASE WHEN (orderSource in ('MediAssist','Online','AskApollo','PRACTO')) THEN (select reason_desc from dbo.CC_OrderCancelRemarks where reason_code = h.cancelRemarks) ELSE ISNULL(CONVERT(varchar,h.cancelRemarks),'-') END) as CancelRemarks,h.paymentmode,h.invvalue,h.invno,i.tpordid,i.inventoryStatus,i.CASHVALUE,i.CREDITVALUE,ISNULL(CONVERT(varchar, th.createddatetime,120),'-'),tp.ORDERAMOUNT,ISNULL(CONVERT(varchar, h.returnDate,120),'-'),ISNULL(CONVERT(nvarchar,(select sum(billqty*mrp) from cc_returnitemdet where ordno = h.ordno)),'-'),tp.city,tp.postal_code from cc_ordhead h , cc_sun_locationdetails l, CC_Patient_Registration r, cclogin log, cc_tporderinfo i, ThirdPartyOrderHeader th,TPDELADDUPDATE tp where i.tpordid = th.orderid and i.tpvname = th.vendorname and th.OrderId = tp.orderid and th.VendorName = tp.vendorname and h.siteid = l.siteid and h.patid = r.patientid and h.loginid = log.id and cast(h.ordno as nvarchar)= i.apordid and CONVERT (DATE, h.ORDDATE)>=CONVERT (DATE, ?) AND CONVERT (DATE, h.ORDDATE) <= CONVERT (DATE, ?)and h.status in (0,1,2,3,4,5,9,10)";
                    PreparedStatement ps = con.prepareStatement(qry);
                    ps.setString(1, df.format(ordFDate));
                    ps.setString(2, df.format(ordTDate));
                    rs = ps.executeQuery();
                } else {
                    qry = "select h.ordno,h.siteid,l.name,ISNULL(CONVERT(varchar, h.orddate,120),'-') orddate ,ISNULL(CONVERT(varchar, h.initdate,120),'-') initdate, ISNULL(CONVERT(varchar, h.deldate,120),'-')deldate,l.city,r.firstname +' '+r.lastname AS PATNAME,r.mobileno, r.gender,h.status,log.username,h.ordersource,ISNULL(CONVERT(varchar, cancelDate,120),'-') canceldate,(CASE WHEN (orderSource in ('MediAssist','Online','AskApollo','PRACTO')) THEN (select reason_desc from dbo.CC_OrderCancelRemarks where reason_code = h.cancelRemarks) ELSE ISNULL(CONVERT(varchar,h.cancelRemarks),'-') END) as CancelRemarks,h.paymentmode,h.invvalue,h.invno,i.tpordid,i.inventoryStatus,i.CASHVALUE,i.CREDITVALUE,ISNULL(CONVERT(varchar, th.createddatetime,120),'-'),tp.ORDERAMOUNT,ISNULL(CONVERT(varchar, h.returnDate,120),'-'),ISNULL(CONVERT(nvarchar,(select sum(billqty*mrp) from cc_returnitemdet where ordno = h.ordno)),'-'),tp.city,tp.postal_code from cc_ordhead h , cc_sun_locationdetails l, CC_Patient_Registration r, cclogin log, cc_tporderinfo i, ThirdPartyOrderHeader th,TPDELADDUPDATE tp where i.tpordid = th.orderid and i.tpvname = th.vendorname and th.OrderId = tp.orderid and th.VendorName = tp.vendorname and h.siteid = l.siteid and h.patid = r.patientid and h.loginid = log.id and cast(h.ordno as nvarchar)= i.apordid and CONVERT (DATE, h.ORDDATE)>=CONVERT (DATE, ?) AND CONVERT (DATE, h.ORDDATE) <= CONVERT (DATE, ?) and h.orderSource = ? and h.status in (0,1,2,3,4,5,9,10)";
                    PreparedStatement ps = con.prepareStatement(qry);
                    ps.setString(1, df.format(ordFDate));
                    ps.setString(2, df.format(ordTDate));
                    ps.setString(3, VendorName);
                    rs = ps.executeQuery();
                }
            } else if (usr_group.equals("2")) {
                if (VendorName.equals("All")) {
                    qry = "select h.ordno,h.siteid,l.name, ISNULL(CONVERT(varchar, h.orddate,120),'-') orddate ,ISNULL(CONVERT(varchar, h.initdate,120),'-') initdate, ISNULL(CONVERT(varchar, h.deldate,120),'-')deldate,l.city,r.firstname +' '+r.lastname AS PATNAME,r.mobileno, r.gender,h.status,log.username,h.ordersource, ISNULL(CONVERT(varchar, cancelDate,120),'-') canceldate,(CASE WHEN (orderSource in ('MediAssist','Online','AskApollo','PRACTO')) THEN (select reason_desc from dbo.CC_OrderCancelRemarks where reason_code = h.cancelRemarks) ELSE ISNULL(CONVERT(varchar,h.cancelRemarks),'-') END) as CancelRemarks,h.paymentmode,h.invvalue,h.invno,i.tpordid,i.inventoryStatus,i.CASHVALUE,i.CREDITVALUE,ISNULL(CONVERT(varchar, th.createddatetime,120),'-'),tp.ORDERAMOUNT,ISNULL(CONVERT(varchar, h.returnDate,120),'-'),ISNULL(CONVERT(nvarchar,(select sum(billqty*mrp) from cc_returnitemdet where ordno = h.ordno)),'-'),tp.city,tp.postal_code from cc_ordhead h , cc_sun_locationdetails l, CC_Patient_Registration r, cclogin log, cc_tporderinfo i, ThirdPartyOrderHeader th,TPDELADDUPDATE tp  where i.tpordid = th.orderid and i.tpvname = th.vendorname and th.OrderId = tp.orderid and th.VendorName = tp.vendorname and h.siteid = l.siteid and h.patid = r.patientid and h.loginid = log.id and cast(h.ordno as nvarchar)= i.apordid and h.siteid IN (select userid from openquery (CMS,'select  userid from WEB..web_login_master where userid in(''" + siteid + "'') and dept  = ''br'''))and CONVERT (DATE, h.ORDDATE)>=CONVERT (DATE, ?) AND CONVERT (DATE, h.ORDDATE) <=CONVERT (DATE, ?)and h.status in (0,1,2,3,4,5,9,10)";
                    PreparedStatement ps = con.prepareStatement(qry);
                    ps.setString(1, df.format(ordFDate));
                    ps.setString(2, df.format(ordTDate));
                    rs = ps.executeQuery();
                } else {
                    qry = "select h.ordno,h.siteid,l.name, ISNULL(CONVERT(varchar, h.orddate,120),'-') orddate ,ISNULL(CONVERT(varchar, h.initdate,120),'-') initdate, ISNULL(CONVERT(varchar, h.deldate,120),'-')deldate,l.city,r.firstname +' '+r.lastname AS PATNAME,r.mobileno, r.gender,h.status,log.username,h.ordersource, ISNULL(CONVERT(varchar, cancelDate,120),'-') canceldate,(CASE WHEN (orderSource in ('MediAssist','Online','AskApollo','PRACTO')) THEN (select reason_desc from dbo.CC_OrderCancelRemarks where reason_code = h.cancelRemarks) ELSE ISNULL(CONVERT(varchar,h.cancelRemarks),'-') END) as CancelRemarks,h.paymentmode,h.invvalue,h.invno,i.tpordid,i.inventoryStatus,i.CASHVALUE,i.CREDITVALUE,ISNULL(CONVERT(varchar, th.createddatetime,120),'-'),tp.ORDERAMOUNT,ISNULL(CONVERT(varchar, h.returnDate,120),'-'),ISNULL(CONVERT(nvarchar,(select sum(billqty*mrp) from cc_returnitemdet where ordno = h.ordno)),'-'),tp.city,tp.postal_code from cc_ordhead h , cc_sun_locationdetails l, CC_Patient_Registration r, cclogin log, cc_tporderinfo i, ThirdPartyOrderHeader th,TPDELADDUPDATE tp where i.tpordid = th.orderid and i.tpvname = th.vendorname and th.OrderId = tp.orderid and th.VendorName = tp.vendorname and h.siteid = l.siteid and h.patid = r.patientid and h.loginid = log.id and cast(h.ordno as nvarchar)= i.apordid and h.siteid IN (select userid from openquery (CMS,'select  userid from WEB..web_login_master where userid in(''" + siteid + "'') and dept  = ''br'''))and CONVERT (DATE, h.ORDDATE)>=CONVERT (DATE, ?) AND CONVERT (DATE, h.ORDDATE) <=CONVERT (DATE, ?) and h.orderSource = ? and h.status in (0,1,2,3,4,5,9,10)";
                    PreparedStatement ps = con.prepareStatement(qry);
                    ps.setString(1, df.format(ordFDate));
                    ps.setString(2, df.format(ordTDate));
                    ps.setString(3, VendorName);
                    rs = ps.executeQuery();
                }
            }
            String statusval = "";
            String statusurl = "";
            while (rs.next()) {
                //    String delCanDate = rs.getString(6);
                statusval = rs.getString(11);
                if (statusval.equals("0")) {
                    //statusurl = "./yellow.jpg";
                    statusurl = "Processing";
                    delaySMSBTN = "flase";
                    commentBTN = "false";
                } else if (statusval.equals("1")) {
                    //statusurl = "./green.jpg";
                    statusurl = "Billed";
                    delaySMSBTN = "true";
                    commentBTN = "false";
                } else if (statusval.equals("2")) {
                    //statusurl = "./redcan.jpg";
                    statusurl = "Cancelled";
                    delaySMSBTN = "true";
                    commentBTN = "true";
                } else if (statusval.equals("3")) {
                    //statusurl = "./red.jpg";
                    statusurl = "Delivered";
                    delaySMSBTN = "true";
                    commentBTN = "true";
                } else if (statusval.equals("4")) {
                    //statusurl = "./red.jpg";
                    statusurl = "OFD";
                    delaySMSBTN = "true";
                    commentBTN = "false";
                } else if (statusval.equals("5")) {
                    //statusurl = "./red.jpg";
                    statusurl = "MVPOS";
                    delaySMSBTN = "true";
                    commentBTN = "false";
                } else if (statusval.equals("9")) {
                    //statusurl = "./red.jpg";
                    statusurl = "Partial Returns";
                    delaySMSBTN = "true";
                    commentBTN = "false";
                } else if (statusval.equals("10")) {
                    //statusurl = "./red.jpg";
                    statusurl = "Full Returns";
                    delaySMSBTN = "true";
                    commentBTN = "false";
                }

                String availability = rs.getString(20);

                sumRep.add(new summaryReport(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9), rs.getString(10), rs.getString(11), statusurl, rs.getString(12), rs.getString(13), rs.getString(14), rs.getString(15), rs.getString(16), rs.getString(17), rs.getString(18), rs.getString(19), availability, delaySMSBTN, rs.getDouble(21), rs.getDouble(22), commentBTN, rs.getString(23), rs.getString(24), rs.getString(25), rs.getString(26), rs.getString(27), rs.getString(28)));
                ordStatusVendorwise.add(rs.getString(13));
                ordStatus.add(statusurl);
                ordStatusSitewise.add(rs.getString(7));

            }
            result = ordStatus.stream().collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
            result.forEach((key, value) -> sumRepStatuswise.add(new summaryReport(key, value)));
            ///result.forEach((key, value) -> System.out.println(key + " - " + value));

            result = ordStatusVendorwise.stream().collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
            result.forEach((key, value) -> sumRepVendorwise.add(new summaryReport(key, value)));
            //  result.forEach((key, value) -> System.out.println(key + " - " + value));

            result = ordStatusSitewise.stream().collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
            result.forEach((key, value) -> sumRepSitewise.add(new summaryReport(key, value)));

        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage() + " - " + e.getCause());
        }
        //return sumRep;
    }

    public List<summaryReport> getSumRep() {
        return sumRep;
    }

    public List<summaryReport> getSumRepVendorwise() {
        return sumRepVendorwise;
    }

    public List<summaryReport> getSumRepStatuswise() {
        return sumRepStatuswise;
    }

    public List<summaryReport> getSumRepSitewise() {
        return sumRepSitewise;
    }

    List<invAvailability> invAvail = new ArrayList<invAvailability>();

    public List showInvAvailability(String ordNo, String siteid) {
        /*FacesContext fc = FacesContext.getCurrentInstance();
        Map<String, String> params = fc.getExternalContext().getRequestParameterMap();
        String ordNo = params.get("ordNo");
        String siteid = params.get("siteid");*/
        String art = "";
        List<String> artcodes = new ArrayList<String>();
        invAvail.clear();
        try {
            Connection con = getLDBConnection();
            PreparedStatement ps = con.prepareStatement("select artcode from cc_orddet where ordno = " + ordNo);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                artcodes.add(rs.getString(1));
            }
        } catch (Exception e) {
            out.println(e.getLocalizedMessage());
        }
        out.println(artcodes);
        art = artcodes.toString().replace("[", "").replace("]", "").replace(" ", "").trim();
        out.println(art);
        String url = "http://172.16.2.251:8085/WSS/mapp/inventoryCheck/" + siteid + "/" + art;
        Client restClient = create();
        WebResource webResource = restClient.resource(url);
        ClientResponse resp = webResource.accept("application/json")
                .get(ClientResponse.class
                );
        if (resp.getStatus() != 200) {
            err.println("Unable to connect to the server");

        }
        String output = resp.getEntity(String.class
        );
        out.println("response: " + output);
        try {
            JSONObject obj = new JSONObject(output);
            JSONArray jsonArray = obj.getJSONArray("item");
            int count = jsonArray.length();
            for (int i = 0; i < count; i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                String invartCode = jsonObject.getString("artCode");
                int invQty = jsonObject.getInt("qoh");
                invAvail.add(new invAvailability(invartCode, invQty));
            }

        } catch (JSONException e) {
            out.println(e.getLocalizedMessage());
        }

        return invAvail;
    }

    public List<invAvailability> getInvAvail() {
        return invAvail;
    }
    String custAddr;

    public String getCustAddr() {
        return custAddr;
    }

    public void setCustAddr(String custAddr) {
        this.custAddr = custAddr;
    }

    public void showDetails() {
        FacesContext fc = getCurrentInstance();
        Map<String, String> params = fc.getExternalContext().getRequestParameterMap();
        String ordNo = params.get("ordNo");
        String ordsrc = params.get("ordsrc");
        String siteid = params.get("siteid");
        String ordStatus = params.get("status");
        custAddr = fetchAddr(ordNo);
        invAvail.clear();
        BilleditemDet.clear();
        billInfo.clear();
        prescImage(ordNo);
        showitemDetail(ordNo);
        showReturnitemDetail(ordNo);
        if (ordStatus.equals("0")) {
            showInvAvailability(ordNo, siteid);
            delaySMSBTN = "flase";
        }
        if (ordStatus.equals("1") || ordStatus.equals("3") || ordStatus.equals("4") || ordStatus.equals("9") || ordStatus.equals("10")) {
            showBilleditemDetail(ordNo);
            showBillingDetail(ordNo, ordsrc);
            delaySMSBTN = "true";
        }
        //showCancelRemarks();
        //shoptimize bases = new shoptimize();
        //bases.customerInfo(mobNo);
    }

    public String fetchAddr(String _ordno) {
        try {
            Connection con = getLDBConnection();
            PreparedStatement ps = con.prepareStatement("select del_add,city,postal_code from tpdeladdupdate where orderid = (select tpordid from cc_tporderinfo where apordid = ?)");
            ps.setString(1, _ordno);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                custAddr = rs.getString(1) + ", " + rs.getString(2) + ", " + rs.getString(3);
            }
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
        }
        return custAddr;
    }

    String shoptimize_pres_img_url = "https://upload.wikimedia.org/wikipedia/commons/a/ac/No_image_available.svg";

    public String getShoptimize_pres_img_url() {
        return shoptimize_pres_img_url;
    }

    public void setShoptimize_pres_img_url(String shoptimize_pres_img_url) {
        this.shoptimize_pres_img_url = shoptimize_pres_img_url;
    }

    List<UploadedPrescDocs> upldprescimg = new ArrayList<UploadedPrescDocs>();

    public List prescImage(String _ordNo) {
        Connection con = null;
        upldprescimg.clear();
        try {
            //ordHeader.clear();
            con = getLDBConnection();
            //PreparedStatement ps = con.prepareStatement("select shippingmethod, paymentmethod,site_id, presc_image,is_medicine from CC_Shoptimizeheader where orderID = ?");
            PreparedStatement ps = con.prepareStatement("select PRESC_IMAGE,VendorName from thirdpartyorderheader where ORDERID IN (SELECT tpordid FROM CC_TPOrderInfo WHERE APORDID = '" + _ordNo + "')  ");
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                if (rs.getString(2).equalsIgnoreCase("AskApollo") || rs.getString(2).equalsIgnoreCase("Test")) {
                    shoptimize_pres_img_url = rs.getString(1).replace("[", "").replace("]", "").replace("\\/", "/").replace("\"", "/").replace("E:\\AppDoc\\", "http://172.16.2.251:8085/IA/Images/");
                } else {
                    shoptimize_pres_img_url = rs.getString(1).replace("[", "").replace("]", "").replace("\\/", "/").replace("\"", "");
                }
                //shoptimize_pres_img_url = rs.getString(1).replace("[", "").replace("]", "").replace("\\/", "/").replace("\"", "");
                String strArray[] = shoptimize_pres_img_url.split(",");
                out.println("String Array is : ");
                for (int i = 0; i < strArray.length; i++) {
                    upldprescimg.add(new UploadedPrescDocs("Prescription " + (i + 1), strArray[i]));
                }
                //ordHeader.add(new shoptimizeOrderHeader(rs.getString(1).toUpperCase(), rs.getString(2).toUpperCase(), rs.getString(3), rs.getString(4), rs.getString(5)));
            }
        } catch (Exception e) {
            out.println(e.getLocalizedMessage());
        }
        return upldprescimg;
    }

    public List<UploadedPrescDocs> getUpldprescimg() {
        return upldprescimg;
    }

    public List showCancelRemarks() {
        Connection con = null;
        cancelRem.clear();
        try {
            con = getLDBConnection();
            PreparedStatement ps = con.prepareStatement("select * from CC_OrderCancelRemarks");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                cancelRem.add(new CancelRemarks(rs.getString(1), rs.getString(2)));
            }
        } catch (Exception e) {
            out.println(e.getLocalizedMessage());
        }
        return cancelRem;
    }

    public List<CancelRemarks> getCancelRem() {
        return cancelRem;
    }

    public List showBillingDetail(String _ordNo, String _ordsrc) {
        Connection con = null;
        billInfo.clear();
        try {
            con = getLDBConnection();
            PreparedStatement ps = null;
            ResultSet rs = null;
            ps = con.prepareStatement("select APORDID,siteid,'001',apbillno,TOTALAMT,CASHVALUE,CREDITVALUE from CC_TPOrderInfo where APORDID = ?");
            ps.setString(1, _ordNo);
            rs = ps.executeQuery();
            while (rs.next()) {
                billInfo.add(new billingInfo(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7)));
            }
        } catch (Exception e) {
            out.println(e.getLocalizedMessage());
        }
        return billInfo;
    }

    public List<billingInfo> getBillInfo() {
        return billInfo;
    }

    public List showitemDetail(String _ordNo) {
        Connection con = null;
        itemDet.clear();
        try {
            con = getLDBConnection();
            //PreparedStatement ps = con.prepareStatement("select o.ordno,o.orddate,o.siteid,(select reg_name from dbo.CClogin where loginid=id) as regname,(select FirstName + ' ' + LastName from dbo.CC_Patient_Registration where patientid=patid) as Patname,(select mobileno from dbo.CC_Patient_Registration where patientid=patid) as Patmobileno,(select gender from dbo.CC_Patient_Registration where patientid=patid) as Gender, status,(select username from dbo.CClogin where loginid=id) as loginusername from cc_ordhead o where CONVERT (DATE, ORDDATE)>=CONVERT (DATE, ?) AND CONVERT (DATE, ORDDATE) <=CONVERT (DATE, ?)");
            PreparedStatement ps = con.prepareStatement("select artCode,artName,reqqoh from cc_orddet where ordNo = ?");
            ps.setString(1, _ordNo);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                itemDet.add(new itemDetails(rs.getString(1), rs.getString(2), rs.getInt(3), 0.01, 0));
                // sumRep.add(new summaryReport(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getInt(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9), rs.getString(11), statusurl, statusval, rs.getString(12)));
            }
            out.println(itemDet.size());
        } catch (Exception e) {
            out.println(e.getLocalizedMessage());
        } finally {
        }
        return itemDet;
    }

    public List<itemDetails> getItemDet() {
        return itemDet;
    }

    public List showReturnitemDetail(String _ordNo) {
        Connection con = null;
        retitemDet.clear();
        try {
            con = getLDBConnection();
            //PreparedStatement ps = con.prepareStatement("select o.ordno,o.orddate,o.siteid,(select reg_name from dbo.CClogin where loginid=id) as regname,(select FirstName + ' ' + LastName from dbo.CC_Patient_Registration where patientid=patid) as Patname,(select mobileno from dbo.CC_Patient_Registration where patientid=patid) as Patmobileno,(select gender from dbo.CC_Patient_Registration where patientid=patid) as Gender, status,(select username from dbo.CClogin where loginid=id) as loginusername from cc_ordhead o where CONVERT (DATE, ORDDATE)>=CONVERT (DATE, ?) AND CONVERT (DATE, ORDDATE) <=CONVERT (DATE, ?)");
            PreparedStatement ps = con.prepareStatement("select RREFNO,ARTCODE,artname,BILLQTY,REQDATE from CC_RETURNITEMDET where ordNo = ?");
            ps.setString(1, _ordNo);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                retitemDet.add(new returnItemDetails(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5)));
                // sumRep.add(new summaryReport(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getInt(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9), rs.getString(11), statusurl, statusval, rs.getString(12)));
            }
            out.println(itemDet.size());
        } catch (Exception e) {
            out.println(e.getLocalizedMessage());
        } finally {
        }
        return retitemDet;
    }

    public List<returnItemDetails> getRetitemDet() {
        return retitemDet;
    }

    public List showBilleditemDetail(String _ordNo) {
        Connection con = null;
        BilleditemDet.clear();
        try {
            con = getLDBConnection();
            //PreparedStatement ps = con.prepareStatement("select o.ordno,o.orddate,o.siteid,(select reg_name from dbo.CClogin where loginid=id) as regname,(select FirstName + ' ' + LastName from dbo.CC_Patient_Registration where patientid=patid) as Patname,(select mobileno from dbo.CC_Patient_Registration where patientid=patid) as Patmobileno,(select gender from dbo.CC_Patient_Registration where patientid=patid) as Gender, status,(select username from dbo.CClogin where loginid=id) as loginusername from cc_ordhead o where CONVERT (DATE, ORDDATE)>=CONVERT (DATE, ?) AND CONVERT (DATE, ORDDATE) <=CONVERT (DATE, ?)");
            PreparedStatement ps = con.prepareStatement("select artcode,artname,billqty,mrp from cc_itemdet where ordno = ?");
            ps.setString(1, _ordNo);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                BilleditemDet.add(new itemDetails(rs.getString(1), rs.getString(2), rs.getInt(3), rs.getDouble(4), 0));
                // sumRep.add(new summaryReport(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getInt(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9), rs.getString(11), statusurl, statusval, rs.getString(12)));
            }
            out.println(BilleditemDet.size());
        } catch (Exception e) {
            out.println(e.getLocalizedMessage());
        } finally {
        }
        return BilleditemDet;
    }

    public List<itemDetails> getBilleditemDet() {
        return BilleditemDet;
    }

    public List showNewOrders() {
        Connection con = null;
        try {
            con = getLDBConnection();
            PreparedStatement ps = con.prepareStatement("select o.ordno,o.orddate,o.siteid,(select reg_name from dbo.CClogin where loginid=id) as regname,(select FirstName from dbo.CC_Patient_Registration where patientid=patid) as Patname,(select mobileno from dbo.CC_Patient_Registration where patientid=patid) as Patmobileno,(select gender from dbo.CC_Patient_Registration where patientid=patid) as Gender, status,(select username from dbo.CClogin where loginid=id) as loginusername from cc_ordhead o where status = 0");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                String orddt = rs.getString(2).substring(0, 16);
                String statusurl = "";
                nrep.add(new newRep(rs.getInt(1), orddt, rs.getInt(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), statusurl, rs.getString(9)));
            }
        } catch (Exception e) {
            out.println(e.getLocalizedMessage());
        } finally {
        }
        return nrep;
    }

    public List<newRep> getNrep() {
        return nrep;
    }

    public List showProcessingOrders() {
        Connection con = null;
        try {
            con = getLDBConnection();
            PreparedStatement ps = con.prepareStatement("select o.ordno,o.orddate,o.siteid,(select reg_name from dbo.CClogin where loginid=id) as regname,(select FirstName + ' ' + LastName from dbo.CC_Patient_Registration where patientid=patid) as Patname,(select mobileno from dbo.CC_Patient_Registration where patientid=patid) as Patmobileno,(select gender from dbo.CC_Patient_Registration where patientid=patid) as Gender, status,(select username from dbo.CClogin where loginid=id) as loginusername from cc_ordhead o where status IN (1,2)");
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                String orddt = rs.getString(2).substring(0, 16);
                String statusurl = "";
                prep.add(new procRep(rs.getInt(1), orddt, rs.getInt(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), statusurl, rs.getString(9)));
            }
        } catch (Exception e) {
            out.println(e.getLocalizedMessage());
        } finally {
        }
        return prep;
    }

    public List<procRep> getPrep() {
        return prep;
    }

    public List showDeliveredOrders() {
        Connection con = null;
        try {
            con = getLDBConnection();
            PreparedStatement ps = con.prepareStatement("select o.ordno,o.orddate,o.siteid,(select reg_name from dbo.CClogin where loginid=id) as regname,(select FirstName + ' ' + LastName from dbo.CC_Patient_Registration where patientid=patid) as Patname,(select mobileno from dbo.CC_Patient_Registration where patientid=patid) as Patmobileno,(select gender from dbo.CC_Patient_Registration where patientid=patid) as Gender, status,(select username from dbo.CClogin where loginid=id) as loginusername from cc_ordhead o where status = 3");
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                String orddt = rs.getString(2).substring(0, 16);
                String statusurl = "";
                drep.add(new delRep(rs.getInt(1), orddt, rs.getInt(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), statusurl, rs.getString(9)));
            }
        } catch (Exception e) {
            out.println(e.getLocalizedMessage());
        } finally {
        }
        return drep;
    }

    public List<delRep> getDrep() {
        return drep;
    }

    public void ordClosureInit() {
        FacesContext fc = getCurrentInstance();
        Map<String, String> params = fc.getExternalContext().getRequestParameterMap();
        String ordNo = (String) params.get("ordNo");
        String ordSRC = (String) params.get("ordSrc");
        boolean result = checkOrderStatusbeforeclosureInit(ordNo);
        boolean vendUpdateStatus;
        if (result) {
            vendUpdateStatus = getDeliveryDetails(ordNo);
            if (vendUpdateStatus) {
                initClose s1 = new initClose();
                s1.doorderDeliveredwithoutPin(ordNo);
                baseReport brep = new baseReport();
                //List<tpOrderClose> tpSum = new ArrayList();
                this.tpSum.remove(this.tpcloseselectedOrder);
                getCurrentInstance().addMessage(null, new FacesMessage(SEVERITY_WARN, "Order no: " + ordNo + " Could not able to Mark Delivery at MediAssist!!! Please contact IT!!!", ""));
            } else {
                showInitiatedOrders();
                getCurrentInstance().addMessage(null, new FacesMessage(SEVERITY_WARN, "Order no: " + ordNo + " Delivery Marked!!!", ""));
            }
        } else {
            getCurrentInstance().addMessage(null, new FacesMessage(SEVERITY_WARN, "Order no: " + ordNo + " already Initiated!!!", ""));
        }
    }

    public boolean getDeliveryDetails(String _ordNo) {
        Connection con = null;
        boolean vendUpdateStatus = false;
        try {
            con = getLDBConnection();
            PreparedStatement ps = con.prepareStatement("select siteid,DOCNUM,APORDID,APBILLNO,TOTALAMT, (select paymentmode from cc_ordhead where ordno = ?), CASHVALUE,CREDITVALUE \n"
                    + "from CC_TPOrderInfo where apordid = ? and APBILLNO is not null");
            ps.setString(1, _ordNo);
            ps.setString(2, _ordNo);
            //String qry = "select siteid,tid,posdocnum,aporderid,invoicenum,invoiceamount, (select paymentmode from cc_ordhead where ordno = " + _ordNo + ") from MEDASSITTRANSACTIONS where aporderid like '%" + _ordNo + "' and invoicenum is not null ";
            //String qry = "select i.siteid,'001' as tid,'' as posdocnum,i.APORDID,i.APBILLNO,i.TOTALAMT,h.PaymentMode,h.orderSource,i.TPORDID from CC_TPOrderInfo i, cc_ordhead h where i.APORDID =" + _ordNo + " and i.APORDID = h.ordNo and h.invNo is not null";
            //PreparedStatement ps = con.prepareStatement("select i.siteid,'001' as tid,'' as posdocnum,i.APORDID,i.APBILLNO,i.TOTALAMT,h.PaymentMode,h.orderSource,i.TPORDID from CC_TPOrderInfo i, cc_ordhead h where convert(nvarchar,i.APORDID) = ? and convert(nvarchar,i.APORDID) = convert(nvarchar,h.ordNo) and h.invNo is not null");
            //ps.setString(1, _ordNo);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                String siteid = rs.getString(1);
                String docnum = rs.getString(2);
                String APOrderId = rs.getString(3);
                String invoicenum = rs.getString(4);
                String billvalue = rs.getString(5);
                String ordertype = rs.getString(6);
                //String orderSource = rs.getString(8);
                //String TPOrderId = rs.getString(9);
                String cash = rs.getString(7);
                String credit = rs.getString(8);
                String tid = "001";
                vendUpdateStatus = updateDeliveryStatustoVendor(siteid, tid, docnum, APOrderId, invoicenum, billvalue, ordertype, cash, credit);
            }
        } catch (Exception e) {
            out.println(e.getLocalizedMessage());
        }
        return vendUpdateStatus;
    }

    public boolean updateDeliveryStatustoVendor(String _siteid, String _tid, String _docnum, String _APOrderId, String _invoicenum, String _billvalue, String _ordertype, String cash, String credit) {
        boolean vendUpdateStatus = false;
        try {
            String USER_AGENT = "Mozilla/5.0";

            String GET_URL = "http://172.16.2.251:84/MediAssist_pos.aspx?siteid=" + _siteid + "&tid=" + _tid + "&docnum=" + _docnum + "&APOrderId=" + _APOrderId + "&invoicenum=" + _invoicenum + "&billvalue=" + _billvalue + "&remarks=ORDER_DELIVERED&ordertype=" + _ordertype + "&statusid=2&Cancelfrom=CRM&Action=OrderStatus&Cashvalue=" + cash + "&Creditvalue=" + credit;
            URL obj = new URL(GET_URL);
            HttpURLConnection smscon = (HttpURLConnection) obj.openConnection();
            smscon.setRequestMethod("GET");
            smscon.setRequestProperty("User-Agent", "Mozilla/5.0");
            int responseCode = smscon.getResponseCode();
            out.println("GET Response Code :: " + responseCode);
            if (responseCode == 200) {
                BufferedReader in = new BufferedReader(new InputStreamReader(smscon.getInputStream()));

                StringBuffer response = new StringBuffer();
                String inputLine;
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
                System.out.println(response.toString());
                if (response.toString().contains("true")) {
                    vendUpdateStatus = true;
                } else {
                    vendUpdateStatus = false;
                }
            } else {
                out.println("GET request not worked");
            }
        } catch (IOException ex) {
            out.println(ex.getLocalizedMessage());
        }
        return vendUpdateStatus;
    }

    //private BarChartModel barModel;
    //BarChartModel AGENTChartmodel = new BarChartModel();
    public List generateOTPlog() {
        Connection con = null;
        otplog.clear();
        otplogpersonwise.clear();
        try {
            con = connectivity.getLDBConnection();
            PreparedStatement ps = con.prepareStatement("select siteid,contactno,otp,(select username from cclogin where id = agentid),loggedon from CRM_OTPOFFERS_LOG where loggedon >= ? and loggedon <= ? ");
            ps.setString(1, df.format(ordFDate));
            ps.setString(2, df.format(ordTDate));
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                otplog.add(new otpLog(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5)));
                pb.add(rs.getString(4));
            }
            Map<String, Long> result = pb.stream().collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
            result.forEach((key, value) -> otplogpersonwise.add(new otpLog(key, value)));
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
        }
        return otplog;
    }

    public List<otpLog> getOtplog() {
        return otplog;
    }

    public List<otpLog> getOtplogpersonwise() {
        return otplogpersonwise;
    }

    public void onRowEditing(RowEditEvent event) {
        FacesMessage msg = new FacesMessage("Site Changed ", ((summaryReport) event.getObject()).getSiteid() + "," + ((summaryReport) event.getObject()).getOrdno() + "," + ((summaryReport) event.getObject()).getOrdersource());
        getCurrentInstance().addMessage(null, msg);
    }

    public void onRowCancel(RowEditEvent event) {
        FacesMessage msg = new FacesMessage("Site Change cancelled", ((summaryReport) event.getObject()).getSiteid());
        getCurrentInstance().addMessage(null, msg);
    }

    public List<String> complete(String query) {
        List<String> results = new ArrayList<String>();
        Connection con = null;
        try {
            con = getLDBConnection();
            PreparedStatement ps = con.prepareStatement("select siteid,name,HS from cc_sun_locationdetails where name like '%" + query + "%' or siteid like '" + query + "%' group by name,siteid,HS order by name, siteid");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                results.add(rs.getString(1));
            }
        } catch (Exception e) {
        }
        return results;
    }

    public String sendOnDelaySMS() {
        String smsACK = "";
        try {
            smsACK = "On-Delay SMS Send successfully to " + selectedOrder.getPatmobileno() + " for Order number : " + selectedOrder.getTpordid();
            sms _sms = new sms();
            _sms.sendDelaySMS(selectedOrder.getPatmobileno(), selectedOrder.getTpordid());
            HttpSession appSession = (HttpSession) getCurrentInstance().getExternalContext().getSession(false);
            String AgentID = appSession.getAttribute("loginid").toString();
            String ordno = selectedOrder.getTpordid();
            String vendName = selectedOrder.getOrdersource();
            addAuditLog(ordno, AgentID, vendName, "On-Delay-SMS");
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
        }
        return smsACK;
    }

    public void addAuditLog(String _ordno, String _AgentID, String _vendorName, String _comment) {
        OrderComment ordComment = new OrderComment();
        try {
            Connection con = getLDBConnection();
            ordComment.addCommenttoOrder(con, _ordno, _comment, Integer.parseInt(_AgentID), _vendorName);
            con.close();
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
        }
    }
}
