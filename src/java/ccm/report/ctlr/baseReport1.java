/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/*package ccm.report.ctlr;

import ccm.dao.bean.connectivity;
import ccm.logic.model.cancelRemarks;
import ccm.logic.model.itemDetails;
import ccm.report.model.billingInfo;
import ccm.report.model.categorywiseRep;
import ccm.report.model.delRep;
import ccm.report.model.newRep;
import ccm.report.model.procRep;
import ccm.report.model.summaryReport;
import ccm.report.model.tpOrderClose;
import com.logic.branch.ctlr.initClose;
import com.logic.branch.ctlr.initValidator;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author Pitchu
 * @created 31 Oct, 2014 5:08:58 PM
 *
 */
/*@ManagedBean(name = "baseRep")
@SessionScoped
public class baseReport1 implements Serializable {

    public String cancelCode;

    public String getCancelCode() {
        return cancelCode;
    }

    public void setCancelCode(String cancelCode) {
        this.cancelCode = cancelCode;
    }

    List<summaryReport> sumRep = new ArrayList<summaryReport>();
    List<tpOrderClose> tpSum = new ArrayList<tpOrderClose>();
    List<cancelRemarks> cancelRem = new ArrayList<cancelRemarks>();
    List<newRep> nrep = new ArrayList<newRep>();
    List<procRep> prep = new ArrayList<procRep>();
    List<delRep> drep = new ArrayList<delRep>();
    List<categorywiseRep> cate = new ArrayList<categorywiseRep>();
    List<itemDetails> itemDet = new ArrayList<itemDetails>();
    List<billingInfo> billInfo = new ArrayList<billingInfo>();

    private summaryReport selectedOrder;
    private tpOrderClose tpcloseselectedOrder;

    public tpOrderClose getTpcloseselectedOrder() {
        return tpcloseselectedOrder;
    }

    public void setTpcloseselectedOrder(tpOrderClose tpcloseselectedOrder) {
        this.tpcloseselectedOrder = tpcloseselectedOrder;
    }

    public summaryReport getSelectedOrder() {
        return selectedOrder;
    }

    public void setSelectedOrder(summaryReport selectedOrder) {
        this.selectedOrder = selectedOrder;
    }

    Calendar c = Calendar.getInstance();
    SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd");
    Date ordFDate = new java.util.Date();
    Date ordTDate = new java.util.Date();
    DecimalFormat dff = new DecimalFormat("##,###.##");

    public baseReport1() {
        showSummaryReport();
        showNewOrders();
        showProcessingOrders();
        showDeliveredOrders();
    }

    public Date getOrdFDate() {
        return ordFDate;
    }

    public void setOrdFDate(Date ordFDate) {
        this.ordFDate = ordFDate;
    }

    public Date getOrdTDate() {
        return ordTDate;
    }

    public void setOrdTDate(Date ordTDate) {
        this.ordTDate = ordTDate;
    }

    public List showInitiatedOrders() {
        FacesContext fc = FacesContext.getCurrentInstance();
        Map<String, String> params = fc.getExternalContext().getRequestParameterMap();
        String repFor = params.get("repFor");
        Connection con = null;
        PreparedStatement ps = null;
        tpSum.clear();
        try {
            con = connectivity.getLDBConnection();
            if (repFor.equals("Cancel")) {
                ps = con.prepareStatement("select o.ordno,o.siteid,ISNULL(CONVERT(varchar, o.orddate),'-') ,ISNULL(CONVERT(varchar, o.initdate),'-') ,ISNULL(CONVERT(varchar, o.deldate),'-'),(select reg_name from dbo.CClogin where loginid=id) as regname,(select FirstName + ' ' + LastName from dbo.CC_Patient_Registration where patientid=patid) as Patname,(select mobileno from dbo.CC_Patient_Registration where patientid=patid) as Patmobileno,(select gender from dbo.CC_Patient_Registration where patientid=patid) as Gender, status,(select username from dbo.CClogin where loginid=id) as loginusername,ordersource,ISNULL(CONVERT(varchar, cancelDate),'-'),ISNULL(CONVERT(varchar, cancelRemarks),'-'),paymentmode,(select Addr1 from dbo.CC_Patient_Registration where patientid=patid) as PatAddr,(select maorderid from orderupdate where ordno = REPLACE (ap_orderid, 'V' , '')) as MAOrdID from cc_ordhead o where o.status in(0, 1) and ordersource = 'MedAssist' and  CONVERT (DATE, ORDDATE)>=CONVERT (DATE, ?) AND CONVERT (DATE, ORDDATE) <=CONVERT (DATE, ?)");
                ps.setString(1, df.format(ordFDate));
                ps.setString(2, df.format(ordTDate));
            } else if (repFor.equals("DeliveryConfirmation")) {
                ps = con.prepareStatement("select o.ordno,o.siteid,ISNULL(CONVERT(varchar, o.orddate),'-') ,ISNULL(CONVERT(varchar, o.initdate),'-') ,ISNULL(CONVERT(varchar, o.deldate),'-'),(select reg_name from dbo.CClogin where loginid=id) as regname,(select FirstName + ' ' + LastName from dbo.CC_Patient_Registration where patientid=patid) as Patname,(select mobileno from dbo.CC_Patient_Registration where patientid=patid) as Patmobileno,(select gender from dbo.CC_Patient_Registration where patientid=patid) as Gender, status,(select username from dbo.CClogin where loginid=id) as loginusername,ordersource,ISNULL(CONVERT(varchar, cancelDate),'-'),ISNULL(CONVERT(varchar, cancelRemarks),'-'),paymentmode,(select Addr1 from dbo.CC_Patient_Registration where patientid=patid) as PatAddr,(select maorderid from orderupdate where ordno = REPLACE (ap_orderid, 'V' , '')) as MAOrdID from cc_ordhead o where o.status in(1) and ordersource = 'MedAssist' and  CONVERT (DATE, ORDDATE)>=CONVERT (DATE, ?) AND CONVERT (DATE, ORDDATE) <=CONVERT (DATE, ?)");
                ps.setString(1, df.format(ordFDate));
                ps.setString(2, df.format(ordTDate));
            }
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                String delCanDate = rs.getString(5);
                String statusurl = "";

                //String orddt = rs.getString(2).substring(0, 16);
                String statusval = rs.getString(10);
                if (statusval.equals("0")) {
                    statusurl = "./yellow.jpg";
                } else if (statusval.equals("1")) {
                    statusurl = "./green.jpg";
                } else if (statusval.equals("2")) {
                    statusurl = "./redcan.jpg";
                } else if (statusval.equals("3")) {
                    statusurl = "./red.jpg";
                }

                if (delCanDate.equals("-")) {
                    delCanDate = rs.getString(13);
                }
                //summaryReport(int ord, int siteid, String orddate, String initdate, String deldate, String region, String patname, String mobileno, String gender, String statusurl, String orderby, String orderSource, String cancelDate, String cancelRemarks, String paymentMode, String commAddr1)
                tpSum.add(new tpOrderClose(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getString(4), delCanDate, rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9), statusurl, rs.getString(11), rs.getString(12), rs.getString(13), rs.getString(14), rs.getString(15), rs.getString(16), rs.getInt(17)));
            }
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
        } finally {
        }
        return tpSum;
    }

    public List<tpOrderClose> getTpSum() {
        return tpSum;
    }

    public List showSummaryReport() {

        System.out.println(df.format(ordFDate));
        System.out.println(df.format(ordTDate));
        Connection con = null;
        sumRep.clear();
        try {
            con = connectivity.getLDBConnection();
            //PreparedStatement ps = con.prepareStatement("select o.ordno,o.orddate,o.siteid,(select reg_name from dbo.CClogin where loginid=id) as regname,(select FirstName + ' ' + LastName from dbo.CC_Patient_Registration where patientid=patid) as Patname,(select mobileno from dbo.CC_Patient_Registration where patientid=patid) as Patmobileno,(select gender from dbo.CC_Patient_Registration where patientid=patid) as Gender, status,(select username from dbo.CClogin where loginid=id) as loginusername from cc_ordhead o where CONVERT (DATE, ORDDATE)>=CONVERT (DATE, ?) AND CONVERT (DATE, ORDDATE) <=CONVERT (DATE, ?)");
            PreparedStatement ps = con.prepareStatement("select o.ordno,o.siteid,ISNULL(CONVERT(varchar, o.orddate),'-') ,ISNULL(CONVERT(varchar, o.initdate),'-') ,ISNULL(CONVERT(varchar, o.deldate),'-'),(select reg_name from dbo.CClogin where loginid=id) as regname,(select FirstName + ' ' + LastName from dbo.CC_Patient_Registration where patientid=patid) as Patname,(select mobileno from dbo.CC_Patient_Registration where patientid=patid) as Patmobileno,(select gender from dbo.CC_Patient_Registration where patientid=patid) as Gender, status,(select username from dbo.CClogin where loginid=id) as loginusername,ordersource,ISNULL(CONVERT(varchar, cancelDate),'-'),ISNULL(CONVERT(varchar, cancelRemarks),'-'),paymentmode,(select Addr1 from dbo.CC_Patient_Registration where patientid=patid) as PatAddr from cc_ordhead o where CONVERT (DATE, ORDDATE)>=CONVERT (DATE, ?) AND CONVERT (DATE, ORDDATE) <=CONVERT (DATE, ?)");
            ps.setString(1, df.format(ordFDate));
            ps.setString(2, df.format(ordTDate));
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                String delCanDate = rs.getString(5);
                String statusurl = "";

                //String orddt = rs.getString(2).substring(0, 16);
                String statusval = rs.getString(10);
                if (statusval.equals("0")) {
                    statusurl = "./yellow.jpg";
                } else if (statusval.equals("1")) {
                    statusurl = "./green.jpg";
                } else if (statusval.equals("2")) {
                    statusurl = "./redcan.jpg";
                } else if (statusval.equals("3")) {
                    statusurl = "./red.jpg";
                }

                if (delCanDate.equals("-")) {
                    delCanDate = rs.getString(13);
                }
                //summaryReport(int ord, int siteid, String orddate, String initdate, String deldate, String region, String patname, String mobileno, String gender, String statusurl, String orderby, String orderSource, String cancelDate, String cancelRemarks, String paymentMode, String commAddr1)
                sumRep.add(new summaryReport(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getString(4), delCanDate, rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9), statusurl, rs.getString(11), rs.getString(12), rs.getString(13), rs.getString(14), rs.getString(15), rs.getString(16)));
            }
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
        } finally {
        }
        return sumRep;
    }

    public List<summaryReport> getSumRep() {
        return sumRep;
    }

    public void showDetails() {
        FacesContext fc = FacesContext.getCurrentInstance();
        Map<String, String> params = fc.getExternalContext().getRequestParameterMap();
        String ordNo = params.get("ordNo");
        String mobno = params.get("mobNo");
        Long mobNo = Long.parseLong(mobno);
        showitemDetail(ordNo);
        showBillingDetail(ordNo);
        //showCancelRemarks();
        //shoptimize bases = new shoptimize();
        //bases.customerInfo(mobNo);
    }

    public List showCancelRemarks() {
        Connection con = null;
        cancelRem.clear();
        try {
            con = connectivity.getLDBConnection();
            PreparedStatement ps = con.prepareStatement("select * from CC_OrderCancelRemarks");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                cancelRem.add(new cancelRemarks(rs.getString(1), rs.getString(2)));
            }
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
        }
        return cancelRem;
    }

    public List<cancelRemarks> getCancelRem() {
        return cancelRem;
    }

    public List showBillingDetail(String _ordNo) {
        Connection con = null;
        billInfo.clear();
        try {
            con = connectivity.getLDBConnection();
            PreparedStatement ps = con.prepareStatement("select aporderId,siteId,tid, invoicenum, invoiceamount ,cashvalue, creditvalue from MEDASSITTRANSACTIONS where len(invoicenum)> 5 and aporderid like ?");
            ps.setString(1, "%" + _ordNo);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                billInfo.add(new billingInfo(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7)));
            }
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
        }
        return billInfo;
    }

    public List<billingInfo> getBillInfo() {
        return billInfo;
    }

    public List showitemDetail(String _ordNo) {
        Connection con = null;
        itemDet.clear();
        try {
            con = connectivity.getLDBConnection();
            //PreparedStatement ps = con.prepareStatement("select o.ordno,o.orddate,o.siteid,(select reg_name from dbo.CClogin where loginid=id) as regname,(select FirstName + ' ' + LastName from dbo.CC_Patient_Registration where patientid=patid) as Patname,(select mobileno from dbo.CC_Patient_Registration where patientid=patid) as Patmobileno,(select gender from dbo.CC_Patient_Registration where patientid=patid) as Gender, status,(select username from dbo.CClogin where loginid=id) as loginusername from cc_ordhead o where CONVERT (DATE, ORDDATE)>=CONVERT (DATE, ?) AND CONVERT (DATE, ORDDATE) <=CONVERT (DATE, ?)");
            PreparedStatement ps = con.prepareStatement("select artCode,artName,reqqoh from cc_orddet where ordNo = ?");
            ps.setString(1, _ordNo);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                itemDet.add(new itemDetails(rs.getString(1), rs.getString(2), rs.getInt(3), 0.01));
                // sumRep.add(new summaryReport(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getInt(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9), rs.getString(11), statusurl, statusval, rs.getString(12)));
            }
            System.out.println(itemDet.size());
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
        } finally {
        }
        return itemDet;
    }

    public List<itemDetails> getItemDet() {
        return itemDet;
    }

    public List showNewOrders() {
        Connection con = null;
        try {
            con = connectivity.getLDBConnection();
            PreparedStatement ps = con.prepareStatement("select o.ordno,o.orddate,o.siteid,(select reg_name from dbo.CClogin where loginid=id) as regname,(select FirstName + ' ' + LastName from dbo.CC_Patient_Registration where patientid=patid) as Patname,(select mobileno from dbo.CC_Patient_Registration where patientid=patid) as Patmobileno,(select gender from dbo.CC_Patient_Registration where patientid=patid) as Gender, status,(select username from dbo.CClogin where loginid=id) as loginusername from cc_ordhead o where status = 0");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                String orddt = rs.getString(2).substring(0, 16);
                String statusurl = "";
                nrep.add(new newRep(rs.getInt(1), orddt, rs.getInt(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), statusurl, rs.getString(9)));
            }
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
        } finally {
        }
        return nrep;
    }

    public List<newRep> getNrep() {
        return nrep;
    }

    public List showProcessingOrders() {
        Connection con = null;
        try {
            con = connectivity.getLDBConnection();
            PreparedStatement ps = con.prepareStatement("select o.ordno,o.orddate,o.siteid,(select reg_name from dbo.CClogin where loginid=id) as regname,(select FirstName + ' ' + LastName from dbo.CC_Patient_Registration where patientid=patid) as Patname,(select mobileno from dbo.CC_Patient_Registration where patientid=patid) as Patmobileno,(select gender from dbo.CC_Patient_Registration where patientid=patid) as Gender, status,(select username from dbo.CClogin where loginid=id) as loginusername from cc_ordhead o where status IN (1,2)");
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                String orddt = rs.getString(2).substring(0, 16);
                String statusurl = "";
                prep.add(new procRep(rs.getInt(1), orddt, rs.getInt(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), statusurl, rs.getString(9)));
            }
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
        } finally {
        }
        return prep;
    }

    public List<procRep> getPrep() {
        return prep;
    }

    public List showDeliveredOrders() {
        Connection con = null;
        try {
            con = connectivity.getLDBConnection();
            PreparedStatement ps = con.prepareStatement("select o.ordno,o.orddate,o.siteid,(select reg_name from dbo.CClogin where loginid=id) as regname,(select FirstName + ' ' + LastName from dbo.CC_Patient_Registration where patientid=patid) as Patname,(select mobileno from dbo.CC_Patient_Registration where patientid=patid) as Patmobileno,(select gender from dbo.CC_Patient_Registration where patientid=patid) as Gender, status,(select username from dbo.CClogin where loginid=id) as loginusername from cc_ordhead o where status = 3");
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                String orddt = rs.getString(2).substring(0, 16);
                String statusurl = "";
                drep.add(new delRep(rs.getInt(1), orddt, rs.getInt(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), statusurl, rs.getString(9)));
            }
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
        } finally {
        }
        return drep;
    }

    public List<delRep> getDrep() {
        return drep;
    }

    public void ordClosureInit() {
        FacesContext fc = FacesContext.getCurrentInstance();
        Map<String, String> params = fc.getExternalContext().getRequestParameterMap();
        String ordNo = params.get("ordNo");
        String ordSRC = params.get("ordSrc");
        boolean result = initValidator.checkOrderStatusbeforeclosureInit(ordNo);
        if (result) {
            initClose s1 = new initClose();
            s1.doorderDeliveredwithoutPin(ordNo);
            List<tpOrderClose> tpSum = new ArrayList<tpOrderClose>();
            tpSum.remove(tpcloseselectedOrder);
            getDeliveryDetails(Integer.parseInt(ordNo));
            showInitiatedOrders();

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Order no: " + ordNo + " Delivered!!!", ""));
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Order no: " + ordNo + " already Initiated!!!", ""));
        }
    }

    public void ordCancelInit() {
        String siteid = "";
        String tid = "";
        String docnum = "";
        String APOrderId = "";
        String invoicenum = "";
        String billvalue = "";
        String ordertype = "";
        String cash = "";
        String credit = "";
        FacesContext fc = FacesContext.getCurrentInstance();
        Map<String, String> params = fc.getExternalContext().getRequestParameterMap();
        String ordNo = params.get("ordNo");
        String siteID = params.get("siteID");
        String payment = params.get("payment");
        String status = "3";
        boolean result = initValidator.checkOrderStatusbeforeclosureInit(ordNo);
        if (result) {
            initClose s1 = new initClose();
            s1.doorderDeliveredwithoutPin(ordNo);
            List<tpOrderClose> tpSum = new ArrayList<tpOrderClose>();
            tpSum.remove(tpcloseselectedOrder);
            updateDeliveryStatustoVendor(siteID, tid, docnum, ordNo, invoicenum, billvalue, payment, cancelCode, status, cash, credit);
            showInitiatedOrders();

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Order no: " + ordNo + " Delivered!!!", ""));
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Order no: " + ordNo + " already Initiated!!!", ""));
        }
    }

    public void getDeliveryDetails(int _ordNo) {
        Connection con = null;
        try {
            String siteid = "";
            String tid = "";
            String docnum = "";
            String APOrderId = "";
            String invoicenum = "";
            String billvalue = "";
            String ordertype = "";
            String remarks = "";
            String status = "2";
            String cash = "";
            String credit = "";
            con = connectivity.getLDBConnection();
            Statement stmt = con.createStatement();
            String qry = "select siteid,tid,posdocnum,aporderid,invoicenum,invoiceamount, (select paymentmode from cc_ordhead where ordno = " + _ordNo + "), cashvalue,creditvalue from MEDASSITTRANSACTIONS where aporderid like '%" + _ordNo + "' and invoicenum is not null ";
            ResultSet rs = stmt.executeQuery(qry);
            if (rs.next()) {
                siteid = rs.getString(1);
                tid = rs.getString(2);
                docnum = rs.getString(3);
                APOrderId = rs.getString(4);
                invoicenum = rs.getString(5);
                billvalue = rs.getString(6);
                ordertype = rs.getString(7);
                cash = rs.getString(8);
                credit = rs.getString(9);
                updateDeliveryStatustoVendor(siteid, tid, docnum, APOrderId, invoicenum, billvalue, ordertype, remarks, status, cash, credit);
            }
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
        }
    }

    public void updateDeliveryStatustoVendor(String _siteid, String _tid, String _docnum, String _APOrderId, String _invoicenum, String _billvalue, String _ordertype, String _remarks, String _status, String _cash, String _credit) {
        try {

            final String USER_AGENT = "Mozilla/5.0";
            //siteid=16001&tid=001&docnum=1151074&APOrderId=1000000532&invoicenum=123456&billvalue=100&remarks=CRO0001&ordertype=CREDIT&statusid=2&Cancelfrom=POS&Action=OrderStatus                
            final String GET_URL = "http://172.16.2.251:84/MediAssist_pos.aspx?"
                    + "siteid=" + _siteid + "&"
                    + "tid=" + _tid + "&"
                    + "docnum=" + _docnum + "&"
                    + "APOrderId=" + _APOrderId + "&"
                    + "invoicenum=" + _invoicenum + "&"
                    + "billvalue=" + _billvalue + "&"
                    + "remarks=" + _remarks + "&"
                    + "ordertype=" + _ordertype + "&"
                    + "statusid=" + _status + "&"
                    + "Cancelfrom=CRM&"
                    + "Cashvalue=" + _cash + "&"
                    + "Creditvalue=" + _credit + "&"
                    + "Action=OrderStatus";
            URL obj = new URL(GET_URL);
            HttpURLConnection smscon = (HttpURLConnection) obj.openConnection();
            smscon.setRequestMethod("GET");
            smscon.setRequestProperty("User-Agent", USER_AGENT);
            int responseCode = smscon.getResponseCode();
            System.out.println("GET Response Code :: " + responseCode);
            if (responseCode == HttpURLConnection.HTTP_OK) { // success
                BufferedReader in = new BufferedReader(new InputStreamReader(
                        smscon.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
                System.out.println(response.toString());
            } else {
                System.out.println("GET request not worked");
            }
        } catch (IOException ex) {
            System.out.println(ex.getLocalizedMessage());
        }
    }
}
*/
