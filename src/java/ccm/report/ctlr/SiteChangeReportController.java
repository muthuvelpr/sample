/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ccm.report.ctlr;

import static ccm.dao.bean.connectivity.getLDBConnection;
import ccm.logic.bean.SiteChangeReportBeanLive;
import ccm.report.model.SiteChangeReportDetail;
import java.io.Serializable;
import java.sql.Connection;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Administrator
 */
@ManagedBean(name = "scr")
@ViewScoped
public class SiteChangeReportController implements Serializable {

    Date SCFDate = new java.util.Date();
    Date SCTDate = new java.util.Date();
    SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
    DecimalFormat dff = new DecimalFormat("##,###.##");

    String fDate = df.format(SCFDate);
    String tDate = df.format(SCTDate);

    Date maxDATE = new java.util.Date();

    public Date getSCFDate() {
        return SCFDate;
    }

    public void setSCFDate(Date SCFDate) {
        this.SCFDate = SCFDate;
    }

    public Date getSCTDate() {
        return SCTDate;
    }

    public void setSCTDate(Date SCTDate) {
        this.SCTDate = SCTDate;
    }

    /**
     * Creates a new instance of SiteChangeReportController
     */
    List<SiteChangeReportBeanLive> scrb = new ArrayList<>();
    List<SiteChangeReportBeanLive> scrbFilter;

    public SiteChangeReportController() {
    }

    public List<SiteChangeReportBeanLive> generateSiteChangeReport() {
        Connection con = null;
        try {
            scrb.clear();
            con = getLDBConnection();
            SiteChangeReportDetail scrd = new SiteChangeReportDetail();
            scrb = scrd.generateDetail(con, df.format(SCFDate), df.format(SCTDate));
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
        }
        return scrb;
    }

    public List<SiteChangeReportBeanLive> getScrb() {
        return scrb;
    }

    public List<SiteChangeReportBeanLive> getScrbFilter() {
        return scrbFilter;
    }

    public void setScrbFilter(List<SiteChangeReportBeanLive> scrbFilter) {
        this.scrbFilter = scrbFilter;
    }

    public Date onSCFDateSelect() {

//        System.out.println("contorller.ReportController.onFromDateSelect()" + getDate1());
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

        String d1 = sdf.format(SCFDate);

        int day = Integer.parseInt(new SimpleDateFormat("dd").format(SCFDate));
        int month = Integer.parseInt(new SimpleDateFormat("MM").format(SCFDate));
        int year = Integer.parseInt(new SimpleDateFormat("yyyy").format(SCFDate));

        System.out.println("contorller.ReportController.onFromDateSelect()" + d1);
        LocalDate ldate = LocalDate.of(year, month, day).plusDays(30);
        SCTDate = Date.from(ldate.atStartOfDay(ZoneId.systemDefault()).toInstant());

//        date2 = java.sql.Timestamp.valueOf(ldate.atStartOfDay());
        if (SCTDate.after(maxDATE)) {
            SCTDate = maxDATE;
        }
        return SCTDate;
    }

    public Date onSCTDateSelect() {

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        String d1 = sdf.format(SCTDate);

        int day = Integer.parseInt(new SimpleDateFormat("dd").format(SCTDate));
        int month = Integer.parseInt(new SimpleDateFormat("MM").format(SCTDate));
        int year = Integer.parseInt(new SimpleDateFormat("yyyy").format(SCTDate));

        System.out.println("contorller.ReportController.onFromDateSelect()" + d1);
        LocalDate ldate = LocalDate.of(year, month, day).minusDays(30);
        SCFDate = java.sql.Timestamp.valueOf(ldate.atStartOfDay());
        return SCFDate;

    }
}
