/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ccm.report.ctlr;

import ccm.report.model.goldBillDetail;
import ccm.report.model.goldBillSummary;
import com.heterolikereport.model.lsrSummary;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Pitchu
 * @created 3 Nov, 2014 12:04:14 PM
 */
@ManagedBean(name = "goldRep")
@ViewScoped
@SessionScoped
public class goldReport implements Serializable {

    private String telNo;
    private String cccNo;
    private String empno;
    private goldBillSummary selectedTrx;

    public goldBillSummary getSelectedTrx() {
        return selectedTrx;
    }

    public void setSelectedTrx(goldBillSummary selectedTrx) {
        this.selectedTrx = selectedTrx;
    }

    public String getEmpno() {
        return empno;
    }

    public void setEmpno(String empno) {
        this.empno = empno;
    }

    public String getTelNo() {
        return telNo;
    }

    public void setTelNo(String telNo) {
        this.telNo = telNo;
    }

    public String getCccNo() {
        return cccNo;
    }

    public void setCccNo(String cccNo) {
        this.cccNo = cccNo;
    }
    List<goldBillSummary> Billsum = new ArrayList<goldBillSummary>();
    List<goldBillSummary> ccBillsum = new ArrayList<goldBillSummary>();
    List<goldBillSummary> apBillsum = new ArrayList<goldBillSummary>();
    List<goldBillDetail> BillDet = new ArrayList<goldBillDetail>();
    List<lsrSummary> lss = new ArrayList<lsrSummary>();

    /* public List goldTBSum() {
        Connection con = null;
        Billsum.clear();
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yy");
            String myrepDate = sdf.format(new java.util.Date());
            con = getProdServerGOLDConnection();
            PreparedStatement ps = con.prepareStatement("select id,to_char(regh_date,'dd-MON-yyyy') bill_date ,regh_site Site,goldprod.PKSITDGENE.GET_SITEDESCRIPTION(regh_site) sitename,regh_tele phone,reghc_name Name,reghc_age Age,reghc_sex Sex,to_char(regh_total) Total,regh_parttr Emp_Id,regh_subdoc Bill_no from goldprod.kposreghead where trunc(regh_date) between '01-jan-15' and '" + myrepDate + "' and regh_tele= ? order by regh_date desc");
            ps.setString(1, telNo);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Billsum.add(new goldBillSummary(rs.getString(1),
                        rs.getString(2), rs.getString(3), rs.getString(4),
                        rs.getString(5), rs.getString(6), rs.getString(7),
                        rs.getString(8), rs.getString(9), rs.getString(10),
                        rs.getString(11)));
            }
        } catch (Exception e) {
            out.println(e.getLocalizedMessage());
        } finally {
        }
        return Billsum;
    }

    public List<goldBillSummary> getBillsum() {
        return Billsum;
    }

     public List goldCCSum() {
        Connection con = null;
        ccBillsum.clear();
        try {
            con = getProdServerGOLDConnection();
            PreparedStatement ps = con.prepareStatement("select id,to_char(regh_date,'dd-MON-yyyy') bill_date,regh_site site,goldprod.PKSITDGENE.GET_SITEDESCRIPTION(regh_site) sitename,regh_tele phone,reghc_name NAME,reghc_age Age,reghc_sex Sex,regh_rndamt TOTAL,regh_parttr Emp_Id,regh_subdoc Bill_no from goldprod.kposreghead where regh_part in ('1','102') and regh_parttr= ? and trunc(regh_date) between '01-jan-15' and '31-dec-15' order by regh_date desc");
            ps.setString(1, cccNo);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                ccBillsum.add(new goldBillSummary(rs.getString(1),
                        rs.getString(2), rs.getString(3), rs.getString(4),
                        rs.getString(5), rs.getString(6), rs.getString(7),
                        rs.getString(8), rs.getString(9), rs.getString(10),
                        rs.getString(11)));
            }
        } catch (Exception e) {
            out.println(e.getLocalizedMessage());
        } finally {
        }
        return ccBillsum;
    }

    public List<goldBillSummary> getCcBillsum() {
        return ccBillsum;
    }

    public List goldAPSum() {
        Connection con = null;
        apBillsum.clear();
        try {
            con = getProdServerGOLDConnection();
            PreparedStatement ps = con.prepareStatement("select id,to_char(regh_date,'dd-MON-yyyy') bill_date,regh_site site,goldprod.PKSITDGENE.GET_SITEDESCRIPTION(regh_site) sitename,regh_tele phone,reghc_name NAME,reghc_age Age,reghc_sex Sex,regh_rndamt TOTAL,regh_parttr Emp_Id,regh_subdoc Bill_no from goldprod.kposreghead where regh_part IN ('5','1771') and regh_parttr=? and trunc(regh_date) >='01-apr-14' order by regh_date desc");
            ps.setString(1, empno.toUpperCase());
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                apBillsum.add(new goldBillSummary(rs.getString(1),
                        rs.getString(2), rs.getString(3), rs.getString(4),
                        rs.getString(5), rs.getString(6), rs.getString(7),
                        rs.getString(8), rs.getString(9), rs.getString(10),
                        rs.getString(11)));
            }
        } catch (Exception e) {
            out.println(e.getLocalizedMessage());
        } finally {
        }
        return apBillsum;
    }

    public List<goldBillSummary> getApBillsum() {
        return apBillsum;
    }

    public List trxDetail() {
        Connection con = null;
        FacesContext context = getCurrentInstance();
        HttpServletRequest myRequest = (HttpServletRequest) context.getExternalContext().getRequest();
        String TID = myRequest.getParameter("trxid").toString();
        BillDet.clear();
        try {
            con = getProdServerGOLDConnection();
            PreparedStatement ps = con.prepareStatement("select to_char(regi_date,'dd-mon-yyy') bill_date,regi_site,regi_artcod,regi_desc1,regi_dept,to_number(regi_issqty) Qty ,regi_price,regi_discamt from goldprod.kposregitem where id= ?");
            ps.setString(1, TID);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                BillDet.add(new goldBillDetail(rs.getString(1).toUpperCase(), rs.getString(2),
                        rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6),
                        rs.getString(7), rs.getString(8)));
            }
        } catch (Exception e) {
            out.println(e.getLocalizedMessage());
        } finally {
        }
        return BillDet;
    }

    public List<goldBillDetail> getBillDet() {
        return BillDet;
    }

    public List lsrSum() {
        Connection con = null;
        String datamon = "";
        lss.clear();
        monCalculator monthsneeded = new monCalculator();
        datamon = monthsneeded.moncalc();
        try {
            con = getProdServerGOLDConnection();
            PreparedStatement ps = con.prepareStatement("SELECT   MAX (lastupdated) lastupdated, SUM (noofbills) noofbills, siteid,cccardno, MAX (customername) customername,MAX (customertelephone) customertelephone, SUM (jan) AS jan,SUM (feb) AS feb, SUM (mar) AS mar, SUM (apr) AS apr,SUM (may) AS may, SUM (jun) AS jun, SUM (jul) AS jul,SUM (aug) AS aug, SUM (sep) AS sep, SUM (oct) AS oct,SUM (nov) AS nov, SUM (DEC) AS DEC FROM (SELECT (SELECT MAX (TRUNC (regh_date))FROM goldprod.kposreghead i WHERE id = s.id) AS lastupdated,COUNT (DISTINCT (s.regh_billno)) noofbills, s.regh_site AS siteid,TRIM (MAX (regh_parttr)) cccardno,(SELECT MAX (reghc_name)FROM goldprod.kposreghead i WHERE id = s.id) AS customername,(SELECT MAX (regh_tele) FROM goldprod.kposreghead i WHERE id = s.id) AS customertelephone, NVL(SUM (DECODE (EXTRACT (MONTH FROM s.regh_date),1,regi_issqty*regi_price))-SUM (DECODE (EXTRACT (MONTH FROM s.regh_date),1,regi_discamt)),0)jan, NVL(SUM (DECODE (EXTRACT (MONTH FROM s.regh_date),2,regi_issqty*regi_price))-SUM (DECODE (EXTRACT (MONTH FROM s.regh_date),2,regi_discamt)),0)feb, NVL(SUM (DECODE (EXTRACT (MONTH FROM s.regh_date),3,regi_issqty*regi_price))-SUM (DECODE (EXTRACT (MONTH FROM s.regh_date),3,regi_discamt)),0)mar,  NVL(SUM (DECODE (EXTRACT (MONTH FROM s.regh_date),4,regi_issqty*regi_price))-SUM (DECODE (EXTRACT (MONTH FROM s.regh_date),4,regi_discamt)),0)apr, NVL(SUM (DECODE (EXTRACT (MONTH FROM s.regh_date),5,regi_issqty*regi_price))-SUM (DECODE (EXTRACT (MONTH FROM s.regh_date),5,regi_discamt)),0)may,NVL(SUM (DECODE (EXTRACT (MONTH FROM s.regh_date),6,regi_issqty*regi_price))-SUM (DECODE (EXTRACT (MONTH FROM s.regh_date),6,regi_discamt)),0)jun, NVL(SUM (DECODE (EXTRACT (MONTH FROM s.regh_date),7,regi_issqty*regi_price))-SUM (DECODE (EXTRACT (MONTH FROM s.regh_date),7,regi_discamt)),0)jul, NVL(SUM (DECODE (EXTRACT (MONTH FROM s.regh_date),8,regi_issqty*regi_price))-SUM (DECODE (EXTRACT (MONTH FROM s.regh_date),8,regi_discamt)),0)aug, NVL(SUM (DECODE (EXTRACT (MONTH FROM s.regh_date),9,regi_issqty*regi_price))-SUM (DECODE (EXTRACT (MONTH FROM s.regh_date),9,regi_discamt)),0)sep, NVL(SUM (DECODE (EXTRACT (MONTH FROM s.regh_date),10,regi_issqty*regi_price))-SUM (DECODE (EXTRACT (MONTH FROM s.regh_date),10,regi_discamt)),0)oct, NVL(SUM (DECODE (EXTRACT (MONTH FROM s.regh_date),11,regi_issqty*regi_price))-SUM (DECODE (EXTRACT (MONTH FROM s.regh_date),11,regi_discamt)),0)nov, NVL(SUM (DECODE (EXTRACT (MONTH FROM s.regh_date),12,regi_issqty*regi_price))-SUM (DECODE (EXTRACT (MONTH FROM s.regh_date),12,regi_discamt)),0)dec FROM goldprod.kposreghead s, goldprod.kposregitem i WHERE s.id=i.id and regh_site = 14438 and trunc(regh_date) between '31-OCT-15' and '01-Nov-15' AND regh_part = 102 AND regh_subdoc NOT LIKE '%GF%' AND regh_transtype IN (0)  and regh_parttr IS NOT NULL  GROUP BY s.id,s.regh_site) a WHERE (  jan > 0 OR feb > 0 OR mar > 0 OR apr > 0 OR may > 0 OR jun > 0 OR jul > 0 OR aug > 0  OR sep > 0 OR oct > 0 OR nov > 0 OR dec > 0) GROUP BY siteid, cccardno ORDER BY 4");
            ResultSet rs = ps.executeQuery();
            String data1 = "";
            String[] mon = datamon.split(" ");
            String m1 = mon[0];
            String m2 = mon[1];
            String m3 = mon[2];
            String m4 = mon[3];
            String m5 = mon[4];
            String m6 = mon[5];
            String m7 = mon[6];
            String m8 = mon[7];
            String m9 = mon[8];
            String m10 = mon[9];
            String m11 = mon[10];
            String m12 = mon[11];
            while (rs.next()) {
                data1 = rs.getString(m1).toString() + " " + rs.getString(m2).toString() + " " + rs.getString(m3).toString() + " " + rs.getString(m4).toString() + " " + rs.getString(m5).toString() + " " + rs.getString(m6).toString();
                String[] columns = data1.split(" ");
                String d1 = columns[0];
                String d2 = columns[1];
                String d3 = columns[2];
                String d4 = columns[3];
                String d5 = columns[4];
                String d6 = columns[5];
                //System.out.println(rs.getString(4) + " - " + rs.getString(5) + " - " + rs.getString(6)+ "M1-" + d1 + "   :M2-" + d2 + "   :M3-" + d3 + "   :M4-" + d4 + "   :M5-" + d5 + "   :M6-" + d6);
                lss.add(new lsrSummary(rs.getString(4), rs.getString(5), rs.getString(6), d1, d2, d3, d4, d5, d6));
            }
        } catch (Exception e) {
        } finally {
            close(con);
        }
        return lss;
    }

    public List<lsrSummary> getLss() {
        return lss;
    }

    public void lsrParticularBillSummary() {
    }*/
}
