/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ccm.report.ctlr;

import ccm.dao.bean.connectivity;
import com.logic.order.bean.comments;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import static java.lang.System.out;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import static javax.faces.context.FacesContext.getCurrentInstance;
import javax.faces.event.AjaxBehaviorEvent;
import javax.servlet.http.HttpSession;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Lenovo
 */
@ManagedBean(name = "sChange")
@ViewScoped
public class siteChange implements Serializable {

    String ordno, siteid, ordersource, orddate, newSite, RespMessage, orderStatus;

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getNewSite() {
        return newSite;
    }

    public String getRespMessage() {
        return RespMessage;
    }

    public void setRespMessage(String RespMessage) {
        this.RespMessage = RespMessage;
    }

    public void setNewSite(String newSite) {
        this.newSite = newSite;
    }

    public String getOrdersource() {
        return ordersource;
    }

    public void setOrdersource(String ordersource) {
        this.ordersource = ordersource;
    }

    public String getOrddate() {
        return orddate;
    }

    public void setOrddate(String orddate) {
        this.orddate = orddate;
    }

    public String getOrdno() {
        return ordno;
    }

    public void setOrdno(String ordno) {
        this.ordno = ordno;
    }

    public String getSiteid() {
        return siteid;
    }

    public void setSiteid(String siteid) {
        this.siteid = siteid;
    }

    /**
     * Creates a new instance of siteChange
     */
    public siteChange() {
    }

    public List<String> complete(String query) {
        List<String> results = new ArrayList<>();
        Connection con = null;
        try {
            con = connectivity.getLDBConnection();
            PreparedStatement ps = con.prepareStatement("select ordno from cc_ordhead where status in (0,5) and ordno like '%" + query + "%'  group by ordno");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                results.add(rs.getString(1));
            }
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
        }
        return results;
    }

    public List<String> completeOrderList(String query) {
        List<String> results = new ArrayList<>();
        Connection con = null;
        try {
            con = connectivity.getLDBConnection();
            PreparedStatement ps = con.prepareStatement("select ordno from cc_ordhead where ordno like '%" + query + "%' group by ordno");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                results.add(rs.getString(1));
            }
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
        }
        return results;
    }

    public List<String> completeSITE(String query) {
        List<String> results = new ArrayList<>();
        Connection con = null;
        try {
            con = connectivity.getLDBConnection();
            PreparedStatement ps = con.prepareStatement("select siteid  from cc_sun_locationdetails where siteid like '%" + query + "%' and siteid != '16001' group by siteid");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                results.add(rs.getString(1));
            }
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
        }
        return results;
    }

    List<comments> comment = new ArrayList<>();

    public List<comments> action(AjaxBehaviorEvent event) {
        Connection con = null;
        try {
            con = connectivity.getLDBConnection();
            PreparedStatement ps = con.prepareStatement("select h.siteid, h.ordersource, h.orddate,h.status, tp.TPORDID from cc_ordhead h, cc_tporderinfo tp where h.ordno = ? and cast(h.ordNo as nvarchar) = tp.APORDID");
            //PreparedStatement ps = con.prepareStatement("select h.siteid, h.ordersource, h.orddate,h.status, tp.TPORDID,'VFD' from cc_ordhead h, cc_tporderinfo tp where tp.TPORDID = '1427495' and cast(h.ordNo as nvarchar) = tp.APORDID union all select '',vendorname,createddatetime,OrderStatus,OrderId,'NVFD' from ThirdPartyOrderHeader where OrderId = ? order by orddate desc");
            ps.setString(1, ordno);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                // if (rs.getString(6).equals("VFD")) {
                siteid = rs.getString(1);
                ordersource = rs.getString(2);
                orddate = rs.getString(3).substring(0, 16);
                orderStatus = rs.getString(4);
                if (orderStatus.equals("0")) {
                    orderStatus = "Pushed to Outlet not yet billed";
                } else if (orderStatus.equals("1")) {
                    orderStatus = "Billed at Outlet yet to deliver";
                } else if (orderStatus.equals("2")) {
                    orderStatus = "Order Cancelled";
                } else if (orderStatus.equals("3")) {
                    orderStatus = "Order Delivered";
                } else if (orderStatus.equals("4")) {
                    orderStatus = "Order out for delivery";
                } else if (orderStatus.equals("5")) {
                    orderStatus = "Order transit from OMS to POS";
                }
                /*  } else if (rs.getString(6).equals("NVFD")) {
                    if (rs.getString(4).equals("0")) {
                        orderStatus = "Order not yet pushed to Outlet";
                    }*/
            }
            //orderCreation agentComments = new orderCreation();
            //comment = agentComments.listComments(rs.getString(5), rs.getString(2));
//            } else {
//                orderStatus = "No Order Found";
//            }
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
        }
        return comment;
    }

    public List<comments> getComment() {
        return comment;
    }

    public String initiateSiteChange() {
        HttpSession appSession = (HttpSession) getCurrentInstance().getExternalContext().getSession(false);
        String username = appSession.getAttribute("loginid").toString();
        boolean eligible = validateOrderforSiteChange(ordno);

        String res = "";

        if (eligible) {
            if (ordersource.equals("PRACTO")) {
                try {
                    String USER_AGENT = "Mozilla/5.0";

                    String GET_URL = "http://172.16.2.251:82/sitechgereq.aspx?Orderid=" + ordno + "&fromsiteid=" + siteid + "&tositeid=" + newSite + "&Action=SITECHANGE&vname=PRACTO";
                    URL obj = new URL(GET_URL);
                    HttpURLConnection smscon = (HttpURLConnection) obj.openConnection();
                    smscon.setRequestMethod("GET");
                    smscon.setRequestProperty("User-Agent", "Mozilla/5.0");
                    int responseCode = smscon.getResponseCode();
                    out.println("GET Response Code :: " + responseCode);
                    if (responseCode == 200) {
                        BufferedReader in = new BufferedReader(new InputStreamReader(smscon.getInputStream()));
                        StringBuffer resp = new StringBuffer();
                        String inputLine;
                        while ((inputLine = in.readLine()) != null) {
                            resp.append(inputLine);
                        }
                        in.close();
                        out.println(resp.toString());
                        Matcher Response = Pattern.compile(Pattern.quote("{") + "(.*?)" + Pattern.quote("}")).matcher(resp.toString());
                        Response.find();
                        res = Response.group(1);
                        res = "[{" + res + "}]";
                    } else {
                        out.println("GET request not worked");
                    }
                } catch (IOException ex) {
                    out.println(ex.getLocalizedMessage());
                }
                String ReferenceNumber, status, message;
                try {
                    JSONArray jsonArray = new JSONArray(res);
                    int count = jsonArray.length();
                    for (int i = 0; i < count; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        ReferenceNumber = jsonObject.getString("ReferenceNumber");
                        status = jsonObject.getString("status");
                        //message = jsonObject.getString("message");
                        System.out.println(ReferenceNumber + " - " + status);
                        if (status.equals("fail")) {
                            RespMessage = "FAILURE";
                        } else if (status.equals("SUCCESS")) {
                            RespMessage = "SUCCESS";
                            doSiteChangeProcessatApollo(ReferenceNumber, siteid, newSite, username);
                        }
                    }
                } catch (JSONException e) {
                    out.println(e.getLocalizedMessage());
                }
                //} else if (ordersource.equals("MediAssist")) {
                //TPOrderUpdate vendorUpdate = new TPOrderUpdate();
                //vendorUpdate.updateAPOrdertoVendor(selectedOrder.getOrdNo(), apolloordno, newSite);
            } else if (ordersource.equalsIgnoreCase("Online") || ordersource.equalsIgnoreCase("AskApollo") || ordersource.equalsIgnoreCase("MediAssist") || ordersource.equalsIgnoreCase("Apollo247")) {
                if (siteid != newSite) {
                    doSiteChangeProcessatApollo(ordno, siteid, newSite, username);
                    RespMessage = "SUCCESS";
                } else {
                    RespMessage = "COULD NOT CHANGE";
                }
            }
        } else {
            RespMessage = "COULD NOT CHANGE";
        }
        return RespMessage;
    }

    public void doSiteChangeProcessatApollo(String _ReferenceNumber, String _siteid, String _newSite, String _username) {
        try {
            Connection con = connectivity.getLDBConnection();
            PreparedStatement ps = con.prepareCall("{call SiteChange(?,?,?,?)}");
            ps.setString(1, _ReferenceNumber);
            ps.setString(2, _siteid);
            ps.setString(3, _newSite);
            ps.setString(4, _username);
            ps.executeUpdate();
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
        }
    }

    public static boolean validateOrderforSiteChange(String _ordno) {
        Connection con = null;
        try {
            con = connectivity.getLDBConnection();
            PreparedStatement ps = con.prepareStatement("select status from cc_ordhead where ordno = ? and status in (0,5)");
            ps.setString(1, _ordno);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
        }
        return false;
    }
}
