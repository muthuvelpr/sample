/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ccm.report.ctlr;

import ccm.dao.bean.connectivity;
import ccm.logic.bean.CommentReportBean;
import ccm.report.model.CommentReportDetail;
import java.io.Serializable;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Administrator
 */
@ManagedBean(name = "crc")
@ViewScoped
public class CommentReportController implements Serializable {

    List<CommentReportBean> crl = new ArrayList<>();
    List<CommentReportBean> crlFilter;
    Date CRFDate;
    Date CRTDate;

    public CommentReportController() {
    }

    public Date getCRFDate() {
        return CRFDate;
    }

    public void setCRFDate(Date CRFDate) {
        this.CRFDate = CRFDate;
    }

    public Date getCRTDate() {
        return CRTDate;
    }

    public List<CommentReportBean> getCrlFilter() {
        return crlFilter;
    }

    public void setCrlFilter(List<CommentReportBean> crlFilter) {
        this.crlFilter = crlFilter;
    }

    public void setCRTDate(Date CRTDate) {
        this.CRTDate = CRTDate;
    }

    SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");

    String fDate = sdf.format(CRFDate);
    String tDate = sdf.format(CRTDate);

    public List<CommentReportBean> generateCommentData() {
        Connection con = null;
        CommentReportDetail crd = new CommentReportDetail();
        try {
            con = connectivity.getLDBConnection();
            crl = crd.generateCommentReport(con, fDate, tDate);
        } catch (Exception e) {

        }
        return crl;
    }

    public List<CommentReportBean> getCrl() {
        return crl;
    }

}
