/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ccm.report.ctlr;

import ccm.dao.bean.connectivity;
import ccm.report.model.stockDetails;
import com.lowagie.text.BadElementException;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.PageSize;
import java.io.IOException;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Lenovo
 */
@ManagedBean(name = "stk")
@ViewScoped
public class stock implements Serializable {

    /**
     * Creates a new instance of stock
     */
    public stock() {
        List FMCGStock = FMCGStock();
    }

    List<stockDetails> stockDet = new ArrayList<>();
    List<stockDetails> filteredStock;

    public List FMCGStock() {
        Connection con = null;
        stockDet.clear();
        try {
            con = connectivity.getMISDB1Connection();
            PreparedStatement ps = con.prepareStatement("select * from FMCG_Stock_Mark1");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                stockDet.add(new stockDetails(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getInt(8), rs.getDouble(9)));
            }

        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
        }
        try {
            con = connectivity.getMISDB2Connection();
            PreparedStatement ps = con.prepareStatement("select * from FMCG_Stock_Mark2");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                stockDet.add(new stockDetails(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getInt(8), rs.getDouble(9)));
            }
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
        }
        return stockDet;
    }

    public List<stockDetails> getStockDet() {
        return stockDet;
    }

    public List<stockDetails> getFilteredStock() {
        return filteredStock;
    }

    public void setFilteredStock(List<stockDetails> filteredStock) {
        this.filteredStock = filteredStock;
    }

    public void preProcesoPDF(Object document) throws IOException, BadElementException, DocumentException {
        System.out.println("PREPROCESO");

        Document pdf = (Document) document;
        try {
            pdf.setPageSize(PageSize.A4.rotate());
            pdf.setMargins(2, 2, 2, 2);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!pdf.isOpen()) {
            pdf.open();
        }
    }
}
