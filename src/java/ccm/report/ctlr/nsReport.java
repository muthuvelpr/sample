/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ccm.report.ctlr;

import ccm.dao.bean.connectivity;
import ccm.report.model.nostock;
import ccm.report.model.summaryReport;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import static javax.faces.context.FacesContext.getCurrentInstance;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Lenovo
 */
@ManagedBean(name = "nostockRep")
@ViewScoped
public class nsReport {

    List<nostock> ns = new ArrayList<>();
    List<summaryReport> nostkrep = new ArrayList();
    List<nostock> filterNS;
    Date ordFDate = new java.util.Date();
    Date ordTDate = new java.util.Date();
    String VendorName = "";
    Date maxDATE = new java.util.Date();

    public String getVendorName() {
        return VendorName;
    }

    public void setVendorName(String VendorName) {
        this.VendorName = VendorName;
    }

    List<String> nostk = new ArrayList();

    public List<nostock> getFilterNS() {
        return filterNS;
    }

    public void setFilterNS(List<nostock> filterNS) {
        this.filterNS = filterNS;
    }

    public nsReport() throws SQLException, ClassNotFoundException {
        //List showNSReport = showNSReport();
    }

    public Date getOrdFDate() {
        return ordFDate;
    }

    public void setOrdFDate(Date ordFDate) {
        this.ordFDate = ordFDate;
    }

    public Date getOrdTDate() {
        return ordTDate;
    }

    public void setOrdTDate(Date ordTDate) {
        this.ordTDate = ordTDate;
    }

    public Date onNSFromDateSelect() {

//        System.out.println("contorller.ReportController.onFromDateSelect()" + getDate1());
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

        String d1 = sdf.format(ordFDate);

        int day = Integer.parseInt(new SimpleDateFormat("dd").format(ordFDate));
        int month = Integer.parseInt(new SimpleDateFormat("MM").format(ordFDate));
        int year = Integer.parseInt(new SimpleDateFormat("yyyy").format(ordFDate));

        System.out.println("contorller.ReportController.onFromDateSelect()" + d1);
        LocalDate ldate = LocalDate.of(year, month, day).plusDays(30);
        ordTDate = Date.from(ldate.atStartOfDay(ZoneId.systemDefault()).toInstant());

//        date2 = java.sql.Timestamp.valueOf(ldate.atStartOfDay());
        if (ordTDate.after(maxDATE)) {
            ordTDate = maxDATE;
        }
        return ordTDate;
    }

    public Date onNSToDateSelect() {

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        String d1 = sdf.format(ordTDate);

        int day = Integer.parseInt(new SimpleDateFormat("dd").format(ordTDate));
        int month = Integer.parseInt(new SimpleDateFormat("MM").format(ordTDate));
        int year = Integer.parseInt(new SimpleDateFormat("yyyy").format(ordTDate));

        System.out.println("contorller.ReportController.onFromDateSelect()" + d1);
        LocalDate ldate = LocalDate.of(year, month, day).minusDays(30);
        ordFDate = java.sql.Timestamp.valueOf(ldate.atStartOfDay());
        return ordFDate;

    }
    SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");

    public List showNSReport() {
        Connection con = null;
        HttpSession appSession = (HttpSession) getCurrentInstance().getExternalContext().getSession(false);
        String region = appSession.getAttribute("reg_id").toString();
        ns.clear();
        nostk.clear();
        nostkrep.clear();
        //long total = 0;
        try {
            con = connectivity.getLDBConnection();
            //PreparedStatement ps = con.prepareStatement("select h.ordno,h.ordersource,h.siteid,(select REGION from openquery (CMS,'select userid,REGION from web..web_login_master')b where userid = CAST(h.siteid as nvarchar)) Regname,d.artcode,d.artname,d.reqqoh, d.onhand,d.onhandupdate,h.orddate,o.tpordid,(select empname from openquery (CMS,'select userid,empname from web..web_login_master')b where userid = CAST(h.siteid as nvarchar)) branchname from cc_ordhead h, cc_orddet d, CC_TPOrderInfo o  where h.ordersource = 'practo' and h.status  = 0 and h.ordno = d.ordno and cast(h.ordNo as nvarchar) = o.apordid");
            String qry = "select h.ordno,h.ordersource,h.siteid,(select REGION from openquery (CMS,'select userid,REGION from web..web_login_master where dc=''" + region + "''')b where userid = CAST(h.siteid as nvarchar)) Regname,d.artcode,d.artname,d.reqqoh, d.onhand,d.onhandupdate,h.orddate,o.tpordid,(select empname from openquery (CMS,'select userid,empname from web..web_login_master where dc=''" + region + "''')b where userid = CAST(h.siteid as nvarchar)) branchname from cc_ordhead h, cc_orddet d, CC_TPOrderInfo o  where h.ordersource IN ('" + VendorName + "') and h.status  = 0 and cast(h.orddate as date) between '" + df.format(this.ordFDate) + "' and  '" + df.format(this.ordTDate) + "' and h.ordno = d.ordno and cast(h.ordNo as nvarchar) = o.apordid and h.siteid in (select userid from openquery (CMS,'select userid from web..web_login_master where dc=''" + region + "'' and dept = ''BR'''))";
            //String qry = "select h.ordno,h.ordersource,h.siteid,(select REGION from openquery (CMS,'select userid,REGION from web..web_login_master where dc=''" + region + "''')b where userid = CAST(h.siteid as nvarchar)) Regname,d.artcode,d.artname,d.reqqoh, d.onhand,d.onhandupdate,h.orddate,o.tpordid,(select empname from openquery (CMS,'select userid,empname from web..web_login_master where dc=''" + region + "''')b where userid = CAST(h.siteid as nvarchar)) branchname from cc_ordhead h, cc_orddet d, CC_TPOrderInfo o  where h.ordersource = 'practo' and h.status  = 0 and h.ordno = d.ordno and cast(h.ordNo as nvarchar) = o.apordid and h.siteid in (select userid from openquery (CMS,'select userid from web..web_login_master where dc=''" + region + "'' and dept = ''BR'''))";
            PreparedStatement ps = con.prepareStatement(qry);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                String onHand = rs.getString(8);
                String updatedate = rs.getString(9).substring(0, 16);
                String orddate = rs.getString(10).substring(0, 16);
                if (onHand == null | onHand == "") {
                    onHand = "0";
                }
                String status = "";
                if (Integer.parseInt(onHand) >= Integer.parseInt(rs.getString(7))) {
                    status = "";
                } else if (onHand.equals("0")) {
                    status = "NO STOCK";
                } else {
                    status = "INSUFFICIENT STOCK";
                }

                ns.add(new nostock(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6),
                        rs.getString(7), onHand, updatedate, orddate, status, rs.getString(11), rs.getString(12)));
                nostk.add(status);
                //total += Integer.parseInt(onHand);
            }
            Map<String, Long> result = nostk.stream().collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
            //result.forEach((key, value) -> nostkrep.add(new summaryReport(key, value)));
            result.forEach((key, value) -> System.out.println(key + " , " + value));
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
        }
        return ns;
    }

    public List<nostock> getNs() {
        return ns;
    }

    public List<summaryReport> getNostkrep() {
        return nostkrep;
    }

    public List<String> getNostk() {
        return nostk;
    }

}
