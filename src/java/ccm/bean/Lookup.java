/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ccm.bean;

/**
 *
 * @author MUTHUVEL
 */
public class Lookup {

    String sku;
    int qty;

    public Lookup(String sku, int qty) {
        this.sku = sku;
        this.qty = qty;
    }

    public Lookup() {
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

}
