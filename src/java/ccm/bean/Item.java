/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ccm.bean;

/**
 *
 * @author MUTHUVEL
 */
public class Item {

    String artCode;
    int qty;

    public Item(String artCode, int qty) {
        this.artCode = artCode;
        this.qty = qty;
    }

    public Item() {
    }

    public String getArtCode() {
        return artCode;
    }

    public void setArtCode(String artCode) {
        this.artCode = artCode;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

}
