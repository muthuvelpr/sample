/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ccm.bean;

/**
 *
 * @author MUTHUVEL
 */
public class SiteItemDetail {

    String siteId;
//    ArrayList<Item> listItem;
    Item listItem;

    public SiteItemDetail() {
    }

    public SiteItemDetail(String siteId, Item listItem) {
        this.siteId = siteId;
        this.listItem = listItem;
    }

    public String getSiteId() {
        return siteId;
    }

    public void setSiteId(String siteId) {
        this.siteId = siteId;
    }

    public Item getListItem() {
        return listItem;
    }

    public void setListItem(Item listItem) {
        this.listItem = listItem;
    }

}
