/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ccm.bean;

import java.util.ArrayList;

/**
 *
 * @author MUTHUVEL
 */
public class RequestPayload {

    String postalcode;
    String ordertype;
    ArrayList<Lookup> lookup;

    public RequestPayload(String postalcode, String ordertype, ArrayList<Lookup> lookup) {
        this.postalcode = postalcode;
        this.ordertype = ordertype;
        this.lookup = lookup;
    }

    public RequestPayload() {
    }

    public String getPostalcode() {
        return postalcode;
    }

    public void setPostalcode(String postalcode) {
        this.postalcode = postalcode;
    }

    public String getOrdertype() {
        return ordertype;
    }

    public void setOrdertype(String ordertype) {
        this.ordertype = ordertype;
    }

    public ArrayList<Lookup> getLookup() {
        return lookup;
    }

    public void setLookup(ArrayList<Lookup> lookup) {
        this.lookup = lookup;
    }

}
