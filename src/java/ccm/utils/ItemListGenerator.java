/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ccm.utils;

import ccm.bean.Item;
import ccm.bean.SiteItemDetail;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author MUTHUVEL
 */
public class ItemListGenerator {

    public List generateListBySku(String sku, List<SiteItemDetail> siteItemList) {
        List<SiteItemDetail> siteList = new ArrayList<>();
        for (int i = 0; i < siteItemList.size(); i++) {
            if (sku.equalsIgnoreCase(siteItemList.get(i).getListItem().getArtCode())) {
                siteList.add(new SiteItemDetail(siteItemList.get(i).getSiteId(), new Item(siteItemList.get(i).getListItem().getArtCode(), siteItemList.get(i).getListItem().getQty())));

            }
        }
        return siteList;
    }
}
