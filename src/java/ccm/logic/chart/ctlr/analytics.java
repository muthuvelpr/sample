/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ccm.logic.chart.ctlr;

import ccm.dao.bean.connectivity;
import static ccm.dao.bean.connectivity.getLDBConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.faces.bean.ManagedBean;
import org.primefaces.model.chart.CartesianChartModel;
import org.primefaces.model.chart.ChartSeries;

/**
 *
 * @author Pitchu
 * @created 11 Nov, 2014 3:58:00 PM
 */
@ManagedBean
public class analytics {

    public analytics() {
        createBasicAnalyticsPanIndia();
    }
    private CartesianChartModel myPANIndiaAnalytics;

    private void createBasicAnalyticsPanIndia() {
        myPANIndiaAnalytics = new CartesianChartModel();

        ChartSeries createdOrder = new ChartSeries();
        ChartSeries processingOrder = new ChartSeries();
        ChartSeries deliveredOrder = new ChartSeries();

        createdOrder.setLabel("CREATED ORDERS");
        processingOrder.setLabel("PROCESSING ORDERS");
        deliveredOrder.setLabel("DELIVERED ORDERS");

        Connection con = null;
        try {
            con = getLDBConnection();
            PreparedStatement ps = con.prepareStatement("select city region,count(ordno) ordcount,status from CC_Ordhead o,cc_apollo_site s where o.siteid=s.siteid group by city,status");
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                int status = rs.getInt(3);
                if (status == 0) {
                    createdOrder.set(rs.getString(1), rs.getInt(2));

                } else if (status == 1) {
                    processingOrder.set(rs.getString(1), rs.getInt(2));

                } else if (status == 2) {
                    deliveredOrder.set(rs.getString(1), rs.getInt(2));

                }
            }
            myPANIndiaAnalytics.addSeries(createdOrder);
            myPANIndiaAnalytics.addSeries(processingOrder);
            myPANIndiaAnalytics.addSeries(deliveredOrder);
            //myPANIndiaAnalytics.setStacked(true);
            //myPANIndiaAnalytics.setAnimate(true);
            //myPANIndiaAnalytics.setLegendPosition("ne");


        } catch (Exception e) {
        }
    }

    public CartesianChartModel getMyPANIndiaAnalytics() {
        return myPANIndiaAnalytics;
    }
}
