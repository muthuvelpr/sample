/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ccm.logic.bean;

/**
 *
 * @author Administrator
 */
public class returnItemDetails {

    private String retRefno, artCode, artName, retQTY, returnDate;

    public returnItemDetails(String retRefno, String artCode, String artName, String retQTY, String returnDate) {
        this.retRefno = retRefno;
        this.artCode = artCode;
        this.artName = artName;
        this.retQTY = retQTY;
        this.returnDate = returnDate;
    }

    public String getRetRefno() {
        return retRefno;
    }

    public void setRetRefno(String retRefno) {
        this.retRefno = retRefno;
    }

    public String getArtCode() {
        return artCode;
    }

    public void setArtCode(String artCode) {
        this.artCode = artCode;
    }

    public String getArtName() {
        return artName;
    }

    public void setArtName(String artName) {
        this.artName = artName;
    }

    public String getRetQTY() {
        return retQTY;
    }

    public void setRetQTY(String retQTY) {
        this.retQTY = retQTY;
    }

    public String getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(String returnDate) {
        this.returnDate = returnDate;
    }

}
