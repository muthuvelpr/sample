/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ccm.logic.bean;

import java.io.Serializable;

/**
 *
 * @author Pitchu
 * @created 30 Oct, 2014 12:54:53 PM
 *
 */
public class customerInfo implements Serializable {

    private String pid, firstName, gender, addr1, commAddr, mailID, cccardno, uhid;
    private int age;

    public customerInfo(String pid, String firstName, String gender, int age, String addr1, String commAddr, String mailID, String cccardno, String uhid) {
        this.pid = pid;
        this.firstName = firstName;
        this.gender = gender;
        this.age = age;
        this.addr1 = addr1;
        this.commAddr = commAddr;
        this.mailID = mailID;
        this.cccardno = cccardno;
        this.uhid = uhid;
    }

    public String getCccardno() {
        return cccardno;
    }

    public void setCccardno(String cccardno) {
        this.cccardno = cccardno;
    }

    public String getUhid() {
        return uhid;
    }

    public void setUhid(String uhid) {
        this.uhid = uhid;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getAddr1() {
        return addr1;
    }

    public void setAddr1(String addr1) {
        this.addr1 = addr1;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getCommAddr() {
        return commAddr;
    }

    public void setCommAddr(String commAddr) {
        this.commAddr = commAddr;
    }

    public String getMailID() {
        return mailID;
    }

    public void setMailID(String mailID) {
        this.mailID = mailID;
    }
}
