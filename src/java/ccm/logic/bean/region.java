/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ccm.logic.bean;

/**
 *
 * @author Pitchu
 * @created 29 Oct, 2014 7:37:44 PM
 *
 */
public class region {

    private String region_name;

    public region(String region_name) {

        this.region_name = region_name;
    }

    public String getRegion_name() {
        return region_name;
    }

    public void setRegion_name(String region_name) {
        this.region_name = region_name;
    }
}
