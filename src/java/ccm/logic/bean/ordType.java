/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ccm.logic.bean;

/**
 *
 * @author Pitchu
 * @created Sep 6, 2016 7:24:36 PM
 */
public class ordType {

    String ordertype, pushsms;

    public String getOrdertype() {
        return ordertype;
    }

    public void setOrdertype(String ordertype) {
        this.ordertype = ordertype;
    }

    public String getPushsms() {
        return pushsms;
    }

    public void setPushsms(String pushsms) {
        this.pushsms = pushsms;
    }

    public ordType(String ordertype, String pushsms) {
        this.ordertype = ordertype;
        this.pushsms = pushsms;
    }
}
