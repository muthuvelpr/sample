/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ccm.logic.bean;

/**
 *
 * @author Lenovo
 */
public class siteChangeDataContract {

    public String ordno, originSiteID, NewSiteID, orderStatus;

    public siteChangeDataContract(String ordno, String orderStatus, String NewSiteID) {
        this.ordno = ordno;
        this.orderStatus = orderStatus;
        this.NewSiteID = NewSiteID;
    }

    public siteChangeDataContract() {
    }

    public String getOrdno() {
        return ordno;
    }

    public void setOrdno(String ordno) {
        this.ordno = ordno;
    }

    public String getOriginSiteID() {
        return originSiteID;
    }

    public void setOriginSiteID(String originSiteID) {
        this.originSiteID = originSiteID;
    }

    public String getNewSiteID() {
        return NewSiteID;
    }

    public void setNewSiteID(String NewSiteID) {
        this.NewSiteID = NewSiteID;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

}
