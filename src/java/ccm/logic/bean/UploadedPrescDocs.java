/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ccm.logic.bean;

/**
 * @created for APOLLO PHARMACY DPAPP
 * @author PITCHU
 * @created Jul 26, 2017 7:59:57 AM
 */
public class UploadedPrescDocs {

    String displayURL, prescURL;

    public UploadedPrescDocs(String displayURL, String prescURL) {
        this.displayURL = displayURL;
        this.prescURL = prescURL;
    }

    public String getDisplayURL() {
        return displayURL;
    }

    public void setDisplayURL(String displayURL) {
        this.displayURL = displayURL;
    }

    public String getPrescURL() {
        return prescURL;
    }

    public void setPrescURL(String prescURL) {
        this.prescURL = prescURL;
    }

}
