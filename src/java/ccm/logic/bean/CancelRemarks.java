/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ccm.logic.bean;

import java.io.Serializable;

/**
 *
 * @author pitchaiah_m
 */
public class CancelRemarks implements Serializable {

    private String crcode, cr;

    public CancelRemarks(String crcode, String cr) {
        this.crcode = crcode;
        this.cr = cr;
    }

    public String getCrcode() {
        return crcode;
    }

    public void setCrcode(String crcode) {
        this.crcode = crcode;
    }

    public String getCr() {
        return cr;
    }

    public void setCr(String cr) {
        this.cr = cr;
    }

}
