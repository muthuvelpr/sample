/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ccm.logic.bean;

/**
 *
 * @author Administrator
 */
public class CommentReportBean {

    private String Ordno, CommentData, CommentBy, CommentOn, OrderSource;

    public CommentReportBean(String Ordno, String CommentData, String CommentBy, String CommentOn, String OrderSource) {
        this.Ordno = Ordno;
        this.CommentData = CommentData;
        this.CommentBy = CommentBy;
        this.CommentOn = CommentOn;
        this.OrderSource = OrderSource;
    }

    public String getOrdno() {
        return Ordno;
    }

    public void setOrdno(String Ordno) {
        this.Ordno = Ordno;
    }

    public String getCommentData() {
        return CommentData;
    }

    public void setCommentData(String CommentData) {
        this.CommentData = CommentData;
    }

    public String getCommentBy() {
        return CommentBy;
    }

    public void setCommentBy(String CommentBy) {
        this.CommentBy = CommentBy;
    }

    public String getCommentOn() {
        return CommentOn;
    }

    public void setCommentOn(String CommentOn) {
        this.CommentOn = CommentOn;
    }

    public String getOrderSource() {
        return OrderSource;
    }

    public void setOrderSource(String OrderSource) {
        this.OrderSource = OrderSource;
    }

}
