/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ccm.logic.bean;

/**
 *
 * @author Lenovo
 */
public class RegionwiseOrderstatus {

    private String Region, Vendor;
    private int Count;

    public RegionwiseOrderstatus(String Region, int Count) {
        this.Region = Region;
        this.Count = Count;
    }

    public RegionwiseOrderstatus(String Region, String Vendor, int Count) {
        this.Region = Region;
        this.Vendor = Vendor;
        this.Count = Count;
    }

    public String getRegion() {
        return Region;
    }

    public void setRegion(String Region) {
        this.Region = Region;
    }

    public String getVendor() {
        return Vendor;
    }

    public void setVendor(String Vendor) {
        this.Vendor = Vendor;
    }

    public int getCount() {
        return Count;
    }

    public void setCount(int Count) {
        this.Count = Count;
    }

}
