/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ccm.logic.bean;

/**
 *
 * @author Pitchu
 * @created 30/10/2014 2:38 A.M
 */
public class myCart {

    private String artcode, artname;
    private int qoh, reqQTY;
    private double mrp, pack;
    private double subtotal;
    private String aplinecomment;
    private String tplinecomment;

//    public myCart(String artcode, String artname, int qoh, int reqQTY, double mrp, double subtotal, double pack) {
//        this.artcode = artcode;
//        this.artname = artname;
//        this.qoh = qoh;
//        this.reqQTY = reqQTY;
//        this.mrp = mrp;
//        this.subtotal = subtotal;
//        this.pack = pack;
//    }
//    public myCart(String artcode, String artname, int qoh, int reqQTY, double mrp, double subtotal, double pack, String aplinecomment, String tplinecomment) {
//        this.artcode = artcode;
//        this.artname = artname;
//        this.qoh = qoh;
//        this.reqQTY = reqQTY;
//        this.mrp = mrp;
//        this.subtotal = subtotal;
//        this.pack = pack;
//        this.aplinecomment = aplinecomment;
//        this.tplinecomment = tplinecomment;
//    }
    public myCart(String artcode, String artname, int qoh, int reqQTY, double mrp, double subtotal, double pack, String aplinecomment, String tplinecomment) {
        this.artcode = artcode;
        this.artname = artname;
        this.qoh = qoh;
        this.reqQTY = reqQTY;
        this.mrp = mrp;
        this.pack = pack;
        this.subtotal = subtotal;
        this.aplinecomment = aplinecomment;
        this.tplinecomment = tplinecomment;
    }

    public String getArtcode() {
        return artcode;
    }

    public void setArtcode(String artcode) {
        this.artcode = artcode;
    }

    public String getArtname() {
        return artname;
    }

    public void setArtname(String artname) {
        this.artname = artname;
    }

    public double getMrp() {
        return mrp;
    }

    public void setMrp(double mrp) {
        this.mrp = mrp;
    }

    public int getQoh() {
        return qoh;
    }

    public void setQoh(int qoh) {
        this.qoh = qoh;
    }

    public int getReqQTY() {
        return reqQTY;
    }

    public void setReqQTY(int reqQTY) {
        this.reqQTY = reqQTY;
    }

    public double getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(double subtotal) {
        this.subtotal = subtotal;
    }

    public double getPack() {
        return pack;
    }

    public void setPack(double pack) {
        this.pack = pack;
    }

    public String getAplinecomment() {
        return aplinecomment;
    }

    public void setAplinecomment(String aplinecomment) {
        this.aplinecomment = aplinecomment;
    }

    public String getTplinecomment() {
        return tplinecomment;
    }

    public void setTplinecomment(String tplinecomment) {
        this.tplinecomment = tplinecomment;
    }

    @Override
    public String toString() {
        return "" + this.artcode + ", " + this.artname + ", " + this.qoh + ", " + this.reqQTY;
    }
}
