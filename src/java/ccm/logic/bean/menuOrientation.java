/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ccm.logic.bean;

import javax.faces.bean.ManagedBean;

/**
 *
 * @author Pitchu
 * @created 4 Nov, 2014 2:39:54 PM
 */
@ManagedBean
public class menuOrientation {

    private String orientation = "vertical";

    public String getOrientation() {
        return orientation;
    }

    public void setOrientation(String orientation) {
        this.orientation = orientation;
    }
}
