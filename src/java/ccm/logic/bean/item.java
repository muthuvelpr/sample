/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ccm.logic.bean;

/**
 *
 * @author Pitchu
 * @created 30-10-2014 1:12 A.M.
 */
public class item {

    private String artcode, artname;

    public item(String artcode, String artname) {
        this.artcode = artcode;
        this.artname = artname;
    }

    public String getArtcode() {
        return artcode;
    }

    public void setArtcode(String artcode) {
        this.artcode = artcode;
    }

    public String getArtname() {
        return artname;
    }

    public void setArtname(String artname) {
        this.artname = artname;
    }
}
