/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ccm.logic.bean;

/**
 *
 * @author Pitchu
 * @created 5 Nov, 2014 6:06:22 PM
 *
 */
public class ordHistItem {

    private String artcode, artname;
    int qty, ordNo;
    double mrp;

    public ordHistItem(String artcode, String artname, int qty, int ordNo, double mrp) {
        this.artcode = artcode;
        this.artname = artname;
        this.qty = qty;
        this.ordNo = ordNo;
        this.mrp = mrp;
    }

    public double getMrp() {
        return mrp;
    }

    public void setMrp(double mrp) {
        this.mrp = mrp;
    }

    public int getOrdNo() {
        return ordNo;
    }

    public void setOrdNo(int ordNo) {
        this.ordNo = ordNo;
    }

    public String getArtcode() {
        return artcode;
    }

    public void setArtcode(String artcode) {
        this.artcode = artcode;
    }

    public String getArtname() {
        return artname;
    }

    public void setArtname(String artname) {
        this.artname = artname;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }
}
