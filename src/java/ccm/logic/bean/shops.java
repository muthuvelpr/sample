/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ccm.logic.bean;

/**
 *
 * @author Pitchu
 * @created 30 Oct, 2014 11:10:14 AM
 *
 */
public class shops {

    private String sitename;
    private int siteid;

    public shops(int siteid, String sitename) {
        this.sitename = sitename.toString();
        this.siteid = siteid;
    }

    public int getSiteid() {
        return siteid;
    }

    public void setSiteid(int siteid) {
        this.siteid = siteid;
    }

    public String getSitename() {
        return sitename.toString();
    }

    public void setSitename(String sitename) {
        this.sitename = sitename.toString();
    }
}
