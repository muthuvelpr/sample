/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ccm.logic.bean;

/**
 *
 * @author Pitchu
 * @created 30/10/2014 2:17 P.M
 */
public class itemDetails {

    private String artcode, artname;
    private int qoh, pack;
    private double mrp;

    public itemDetails(String artcode, String artname, int qoh, double mrp, int pack) {
        this.artcode = artcode;
        this.artname = artname;
        this.qoh = qoh;
        this.mrp = mrp;
        this.pack = pack;
    }

    public itemDetails(String artcode, int qoh, double mrp) {
        this.artcode = artcode;
        this.qoh = qoh;
        this.mrp = mrp;
    }

    public String getArtcode() {
        return artcode;
    }

    public void setArtcode(String artcode) {
        this.artcode = artcode;
    }

    public String getArtname() {
        return artname;
    }

    public void setArtname(String artname) {
        this.artname = artname;
    }

    public double getMrp() {
        return mrp;
    }

    public void setMrp(double mrp) {
        this.mrp = mrp;
    }

    public int getQoh() {
        return qoh;
    }

    public void setQoh(int qoh) {
        this.qoh = qoh;
    }

    public int getPack() {
        return pack;
    }

    public void setPack(int pack) {
        this.pack = pack;
    }

}
