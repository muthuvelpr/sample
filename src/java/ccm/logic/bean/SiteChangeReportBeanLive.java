/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ccm.logic.bean;

/**
 *
 * @author Administrator
 */
public class SiteChangeReportBeanLive {

    private String ordNo, TPOrdNo, OrdSrc, originSiteID, verifiedBy, verifiedOn, ChangeSiteID, changedby, changedon;

    public SiteChangeReportBeanLive(String ordNo, String TPOrdNo, String OrdSrc, String originSiteID, String verifiedBy, String verifiedOn, String ChangeSiteID, String changedby, String changedon) {
        this.ordNo = ordNo;
        this.TPOrdNo = TPOrdNo;
        this.OrdSrc = OrdSrc;
        this.originSiteID = originSiteID;
        this.verifiedBy = verifiedBy;
        this.verifiedOn = verifiedOn;
        this.ChangeSiteID = ChangeSiteID;
        this.changedby = changedby;
        this.changedon = changedon;
    }

    public String getOrdNo() {
        return ordNo;
    }

    public void setOrdNo(String ordNo) {
        this.ordNo = ordNo;
    }

    public String getTPOrdNo() {
        return TPOrdNo;
    }

    public void setTPOrdNo(String TPOrdNo) {
        this.TPOrdNo = TPOrdNo;
    }

    public String getOrdSrc() {
        return OrdSrc;
    }

    public void setOrdSrc(String OrdSrc) {
        this.OrdSrc = OrdSrc;
    }

    public String getOriginSiteID() {
        return originSiteID;
    }

    public void setOriginSiteID(String originSiteID) {
        this.originSiteID = originSiteID;
    }

    public String getVerifiedBy() {
        return verifiedBy;
    }

    public void setVerifiedBy(String verifiedBy) {
        this.verifiedBy = verifiedBy;
    }

    public String getVerifiedOn() {
        return verifiedOn;
    }

    public void setVerifiedOn(String verifiedOn) {
        this.verifiedOn = verifiedOn;
    }

    public String getChangeSiteID() {
        return ChangeSiteID;
    }

    public void setChangeSiteID(String ChangeSiteID) {
        this.ChangeSiteID = ChangeSiteID;
    }

    public String getChangedby() {
        return changedby;
    }

    public void setChangedby(String changedby) {
        this.changedby = changedby;
    }

    public String getChangedon() {
        return changedon;
    }

    public void setChangedon(String changedon) {
        this.changedon = changedon;
    }

}
