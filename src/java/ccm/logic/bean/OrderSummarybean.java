/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ccm.logic.bean;

/**
 *
 * @author Lenovo
 */
public class OrderSummarybean {

    private String region, ordSource, count, status, statusval, orderType;

    public OrderSummarybean(String ordSource, String orderType, String status, String count) {
        this.ordSource = ordSource;
        this.count = count;
        this.status = status;
        this.orderType = orderType;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getOrdSource() {
        return ordSource;
    }

    public void setOrdSource(String ordSource) {
        this.ordSource = ordSource;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusval() {
        return statusval;
    }

    public void setStatusval(String statusval) {
        this.statusval = statusval;
    }

    public OrderSummarybean(String region, String ordSource, String count, String status, String statusval) {
        this.region = region;
        this.ordSource = ordSource;
        this.count = count;
        this.status = status;
        this.statusval = statusval;
    }

}
