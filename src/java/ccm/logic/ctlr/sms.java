/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ccm.logic.ctlr;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import static java.lang.System.out;
import java.net.HttpURLConnection;
import static java.net.HttpURLConnection.HTTP_OK;
import java.net.URL;

/**
 *
 * @author Pitchu
 * @created Aug 22, 2016 3:58:20 PM
 */
public class sms {

    public void sendSMSOrderWelcome(String _mobileNo, String _name, String _orderNo, String _date) {
        String smsString = "Thanks for choosing Apollo .We have received your order. Your Order No. is " + _orderNo + ". For any queries call 18605000101.";
        smsCustomer(_mobileNo, smsString);
    }

    public void sendSMSOrderCreation(String _mobileNo, String _orderNo) {
        String smsString = "Thanks for choosing Apollo. Your Order No. is " + _orderNo + " has been initiated and will be processed shortly. For any queries call 18605000101.";
        smsCustomer(_mobileNo, smsString);
    }

    public void sendSMSOrderCreationtoManager(String _alertmobileNo, String _orderNo, String _shopid, String _siteName, String _ordDate) {
        String smsString = "'New order - " + _orderNo + ", " + _shopid + ", " + _siteName + ", " + _ordDate;
        smsCustomer(_alertmobileNo, smsString);
    }

    public void sendSMSOrderDeliveryInitimation(String _mobileNo, String _name, String _orderNo, String _date) {
        String smsString = "Your Order No. " + _orderNo + " has been processed and will be delivered shortly.";
        smsCustomer(_mobileNo, smsString);
    }

    public void sendSMSOrderDelivered(String _mobileNo, String _name, String _orderNo, String _date) {
        String smsString = "Thanks for choosing Apollo. Your Order No. " + _orderNo + " has been delivered.";
        smsCustomer(_mobileNo, smsString);
    }
    //Your order no<1234> has been cancelled due to <Remarks>. For further details pls call 18605000101.Looking forward to serve you next time

    public void sendSMSOrderCancel(String _mobileNo, String _orderNo, String _remarks) {
        String smsString = "Your order no " + _orderNo + " has been cancelled due to " + _remarks + ". For further details pls call 18605000101.Looking forward to serve you next time";
        smsCustomer(_mobileNo, smsString);
    }

    public void sendDelaySMS(String _mobileNo, String _orderNo) {
        String smsString = "Dear Customer, We Apologize There Is Some Delay in Order procurement With Order Id (" + _orderNo + ") We Would like to Inform You That the Order Fulfilment Will Take Some More Time than Expected. For Any Queries Please Contact On 1860-500-0101.";
        smsCustomer(_mobileNo, smsString);
    }

    public void sendOnHoldSMS(String _mobileNo, String _orderNo) {
        String smsString = "Dear Customer, Your Order: (" + _orderNo + "), is on Hold as There is No Response from Your End. Will Resume Order Process After Receiving Confirmation From Your End. For Any Further Queries Please Contact 1860-500-0101 THANK You For Choosing Apollo Pharmacy...!";
        smsCustomer(_mobileNo, smsString);
    }

    public void sendDigitalizedSMS(String _mobileNo, String _orderNo) {
        String smsString = "Order No.(" + _orderNo + ") has been digitized and has been sent to MD India Reviewer team. Soon you will receive validation notification on the MD India portal.";
        smsCustomer(_mobileNo, smsString);
    }

    public void sendSMSMDIndiaOrderCancel(String _mobileNo, String _orderNo, String _remarks) {
        String smsString = "Your order no " + _orderNo + " has been cancelled due to " + _remarks + ". For further details pls call 18605000101.Looking forward to serve you next time";
        smsCustomer(_mobileNo, smsString);
    }

    public void smsCustomer(String mob, String pc) {
        String mysms = pc.replace(" ", "%20");
        try {
            final String USER_AGENT = "Mozilla/5.0";
            // final String GET_URL = "http://api.smscountry.com/SMSCwebservice_bulk.aspx?"                    + "User=apollopharmacy1&"                   + "passwd=openingsms&"                    + "mobilenumber=91" + mob + "&"                    + "message=" + mysms + "&"                   + "sid=APOLLO&"                    + "mtype=N&"                    + "DR=Y";
            final String GET_URL = "http://api-alerts.solutionsinfini.com/v3/?method=sms&api_key=A2c9c7ceeff799bbbf78fd1020051fa79&to=" + mob + "&sender=APOLLO&message=" + mysms;
            URL obj = new URL(GET_URL);
            HttpURLConnection smscon = (HttpURLConnection) obj.openConnection();
            smscon.setRequestMethod("GET");
            smscon.setRequestProperty("User-Agent", USER_AGENT);
            int responseCode = smscon.getResponseCode();
            out.println("GET Response Code :: " + responseCode);
            if (responseCode == HTTP_OK) { // success
                BufferedReader in = new BufferedReader(new InputStreamReader(
                        smscon.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
                out.println(response.toString());
            } else {
                out.println("GET request not worked");
            }
        } catch (IOException ex) {
            out.println(ex.getLocalizedMessage());
        }
    }
    //
}
