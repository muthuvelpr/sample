/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ccm.logic.ctlr;

import java.io.InputStream;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import static javax.faces.context.FacesContext.getCurrentInstance;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

/**
 *
 * @author Pitchu
 * @created Aug 22, 2016 3:11:55 PM
 */
@ManagedBean
public class FileDownloadView {

    private StreamedContent file;

    public void FileDownload() {
        retriveImage pimage = new retriveImage();
        //pimage.doGet(null, null)/digiPresc?#{bappLander.selectedrow.ordNo}
        InputStream stream = getCurrentInstance().getExternalContext().getResourceAsStream("/resources/demo/images/optimus.jpg");
        file = new DefaultStreamedContent(stream, "image/jpg", "");
    }

    public StreamedContent getFile() {
        return file;
    }
}
