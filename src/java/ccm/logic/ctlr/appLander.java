/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ccm.logic.ctlr;

import static ccm.logic.bean.Util.getSession;
import ccm.logic.model.loginAuthentication;
import static ccm.logic.model.loginAuthentication.changeLogoutStatus;
import static ccm.logic.model.loginAuthentication.loginAuth;
import javax.faces.application.FacesMessage;
import static javax.faces.application.FacesMessage.SEVERITY_WARN;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import static javax.faces.context.FacesContext.getCurrentInstance;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Pitchu
 */
@ManagedBean(name = "al")
@SessionScoped
public class appLander {

    private String un;
    private String pw;
    private String username;
    private String newPassword;

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public void welcomeListener() {
        getCurrentInstance().addMessage(null,
                new FacesMessage(SEVERITY_WARN, "Welcome Back",
                        "Continue your works."));

    }

    public void logoutListener() {
        getCurrentInstance().addMessage(null,
                new FacesMessage(SEVERITY_WARN,
                        "You Have Logged Out!",
                        "Thank you for using Command Centre Module"));

        // invalidate session, and redirect to other pages
    }

    public void onIdle() {
        getCurrentInstance().addMessage(null, new FacesMessage(SEVERITY_WARN,
                "No activity.", "Are you there?"));
    }

    public void onActive() {
        getCurrentInstance().addMessage(null, new FacesMessage(SEVERITY_WARN,
                "Welcome Back", "Well, that's a long break!"));
    }

    public String getUsername() {
        HttpSession appSession = (HttpSession) getCurrentInstance().getExternalContext().getSession(false);
        username = appSession.getAttribute("username").toString();
        return username;
    }

    public String getPw() {
        return pw;
    }

    public void setPw(String pw) {
        this.pw = pw;
    }

    public String getUn() {
        return un;
    }

    public void setUn(String un) {
        this.un = un;
    }

    public String loginApp() {
        boolean loginValidated = loginAuth(un, pw);
        if (loginValidated) {
//            boolean status = changeLoginStatus(un, pw);
            boolean status = true;
            HttpSession appSession = (HttpSession) getCurrentInstance().getExternalContext().getSession(false);
            String usr_group = appSession.getAttribute("usr_group").toString();
            if (usr_group.equals("0") && status) {
                return "Dashboard?faces-redirect=true";
            } else if (usr_group.equals("1") && status) {
                return "TPOrders?faces-redirect=true";
            } else if (usr_group.equals("2") && status) {
                return "bLander?faces-redirect=true";
            } else if (usr_group.equals("3") && status) {
                return "rhLander?faces-redirect=true";
            } else {
                appSession.invalidate();
                loginAuthentication a = new loginAuthentication();
//                getCurrentInstance().addMessage(null, new FacesMessage(SEVERITY_WARN, "You have logged in Already!!!", "You have logged in Already!!!"));
//                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "You have logged in Already!!!"));
                return "login?faces-redirect=true";
            }
        } else {
//            getCurrentInstance().addMessage(null, new FacesMessage(SEVERITY_WARN, "Invalid Username and/or Password!!!", "Invalid Username and/or Password!!!"));
            return "Dashboard?faces-redirect=true";
        }
    }

    public String Blogin(String un, String pw) {
        boolean loginValidated = loginAuth(un, pw);
        return "bLander?faces-redirect=true";
    }

    public String passwordCheck() {
        if (newPassword.contains("@")) {
            getCurrentInstance().addMessage(null, new FacesMessage(SEVERITY_WARN, "Security is ok in " + newPassword, ""));
        } else {
            getCurrentInstance().addMessage(null, new FacesMessage(SEVERITY_WARN, "Password Security not met in " + newPassword, ""));
        }
        return "";
    }

    public String logout() {
        changeLogoutStatus();
        HttpSession session = getSession();
        session.invalidate();
        return "login?faces-redirect=true";
    }
}
