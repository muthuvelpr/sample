/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ccm.logic.ctlr;

import static ccm.dao.bean.connectivity.getLDBConnection;
import java.awt.image.BufferedImage;
import java.io.File;
import static java.io.File.separator;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import static javax.imageio.ImageIO.read;
import static javax.imageio.ImageIO.write;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Administrator
 */
public class retriveImage extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        processRequest(req, res);
    }

    protected void processRequest(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        res.setContentType("image/jpeg");
        byte[] rawBytes = null;
        ServletOutputStream out = res.getOutputStream();
        Connection con = null;
        String id = req.getQueryString();
        String[] prescdigi;
        String delimiter = "&";
        prescdigi = id.split(delimiter);
        String prescriptionID = prescdigi[0];
        try {
            con = getLDBConnection();
            String qry = "select prescription from cc_uploadedprescriptions where ordno = ?";
            PreparedStatement ps = con.prepareStatement(qry);
            ps.setString(1, prescriptionID);
            out.println(qry);
            ResultSet rs = ps.executeQuery();
            BufferedImage bi;
            if (rs.next()) {
                rawBytes = rs.getBytes(1);
                out.write(rawBytes);
                out.flush();
            } else {
                String pathToWeb = getServletContext().getRealPath(separator);
                File f = new File(pathToWeb + "Not_available.jpg");
                bi = read(f);
                write(bi, "jpg", out);
                out.close();
                out.println("<img src=bi.jpg />");
            }
            //out.write(ImageIO.write(bi, "jpg", out));
        } catch (Exception ex) {
            out.println(ex.getMessage());
        } finally {
            try {
                con.close();
                out.close();
            } catch (Exception ex) {
            }
        }
    }
}
