/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ccm.logic.ctlr;

import ccm.dao.bean.connectivity;
import static ccm.dao.bean.connectivity.close;
import static ccm.dao.bean.connectivity.getHSPConnection;
import static ccm.dao.bean.connectivity.getLDBConnection;
import ccm.logic.bean.Util;
import static ccm.logic.bean.Util.getSession;
import ccm.logic.model.customerAvailability;
import static ccm.logic.model.customerAvailability.checkUHIDinHSPDB;
import static ccm.logic.model.customerAvailability.checkcccinHYDDB;
import static ccm.logic.model.customerAvailability.checkcccustomerinLocalDB;
import static ccm.logic.model.customerAvailability.checkcustomerinLocalDB;
import static ccm.logic.model.customerAvailability.checkuhidcustomerinLocalDB;
import ccm.logic.bean.customerInfo;
import ccm.logic.bean.prevHistory;
import java.io.Serializable;
import static java.lang.Double.parseDouble;
import static java.lang.Integer.parseInt;
import static java.lang.System.out;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.faces.application.FacesMessage;
import static javax.faces.application.FacesMessage.SEVERITY_INFO;
import static javax.faces.application.FacesMessage.SEVERITY_WARN;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import static javax.faces.context.FacesContext.getCurrentInstance;
import javax.servlet.http.HttpSession;
import org.primefaces.event.CellEditEvent;
import org.primefaces.event.FlowEvent;
import org.primefaces.event.RowEditEvent;

/**
 *
 * @author Pitchu
 * @created 29 Oct, 2014 5:57:50 PM
 *
 */
@ManagedBean(name = "pa")
@SessionScoped
public class patient implements Serializable {

    private long mobNo;
    private long ccn = 0;
    private String uhid = "0";
    private String mailID, commAddr, patID;
    private String url;

    public String getPatID() {
        return patID;
    }

    public void setPatID(String patID) {
        this.patID = patID;
    }

    public void patient() {
        setAddr1("");
        setAddr2("");
        setAge(0);
        setCcn(0);
        setCity("");
        setCommAddr("");
        setFirstName("");
        setGender("");
        setLastName("");
        setLat(0.0);
        setLon(0.0);
        setMailID("");
        setPatID("");
        setRegisteredDT(new java.util.Date());
        setUhid("");
        setUrl("");
        setZip(0);
        setSkip(false);
        //setMobNo(0);
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCommAddr() {
        return commAddr;
    }

    public void setCommAddr(String commAddr) {
        this.commAddr = commAddr;
    }

    public String getMailID() {
        return mailID;
    }

    public void setMailID(String mailID) {
        this.mailID = mailID;
    }

    public String getUhid() {
        return uhid;
    }

    public void setUhid(String uhid) {
        this.uhid = uhid.toUpperCase();
    }

    public long getCcn() {
        return ccn;
    }

    public void setCcn(long ccn) {
        this.ccn = ccn;
    }

    public long getMobNo() {
        return mobNo;
    }

    public void setMobNo(long mobNo) {
        this.mobNo = mobNo;
    }
    private String firstName;
    private String lastName = "";
    private String gender;
    private int age;
    private String addr1;
    private String addr2;
    private String city;
    private int zip;
    private Date registeredDT;
    private Double lat;
    private Double lon;

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public String getAddr1() {
        return addr1;
    }

    public void setAddr1(String addr1) {
        this.addr1 = addr1;
    }

    public String getAddr2() {
        return addr2;
    }

    public void setAddr2(String addr2) {
        this.addr2 = addr2;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getRegisteredDT() {
        return registeredDT;
    }

    public void setRegisteredDT(Date registeredDT) {
        this.registeredDT = registeredDT;
    }

    public int getZip() {
        return zip;
    }

    public void setZip(int zip) {
        this.zip = zip;
    }

    public String registerPatient() {
        Connection con = null;
        HttpSession appSession = (HttpSession) getCurrentInstance().getExternalContext().getSession(false);
        int loginid = parseInt(appSession.getAttribute("loginid").toString());
        boolean customeravailable = checkcustomerinLocalDB(mobNo);
        if (customeravailable) {
            getCurrentInstance().addMessage(null, new FacesMessage(SEVERITY_WARN, "This Customer Already Registered with this" + mobNo + "!", ""));
            registredCustomer(mobNo);
            return "orderCreation?faces-redirect=true";
        } else {
            try {
                con = getLDBConnection();
                PreparedStatement ps = con.prepareStatement("insert into CC_Patient_Registration (FirstName,LastName,Gender,Age,MobileNo,Addr1,Addr2,City,Zip,loginid,CommAddr,MailID,Lat1,Lon1,Lat2,Lon2,cccardno,UHID) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
                ps.setString(1, firstName);
                ps.setString(2, lastName);
                ps.setString(3, gender);
                ps.setInt(4, age);
                ps.setLong(5, mobNo);
                ps.setString(6, addr1);
                ps.setString(7, addr2);
                ps.setString(8, "");
                ps.setInt(9, zip);
                ps.setInt(10, loginid);
                ps.setString(11, addr1 + "," + addr2 + "," + zip);
                ps.setString(12, mailID);
                ps.setDouble(13, lat);
                ps.setDouble(14, lon);
                ps.setDouble(15, lat);
                ps.setDouble(16, lon);
                ps.setLong(17, ccn);
                ps.setString(18, uhid);
                ps.executeQuery();
                getCurrentInstance().addMessage(null, new FacesMessage(SEVERITY_INFO, "Registered Successfully", ""));
            } catch (Exception e) {
                out.println(e.getLocalizedMessage());
            } finally {
                close(con);
            }
            registredCustomer(mobNo);
            return "orderCreation?faces-redirect=true";
        }
    }

    public String registredCustomer(Long mobNo) {
        customerInfo(mobNo);
        orderHistory();
        HttpSession session = getSession();
        Double cust_lat = parseDouble(session.getAttribute("Lat").toString());
        Double cust_lon = parseDouble(session.getAttribute("Lon").toString());
        //url = "http://172.16.76.121/Order/NearestPharmacy.aspx?Lat=" + cust_lat + "&Lon=" + cust_lon;
        url = "http://172.16.2.251:89/Order/NearestPharmacy.aspx?Lat=" + cust_lat + "&Lon=" + cust_lon;
        return "orderCreationinitial?faces-redirect=true";
    }

    public String UHIDCustomerDetails() {
        boolean customeravailable = checkuhidcustomerinLocalDB(uhid);
        HttpSession session = getSession();
        session.setAttribute("origin", "U");
        if (customeravailable) {
            Connection con = null;
            try {
                con = getLDBConnection();
                PreparedStatement ps = con.prepareStatement("select MobileNo from CC_Patient_Registration where UHID = ?");
                ps.setString(1, uhid);
                ResultSet rs = ps.executeQuery();
                if (rs.next()) {
                    Long mn = rs.getLong(1);
                    registredCustomer(mn);
                }
            } catch (Exception e) {
                out.println(e.getLocalizedMessage());
            } finally {
                close(con);
            }
            return "orderCreation?faces-redirect=true";
        } else {
            boolean customeravailableinHSP = checkUHIDinHSPDB(uhid);
            if (customeravailableinHSP) {
                Connection con = null;
                try {
                    con = getHSPConnection();
                    Statement stmt = con.createStatement();
                    String sql = "select REGISTRATION_NUMBER as uhid,patientname firstname,'Mr./Mrs.'lastname,gender, age,contact mobileno,address1 addr1,address2 addr2, city,pincode zip from OPENQUERY (EHISCHNSEP,'select P.UHID AS REGISTRATION_NUMBER, (P.FIRSTNAME || '' '' || P.MIDDLENAME || '' '' || P.LASTNAME) AS PATIENTNAME, (SELECT UPPER(GM.GENDERTYPE) FROM EHIS.GENDERMASTER GM WHERE GM.GENDERID = P.GENDER) AS GENDER,        FLOOR(MONTHS_BETWEEN(SYSDATE, P.BIRTHDATE) / 12) AS AGE, (SELECT UPPER(CM.CITYNAME) FROM EHIS.citymaster CM WHERE CM.cityID = adm.city) AS CITY, ADM.address1,ADM.address2,ADM.pincode,locationid, substr(COALESCE(ADM.MOBILENUMBER, ADM.EMERGENCYNUMBER, ADM.CONTACTNUMBER1),4,10) CONTACT,UPPER(ADM.PRIMARYEMAIL) AS EMAIL, (select max(TO_CHAR(IM.DATEOFADMISSION,''YYYY-MM-DD HH:MM:SS'')) from adt.inpatientmaster im where im.uhid=p.uhid) AS DATEOFADMISSION, (select max(TO_CHAR(IM.DISCHARGEDATE,''YYYY-MM-DD HH:MM:SS'')) from adt.inpatientmaster im where im.uhid=p.uhid) AS DISCHARGEDATE, (select max(IM.INPATIENTNO) from adt.inpatientmaster im where im.uhid=p.uhid) AS INPATIENTNO,case when(select max(IM.INPATIENTNO) from adt.inpatientmaster im where im.uhid=p.uhid) is null then ''OP'' else ''IP'' end Type, TO_CHAR(p.LOCATIONID) REGSLOCATION,(SELECT UPPER(ED.LEVELDETAILNAME) FROM EHIS.COA_STRUCT_DETAILS ED WHERE ED.CHARTID = P.LOCATIONID) AS REGISTERPLACE from REGISTRATION.PATIENT P inner join REGISTRATION.ADDRESSMASTER adm on ADM.REGISTRATIONID = P.REGISTRATIONID AND ADM.ADDRESSTYPEID = 2 and  P.UHID in(''" + uhid + "'')')";
                    out.println(sql);
                    ResultSet rs = stmt.executeQuery(sql);
                    if (rs.next()) {
                        setUhid(rs.getString(1).toUpperCase());
                        setFirstName(rs.getString(2).toUpperCase());
                        String gend = "";
                        if (rs.getString(4).equals("MALE")) {
                            gend = "M";
                        } else {
                            gend = "F";
                        }
                        setGender(gend);
                        setAge(rs.getInt(5));
                        setMobNo(rs.getLong(6));
                        setZip(rs.getInt(10));
                        setCcn(0);
                        setAddr1("");
                        setAddr2("");
                        setCommAddr("");
                    }
                } catch (Exception e) {
                    out.println(e.getLocalizedMessage());
                } finally {
                    close(con);
                }

                return "ExistingCustReg?faces-redirect=true";
            } else {
                patient();
                return "patientRegistration?faces-redirect=true";
            }
        }
    }

    public String CCCustomerDetails() {
        boolean customeravailable = checkcccustomerinLocalDB(ccn);
        HttpSession session = getSession();
        session.setAttribute("origin", "C");

        if (customeravailable) {
            Connection con = null;
            try {
                con = getLDBConnection();
                PreparedStatement ps = con.prepareStatement("select MobileNo from CC_Patient_Registration where cccardno = ?");
                ps.setLong(1, ccn);
                ResultSet rs = ps.executeQuery();
                if (rs.next()) {
                    Long mn = rs.getLong(1);
                    registredCustomer(mn);
                }
            } catch (Exception e) {
                out.println(e.getLocalizedMessage());
            } finally {
                close(con);
            }
            return "orderCreation?faces-redirect=true";
        } else {
            boolean customeravailableinHYD = checkcccinHYDDB(ccn);
            if (customeravailableinHYD) {
                Connection con = null;
                try {
                    con = getLDBConnection();
                    //PreparedStatement ps = con.prepareStatement("select CCCARDNO from CC_Patient_Registration where CCCARDNO = ?");
                    //PreparedStatement ps = con.prepareStatement("select cust_name firstname,cust_title lastname,sex gender,age,phone1 mobileno,addr1,addr2,city,zipcode zip,getdate() as registereddt,'2' loginid,cust_no cccardno,''uhid from openrowset('SQLOLEDB','SERVER=172.16.200.100;UID=sa;Pwd=hyd','select * from atmchron..chronic_reg  where cust_no =''"++"'') where phone1 in(select phone1 mobileno from openrowset('SQLOLEDB','SERVER=172.16.200.100;UID=sa;Pwd=hyd','select * from atmchron..chronic_reg  where cust_no =''"++"'')except select mobileno from dbo.CC_Patient_Registration)");
                    Statement stmt = con.createStatement();
                    String sql = "select cust_name firstname,cust_title lastname,sex gender,age,phone1 mobileno,addr1,addr2,city,zipcode zip,getdate() "
                            + "as registereddt,2 loginid,cust_no cccardno,'''' uhid from openrowset('SQLOLEDB','SERVER=172.16.200.100;UID=sa;Pwd=hyd',"
                            + "'select * from atmchron..chronic_reg  where cust_no =''" + ccn + "''') where phone1 in(select phone1 mobileno from "
                            + "openrowset('SQLOLEDB','SERVER=172.16.200.100;UID=sa;Pwd=hyd','select * from atmchron..chronic_reg  where cust_no =''" + ccn + "'''))";
                    out.println(sql);
                    ResultSet rs = stmt.executeQuery(sql);
                    if (rs.next()) {
                        setFirstName(rs.getString(1).toUpperCase());
                        setLastName(rs.getString(2).toUpperCase());
                        String gend = "";
                        if (rs.getString(3).equals("0")) {
                            gend = "M";
                        } else {
                            gend = "F";
                        }
                        setGender(gend);
                        setAge(rs.getInt(4));
                        setMobNo(rs.getLong(5));
                        setCcn(rs.getLong(12));
                        setUhid("");
                        setAddr1("");
                        setAddr2("");
                        setZip(0);
                    }
                } catch (Exception e) {
                    out.println(e.getLocalizedMessage());
                } finally {
                    close(con);
                }
                return "ExistingCustReg?faces-redirect=true";
            } else {
                patient();
                return "patientRegistration?faces-redirect=true";
            }
        }
    }
    String actionFor;

    public String getActionFor() {
        return actionFor;
    }

    public void setActionFor(String actionFor) {
        this.actionFor = actionFor;
    }

    public String CustomerDetails() {
        try {

            boolean customeravailable = checkcustomerinLocalDB(mobNo);
            if (customeravailable) {
                FacesContext fc = getCurrentInstance();
                Map<String, String> params = fc.getExternalContext().getRequestParameterMap();
                actionFor = params.get("actionFor");
                HttpSession session = getSession();
                session.setAttribute("origin", "P");
                registredCustomer(mobNo);
                if (actionFor.equalsIgnoreCase("callLogCustomerInfo")) {
                    return "callLog?faces-redirect=true";
                } else {
                    return "orderCreation?faces-redirect=true";
                }
            } else {
                patient();
                return "patientRegistration?faces-redirect=true";
            }
        } catch (Exception e) {
            System.out.println("Page redirection : " + e.getLocalizedMessage());
        }
        return null;
    }

    public void updateAM() {
        FacesContext fc = getCurrentInstance();
        Map<String, String> params = fc.getExternalContext().getRequestParameterMap();
        patID = params.get("patID").toString();
        mailID = params.get("mailID").toString();
    }

    public void updateCommDetail() {
        HttpSession session = getSession();
        FacesContext fc = getCurrentInstance();
        Map<String, String> params = fc.getExternalContext().getRequestParameterMap();
        patID = params.get("patID").toString();
        Connection con = null;
        try {
            con = getLDBConnection();
            PreparedStatement ps = con.prepareStatement("update cc_patient_registration set commAddr = ?, mailid = ?, lat2 = ?, lon2 = ? where patientid = ?");
            ps.setString(1, commAddr);
            ps.setString(2, mailID);
            ps.setDouble(3, lat);
            ps.setDouble(4, lon);
            ps.setString(5, patID);
            ps.execute();
            session.setAttribute("Lat", lat);
            session.setAttribute("Lon", lon);
        } catch (Exception e) {
            out.println(e.getLocalizedMessage());
        }
        registredCustomer(mobNo);
        //return "orderCreation?faces-redirect=true";
    }
    List<customerInfo> custInfo = new ArrayList<customerInfo>();
    List<customerInfo> cccustIncustInfo = new ArrayList<customerInfo>();

    public List customerInfo(Long mobNo) {
        HttpSession patientSession = getSession();
        Connection con = null;
        try {
            custInfo.clear();
            con = getLDBConnection();
            PreparedStatement ps = con.prepareStatement("select PatientID,FirstName,Gender,Age, Addr1,City,Zip,CommAddr,MailID,LastName,Lat2,Lon2,cccardno,uhid from CC_Patient_Registration where Mobileno = ?");
            ps.setLong(1, mobNo);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                String pid = rs.getString(1);
                String fn = rs.getString(2).toUpperCase() + " " + rs.getString(10).toUpperCase();
                String custgender = rs.getString(3);
                if (custgender.equals("M")) {
                    custgender = "Male";
                } else if (custgender.equals("F")) {
                    custgender = "Female";
                }
                int custage = rs.getInt(4);
                String custcity = rs.getString(6);
                int custzip = rs.getInt(7);
                String address = rs.getString(5) + "," + custcity + "," + custzip;
                String commAdd = rs.getString(8);
                String mailid = rs.getString(9);
                String cccardno = rs.getString(13);
                String custuhid = rs.getString(14);
                patientSession.setAttribute("pid", pid);
                patientSession.setAttribute("commAddr", commAdd);
                patientSession.setAttribute("mailID", mailid);
                patientSession.setAttribute("Lat", rs.getDouble(11));
                patientSession.setAttribute("Lon", rs.getDouble(12));
                custInfo.add(new customerInfo(pid, fn, custgender, custage, address, commAdd, mailid, cccardno, custuhid));
            }
        } catch (Exception e) {
            System.out.println("Error at customer Details : " + e.getLocalizedMessage());
        } finally {
            close(con);
        }
        return custInfo;
    }

    public List<customerInfo> getCustInfo() {
        return custInfo;
    }

    public List cccustomerInfo() {
        HttpSession patientSession = getSession();
        Connection con = null;
        try {
            custInfo.clear();
            con = getLDBConnection();
            PreparedStatement ps = con.prepareStatement("select PatientID,FirstName,Gender,Age, Addr1,City,Zip,LastName,lat2,lon2,cccardno,uhid,Addr2 from CC_Patient_Registration where CCCARDNO = ?");
            ps.setLong(1, ccn);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                String pid = rs.getString(1);
                String fn = rs.getString(2);
                String custgender = rs.getString(3);
                int custage = rs.getInt(4);

                String cit = rs.getString(6);
                String custcity = rs.getString(13);
                if (cit.equals("null") || cit.equals("")) {
                    custcity = "";
                } else {
                    custcity = cit;
                }
                int custzip = rs.getInt(7);
                String address = rs.getString(5) + "," + custcity + "," + custzip;
                String custcommAddr = address;
                String custmailID = "";
                patientSession.setAttribute("pid", pid);
                patientSession.setAttribute("Lat2", rs.getDouble(9));
                patientSession.setAttribute("Lon2", rs.getDouble(10));
                String cccardno = rs.getString(11);
                String custuhid = rs.getString(12);
                custInfo.add(new customerInfo(pid, fn, custgender, custage, address, custcommAddr, custmailID, cccardno, custuhid));
            }
        } catch (Exception e) {
            out.println(e.getLocalizedMessage());
        } finally {
            close(con);
        }
        return custInfo;
    }
    private boolean skip;

    public boolean isSkip() {
        return skip;
    }

    public void setSkip(boolean skip) {
        this.skip = skip;
    }

    public String onFlowProcess(FlowEvent event) {
        if (skip) {
            skip = false;
            return "confirm";
        } else {
            return event.getNewStep();
        }
    }
    List<prevHistory> prevHist = new ArrayList<prevHistory>();
    private prevHistory selectedRec;

    public prevHistory getSelectedRec() {
        return selectedRec;
    }

    public void setSelectedRec(prevHistory selectedRec) {
        this.selectedRec = selectedRec;
    }

    public List orderHistory() {
        Connection con = null;
        HttpSession appSession = getSession();
        String pat = appSession.getAttribute("pid").toString();
        prevHist.clear();
        try {
            con = getLDBConnection();
            PreparedStatement ps = con.prepareStatement("select patid,o.ordno,o.siteid,(select sitename from CC_Apollo_Site where siteid=o.siteid) as sitename,o.orddate,o.status from cc_ordhead o where patid = ? order by orddate desc");
            ps.setString(1, pat);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                out.println(rs.getString(5).substring(0, 16));
                String ordNo = rs.getString(2);
                String siteID = rs.getString(3);
                String siteName = rs.getString(4);
                String ordDate = rs.getString(5).substring(0, 16);
                String status = rs.getString(6);
                if (status.equals("0")) {
                    status = "Pending Order";
                } else if (status.equals("1") || status.equals("2")) {
                    status = "Processing Order";
                } else if (status.equals("3")) {
                    status = "Delivered Order";
                }
                prevHist.add(new prevHistory(ordNo, siteID, siteName, ordDate, status));
            }
        } catch (Exception e) {
            out.println(e.getLocalizedMessage());
        } finally {
        }
        return prevHist;
    }

    public List<prevHistory> getPrevHist() {
        return prevHist;
    }

    public void onRowEditing(RowEditEvent event) {
        Connection con = null;
        HttpSession appSession = getSession();
        String pat = appSession.getAttribute("pid").toString();
        try {
            con = getLDBConnection();
            PreparedStatement ps = con.prepareStatement("update CC_Patient_Registration set commAddr =?, mailid = ? where patientID = ?");
            ps.setString(1, commAddr);
            ps.setString(2, mailID);
            ps.setString(3, pat);
            ps.execute();
        } catch (Exception e) {
            out.println(e.getLocalizedMessage());
        } finally {
            close(con);
        }
        customerInfo(mobNo);
        FacesMessage msg = new FacesMessage("Address Edited for", ((customerInfo) event.getObject()).getFirstName());
        getCurrentInstance().addMessage(null, msg);
    }

    public void onRowCancel(RowEditEvent event) {
        FacesMessage msg = new FacesMessage("Edit Cancelled for", ((customerInfo) event.getObject()).getFirstName());
        getCurrentInstance().addMessage(null, msg);
    }

    public void onCellEdit(CellEditEvent event) {
        Object oldValue = event.getOldValue();
        Object newValue = event.getNewValue();

        if (newValue != null && !newValue.equals(oldValue)) {
            FacesMessage msg = new FacesMessage(SEVERITY_INFO, "Cell Changed", "Old: " + oldValue + ", New:" + newValue);
            getCurrentInstance().addMessage(null, msg);
        }
    }
}
