/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ccm.logic.ctlr;

/**
 *
 * @author PITCHU
 * @created Sep 11, 2016 3:16:01 PM
 */
import java.io.IOException;
import static java.lang.System.out;
import java.net.HttpURLConnection;
import java.net.URL;
import static java.net.URLEncoder.encode;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.xpath.XPath;
import static javax.xml.xpath.XPathConstants.STRING;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;
import static javax.xml.xpath.XPathFactory.newInstance;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.xml.sax.EntityResolver;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class GoogleGeoCode {

    public static void main(String[] args) throws Exception {
        String latLongs[] = getLatLongPositions("600100");
        out.println("Latitude: " + latLongs[0] + " and Longitude: " + latLongs[1]);
    }

    public static String[] getLatLongPositions(String address) throws Exception {
        int responseCode = 0;
        String api = "http://maps.googleapis.com/maps/api/geocode/xml?address=" + encode(address, "UTF-8") + "&sensor=true";
        URL url = new URL(api);
        HttpURLConnection httpConnection = (HttpURLConnection) url.openConnection();
        httpConnection.connect();
        responseCode = httpConnection.getResponseCode();
        if (responseCode == 200) {
            DocumentBuilder builder = new DocumentBuilder() {
                @Override
                public Document parse(InputSource is) throws SAXException, IOException {
                    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                }

                @Override
                public boolean isNamespaceAware() {
                    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                }

                @Override
                public boolean isValidating() {
                    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                }

                @Override
                public void setEntityResolver(EntityResolver er) {
                    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                }

                @Override
                public void setErrorHandler(ErrorHandler eh) {
                    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                }

                @Override
                public Document newDocument() {
                    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                }

                @Override
                public DOMImplementation getDOMImplementation() {
                    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                }
            };

            Document document = builder.parse(httpConnection.getInputStream());
            XPathFactory xPathfactory = newInstance();
            XPath xpath = xPathfactory.newXPath();
            XPathExpression expr = xpath.compile("/GeocodeResponse/status");
            String status = (String) expr.evaluate(document, STRING);
            if (status.equals("OK")) {
                expr = xpath.compile("//geometry/location/lat");
                String latitude = (String) expr.evaluate(document, STRING);
                expr = xpath.compile("//geometry/location/lng");
                String longitude = (String) expr.evaluate(document, STRING);
                return new String[]{latitude, longitude};
            } else {
                throw new Exception("Error from the API - response status: " + status);
            }
        }
        return new String[]{"12.5", "78.12"};
    }
}
