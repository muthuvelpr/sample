/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ccm.logic.ctlr;

import ccm.dao.bean.connectivity;
import static ccm.dao.bean.connectivity.close;
import static ccm.dao.bean.connectivity.getLDBConnection;
import ccm.logic.model.customerAvailability;
import static ccm.logic.model.customerAvailability.checkcustomerinLocalDB;
import ccm.logic.bean.region;
import java.io.Serializable;
import static java.lang.Integer.parseInt;
import static java.lang.Long.parseLong;
import static java.lang.Long.parseLong;
import static java.lang.Long.parseLong;
import static java.lang.Long.parseLong;
import static java.lang.System.out;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.faces.application.FacesMessage;
import static javax.faces.application.FacesMessage.SEVERITY_INFO;
import static javax.faces.application.FacesMessage.SEVERITY_WARN;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import static javax.faces.context.FacesContext.getCurrentInstance;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Pitchu
 * @created 29 Oct, 2014 7:15:01 PM
 */
@ManagedBean(name = "pr")
@SessionScoped
public class patientRegistration implements Serializable {

    private String firstName;
    private String lastName = "";
    private String gender;
    private int age;
    private String addr1;
    private String addr2;
    private String city;
    private int zip;
    private String mailID;
    private Date registeredDT;
    private Double lat;
    private Double lon;
//    List<region> region = new ArrayList<region>();
//
//    public patientRegistration() {
//        showRegion();
//    }
//
//    public List showRegion() {
//
//        Connection con = null;
//        try {
//            con = connectivity.getLDBConnection();
//            PreparedStatement ps = con.prepareStatement("select city from cc_Apollo_site group by city order by city");
//            ResultSet rs = ps.executeQuery();
//            while (rs.next()) {
//                region.add(new region(rs.getString(1).toUpperCase().trim()));
//            }
//        } catch (Exception e) {
//        }
//        return region;
//    }
//
//    public List<region> getRegion() {
//        return region;
//    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public String getMailID() {
        return mailID;
    }

    public void setMailID(String mailID) {
        this.mailID = mailID;
    }

    public String getAddr1() {
        return addr1;
    }

    public void setAddr1(String addr1) {
        this.addr1 = addr1;
    }

    public String getAddr2() {
        return addr2;
    }

    public void setAddr2(String addr2) {
        this.addr2 = addr2;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getRegisteredDT() {
        return registeredDT;
    }

    public void setRegisteredDT(Date registeredDT) {
        this.registeredDT = registeredDT;
    }

    public int getZip() {
        return zip;
    }

    public void setZip(int zip) {
        this.zip = zip;
    }
    String mobNo;

    public String getMobNo() {
        return mobNo;
    }

    public void setMobNo(String mobNo) {
        this.mobNo = mobNo;
    }

    public String registerPatient() {
        Connection con = null;
        HttpSession appSession = (HttpSession) getCurrentInstance().getExternalContext().getSession(false);
        int loginid = parseInt(appSession.getAttribute("loginid").toString());
        boolean customeravailable = checkcustomerinLocalDB(parseLong(mobNo));
        patient custinf = new patient();
        if (customeravailable) {
            getCurrentInstance().addMessage(null, new FacesMessage(SEVERITY_WARN, "This Customer Already Registered with this" + mobNo + "!", ""));
            custinf.registredCustomer(parseLong(mobNo));
            //return "orderCreation?faces-redirect=true";
        } else {
            try {
                con = getLDBConnection();
                PreparedStatement ps = con.prepareStatement("insert into CC_Patient_Registration (FirstName,LastName,Gender,Age,MobileNo,Addr1,Addr2,City,Zip,loginid,CommAddr,MailID,Lat1,Lon1,Lat2,Lon2) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
                ps.setString(1, firstName);
                ps.setString(2, lastName);
                ps.setString(3, gender);
                ps.setInt(4, age);
                ps.setLong(5, parseLong(mobNo));
                ps.setString(6, addr1);
                ps.setString(7, addr2);
                ps.setString(8, "");
                ps.setInt(9, zip);
                ps.setInt(10, loginid);
                ps.setString(11, addr1 + "," + addr2 + "," + zip);
                ps.setString(12, mailID);
                ps.setDouble(13, lat);
                ps.setDouble(14, lon);
                ps.setDouble(15, lat);
                ps.setDouble(16, lon);
                ps.executeQuery();
                getCurrentInstance().addMessage(null, new FacesMessage(SEVERITY_INFO, "Registered Successfully", ""));
            } catch (Exception e) {
                out.println(e.getLocalizedMessage());
            } finally {
                close(con);
            }
            custinf.registredCustomer(parseLong(mobNo));
            //return "orderCreation?faces-redirect=true";
        }
        return "orderCreation?faces-redirect=true";
    }
}
