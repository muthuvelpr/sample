/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ccm.logic.ctlr;

import ccm.dao.bean.connectivity;
import ccm.logic.model.ordSiteChange;
import ccm.logic.bean.siteChangeDataContract;
import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import java.sql.Connection;
import java.sql.PreparedStatement;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Lenovo
 */
public class coreSiteChange {

    public static void main(String args[]) {
        ordSiteChange agent = new ordSiteChange();
        boolean readyforChange = ordSiteChange.checkforSiteChange("3000001095");
        if (readyforChange) {
            //siteChangeDataContract sitechange = createDataReq(ordNo, ordStatus, dSiteid);
            siteChangeDataContract sitechange = new siteChangeDataContract();
            sitechange.setOrdno("3000001095");
            sitechange.setOrderStatus("PHARMACY_CHANGE");
            sitechange.setNewSiteID("16303");
            Gson gson = new Gson();
            String reqData = gson.toJson(sitechange);
            ClientResponse resp = agent.siteChange("3000001095", reqData);
            String ReferenceNumber, status, message;
            try {
                JSONArray jsonArray = new JSONArray();
                int count = jsonArray.length();
                for (int i = 0; i < count; i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    ReferenceNumber = jsonObject.getString("ReferenceNumber");
                    status = jsonObject.getString("OrderStatus");
                    if (status.equals("FAILURE")) {
                        message = jsonObject.getString("message");
                    } else if (status.equals("SUCCESS")) {
                        message = "SUCCESS";
                        try {
                            Connection con = connectivity.getLDBConnection();
                            PreparedStatement ps = con.prepareCall("{call SiteChange(?,?,?,?)}");
                            ps.setString(1, "3000001095");
                            ps.setString(2, "16303");
                            ps.setString(3, "16001");
                            ps.setString(4, "Admin");
                            ps.executeUpdate();
                        } catch (Exception e) {

                        }
                    }
                }
            } catch (JSONException e) {
                System.out.println(e.getLocalizedMessage());
            }
            // return null;
        } else {
            //  return null;
        }
    }
}
