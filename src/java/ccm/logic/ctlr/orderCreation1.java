///*
// * To change this template, choose Tools | Templates
// * and open the template in the editor.
// */
//package ccm.logic.ctlr;
//
//import ccm.dao.bean.connectivity;
//import static ccm.dao.bean.connectivity.close;
//import static ccm.dao.bean.connectivity.getAXConnection;
//import static ccm.dao.bean.connectivity.getLDBConnection;
//import ccm.logic.bean.Util;
//import static ccm.logic.bean.Util.getSession;
//import ccm.logic.bean.validateOrder;
//import static ccm.logic.bean.validateOrder.checkforTPOrderduplicate;
//import ccm.logic.model.item;
//import ccm.logic.model.itemDetails;
//import ccm.logic.model.myCart;
//import ccm.logic.model.ordHistItem;
//import ccm.logic.model.shops;
//import java.io.BufferedReader;
//import java.io.BufferedWriter;
//import java.io.File;
//import static java.io.File.separator;
//import java.io.FileInputStream;
//import java.io.FileNotFoundException;
//import java.io.FileWriter;
//import java.io.IOException;
//import java.io.InputStreamReader;
//import java.io.Serializable;
//import static java.lang.Double.parseDouble;
//import static java.lang.Integer.parseInt;
//import static java.lang.Integer.parseInt;
//import static java.lang.Integer.parseInt;
//import static java.lang.Integer.parseInt;
//import static java.lang.Integer.parseInt;
//import static java.lang.Integer.parseInt;
//import static java.lang.Integer.valueOf;
//import static java.lang.System.arraycopy;
//import static java.lang.System.out;
//import java.net.HttpURLConnection;
//import static java.net.HttpURLConnection.HTTP_OK;
//import java.net.URL;
//import java.sql.Connection;
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
//import java.sql.Statement;
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.Calendar;
//import static java.util.Calendar.DATE;
//import static java.util.Calendar.getInstance;
//import java.util.Date;
//import java.util.List;
//import java.util.Map;
//import java.util.logging.Level;
//import static java.util.logging.Level.SEVERE;
//import java.util.logging.Logger;
//import static java.util.logging.Logger.getLogger;
//import javax.faces.application.FacesMessage;
//import static javax.faces.application.FacesMessage.SEVERITY_INFO;
//import static javax.faces.application.FacesMessage.SEVERITY_WARN;
//import javax.faces.bean.ManagedBean;
//import javax.faces.bean.ViewScoped;
//import javax.faces.context.FacesContext;
//import static javax.faces.context.FacesContext.getCurrentInstance;
//import javax.imageio.stream.FileImageOutputStream;
//import javax.servlet.ServletContext;
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpSession;
//import org.apache.commons.net.ftp.FTPClient;
//import org.primefaces.event.FileUploadEvent;
//import org.primefaces.event.RowEditEvent;
//import org.primefaces.model.DefaultStreamedContent;
//import org.primefaces.model.StreamedContent;
//import org.primefaces.model.UploadedFile;
//
///**
// *
// * @author Pitchu
// * @created 30-10-2014 1:10 A.M.
// *
// */
////@ManagedBean(name = "oc")
//@ViewScoped
//public class orderCreation1 implements Serializable {
//
//    private String artname;
//    private String artcode;
//    private String pushsms;
//    private int reqQTY;
//    private String shopid;
//    private String deliveryMode;
//    private Date delDT = new java.util.Date();
//    private Date maxDelDT;
//    private Date newmaxDelDT;
//    private String paymentMode;
//    List<ordHistItem> ohi = new ArrayList<ordHistItem>();
//    List<shops> shop = new ArrayList<shops>();
//    private static final long serialVersionUID = 1L;
//    UploadedFile uploaded_image;
//    private String othItem;
//    private int othreqQTY;
//    private String remarks;
//
//    public String onIdle() {
//        HttpSession session = getSession();
//        session.invalidate();
//        return "login?faces-redirect=true";
//    }
//
//    public String getPushsms() {
//        return pushsms;
//    }
//
//    public void setPushsms(String pushsms) {
//        this.pushsms = pushsms;
//    }
//
//    public String getRemarks() {
//        return remarks;
//    }
//
//    public void setRemarks(String remarks) {
//        this.remarks = remarks;
//    }
//
//    public String getOthItem() {
//        return othItem;
//    }
//
//    public void setOthItem(String othItem) {
//        this.othItem = othItem;
//    }
//
//    public int getOthreqQTY() {
//        return othreqQTY;
//    }
//
//    public void setOthreqQTY(int othreqQTY) {
//        this.othreqQTY = othreqQTY;
//    }
//
//    public UploadedFile getUploaded_image() {
//        return uploaded_image;
//    }
//
//    public void setUploaded_image(UploadedFile uploaded_image) {
//        this.uploaded_image = uploaded_image;
//    }
//    String upload_location;
//
//    public String getUpload_location() {
//        return upload_location;
//    }
//
//    public void setUpload_location(String upload_location) {
//        this.upload_location = upload_location;
//    }
//    private StreamedContent fileToDisplay;
//
//    public StreamedContent getFileToDisplay() {
//        return fileToDisplay;
//    }
//
//    public void setFileToDisplay(StreamedContent fileToDisplay) {
//        this.fileToDisplay = fileToDisplay;
//    }
//
//    public void handleFileUpload(FileUploadEvent event) {
//        uploaded_image = event.getFile();
//        ServletContext servletContext = (ServletContext) getCurrentInstance().getExternalContext().getContext();
//        String v_file_ext = uploaded_image.getFileName().split("\\.")[(uploaded_image.getFileName().split("\\.").length) - 1];
//        upload_location = servletContext.getRealPath("") + separator + "temp-images" + separator + "3" + "." + v_file_ext;
//        FileImageOutputStream imageOutput;
//        try {
//            imageOutput = new FileImageOutputStream(new File(upload_location));
//            imageOutput.write(uploaded_image.getContents(), 0, uploaded_image.getContents().length);
//            fileToDisplay = new DefaultStreamedContent(event.getFile().getInputstream(), "image/jpg");
//            imageOutput.close();
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//    }
//    private myCart selectedItem;
//
//    public myCart getSelectedItem() {
//        return selectedItem;
//    }
//
//    public void setSelectedItem(myCart selectedItem) {
//        this.selectedItem = selectedItem;
//    }
//
//    public List<String> complete(String query) {
//        List<String> results = new ArrayList<String>();
//        Connection con = null;
//        try {
//            con = getLDBConnection();
//            PreparedStatement ps = con.prepareStatement("select siteid,name,HS from cc_sun_locationdetails where name like '%" + query + "%' or siteid like '" + query + "%' group by name,siteid,HS order by name, siteid");
//            ResultSet rs = ps.executeQuery();
//            while (rs.next()) {
//                results.add(rs.getString(1) + " " + rs.getString(2).toUpperCase() + " - " + rs.getString(3));
//            }
//        } catch (Exception e) {
//        }
//        return results;
//    }
//
//    public orderCreation1() {
//        Calendar c = getInstance();
//        c.setTime(delDT);
//        c.add(DATE, +1);
//        newmaxDelDT = c.getTime();
//        maxDelDT = newmaxDelDT;
//    }
//
//    public String getPaymentMode() {
//        return paymentMode;
//    }
//
//    public void setPaymentMode(String paymentMode) {
//        this.paymentMode = paymentMode;
//    }
//
//    public Date getMaxDelDT() {
//        return maxDelDT;
//    }
//
//    public void setMaxDelDT(Date maxDelDT) {
//        this.maxDelDT = maxDelDT;
//    }
//
//    public Date getDelDT() {
//        return delDT;
//    }
//
//    public void setDelDT(Date delDT) {
//        this.delDT = delDT;
//    }
//
//    public String getDeliveryMode() {
//        return deliveryMode;
//    }
//
//    public void setDeliveryMode(String deliveryMode) {
//        this.deliveryMode = deliveryMode;
//    }
//
//    public String getShopid() {
//        return shopid;
//    }
//
//    public void setShopid(String shopid) {
//        this.shopid = shopid.substring(0, 5);
//    }
//
//    public int getReqQTY() {
//        return reqQTY;
//    }
//
//    public void setReqQTY(int reqQTY) {
//        this.reqQTY = reqQTY;
//    }
//
//    public String getArtname() {
//        return artname;
//    }
//
//    public void setArtname(String artname) {
//        this.artname = artname;
//    }
//
//    public String getArtcode() {
//        return artcode;
//    }
//
//    public void setArtcode(String artcode) {
//        this.artcode = artcode;
//    }
//    List<item> item = new ArrayList<item>();
//    List<itemDetails> itemdetail = new ArrayList<itemDetails>();
//
//    public List<String> collectMedicineList(String query) {
//        List<String> results = new ArrayList<String>();
//        Connection con = null;
//        try {
//            con = getAXConnection();
//            PreparedStatement ps = con.prepareStatement("select itemid,acxitemdescription from inventtable where acxitemdescription like '" + query + "%'");
//            ResultSet rs = ps.executeQuery();
//            while (rs.next()) {
//                results.add(rs.getString(1) + "-" + rs.getString(2).toUpperCase());
//            }
//        } catch (Exception e) {
//        }
//        return results;
//    }
//
//    public List ordDetail() {
//        Connection con = null;
//        FacesContext fc = getCurrentInstance();
//        Map<String, String> params = fc.getExternalContext().getRequestParameterMap();
//        String ordNo = params.get("ordNo");
//        ohi.clear();
//        try {
//            con = getLDBConnection();
//            PreparedStatement ps = con.prepareStatement("select artcode,artname,reqqoh,mrp  from cc_orddet  where ordno = ?");
//            ps.setString(1, ordNo);
//            ResultSet rs = ps.executeQuery();
//            while (rs.next()) {
//                String h_artcode = rs.getString(1);
//                String h_artname = rs.getString(2);
//                int h_qty = rs.getInt(3);
//                int tem = 0;
//                double mrp = rs.getDouble(4);
//                ohi.add(new ordHistItem(h_artcode, h_artname, h_qty, parseInt(ordNo), mrp));
//                out.println(h_artcode + ", " + h_artname + ", " + h_qty);
//            }
//        } catch (Exception e) {
//            out.println(e.getLocalizedMessage());
//        } finally {
//            close(con);
//        }
//        return ohi;
//    }
//
//    public List<ordHistItem> getOhi() {
//        return ohi;
//    }
//
//    public List collectSelectedMedicineDetails() {
//        Connection con = null;
//        itemdetail.clear();
//        String string = artcode;
//        String[] art = string.split("-");
//        String artcode = art[0]; // 004
//        String artname = art[1];
//        try {
//            con = getLDBConnection();
//            //Statement stmt = con.createStatement();
//            //ResultSet rs = stmt.executeQuery("SELECT pkstrucobj.get_desc (stocinl, 'GB') artname,pkartrac.get_artcexr (pkartul.getcinr (stocinl)) artcode,SUM (goldprod.sitestock.get_cinlstock (site, stocinl)) qty FROM (SELECT DISTINCT site, NAME STORE, stocinl FROM goldprod.stocouch, goldprod.shops WHERE stosite = site AND site = '" + shopid + "' AND stocinl IN (SELECT arvcinv FROM goldprod.artuv WHERE arvcexr = UPPER ('" + artcode.toUpperCase() + "'))) a GROUP BY site, STORE, pkstrucobj.get_desc (stocinl, 'GB'), pkartrac.get_artcexr (pkartul.getcinr (stocinl)) HAVING SUM (goldprod.sitestock.get_cinlstock (site, stocinl)) > 0 ORDER BY site");
//            //PreparedStatement ps = con.prepareStatement("select art_desc artname,art_code artcode,mrp from cc_itemmaster where art_code = ?");
//            PreparedStatement ps = con.prepareStatement("select replace(art_desc,'''','') artname,art_code artcode,mrp from rvk_item1 where art_code = ?");
//            ps.setString(1, artcode);
//            ResultSet rs = ps.executeQuery();
//            if (rs.next()) {
//                String gart = rs.getString(2).toUpperCase();
//                String gname = rs.getString(1).toUpperCase();
//                int gqoh = 5;
//                double mrp = rs.getDouble(3);
//                itemdetail.add(new itemDetails(gart, gname, gqoh, mrp));
//            }
//        } catch (Exception e) {
//            out.println(e.getLocalizedMessage());
//        } finally {
//        }
//        return itemdetail;
//    }
//
//    public List<itemDetails> getItemdetail() {
//        return itemdetail;
//    }
//    List<myCart> cart = new ArrayList<myCart>();
//
//    public List movetoCart() {
//        Connection con = null;
//        FacesContext fc = getCurrentInstance();
//        Map<String, String> params = fc.getExternalContext().getRequestParameterMap();
//        String ordNo = params.get("ordNo");
//        cart.clear();
//        try {
//            con = getLDBConnection();
//            PreparedStatement ps = con.prepareStatement("select artcode,artname,reqqoh,mrp  from cc_orddet  where ordno = ?");
//            ps.setString(1, ordNo);
//            ResultSet rs = ps.executeQuery();
//            while (rs.next()) {
//                String h_artcode = rs.getString(1);
//                String h_artname = rs.getString(2);
//                int h_qty = rs.getInt(3);
//                int tem = 0;
//                double mrp = rs.getDouble(4);
//                cart.add(new myCart(h_artcode, h_artname, tem, h_qty, mrp));
//                out.println(h_artcode + ", " + h_artname + ", " + h_qty);
//            }
//        } catch (Exception e) {
//            out.println(e.getLocalizedMessage());
//        } finally {
//            close(con);
//        }
//        return cart;
//    }
//
//    public void deleteAction(myCart item) {
//        cart.remove(item);
//    }
//
//    public List moveShoptimizeItemDetailtoCart(String _ordID) {
//        Connection con = null;
//        //cart.clear();
//        try {
//            con = getLDBConnection();
//            PreparedStatement ps = con.prepareStatement("select ItemID,ItemName,Qty from ThirdPartyOrderDetails where orderid = ?");
//            ps.setString(1, _ordID);
//            ResultSet rs = ps.executeQuery();
//            while (rs.next()) {
//                String h_artcode = rs.getString(1);
//                String h_artname = rs.getString(2);
//                Double D = rs.getDouble(3);
//                int h_qty = D.intValue();
//                int tem = 0;
//                double mrp = 0.01;
//                cart.add(new myCart(h_artcode, h_artname, tem, h_qty, mrp));
//                out.println(h_artcode + ", " + h_artname + ", " + h_qty);
//            }
//        } catch (Exception e) {
//            out.println(e.getLocalizedMessage());
//        } finally {
//            close(con);
//        }
//        return cart;
//    }
//
//    public List addOtherItemCart() {
//
//        if (othreqQTY <= 0) {
//            getCurrentInstance().addMessage(null, new FacesMessage(SEVERITY_INFO, "Please give QTY!!!", ""));
//        } else {
//            cart.add(new myCart("NEWITEM", othItem.toUpperCase(), 10, othreqQTY, 0));
//            getCurrentInstance().addMessage(null, new FacesMessage(SEVERITY_WARN, othItem + "Added to Order", ""));
//        }
//        return cart;
//    }
//
//    public void addCart() {
//        FacesContext fc = getCurrentInstance();
//        Map<String, String> params = fc.getExternalContext().getRequestParameterMap();
//        String item_artcode = params.get("gartcode");
//        String item_artname = params.get("gartname");
//        int item_qoh = parseInt(params.get("gqoh"));
//        double item_mrp = parseDouble(params.get("gmrp")) * reqQTY;
//        out.println(reqQTY);
//        if (reqQTY <= 0) {
//            getCurrentInstance().addMessage(null, new FacesMessage(SEVERITY_INFO, "Please give QTY!!!", "Please give QTY!!!"));
//        } else {
//            cart.add(new myCart(item_artcode, item_artname, item_qoh, reqQTY, item_mrp));
//            setReqQTY(0);
//            getCurrentInstance().addMessage(null, new FacesMessage(SEVERITY_WARN, item_artcode + "Added to Order", ""));
//        }
//    }
//
//    public List<myCart> getCart() {
//        return cart;
//    }
//    int genOrdNo;
//
//    public int getGenOrdNo() {
//        return genOrdNo;
//    }
//
//    public void setGenOrdNo(int genOrdNo) {
//        this.genOrdNo = genOrdNo;
//    }
//    private boolean _isDialogVisible;
//    private boolean _isCancelDialogVisible;
//
//    public String hideDialog() {
//        setDialogVisible(false);
//        return null;
//    }
//
//    public String hideCancelDialog() {
//        setCancelDialogVisible(false);
//        return null;
//    }
//
//    public String showConfirm() {
//        setDialogVisible(true);
//        setCancelDialogVisible(false);
//        return null;
//    }
//
//    public String showCancelConfirm() {
//        setCancelDialogVisible(true);
//        setDialogVisible(false);
//        return null;
//    }
//
//    public boolean getDialogVisible() {
//        return _isDialogVisible;
//    }
//
//    public boolean getCancelDialogVisible() {
//        return _isCancelDialogVisible;
//    }
//
//    public void setDialogVisible(boolean isDialogVisible) {
//        _isDialogVisible = isDialogVisible;
//    }
//
//    public void setCancelDialogVisible(boolean isCancelDialogVisible) {
//        _isCancelDialogVisible = isCancelDialogVisible;
//    }
//
//    public void doCancel() {
//        try {
//            doShoptimizeOrderFinalize();
//        } catch (ServletException ex) {
//            getLogger(orderCreation1.class.getName()).log(SEVERE, null, ex);
//        } catch (IOException ex) {
//            getLogger(orderCreation1.class.getName()).log(SEVERE, null, ex);
//        }
//    }
//
//    public void doShoptimizeOrderFinalize() throws ServletException, IOException {
//        FacesContext fc = getCurrentInstance();
//        Map<String, String> params = fc.getExternalContext().getRequestParameterMap();
//        String origin = params.get("origin");
//        String patID = params.get("patID");
//        String VendordID = params.get("vendOrdNo");
//        String shipping = params.get("shipping");
//        String payment = params.get("payment");
//        String vendName = params.get("vendName");
//        String cancel = params.get("cancel");
//        HttpSession appSession = (HttpSession) getCurrentInstance().getExternalContext().getSession(false);
//        appSession.setAttribute("origin", origin);
//        appSession.setAttribute("pid", patID);
//        appSession.setAttribute("vendOrdID", VendordID);
//        appSession.setAttribute("shipping", shipping);
//
//        appSession.setAttribute("payment", payment);
//        appSession.setAttribute("vendName", vendName);
//        moveShoptimizeItemDetailtoCart(VendordID);
//
//        doOrderFinalize();
//        String genOrdno = Integer.toString(genOrdNo);
//
//    }
//
//    public void updateStatus(String _VendordID, String _genOrdno) {
//
//        // updateStatus(VendordID, apOrdID);
//        Connection con = null;
//        try {
//            con = getLDBConnection();
//            //PreparedStatement ps = con.prepareStatement("update ThirdPartyOrderDetails set ordseq = ?, filestatus = 1 where orderid = ?");
//            PreparedStatement ps = con.prepareCall("{call UpdateOrderStatus(?, ?)}");
//            ps.setString(1, _genOrdno);
//            ps.setString(2, _VendordID);
//            ps.execute();
//        } catch (Exception e) {
//            out.println(e.getLocalizedMessage());
//        }
//    }
//
//    public void updateAPOrdertoVendor(String _VendordID, String _genOrdno, String _siteID) {
//        try {
//            final String USER_AGENT = "Mozilla/5.0";
//            final String GET_URL = "http://172.16.2.251:84/MediAssist_pos.aspx?MA_Orderid=" + _VendordID + "&AP_orderid=" + _genOrdno + "&siteid=" + _siteID + "&Action=orderupdate";
//            URL obj = new URL(GET_URL);
//            out.println(GET_URL);
//            HttpURLConnection smscon = (HttpURLConnection) obj.openConnection();
//            smscon.setRequestMethod("GET");
//            smscon.setRequestProperty("User-Agent", USER_AGENT);
//            int responseCode = smscon.getResponseCode();
//            out.println("GET Response Code :: " + responseCode);
//            if (responseCode == HTTP_OK) { // success
//                BufferedReader in = new BufferedReader(new InputStreamReader(
//                        smscon.getInputStream()));
//                String inputLine;
//                StringBuilder response = new StringBuilder();
//                while ((inputLine = in.readLine()) != null) {
//                    response.append(inputLine);
//                }
//                in.close();
//                out.println(response.toString());
//            } else {
//                out.println("GET request not worked");
//            }
//        } catch (IOException ex) {
//            out.println(ex.getLocalizedMessage());
//        }
//    }
//
//    public void doOrderFinalize() throws ServletException, IOException {
//        HttpSession appSession = (HttpSession) getCurrentInstance().getExternalContext().getSession(false);
//        String username = appSession.getAttribute("loginid").toString();
//        String pat = appSession.getAttribute("pid").toString();
//        String origin = appSession.getAttribute("origin").toString().trim();
//        String VendordID = "";
//        String shipping = "";
//        String payment = "";
//        String vendName = "";
//        if (origin.equals("V")) {
//            VendordID = appSession.getAttribute("vendOrdID").toString();
//            shipping = appSession.getAttribute("shipping").toString();
//            payment = appSession.getAttribute("payment").toString();
//            deliveryMode = shipping;
//            paymentMode = payment;
//            vendName = appSession.getAttribute("vendName").toString();
//        }
//
//        int loginid = parseInt(username);
//        int patid = parseInt(pat);
//        //  boolean checkPreviousOrder = findPreviousOrder.checkLastOrder(patid);
//        /*if (checkPreviousOrder) {
//            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Previous Order is available for the same customer!!!", ""));
//        } else {*/
//        if (cart.isEmpty()) {
//            getCurrentInstance().addMessage(null, new FacesMessage(SEVERITY_INFO, "Please add Item to cart!!!", ""));
//        } else {
//            boolean noduplicate = checkforTPOrderduplicate(VendordID);
//            if (noduplicate) {
//                Connection con = null;
//                Connection ordcon = null;
//                try {
//                    con = getLDBConnection();
//                    Statement stmt = con.createStatement();
//                    ResultSet ors = stmt.executeQuery("select max(OrdNo)+1 from CC_OrdMaster");
//                    if (ors.next()) {
//                        genOrdNo = ors.getInt(1);
//                        ordcon = getLDBConnection();
//                        PreparedStatement ordps = ordcon.prepareStatement("update CC_OrdMaster set OrdNo = ?");
//                        ordps.setInt(1, genOrdNo);
//                        ordps.execute();
//                        finalizeOrder(genOrdNo, patid, loginid);
//                        if (pushsms.equals("HAPP") || pushsms.equals("PRACTO")) {
//                        } else {
//                            ordInitSMS(genOrdNo);
//                        }
//                        if (origin.equals("V")) {
//                            //ordInitFTPProcess(genOrdNo, patid, origin, VendordID, shipping, payment, vendName);
//                            /* CDX Process
//                            try {
//                                con = connectivity.getCMSDBConnection();
//                                PreparedStatement ps = con.prepareStatement("select server from dcmaster where dccode = (select dc from WEB_LOGIN_MASTER where UserID = ?)");
//                                ps.setString(1, shopid);
//                                ResultSet rs = ps.executeQuery();
//                                while (rs.next()) {
//                                    CDX cdx = new CDX();
//                                    cdx.PushOrdertoCDX(genOrdNo, rs.getString(1).toString());
//                                }
//                            } catch (Exception e) {
//
//                            }*/
//                            String apOrdID = Integer.toString(genOrdNo);
//                            updateStatus(VendordID, apOrdID);
//                            if (pushsms.equals("MediAssist")) {
//                                updateAPOrdertoVendor(VendordID, apOrdID, shopid);
//                            }
//                        
//                                try {
//                                    Connection ordupdcon = getLDBConnection();
//                                    //PreparedStatement ordupdps = ordupdcon.prepareStatement("insert into orderupdate (MAOrderid,siteid,AP_Orderid) values (?,?,?)");
//                                    PreparedStatement ordupdps = con.prepareCall("{call CC_TPOrder(?,?,?,?)}");
//                                    ordupdps.setString(1, apOrdID);
//                                    ordupdps.setString(2, VendordID);
//                                    ordupdps.setString(3, pushsms);
//                                    ordupdps.setString(4, shopid);
//                                    ordupdps.execute();
//                                } catch (Exception e) {
//
//                                }
//                            
//                        } else {
//                            upload(genOrdNo, uploaded_image);
//                            //ordInitFTPProcess(genOrdNo, patid, origin, VendordID, shipping, payment, vendName);
//                            /*CDX Process
//                            try {
//                                con = connectivity.getLDBConnection();
//                                PreparedStatement ps = con.prepareStatement("select server from dcmaster where dccode = (select dc from WEB_LOGIN_MASTER where UserID = ?)");
//                                ps.setString(1, shopid);
//                                ResultSet rs = ps.executeQuery();
//                                while (rs.next()) {
//                                    CDX cdx = new CDX();
//                                    cdx.PushOrdertoCDX(genOrdNo, rs.getString(1).toString());
//                                }
//                            } catch (Exception e) {
//
//                            }*/
//                            String apOrdID = Integer.toString(genOrdNo);
//                        }
//                        ordInitSMS(genOrdNo);
//                    }
//                } catch (Exception e) {
//                    out.println(e.getLocalizedMessage());
//                } finally {
//                    close(con);
//                    close(ordcon);
//                }
//                //FacesContext.getCurrentInstance().getExternalContext().redirect("mail.jsp?ordNo=" + genOrdNo + "&ForThis=SHOP");
//                //FacesContext.getCurrentInstance().getExternalContext().redirect("hoLander.jsf");
//
//                showConfirm();
//            } else {
//
//            }
//        }
//        //}
//    }
//
//    public String goHome() {
//        return "hoLander.jsf?faces-redirect=true";
//    }
//
//    public void finalizeOrder(int OrdNo, int patid, int loginid) {
//        Connection conn = null;
//        int tOrdNo = OrdNo;
//        int tpatid = patid;
//        int tloginid = loginid;
//        try {
//            conn = getLDBConnection();
//            PreparedStatement ps = conn.prepareStatement("insert into cc_ordhead (ordno,patid,siteid,ordDate,loginid,status,DeliveryMode,PaymentMode,Remarks,orderSource) values (?,?,?,getDate(),?,'5',?,?,?,?)");
//            ps.setInt(1, tOrdNo);
//            ps.setInt(2, tpatid);
//            ps.setInt(3, parseInt(shopid));
//            ps.setInt(4, tloginid);
//            ps.setString(5, deliveryMode);
//            ps.setString(6, paymentMode);
//            //ps.setDate(7, (java.sql.Date) delDT);
//            ps.setString(7, remarks);
//            ps.setString(8, pushsms);
//            ps.execute();
//            /*Statement stm = conn.createStatement();
//            ResultSet rs = stm.executeQuery("insert into cc_ordhead values (tOrdNo,tpatid,Integer.parseInt(shopid),tloginid)");*/
//        } catch (Exception e) {
//            e.getLocalizedMessage();
//            out.println(e.getLocalizedMessage());
//        } finally {
//            close(conn);
//        }
//        Connection conne = null;
//        try {
//            PreparedStatement psd = null;
//            conne = getLDBConnection();
//            out.println(cart.size());
//            for (myCart p : cart) {
//                String cartartcode = p.getArtcode();
//                String cartartname = p.getArtname();
//                Double cartmrp = p.getMrp();
//                int cartreqQTY = p.getReqQTY();
//                psd = conne.prepareStatement("insert into cc_orddet values(?,?,?,?,?)");
//                psd.setInt(1, OrdNo);
//                psd.setString(2, cartartcode);
//                psd.setString(3, cartartname);
//                psd.setInt(4, cartreqQTY);
//                psd.setDouble(5, cartmrp);
//                psd.execute();
//            }
//
//            getCurrentInstance().addMessage(null, new FacesMessage(SEVERITY_WARN, "Your order no is " + OrdNo, ""));
//            patid = 0;
//        } catch (Exception e) {
//            out.println(e.getLocalizedMessage());
//            out.println(Arrays.toString(e.getStackTrace()));
//            out.println(e.getMessage());
//        } finally {
//            close(conne);
//        }
//    }
//
//    public void imgUPLOAD() {
//        FacesContext fc = getCurrentInstance();
//        Map<String, String> params = fc.getExternalContext().getRequestParameterMap();
//        int ordNo = parseInt(params.get("ordNo"));
//        upload(ordNo, uploaded_image);
//        goHome();
//    }
//
//    public void upload(int _OrdNo, UploadedFile _uploaded_image) {
//
//        byte[] file = new byte[_uploaded_image.getContents().length];
//        arraycopy(_uploaded_image.getContents(), 0, file, 0, _uploaded_image.getContents().length);
//        out.println(file);
//        try {
//            Connection con = getLDBConnection();
//            PreparedStatement ps = con.prepareStatement("insert into CC_UploadedPrescriptions (ordno,prescription) values(?,?)");
//            ps.setInt(1, _OrdNo);
//            ps.setBytes(2, file);
//            ps.execute();
//
//        } catch (Exception e) {
//            out.println(e.getLocalizedMessage());
//        }
//    }
//
//    /*
//     * Before Manager SMS
//    public void ordInitSMS(int OrdNo) {
//    try {
//    ccm.sms.Medical service = new ccm.sms.Medical();
//    ccm.sms.MedicalSoap port = service.getMedicalSoap12();
//    Connection con = null;
//    try {
//    con = connectivity.getLDBConnection();
//    PreparedStatement ps = con.prepareStatement("select mobileno,firstname+' '+lastname from cc_patient_registration where patientid = (select patid from cc_ordhead where ordno = ?)");
//    ps.setInt(1, OrdNo);
//    ResultSet rs = ps.executeQuery();
//    while (rs.next()) {
//    String MobNo = rs.getString(1);
//    String Name = rs.getString(2);
//    if (rs.getString(1).length() != 10) {
//    System.out.println("Invalid Mobile No");
//    } else {
//    java.lang.String mobileNo = MobNo;
//    java.lang.String name = Name;
//    java.lang.String orderNo = Integer.toString(OrdNo);
//    java.lang.String date = new java.util.Date().toString();
//    sms _sms = new sms();
//    _sms.sendSMSOrderCreation(mobileNo, name, orderNo, date);
//    //System.out.println("Result = " + result);
//    }
//    }
//    } catch (Exception e) {
//    System.out.println(e.getLocalizedMessage());
//    }
//    } catch (Exception ex) {
//    System.out.println(ex.getLocalizedMessage());
//    }
//    }
//     */
//    public void ordInitSMS(int OrdNo) {
//        try {
//            //ccm.sms.Medical service = new ccm.sms.Medical();
//            //ccm.sms.MedicalSoap port = service.getMedicalSoap12();
//            Connection con = null;
//            try {
//                con = getLDBConnection();
//                PreparedStatement ps = con.prepareStatement("select mobileno, firstname+' '+LastName, (select orddate from CC_Ordhead where ordNo = ?),(select Name from CC_SUN_LocationDetails where SiteId = ?),(select alertMobileNo from CClogin where usr = ?) from CC_Patient_Registration where PatientID = (select patid from CC_Ordhead where ordNo = ?)");
//                ps.setInt(1, OrdNo);
//                ps.setString(2, shopid);
//                ps.setString(3, shopid);
//                ps.setInt(4, OrdNo);
//                ResultSet rs = ps.executeQuery();
//                while (rs.next()) {
//                    String MobNo = rs.getString(1);
//                    String Name = rs.getString(2);
//                    String alertMobNo = rs.getString(5);
//                    if (rs.getString(1).length() != 10) {
//                        out.println("Invalid Mobile No");
//                    } else {
//                        java.lang.String mobileNo = MobNo;
//                        java.lang.String name = Name;
//                        java.lang.String orderNo = Integer.toString(OrdNo);
//                        java.lang.String date = new java.util.Date().toString();
//                        sms _sms = new sms();
//                        _sms.sendSMSOrderCreation(mobileNo, name, orderNo, date);
//                        if (alertMobNo.length() != 10) {
//                        } else {
//                            String ordDate = rs.getString(3);
//                            String siteName = rs.getString(4);
//                            _sms.sendSMSOrderCreationtoManager(alertMobNo, orderNo, shopid, siteName, ordDate);
//                        }
//                        //System.out.println("Result = " + result);
//                    }
//                }
//            } catch (Exception e) {
//                out.println(e.getLocalizedMessage());
//            }
//        } catch (Exception ex) {
//            out.println(ex.getLocalizedMessage());
//        }
//    }
//
//    public void ordInitFTPProcess(int OrdNo, int patid, String origin, String _VendordID, String _shipping, String _payment, String _vendName) {
//        FTPClient client = new FTPClient();
//        FileInputStream fis = null;
//        if (_VendordID == "" || _VendordID == null) {
//            _VendordID = "0";
//        } else if (_shipping == "" || _shipping == null) {
//            _shipping = "0";
//        } else if (_payment == "" || _payment == null) {
//            _payment = "";
//        } else if (_vendName == "" || _vendName == null) {
//            _vendName = "";
//        }
//        try {
//            client.connect("172.16.1.16");
//            client.login("goldprod", "G$pr0d@p0110");
//            //client.login("posuser", "p0sUs3r");
//            try {
//                try {
//                    client.changeWorkingDirectory("data/shop/" + shopid);
//                    FileWriter fstream = new FileWriter(origin + OrdNo + "_" + shopid + ".AP");
//                    BufferedWriter outr = new BufferedWriter(fstream);
//                    try {
//                        Connection conn = null;
//                        conn = getLDBConnection();
//                        PreparedStatement ps = conn.prepareStatement("SELECT ORDNO,ORDDATE,FIRSTNAME CUSTNAME,MOBILENO,GENDER,AGE,COMMADDR,siteID,CCCARDNO,UHID FROM CC_ORDHEAD,CC_Patient_Registration WHERE patId=PATIENTID and patID = ? AND ordNo = ?");
//                        ps.setInt(1, patid);
//                        ps.setInt(2, OrdNo);
//                        ResultSet rs0 = ps.executeQuery();
//                        Connection con = getLDBConnection();
//                        PreparedStatement pst = con.prepareStatement("SELECT A.ORDNO,artCode,artName,reqqoh,MRP FROM CC_ORDDET A,CC_ORDHEAD B,CC_Patient_Registration WHERE A.ordNo= ? AND  patId=PATIENTID AND A.ordNo=B.ORDNO");
//                        pst.setInt(1, OrdNo);
//                        out.println("Writing data to the Text File");
////                        System.out.println("<br>");
//                        ResultSet rst = pst.executeQuery();
//                        while (rs0.next()) {
//                            if (origin.equalsIgnoreCase("C")) {
//                                outr.write(origin + rs0.getString(1) + "|"
//                                        + rs0.getString(2) + "|" + rs0.getString(3) + "|"
//                                        + rs0.getString(4) + "|" + rs0.getString(5) + "|"
//                                        + rs0.getString(6) + "|" + rs0.getString(7) + ","
//                                        + "" + "|" + "Apollo" + "|"
//                                        + "" + "|" + "" + "|" + "0" + "|" + "0" + "|" + "0" + "|" + "" + "|" + rs0.getString(8) + "|" + ""
//                                        + "|" + "5" + "|" + "" + "|" + rs0.getString(9)
//                                        + "\n");
//                            } else if (origin.equalsIgnoreCase("U")) {
//                                outr.write(origin + rs0.getString(1) + "|"
//                                        + rs0.getString(2) + "|" + rs0.getString(3) + "|"
//                                        + rs0.getString(4) + "|" + rs0.getString(5) + "|"
//                                        + rs0.getString(6) + "|" + rs0.getString(7) + ","
//                                        + "" + "|" + "Apollo" + "|"
//                                        + "" + "|" + "" + "|" + "0" + "|" + "0" + "|" + "0" + "|" + "" + "|" + rs0.getString(8) + "|" + ""
//                                        + "|" + "5" + "|" + "" + "|" + rs0.getString(10)
//                                        + "\n");
//                            } else if (origin.equalsIgnoreCase("P")) {
//                                outr.write(origin + rs0.getString(1) + "|"
//                                        + rs0.getString(2) + "|" + rs0.getString(3) + "|"
//                                        + rs0.getString(4) + "|" + rs0.getString(5) + "|"
//                                        + rs0.getString(6) + "|" + rs0.getString(7) + ","
//                                        + "" + "|" + "Apollo" + "|"
//                                        + "" + "|" + "" + "|" + "0" + "|" + "0" + "|" + "0" + "|" + "" + "|" + rs0.getString(8) + "|" + ""
//                                        + "|" + "5" + "|" + "" + "|" + "0"
//                                        + "\n");
//                            } else if (origin.equalsIgnoreCase("V")) {
//                                outr.write(origin + rs0.getString(1) + "|"
//                                        + rs0.getString(2) + "|" + rs0.getString(3) + "|"
//                                        + rs0.getString(4) + "|" + rs0.getString(5) + "|"
//                                        + rs0.getString(6) + "|" + rs0.getString(7) + ","
//                                        + "" + "|" + "Apollo" + "|"
//                                        + "" + "|" + "" + "|" + _VendordID + "|" + _vendName + "|" + _payment + "|" + "" + "|" + rs0.getString(8) + "|" + ""
//                                        + "|" + "5" + "|" + "" + "|" + "0"
//                                        + "\n");
//                                /*System.out.println(origin + rs0.getString(1) + "|"
//                                                + rs0.getString(2) + "|" + rs0.getString(3) + "|"
//                                                + rs0.getString(4) + "|" + rs0.getString(5) + "|"
//                                                + rs0.getString(6) + "|" + rs0.getString(7) + ","
//                                                + "" + "|" + "Apollo" + "|"
//                                                + "" + "|" + "" + "|" + "0" + "|" + "0" + "|" + "0" + "|" + "" + "|" + rs0.getString(8) + "|" + ""
//                                                + "|" + "5" + "|" + "" + "|" + "0"
//                                                + "\n");*/
//                            }
//                            /*System.out.println("CP" + rs0.getString(1) + "|"
//                                           + rs0.getString(2) + " " + rs0.getString(3) + "|"
//                                           + rs0.getString(4) + "|" + rs0.getString(5) + "|"
//                                           + rs0.getString(6) + "|" + rs0.getString(7) + ","
//                                           + "" + "|" + "" + "|"
//                                           + "" + "|" + "0" + "|"
//                                           + "0" + "|" + "0" + "|" + "" + "|" + rs0.getString(8) + "|" + ""
//                                           + "|" + "5" + "|" + "" + "|" + "0"
//                                           + "\n");*/
//                        }
//                        int i = 1;
//                        while (rst.next()) {
//                            outr.write(origin + rst.getString(1) + "|" + i + "|"
//                                    + "" + "|" + rst.getString(2) + "|"
//                                    + rst.getString(3) + "|" + rst.getString(4) + "|"
//                                    + "" + "|" + "" + "|"
//                                    + "" + "|"
//                                    + "\n");
//                            out.println("CP" + rst.getString(1) + "|" + i + "|"
//                                    + rst.getString(2) + "|" + rst.getString(2) + "|"
//                                    + rst.getString(3) + "|" + rst.getString(4) + "|"
//                                    + "" + "|" + "" + "|"
//                                    + "" + "|"
//                                    + "\n");
//                            i++;
//                        }
//                        outr.close();
//                    } catch (Exception e) {
//                        out.println("Hi i'm here" + e.getLocalizedMessage());
//                    }
//                    out.println("File Updated with Data");
//                    String filename = origin + OrdNo + "_" + shopid + ".AP";
//                    fis = new FileInputStream(filename);
//                    try {
//                        client.enterLocalPassiveMode();
//                        client.storeFile(filename, fis);
//                    } catch (Exception e) {
//                        out.println("e1" + e.getLocalizedMessage());
//                    }
//                    client.logout();
//                } catch (Exception fe) {
//                    out.println("er" + fe.getLocalizedMessage());
//                    out.println(fe.getStackTrace() + "/n");
//
//                }
//            } catch (Exception e) {
//                out.println("Error: " + e.getLocalizedMessage());
//                out.println(e.getStackTrace());
//            }
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        } finally {
//            try {
//                if (fis != null) {
//                    fis.close();
//                    //System.out.println("<br>");
//                }
//                client.disconnect();
//            } catch (Exception e) {
//                out.println(e.getLocalizedMessage());
//            }
//        }
//        makeTRG(OrdNo, origin);
//    }
//
//    public void makeTRG(int OrdNo, String origin) {
//        FTPClient client = new FTPClient();
//        FileInputStream fis = null;
//        try {
//            client.connect("172.16.1.16");
//            client.login("goldprod", "G$pr0d@p0110");
//            try {
//                try {
//                    client.changeWorkingDirectory("data/shop/" + shopid);
//                    FileWriter fstream = new FileWriter(origin + OrdNo + "_" + shopid + ".TRG");
//                    BufferedWriter outr = new BufferedWriter(fstream);
//                    //System.out.println("File Updated with Data");
//                    String filename = origin + OrdNo + "_" + shopid + ".TRG";
//                    fis = new FileInputStream(filename);
//                    try {
//                        client.enterLocalPassiveMode();
//                        client.storeFile(filename, fis);
//                    } catch (Exception e) {
//                        out.println("e1" + e.getLocalizedMessage());
//                    }
//                    client.logout();
//                } catch (Exception fe) {
//                    out.println("er" + fe.getLocalizedMessage());
//                    out.println(fe.getStackTrace() + "/n");
//
//                }
//            } catch (Exception e) {
//                out.println("Error: " + e.getLocalizedMessage());
//                out.println(e.getStackTrace());
//            }
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        } finally {
//            try {
//                if (fis != null) {
//                    fis.close();
//                    //System.out.println("<br>");
//                }
//                client.disconnect();
//            } catch (Exception e) {
//                out.println(e.getLocalizedMessage());
//            }
//        }
//    }
//
//    public void onRowEditing(RowEditEvent event) {
//
//        FacesMessage msg = new FacesMessage("QTY changed for ", ((myCart) event.getObject()).getArtname());
//        getCurrentInstance().addMessage(null, msg);
//    }
//
//    public void onRowCancel(RowEditEvent event) {
//        FacesMessage msg = new FacesMessage("Edit Cancelled for", ((myCart) event.getObject()).getArtname());
//        getCurrentInstance().addMessage(null, msg);
//    }
//
//    public void deleteItem() {
//        cart.remove(selectedItem);
//        selectedItem = null;
//    }
//
//    /*private static String pushDetailsAX(java.lang.String ordno, java.lang.String dccode) {
//        com.acx.push.AXorderservice service = new com.acx.push.AXorderservice();
//        com.acx.push.AXorderserviceSoap port = service.getAXorderserviceSoap();
//        return port.pushDetailsAX(ordno, dccode);
//    }
//
//    private static String pushDetailsAX_1(java.lang.String ordno, java.lang.String dccode) {
//        com.acx.push.AXorderservice service = new com.acx.push.AXorderservice();
//        com.acx.push.AXorderserviceSoap port = service.getAXorderserviceSoap();
//        return port.pushDetailsAX(ordno, dccode);
//    }*/
//
// /*private static String pushDetailsAX_2(java.lang.String ordno, java.lang.String dccode) {
//        com.acx.push.AXorderservice service = new com.acx.push.AXorderservice();
//        com.acx.push.AXorderserviceSoap port = service.getAXorderserviceSoap12();
//        return port.pushDetailsAX(ordno, dccode);
//    }
//
//    private static String pushDetailsAX_3(java.lang.String ordno, java.lang.String dccode) {
//        com.acx.push.AXorderservice service = new com.acx.push.AXorderservice();
//        com.acx.push.AXorderserviceSoap port = service.getAXorderserviceSoap12();
//        return port.pushDetailsAX(ordno, dccode);
//    }*/
//
// /* private static String pushDetailsAX_4(java.lang.String ordno, java.lang.String dccode) {
//        com.acx.push.AXorderservice service = new com.acx.push.AXorderservice();
//        com.acx.push.AXorderserviceSoap port = service.getAXorderserviceSoap12();
//        return port.pushDetailsAX(ordno, dccode);
//    }
//
//    private static String pushDetailsAX_5(java.lang.String ordno, java.lang.String dccode) {
//        com.acx.push.AXorderservice service = new com.acx.push.AXorderservice();
//        com.acx.push.AXorderserviceSoap port = service.getAXorderserviceSoap12();
//        return port.pushDetailsAX(ordno, dccode);
//    }
//     */
//}
