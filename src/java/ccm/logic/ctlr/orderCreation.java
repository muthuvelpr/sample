/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ccm.logic.ctlr;

import com.logic.order.model.OrderCancellation;
import static ccm.dao.bean.connectivity.close;
import static ccm.dao.bean.connectivity.getAXConnection;
import static ccm.dao.bean.connectivity.getLDBConnection;
import com.logic.order.model.OrderComment;
import static ccm.logic.bean.Util.getSession;
import ccm.logic.bean.UploadedPrescDocs;
import static ccm.logic.model.validateOrder.checkforTPOrderduplicate;
import ccm.logic.bean.CancelRemarks;
import com.logic.order.bean.comments;
import ccm.logic.bean.customerInfo;
import ccm.logic.bean.item;
import ccm.logic.bean.itemDetails;
import ccm.logic.bean.myCart;
import ccm.logic.bean.shops;
import ccm.report.model.invAvailability;
import com.logic.order.bean.TPOrder;
import com.logic.thirdparty.model.shoptimizeOrderDetail;
import com.sun.jersey.api.client.Client;
import static com.sun.jersey.api.client.Client.create;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import static java.lang.Integer.parseInt;
import static java.lang.Long.parseLong;
import static java.lang.System.arraycopy;
import static java.lang.System.err;
import static java.lang.System.out;
import java.net.HttpURLConnection;
import static java.net.HttpURLConnection.HTTP_OK;
import java.net.URL;
import static java.net.URLEncoder.encode;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import static javax.faces.application.FacesMessage.SEVERITY_INFO;
import static javax.faces.application.FacesMessage.SEVERITY_WARN;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import static javax.faces.context.FacesContext.getCurrentInstance;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.xpath.XPath;
import static javax.xml.xpath.XPathConstants.STRING;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;
import static javax.xml.xpath.XPathFactory.newInstance;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.primefaces.event.ResizeEvent;
import org.primefaces.event.RowEditEvent;
import org.primefaces.event.ToggleEvent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.xml.sax.EntityResolver;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author Pitchu
 * @created 30-10-2014 1:10 A.M.
 *
 */
@ManagedBean(name = "oc")
@SessionScoped
public class orderCreation implements Serializable {

    private String artname;
    private String artcode;
    private String vendorName;
    private int reqQTY;
    private String shopid;
    private String deliveryMode;
    private Date delDT = new java.util.Date();
    private Date maxDelDT;
    private String paymentMode;
    int genOrdNo;
    private String cancelRemarksCode;

    private static final long serialVersionUID = 1L;
    UploadedFile uploaded_image;
    private String othItem;
    private int othreqQTY;
    private String remarks;
    private int fmcgdisc, pharmdisc, pldisc;

    //List<ordHistItem> ohi = new ArrayList<>(); // Order History
    List<shops> shop = new ArrayList<>();
    List<invAvailability> invAvail = new ArrayList<invAvailability>();

    List<item> item = new ArrayList<item>();
    List<itemDetails> itemdetail = new ArrayList<>();
    List<myCart> cart = new ArrayList<myCart>();
    List<TPOrder> ordSum;
    TPOrder selectedOrder;
    List<TPOrder> filterOrdSum;
    List<shoptimizeOrderDetail> ordDet = new ArrayList<>();
    List<comments> thisOrderComment = new ArrayList<>();
    List<CancelRemarks> CanRem = new ArrayList<>();

    private myCart selectedItem;

    int testCounter = 0;
    String CommentData = "";

    public String getCancelRemarksCode() {
        return cancelRemarksCode;
    }

    public void setCancelRemarksCode(String cancelRemarksCode) {
        this.cancelRemarksCode = cancelRemarksCode;
    }

    public String getCommentData() {
        return CommentData;
    }

    public void setCommentData(String CommentData) {
        this.CommentData = CommentData;
    }

    public int getTestCounter() {
        return testCounter;
    }

    public void setTestCounter(int testCounter) {
        this.testCounter = testCounter;
    }

    public List<comments> getThisOrderComment() {
        return thisOrderComment;
    }

    public List<TPOrder> getFilterOrdSum() {
        return filterOrdSum;
    }

    public void setFilterOrdSum(List<TPOrder> filterOrdSum) {
        this.filterOrdSum = filterOrdSum;
    }

    public TPOrder getSelectedOrder() {
        return selectedOrder;
    }

    public int getFmcgdisc() {
        return fmcgdisc;
    }

    public void setFmcgdisc(int fmcgdisc) {
        this.fmcgdisc = fmcgdisc;
    }

    public int getPharmdisc() {
        return pharmdisc;
    }

    public void setPharmdisc(int pharmdisc) {
        this.pharmdisc = pharmdisc;
    }

    public int getPldisc() {
        return pldisc;
    }

    public void setPldisc(int pldisc) {
        this.pldisc = pldisc;
    }

    public String onIdle() {
        HttpSession session = getSession();
        session.invalidate();
        return "login?faces-redirect=true";
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getOthItem() {
        return othItem;
    }

    public void setOthItem(String othItem) {
        this.othItem = othItem;
    }

    public int getOthreqQTY() {
        return othreqQTY;
    }

    public void setOthreqQTY(int othreqQTY) {
        this.othreqQTY = othreqQTY;
    }

    public UploadedFile getUploaded_image() {
        return uploaded_image;
    }

    public void setUploaded_image(UploadedFile uploaded_image) {
        this.uploaded_image = uploaded_image;
    }
    String upload_location;

    public String getUpload_location() {
        return upload_location;
    }

    public void setUpload_location(String upload_location) {
        this.upload_location = upload_location;
    }
    private StreamedContent fileToDisplay;

    public StreamedContent getFileToDisplay() {
        return fileToDisplay;
    }

    public void setFileToDisplay(StreamedContent fileToDisplay) {
        this.fileToDisplay = fileToDisplay;
    }

    public List<invAvailability> getInvAvail() {
        return invAvail;
    }

    public void setInvAvail(List<invAvailability> invAvail) {
        this.invAvail = invAvail;
    }

    public myCart getSelectedItem() {
        return selectedItem;
    }

    public void setSelectedItem(myCart selectedItem) {
        this.selectedItem = selectedItem;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public Date getMaxDelDT() {
        return maxDelDT;
    }

    public void setMaxDelDT(Date maxDelDT) {
        this.maxDelDT = maxDelDT;
    }

    public Date getDelDT() {
        return delDT;
    }

    public void setDelDT(Date delDT) {
        this.delDT = delDT;
    }

    public String getDeliveryMode() {
        return deliveryMode;
    }

    public void setDeliveryMode(String deliveryMode) {
        this.deliveryMode = deliveryMode;
    }

    public String getShopid() {
        return shopid;
    }

    public void setShopid(String shopid) {
        this.shopid = shopid.substring(0, 5);
    }

    public int getReqQTY() {
        return reqQTY;
    }

    public void setReqQTY(int reqQTY) {
        this.reqQTY = reqQTY;
    }

    public String getArtname() {
        return artname;
    }

    public void setArtname(String artname) {
        this.artname = artname;
    }

    public String getArtcode() {
        return artcode;
    }

    public void setArtcode(String artcode) {
        this.artcode = artcode;
    }

    public List<itemDetails> getItemdetail() {
        return itemdetail;
    }

    public List<myCart> getCart() {
        return cart;
    }

    public int getGenOrdNo() {
        return genOrdNo;
    }

    public void setGenOrdNo(int genOrdNo) {
        this.genOrdNo = genOrdNo;
    }
    private boolean _isDialogVisible;

    public String hideDialog() {
        setDialogVisible(false);
        return null;
    }

    public boolean getDialogVisible() {
        return _isDialogVisible;
    }

    public void setDialogVisible(boolean isDialogVisible) {
        _isDialogVisible = isDialogVisible;
    }

    public List<UploadedPrescDocs> getUpldprescimg() {
        return upldprescimg;
    }

    //Order History
    /*
    public List<ordHistItem> getOhi() {
        return ohi;
    }
     */
    public orderCreation() {
        this.ordSum = new ArrayList<TPOrder>();
    }

    //Handle Upload Prescription
    /*
    public void handleFileUpload(FileUploadEvent event) {
        uploaded_image = event.getFile();
        ServletContext servletContext = (ServletContext) getCurrentInstance().getExternalContext().getContext();
        String v_file_ext = uploaded_image.getFileName().split("\\.")[(uploaded_image.getFileName().split("\\.").length) - 1];
        upload_location = servletContext.getRealPath("") + separator + "temp-images" + separator + "3" + "." + v_file_ext;
        FileImageOutputStream imageOutput;
        try {
            imageOutput = new FileImageOutputStream(new File(upload_location));
            imageOutput.write(uploaded_image.getContents(), 0, uploaded_image.getContents().length);
            fileToDisplay = new DefaultStreamedContent(event.getFile().getInputstream(), "image/jpg");
            imageOutput.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
     */
    //Select Site ID for Order Push
    public List<String> complete(String query) {
        List<String> results = new ArrayList<>();
        Connection con = null;
        try {
            con = getLDBConnection();
            PreparedStatement ps = con.prepareStatement("select siteid,name,HS from cc_sun_locationdetails where name like '%" + query + "%' or siteid like '" + query + "%' group by name,siteid,HS order by name, siteid");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                results.add(rs.getString(1) + " " + rs.getString(2).toUpperCase() + " - " + rs.getString(3));
            }
        } catch (Exception e) {
        }
        return results;
    }

    //Custom Product Add
    public List<String> collectMedicineList(String query) {
        List<String> results = new ArrayList<>();
        Connection con = null;
        try {
            con = getAXConnection();
            PreparedStatement ps = con.prepareStatement("select itemid,acxitemdescription from inventtable where acxitemdescription like '%" + query + "%'");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                results.add(rs.getString(1) + "-" + rs.getString(2).toUpperCase());
            }
        } catch (SQLException e) {
        }
        return results;
    }

    /*Order History Order Item Details*/
 /*
    public List ordDetail() {
        Connection con = null;
        FacesContext fc = getCurrentInstance();
        Map<String, String> params = fc.getExternalContext().getRequestParameterMap();
        String ordNo = params.get("ordNo");
        ohi.clear();
        try {
            con = getLDBConnection();
            PreparedStatement ps = con.prepareStatement("select artcode,artname,reqqoh,mrp  from cc_orddet  where ordno = ?");
            ps.setString(1, ordNo);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                String h_artcode = rs.getString(1);
                String h_artname = rs.getString(2);
                int h_qty = rs.getInt(3);
                int tem = 0;
                double mrp = rs.getDouble(4);
                ohi.add(new ordHistItem(h_artcode, h_artname, h_qty, parseInt(ordNo), mrp));
                out.println(h_artcode + ", " + h_artname + ", " + h_qty);
            }
        } catch (Exception e) {
            out.println(e.getLocalizedMessage());
        } finally {
            close(con);
        }
        return ohi;
    }
     */
 /*Custom Product Add Item Detail*/
    public List collectSelectedMedicineDetails() {
        Connection con = null;
        itemdetail.clear();
        String string = artcode.trim();
        String[] art = string.split("-");
        String artcode = art[0].trim(); // 004
        String artname = art[1].trim();
        try {
            con = getAXConnection();
            PreparedStatement ps = con.prepareStatement("select itemid,(select acxitemname from inventtable where itemid=a.itemid)itemname,acxmaximumretailprice_in mrp,proddate lastcreated from inventbatch a where proddate in(select max(proddate) from inventbatch where a.itemid=itemid) and itemid in(select itemid from ACXITEMSSITESCONTRACTMODIFICATIONS where partition ='5637144576' and dataareaid='ahel' and itemid = ?)group by itemid,acxmaximumretailprice_in,proddate ORDER BY 1 ASC");
            ps.setString(1, artcode);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                String gart = rs.getString(1).toUpperCase();
                String gname = rs.getString(2).toUpperCase();
                int gqoh = 5;
                double mrp = rs.getDouble(3);
                itemdetail.add(new itemDetails(gart, gname, gqoh, mrp, 0));
            }
        } catch (Exception e) {
            out.println(e.getLocalizedMessage());
        } finally {
        }
        return itemdetail;
    }

    /*Moving item to Cart Object - Seems to be not used*/
 /*
    public List movetoCart() {
        Connection con = null;
        FacesContext fc = getCurrentInstance();
        Map<String, String> params = fc.getExternalContext().getRequestParameterMap();
        String ordNo = params.get("ordNo");
        cart.clear();
        try {
            con = getLDBConnection();
            PreparedStatement ps = con.prepareStatement("select artcode,artname,reqqoh,mrp  from cc_orddet  where ordno = ?");
            ps.setString(1, ordNo);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                String h_artcode = rs.getString(1);
                String h_artname = rs.getString(2);
                int h_qty = rs.getInt(3);
                int tem = 0;
                double mrp = rs.getDouble(4);
                cart.add(new myCart(h_artcode, h_artname, tem, h_qty, mrp));
                out.println(h_artcode + ", " + h_artname + ", " + h_qty);
            }
        } catch (Exception e) {
            out.println(e.getLocalizedMessage());
        } finally {
            close(con);
        }
        return cart;
    }
     */
    //Delete Item from Cart
    public void deleteAction(myCart item) {
        cart.remove(item);
    }

    //Moving Vendor Order to Cart Object - Seems to be not used
    /*
    public List moveShoptimizeItemDetailtoCart(String _ordID) {
        Connection con = null;
        //cart.clear();
        try {
            con = getLDBConnection();
            PreparedStatement ps = con.prepareStatement("select ItemID,ItemName,Qty from ThirdPartyOrderDetails where orderid = ?");
            ps.setString(1, _ordID);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                String h_artcode = rs.getString(1);
                String h_artname = rs.getString(2);
                Double D = rs.getDouble(3);
                int h_qty = D.intValue();
                int tem = 0;
                double mrp = 0.01;
                cart.add(new myCart(h_artcode, h_artname, tem, h_qty, mrp));
                out.println(h_artcode + ", " + h_artname + ", " + h_qty);
            }
        } catch (Exception e) {
            out.println(e.getLocalizedMessage());
        } finally {
            close(con);
        }
        return cart;
    }
     */

 /* public List addOtherItemCart() {

        if (othreqQTY <= 0) {
            getCurrentInstance().addMessage(null, new FacesMessage(SEVERITY_INFO, "Please give QTY!!!", ""));
        } else {
            cart.add(new myCart("NEWITEM", othItem.toUpperCase(), 10, othreqQTY, 0));
            getCurrentInstance().addMessage(null, new FacesMessage(SEVERITY_WARN, othItem + "Added to Order", ""));
        }
        return cart;
    }*/
    //Custom Item Add to cart
    public void addCart() {

        FacesContext fc = getCurrentInstance();
        Map<String, String> params = fc.getExternalContext().getRequestParameterMap();
        String item_artcode = params.get("gartcode");
        String item_artname = params.get("gartname");
        int item_qoh = parseInt(params.get("gqoh"));
        double item_mrp = Double.parseDouble(params.get("gmrp"));
        int pack = 0;
        if (reqQTY <= 0) {
            getCurrentInstance().addMessage(null, new FacesMessage(SEVERITY_INFO, "Please give QTY!!!", "Please give QTY!!!"));
        } else {
            cart.add(new myCart(item_artcode, item_artname, item_qoh, reqQTY, item_mrp, 0.0, 0, "", ""));
            setReqQTY(0);
            getCurrentInstance().addMessage(null, new FacesMessage(SEVERITY_WARN, item_artcode + " Added to Order", ""));
        }
    }

    //Order Confirmation
    public void showConfirm() {
        setDialogVisible(true);
        //return null;
    }

    public void doShoptimizeOrderFinalize() throws ServletException, IOException {
        FacesContext fc = getCurrentInstance();
        Map<String, String> params = fc.getExternalContext().getRequestParameterMap();
        String origin = params.get("origin");
        String patID = params.get("patID");
        String VendordID = params.get("vendOrdNo");
        String shipping = params.get("shipping");
        String payment = params.get("payment");
        String vendName = params.get("vendName");
        String cancel = params.get("cancel");
        HttpSession appSession = (HttpSession) getCurrentInstance().getExternalContext().getSession(false);
        appSession.setAttribute("origin", origin);
        appSession.setAttribute("pid", patID);
        appSession.setAttribute("vendOrdID", VendordID);
        appSession.setAttribute("shipping", shipping);

        appSession.setAttribute("payment", payment);
        appSession.setAttribute("vendName", vendName);
        //moveShoptimizeItemDetailtoCart(VendordID);

        doOrderFinalize();
        String genOrdno = Integer.toString(genOrdNo);

    }

    public void updateStatus(String _VendordID, String _genOrdno) {
        // updateStatus(VendordID, apOrdID);
        Connection con = null;
        try {
            con = getLDBConnection();
            //PreparedStatement ps = con.prepareStatement("update ThirdPartyOrderDetails set ordseq = ?, filestatus = 1 where orderid = ?");
            PreparedStatement ps = con.prepareCall("{call UpdateOrderStatus(?, ?)}");
            ps.setString(1, _genOrdno);
            ps.setString(2, _VendordID);
            ps.execute();
        } catch (Exception e) {
            out.println(e.getLocalizedMessage());
        }
    }

    public void updateAPOrdertoVendor(String _VendordID, String _genOrdno, String _siteID) {
        try {
            final String USER_AGENT = "Mozilla/5.0";
            final String GET_URL = "http://172.16.2.251:84/MediAssist_pos.aspx?MA_Orderid=" + _VendordID + "&AP_orderid=" + _genOrdno + "&siteid=" + _siteID + "&Action=orderupdate";
            URL obj = new URL(GET_URL);
            out.println(GET_URL);
            HttpURLConnection smscon = (HttpURLConnection) obj.openConnection();
            smscon.setRequestMethod("GET");
            smscon.setRequestProperty("User-Agent", USER_AGENT);
            int responseCode = smscon.getResponseCode();
            out.println("GET Response Code :: " + responseCode);
            if (responseCode == HTTP_OK) { // success
                BufferedReader in = new BufferedReader(new InputStreamReader(
                        smscon.getInputStream()));
                String inputLine;
                StringBuilder response = new StringBuilder();
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
                out.println(response.toString());
            } else {
                out.println("GET request not worked");
            }
        } catch (IOException ex) {
            out.println(ex.getLocalizedMessage());
        }
    }

    public void doOrderFinalize() throws ServletException, IOException {
        HttpSession appSession = (HttpSession) getCurrentInstance().getExternalContext().getSession(false);
        String username = appSession.getAttribute("loginid").toString();
        String pat = appSession.getAttribute("pid").toString();
        String origin = appSession.getAttribute("origin").toString().trim();
        String VendordID = "";
        String shipping = "";
        String payment = "";
        String vendName = "";
        int loginid = parseInt(username);
        int patid = parseInt(pat);
        if (origin.equals("V")) {
            VendordID = appSession.getAttribute("vendOrdID").toString();
            shipping = appSession.getAttribute("shipping").toString();
            payment = appSession.getAttribute("payment").toString();
            deliveryMode = shipping;
            paymentMode = payment;
            vendName = appSession.getAttribute("vendName").toString();
        } else if (origin.equals("P")) {

            Connection con = null;
            Connection ordcon = null;
            try {
                con = getLDBConnection();
                Statement stmt = con.createStatement();
                ResultSet ors = stmt.executeQuery("select max(OrdNo)+1 from CC_OrdMaster");
                if (ors.next()) {
                    genOrdNo = ors.getInt(1);
                    ordcon = getLDBConnection();
                    PreparedStatement ordps = ordcon.prepareStatement("update CC_OrdMaster set OrdNo = ?");
                    ordps.setInt(1, genOrdNo);
                    ordps.execute();
                    finalizeOrder(genOrdNo, patid, loginid);

                    if (origin.equals("V")) {
                        String apOrdID = Integer.toString(genOrdNo);
                        //updateStatus(VendordID, apOrdID);
                        try {
                            Connection ordupdcon = getLDBConnection();
                            //PreparedStatement ordupdps = ordupdcon.prepareStatement("insert into orderupdate (MAOrderid,siteid,AP_Orderid) values (?,?,?)");
                            PreparedStatement ordupdps = con.prepareCall("{call CC_TPOrder(?,?,?,?)}");
                            ordupdps.setString(1, apOrdID);
                            ordupdps.setString(2, VendordID);
                            ordupdps.setString(3, vendorName);
                            ordupdps.setString(4, shopid);
                            ordupdps.execute();
                        } catch (Exception e) {

                        }
                        if (vendorName.equals("MediAssist")) {
                            updateAPOrdertoVendor(VendordID, apOrdID, shopid);
                        }
                    } else {
                        upload(genOrdNo, uploaded_image);
                        String apOrdID = Integer.toString(genOrdNo);
                        if (shopid != "16001") {
                            ordInitSMS(genOrdNo);
                        }
                    }
                    if (vendorName.equals("HAPP") || vendorName.equals("PRACTO")) {
                    } else {
                        ordInitSMS(genOrdNo);
                    }
                }
            } catch (Exception e) {
                System.out.println(e.getLocalizedMessage());
            } finally {
                close(con);
                close(ordcon);
            }
            showConfirm();
        }

        //  boolean checkPreviousOrder = findPreviousOrder.checkLastOrder(patid);
        /*if (checkPreviousOrder) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Previous Order is available for the same customer!!!", ""));
        } else {*/
        if (cart.isEmpty()) {
            getCurrentInstance().addMessage(null, new FacesMessage(SEVERITY_INFO, "Please add Item to cart!!!", ""));
        } else {

            boolean noduplicate = checkforTPOrderduplicate(VendordID);
            if (noduplicate) {
                Connection con = null;
                Connection ordcon = null;
                try {
                    con = getLDBConnection();
                    Statement stmt = con.createStatement();
                    ResultSet ors = stmt.executeQuery("select max(OrdNo)+1 from CC_OrdMaster");
                    if (ors.next()) {
                        genOrdNo = ors.getInt(1);
                        ordcon = getLDBConnection();
                        PreparedStatement ordps = ordcon.prepareStatement("update CC_OrdMaster set OrdNo = ?");
                        ordps.setInt(1, genOrdNo);
                        ordps.execute();
                        finalizeOrder(genOrdNo, patid, loginid);
                        if (origin.equals("V")) {
                            String apOrdID = Integer.toString(genOrdNo);
                            //updateStatus(VendordID, apOrdID);
                            try {
                                Connection ordupdcon = getLDBConnection();
                                //PreparedStatement ordupdps = ordupdcon.prepareStatement("insert into orderupdate (MAOrderid,siteid,AP_Orderid) values (?,?,?)");
                                PreparedStatement ordupdps = con.prepareCall("{call CC_TPOrder(?,?,?,?)}");
                                ordupdps.setString(1, apOrdID);
                                ordupdps.setString(2, VendordID);
                                ordupdps.setString(3, vendorName);
                                ordupdps.setString(4, shopid);
                                ordupdps.execute();
                            } catch (Exception e) {

                            }
                            if (vendorName.equals("MediAssist")) {
                                updateAPOrdertoVendor(VendordID, apOrdID, shopid);
                            }
                        } else {
                            upload(genOrdNo, uploaded_image);
                            String apOrdID = Integer.toString(genOrdNo);
                            //ordInitSMS(genOrdNo);
                        }
                        if (vendorName.equals("HAPP") || vendorName.equals("PRACTO") || shopid.equals("16001")) {
                        } else {
                            ordInitSMS(genOrdNo);
                        }
                    }
                } catch (Exception e) {
                    out.println(e.getLocalizedMessage());
                } finally {
                    close(con);
                    close(ordcon);
                }
                //FacesContext.getCurrentInstance().getExternalContext().redirect("mail.jsp?ordNo=" + genOrdNo + "&ForThis=SHOP");
                //FacesContext.getCurrentInstance().getExternalContext().redirect("hoLander.jsf");

                showConfirm();
            } else {
                getCurrentInstance().addMessage(null, new FacesMessage(SEVERITY_INFO, "Order Already Pushed to outlet!!!", ""));
            }
        }
        //}
    }

    public void CreateOrder() {
        HttpSession appSession = (HttpSession) getCurrentInstance().getExternalContext().getSession(false);
        String username = appSession.getAttribute("loginid").toString();
        String pat = appSession.getAttribute("pid").toString();
        String origin = appSession.getAttribute("origin").toString().trim();
        String VendordID = "";
        String shipping = "";
        String payment = "";
        String vendName = "";
        int loginid = parseInt(username);
        int patid = parseInt(pat);

    }

    public String goHome() {
        return "hoLander.jsf?faces-redirect=true";
    }

    public void finalizeOrder(int OrdNo, int patid, int loginid) {
        Connection conn = null;
        int tOrdNo = OrdNo;
        int tpatid = patid;
        int tloginid = loginid;
        try {
            conn = getLDBConnection();
            PreparedStatement ps = conn.prepareStatement("insert into cc_ordhead (ordno,patid,siteid,ordDate,loginid,status,DeliveryMode,PaymentMode,Remarks,orderSource) values (?,?,?,getDate(),?,'5',?,?,?,?)");
            ps.setInt(1, tOrdNo);
            ps.setInt(2, tpatid);
            ps.setInt(3, parseInt(shopid));
            ps.setInt(4, tloginid);
            ps.setString(5, deliveryMode);
            ps.setString(6, paymentMode);
            //ps.setDate(7, (java.sql.Date) delDT);
            ps.setString(7, remarks);
            ps.setString(8, vendorName);
            ps.execute();
            /*Statement stm = conn.createStatement();
            ResultSet rs = stm.executeQuery("insert into cc_ordhead values (tOrdNo,tpatid,Integer.parseInt(shopid),tloginid)");*/
        } catch (Exception e) {
            e.getLocalizedMessage();
            out.println(e.getLocalizedMessage());
        } finally {
            close(conn);
        }
        Connection conne = null;
        try {
            PreparedStatement psd = null;
            conne = getLDBConnection();
            out.println(cart.size());
            for (myCart p : cart) {
                String cartartcode = p.getArtcode();
                String cartartname = p.getArtname();
                Double cartmrp = p.getMrp();
                int cartreqQTY = p.getReqQTY();
                psd = conne.prepareStatement("insert into cc_orddet(ordNo,artCode,artName,reqqoh,mrp) values(?,?,?,?,?)");
                psd.setInt(1, OrdNo);
                psd.setString(2, cartartcode);
                psd.setString(3, cartartname);
                psd.setInt(4, cartreqQTY);
                psd.setDouble(5, cartmrp);
                psd.execute();
            }

            getCurrentInstance().addMessage(null, new FacesMessage(SEVERITY_WARN, "Your order no is " + OrdNo, ""));
            patid = 0;
        } catch (Exception e) {
            out.println(e.getLocalizedMessage());
            out.println(Arrays.toString(e.getStackTrace()));
            out.println(e.getMessage());
        } finally {
            close(conne);
        }
    }

    public void imgUPLOAD() {
        FacesContext fc = getCurrentInstance();
        Map<String, String> params = fc.getExternalContext().getRequestParameterMap();
        int ordNo = parseInt(params.get("ordNo"));
        upload(ordNo, uploaded_image);
        goHome();
    }

    public void upload(int _OrdNo, UploadedFile _uploaded_image) {

        byte[] file = new byte[_uploaded_image.getContents().length];
        arraycopy(_uploaded_image.getContents(), 0, file, 0, _uploaded_image.getContents().length);
        out.println(file);
        try {
            Connection con = getLDBConnection();
            PreparedStatement ps = con.prepareStatement("insert into CC_UploadedPrescriptions (ordno,prescription) values(?,?)");
            ps.setInt(1, _OrdNo);
            ps.setBytes(2, file);
            ps.execute();

        } catch (Exception e) {
            out.println(e.getLocalizedMessage());
        }
    }

    /*
     * Before Manager SMS
    public void ordInitSMS(int OrdNo) {
    try {
    ccm.sms.Medical service = new ccm.sms.Medical();
    ccm.sms.MedicalSoap port = service.getMedicalSoap12();
    Connection con = null;
    try {
    con = connectivity.getLDBConnection();
    PreparedStatement ps = con.prepareStatement("select mobileno,firstname+' '+lastname from cc_patient_registration where patientid = (select patid from cc_ordhead where ordno = ?)");
    ps.setInt(1, OrdNo);
    ResultSet rs = ps.executeQuery();
    while (rs.next()) {
    String MobNo = rs.getString(1);
    String Name = rs.getString(2);
    if (rs.getString(1).length() != 10) {
    System.out.println("Invalid Mobile No");
    } else {
    java.lang.String mobileNo = MobNo;
    java.lang.String name = Name;
    java.lang.String orderNo = Integer.toString(OrdNo);
    java.lang.String date = new java.util.Date().toString();
    sms _sms = new sms();
    _sms.sendSMSOrderCreation(mobileNo, name, orderNo, date);
    //System.out.println("Result = " + result);
    }
    }
    } catch (Exception e) {
    System.out.println(e.getLocalizedMessage());
    }
    } catch (Exception ex) {
    System.out.println(ex.getLocalizedMessage());
    }
    }
     */
    public void ordInitSMS(int OrdNo) {

        //ccm.sms.Medical service = new ccm.sms.Medical();
        //ccm.sms.MedicalSoap port = service.getMedicalSoap12();
        Connection con = null;
        try {
            con = getLDBConnection();
            String qry = "select PRICONTACT,patname,vendorname,i.APORDID,i.TPORDID,c.alertMobileNo,det.Name,i.CREATEDDATE from TPDELADDUPDATE d, CC_TPOrderInfo i, cclogin c, CC_SUN_LocationDetails det where i.TPORDID = d.orderid and i.APORDID = '" + OrdNo + "' and i.siteid = c.usr  and i.SITEID = det.SiteId";
            PreparedStatement ps = con.prepareStatement(qry);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                sms _sms = new sms();
                String MobNo = rs.getString(1);
                String alertMobNo = rs.getString(6);
                String apordid = rs.getString(4);
                String tpordid = rs.getString(5);
                String vendor = rs.getString(3);
                if (rs.getString(1).length() != 10) {
                    out.println("Invalid Mobile No");
                } else {
                    if (vendor.equalsIgnoreCase("online")) {
                        _sms.sendSMSOrderCreation(MobNo, tpordid);
                    } else {
                        _sms.sendSMSOrderCreation(MobNo, apordid);
                    }
                }
                if (alertMobNo.length() != 10) {
                } else {
                    String ordDate = rs.getString(8);
                    String siteName = rs.getString(7);
                    _sms.sendSMSOrderCreationtoManager(alertMobNo, apordid, shopid, siteName, ordDate);
                }
                //System.out.println("Result = " + result);
            }
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
        }
    }

    public void onRowEditing(RowEditEvent event) {

        System.out.println("ccm.logic.ctlr.orderCreation.onRowEditing()" + event.getComponent());
        FacesMessage msg = new FacesMessage("QTY changed for ", ((myCart) event.getObject()).getArtname());
        getCurrentInstance().addMessage(null, msg);
    }

    public void onRowCancel(RowEditEvent event) {
        FacesMessage msg = new FacesMessage("Edit Cancelled for", ((myCart) event.getObject()).getArtname());
        getCurrentInstance().addMessage(null, msg);
    }

    public void deleteItem() {
        cart.remove(selectedItem);
        selectedItem = null;
    }

    /*======================================================================================================================*/
    public void setSelectedOrder(TPOrder selectedOrder) {
        this.selectedOrder = selectedOrder;
        String ordNo = selectedOrder.getOrdNo();
        String vendorName = selectedOrder.getVendorName();
        listComments(ordNo, vendorName);
    }

    /*--------------------------------------------------*/
    //showShoptimizeordersummary
    public List<TPOrder> showOrders() {
        Date d1 = new java.util.Date();
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest userReq = (HttpServletRequest) context.getExternalContext().getRequest();
        String vendName = userReq.getParameter("vendorName");
        String orderType = userReq.getParameter("orderType");
        if (orderType.equals("PRE-ORDERS")) {
            orderType = "";
        }
        ordSum.clear();
        Connection con = null;
        try {
            con = getLDBConnection();
            //PreparedStatement ps = con.prepareStatement("select a.ordseq, count(a.itemid),a.orddt,a.patientid, b.is_medicine, b.shippingmethod,b.paymentmethod  from cc_itemdetails a, CC_Shoptimizeheader b where  a.ordseq = b.OrderId and filestatus=0 group by  a.ordseq,a.orddt,a.patientid, b.is_medicine, b.shippingmethod,b.paymentmethod order by a.ordDT asc");and b.is_medicine = 'PHARMA'
            //PreparedStatement ps = con.prepareStatement("select a.ordseq, C.FIRSTNAME +' '+ C.LASTNAME AS CUSTNAME,C.MOBILENO, a.orddt, b.is_medicine, b.shippingmethod,b.paymentmethod,a.patientid  from cc_itemdetails a, CC_Shoptimizeheader b,CC_Patient_Registration C where A.PATIENTID = C.PATIENTID AND a.ordseq = b.OrderId and filestatus=0 group by  a.ordseq, C.FIRSTNAME +' '+ C.LASTNAME,C.MOBILENO, a.orddt,b.is_medicine, b.shippingmethod,b.paymentmethod,a.patientid order by a.ordDT desc");
            //PreparedStatement ps = con.prepareStatement("select a.orderid, C.FIRSTNAME AS CUSTNAME,C.MOBILENO, a.orddt, b.is_medicine, b.shippingmethod, b.paymentmethod,a.patientid, B.VendorName  from ThirdPartyOrderDetails a, ThirdPartyOrderHeader b,CC_Patient_Registration C where  A.PATIENTID = C.PATIENTID AND b.orderid = a.OrderId and filestatus=0 group by a.orderid, C.FIRSTNAME ,C.MOBILENO, a.orddt, b.is_medicine, b.shippingmethod, b.paymentmethod,a.patientid,B.VendorName order by a.orderid asc");
            //PreparedStatement ps = con.prepareStatement("select b.orderid, U.patname AS CUSTNAME,C.MOBILENO, b.createddatetime, b.is_medicine, b.shippingmethod, b.paymentmethod,b.patientid, B.VendorName,ISNULL(u.ORDERAMOUNT ,'-') from ThirdPartyOrderHeader b,tpdeladdupdate U,CC_Patient_Registration C  where  b.PATIENTID = C.PATIENTID AND B.ORDERID=U.ORDERID and b.orderstatus=0 group by b.orderid, U.patname, C.MOBILENO, b.createddatetime, b.is_medicine, b.shippingmethod, b.paymentmethod,b.patientid,B.VendorName,U.ORDERAMOUNT order by b.orderid asc");
            PreparedStatement ps = con.prepareStatement("select  b.orderid, U.patname AS CUSTNAME,C.MOBILENO, b.createddatetime, b.is_medicine, b.shippingmethod, b.paymentmethod,b.patientid, B.VendorName,ISNULL(u.ORDERAMOUNT ,'-') as ordAmount, ISNULL(u.POSTAL_CODE ,'-') as POSTAL, ISNULL(u.CITY,'-') as CITY, isnull(u.PSTATUS,'-') as TXNStatus, isnull(u.PORDERID,'-') as PayTMOrderID,u.del_add,b.site_ID,isnull(b.wod,'0') as WOD from ThirdPartyOrderHeader b,tpdeladdupdate U,CC_Patient_Registration C  where  b.PATIENTID = C.PATIENTID AND B.ORDERID=U.ORDERID and b.orderstatus=0 AND b.VendorName = ? and b.is_medicine = ? group by b.orderid, U.patname, C.MOBILENO, b.createddatetime, b.is_medicine, b.shippingmethod, b.paymentmethod,b.patientid,B.VendorName,U.ORDERAMOUNT,u.POSTAL_CODE,u.CITY,u.PSTATUS,u.PORDERID,u.del_add,b.site_ID,b.WOD order by b.createddatetime desc");
            ps.setString(1, vendName);
            ps.setString(2, orderType);
            ResultSet rs = ps.executeQuery();

            System.out.println(d1);

            while (rs.next()) {

                SimpleDateFormat sd = new SimpleDateFormat("dd-MMM-yy hh:mm");
                String ordDt = rs.getString(4).substring(0, 16);
                String shippingmethod = rs.getString(6).toUpperCase();
                String Postal = rs.getString(11);
                String City = rs.getString(12).toUpperCase();
                /*if (shippingmethod.equalsIgnoreCase("appflatrate_appflatrate")) {
                    shippingmethod = "Store Pickup";
                } else if (shippingmethod.equalsIgnoreCase("tablerate_bestway")) {
                    shippingmethod = "Home Delivery";
                } else {
                    shippingmethod = "Store Pickup";
                }*/
                ordSum.add(new TPOrder(rs.getString(1), rs.getString(2).toUpperCase(), rs.getString(3), ordDt, rs.getString(5), shippingmethod, rs.getString(7), rs.getString(8), rs.getString(9), rs.getString(10), Postal, City, rs.getString(13), rs.getString(14), rs.getString(15), rs.getString(16), rs.getString(17)));
            }
            Date d2 = new java.util.Date();
            System.out.println(d2);
            long getDiff = d2.getTime() - d1.getTime();

            // using TimeUnit class from java.util.concurrent package
            long getDaysDiff = TimeUnit.MILLISECONDS.toDays(getDiff);
            System.out.println(getDaysDiff);

        } catch (Exception e) {
            out.println(e.getLocalizedMessage());
        }
        return ordSum;
    }

    public List<TPOrder> getOrdSum() {
        return ordSum;
    }

    public void onRowToggle(ToggleEvent event) {
        String VendorName = ((TPOrder) event.getData()).getVendorName();
        listCancelRemarks(VendorName);

    }

    public String doCancellation() {
        try {
            FacesContext fc = getCurrentInstance();
            Map<String, String> params = fc.getExternalContext().getRequestParameterMap();
            HttpSession appSession = (HttpSession) getCurrentInstance().getExternalContext().getSession(false);
            int username = Integer.parseInt(appSession.getAttribute("loginid").toString());
            String L_ordNo = params.get("ordNo");
            String L_vendorName = params.get("vendorName");
            OrderCancellation cancellation = new OrderCancellation();
            boolean cancancel = cancellation.checkCancellation(L_vendorName, L_ordNo);
            if (cancancel) {
                cancellation.processCancellation(cancelRemarksCode, username, L_ordNo, L_vendorName);
                getCurrentInstance().addMessage(null, new FacesMessage(SEVERITY_INFO, "Order Cancelled successfully!!!", ""));
            } else {

                getCurrentInstance().addMessage(null, new FacesMessage(SEVERITY_INFO, "Order can't be cancelled at this stage", ""));
            }
            ordSum.remove(selectedOrder);

        } catch (SQLException ex) {
            Logger.getLogger(orderCreation.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "tpo?faces-redirect=true";
    }

    public List<CancelRemarks> listCancelRemarks(String _vendname) {
        CanRem.clear();
        try {
            Connection con = getLDBConnection();
            PreparedStatement ps = con.prepareStatement("select * from CC_OrderCancelRemarks where VENDOR = ? and vendor in ('Online','AskApollo')order by reason_code asc");
            ps.setString(1, _vendname);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                CanRem.add(new CancelRemarks(rs.getString(1), rs.getString(2)));
            }
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
        }
        return CanRem;
    }

    public List<CancelRemarks> getCancelRemarks() {
        return CanRem;
    }

    List<customerInfo> custInfo = new ArrayList<customerInfo>();

    String shoptimize_site_id, shoptimize_ordertype;
    String shoptimize_pres_img_url = "https://upload.wikimedia.org/wikipedia/commons/a/ac/No_image_available.svg";

    public String getShoptimize_ordertype() {
        return shoptimize_ordertype;
    }

    public void setShoptimize_ordertype(String shoptimize_ordertype) {
        this.shoptimize_ordertype = shoptimize_ordertype;
    }

    public String getShoptimize_pres_img_url() {
        return shoptimize_pres_img_url;
    }

    public void setShoptimize_pres_img_url(String shoptimize_pres_img_url) {
        this.shoptimize_pres_img_url = shoptimize_pres_img_url;
    }

    public String getShoptimize_site_id() {
        return shoptimize_site_id;
    }

    public void setShoptimize_site_id(String shoptimize_site_id) {
        this.shoptimize_site_id = shoptimize_site_id;
    }

    public void showShoptimizeOrderDetail() {

        FacesContext fc = getCurrentInstance();
        Map<String, String> params = fc.getExternalContext().getRequestParameterMap();
        String ordNo = params.get("ordNo");
        String patID = params.get("patID");
        //boolean inProgress = orderProcessCheck.checkOrderStatus(ordNo);
        //if (inProgress) {
        //dialogWindow = "PF('myAlertDialog').show()";
        //} else {
        //  doLog(ordNo);
        //updatePrice(ordNo);
        /*orderCreation oc = new orderCreation();
        cart = oc.moveShoptimizeItemDetailtoCart(ordNo);*/
        //getOrderDetail(ordNo);
        addItemtoVendor(ordNo);
        String mobNo = getCustInfo(patID);
        Long mobno = parseLong(mobNo);
        customerInfo(mobno, ordNo);
        shoptimizeheaderInfo(ordNo);
        //dialogWindow = "PF('myDialog').show()";
        //}
        //return "VODetails?faces-redirect=true";
    }

    public void releaseReservation() {
        Connection con = null;
        HttpSession appSession = (HttpSession) getCurrentInstance().getExternalContext().getSession(false);
        String username = appSession.getAttribute("loginid").toString();
        FacesContext fc = getCurrentInstance();
        Map<String, String> params = fc.getExternalContext().getRequestParameterMap();
        String ordNo = params.get("ordNo");
        try {
            con = getLDBConnection();
            PreparedStatement ps = con.prepareStatement("delete from CC_OrdProcessLog where ordNo=? and processby = ?)");
            ps.setString(1, ordNo);
            ps.setString(2, username);
            ps.execute();
        } catch (Exception e) {
            out.println(e.getLocalizedMessage());
        }
    }

    public void doLog(String _ordNo) {
        Connection con = null;
        HttpSession appSession = (HttpSession) getCurrentInstance().getExternalContext().getSession(false);
        String username = appSession.getAttribute("loginid").toString();
        try {
            con = getLDBConnection();
            PreparedStatement ps = con.prepareStatement("insert into CC_OrdProcessLog values(?,?,getDate())");
            ps.setString(1, _ordNo);
            ps.setString(2, username);
            ps.execute();
        } catch (Exception e) {
            out.println(e.getLocalizedMessage());
        }
    }

    public void updatePrice(String _ordNo) {
        Connection con = null;
        try {
            con = getLDBConnection();
            PreparedStatement ps = con.prepareStatement("update ThirdPartyOrderDetails set price_mrp=(select mrp from rvk_item1 where art_code=itemid), subtotal = qty * (select mrp from rvk_item1 where art_code=itemid) where orderid = ?");
            ps.setString(1, _ordNo);

            ps.execute();
        } catch (Exception e) {
            out.println(e.getLocalizedMessage());
        }
    }

    List<UploadedPrescDocs> upldprescimg = new ArrayList<>();

    public List shoptimizeheaderInfo(String _ordNo) {
        Connection con = null;
        upldprescimg.clear();
        try {
            //ordHeader.clear();
            con = getLDBConnection();
            //PreparedStatement ps = con.prepareStatement("select shippingmethod, paymentmethod,site_id, presc_image,is_medicine from CC_Shoptimizeheader where orderID = ?");
            PreparedStatement ps = con.prepareStatement("select shippingmethod, paymentmethod,site_id, presc_image,is_medicine,VendorName from ThirdPartyOrderHeader where orderID = ?");
            ps.setString(1, _ordNo);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                deliveryMode = rs.getString(1).toUpperCase();
                paymentMode = rs.getString(2).toUpperCase();
                shoptimize_site_id = rs.getString(3);
                if (rs.getString(6).equalsIgnoreCase("AskApollo") || rs.getString(6).equalsIgnoreCase("SIDBIE") || rs.getString(6).equalsIgnoreCase("Kaizala") || rs.getString(6).equalsIgnoreCase("FHPL")) {
                    shoptimize_pres_img_url = rs.getString(4).replace("[", "").replace("]", "").replace("\\/", "/").replace("\"", "/").replace("E:\\AppDoc\\", "http://172.16.2.251:8085/IA/Images/");
                    //shoptimize_pres_img_url = rs.getString(4).replace("[", "").replace("]", "").replace("\\/", "/").replace("\"", "/").replace("E:\\AppDoc\\", "http://124.124.88.106:9090/IA/Images/");
                } else {
                    shoptimize_pres_img_url = rs.getString(4).replace("[", "").replace("]", "").replace("\\/", "/").replace("\"", "");
                }
                String strArray[] = shoptimize_pres_img_url.split(",");
                out.println("String Array is : ");
                for (int i = 0; i < strArray.length; i++) {
                    upldprescimg.add(new UploadedPrescDocs("Prescription Data " + (i + 1), strArray[i]));
                }
                shoptimize_ordertype = rs.getString(5);
                //ordHeader.add(new shoptimizeOrderHeader(rs.getString(1).toUpperCase(), rs.getString(2).toUpperCase(), rs.getString(3), rs.getString(4), rs.getString(5)));
            }
        } catch (Exception e) {
            out.println(e.getLocalizedMessage());
        }
        return upldprescimg;
    }

    public List customerInfo(Long _mobNo, String _ordNo) {
        HttpSession patientSession = getSession();
        Connection con = null;
        try {
            custInfo.clear();
            con = getLDBConnection();
            //PreparedStatement ps = con.prepareStatement("select cr.PatientID,FirstName,Gender,Age, Addr1,City,Zip, a.del_add as CommAddr,MailID,LastName,Lat2,Lon2,cccardno, uhid from CC_Patient_Registration cr, deladdressupdate a where Mobileno = ? and cr.PatientID = a.patientid and a.orderid = ?");
            PreparedStatement ps = con.prepareStatement("select tp.PatientID,tp.patname, cr.Gender ,cr.Age, tp.del_add as CommAddr,cr.MailID,tp.patname,cr.Lat2,cr.Lon2,cr.cccardno, cr.uhid from \n"
                    + "TPDELADDUPDATE tp,CC_Patient_Registration cr where cr.Mobileno = ? and cr.PatientID = tp.patientid and tp.orderid = ?");
            ps.setLong(1, _mobNo);
            ps.setString(2, _ordNo);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                String pid = rs.getString(1);
                String fn = rs.getString(2).toUpperCase();
                String custgender = "";
                int custage = rs.getInt(4);
                String commAdd = rs.getString(5);
                String mailid = rs.getString(6);
                String cccardno = rs.getString(10);
                String custuhid = rs.getString(11);
                patientSession.setAttribute("pid", pid);
                patientSession.setAttribute("commAddr", commAdd);
                patientSession.setAttribute("mailID", mailid);
                patientSession.setAttribute("Lat", rs.getDouble(8));
                patientSession.setAttribute("Lon", rs.getDouble(9));
                custInfo.add(new customerInfo(pid, fn, custgender, custage, commAdd, commAdd, mailid, cccardno, custuhid));
            }
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
        } finally {
            close(con);
        }
        return custInfo;
    }

    public List<customerInfo> getCustInfo() {
        return custInfo;
    }
    double grandTot = 0.0;
    double subTot = 0.0;
    double tax = 0.0;
    double disc = 0.0;

    /*Order Detail*/
    public List getOrderDetail(String _ordNo) {
        Connection con = null;
        ordDet.clear();
        grandTot = 0.0;
        try {
            con = getLDBConnection();
            PreparedStatement ps = con.prepareStatement("select itemid,ItemName,Qty,tax_amount,discount_amount,grand_total,subtotal,price_mrp,APLineComment,TPLineComment from ThirdPartyOrderDetails where orderid= ?");
            ps.setString(1, _ordNo);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                grandTot += rs.getDouble(7);
                subTot = rs.getDouble(7);
                tax = rs.getDouble(4);
                disc = rs.getDouble(5);
                ordDet.add(new shoptimizeOrderDetail(rs.getString(1), rs.getString(2), rs.getDouble(3), rs.getDouble(8)));
            }
        } catch (Exception e) {
        }
        return ordDet;
    }

    /*TP Vendor Item to Cart*/
    public List addItemtoVendor(String _ordNo) {
        Connection con = null;
        cart.clear();
        try {
            con = getLDBConnection();
            PreparedStatement ps = con.prepareStatement("select ItemID,ItemName,Qty,subtotal,aplinecomment,tplinecomment from ThirdPartyOrderDetails where orderid = ?");
            ps.setString(1, _ordNo);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                String h_artcode = rs.getString(1);
                String h_artname = rs.getString(2);
                Double D = rs.getDouble(3);
                int h_qty = D.intValue();
                int tem = 0;
                double mrp = rs.getDouble(4);
                String apLC = rs.getString(5);
                String tpLC = rs.getString(6);
                cart.add(new myCart(h_artcode, h_artname, tem, h_qty, mrp, 0.0, 0, apLC, tpLC));
            }
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
        } finally {
            close(con);
        }
        return cart;
    }

    /*Customer Info*/
    public String getCustInfo(String _patID) {
        Connection con = null;
        String mobNo = "";
        try {
            con = getLDBConnection();
            PreparedStatement ps = con.prepareStatement("select * from CC_Patient_Registration where PatientID = ?");
            ps.setString(1, _patID);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                String lat = rs.getString(17);
                String zip = rs.getString(10);
                mobNo = rs.getString(6);
                out.println(lat);
                if (lat != null) {
                    out.println("Latitude is not null");
                } else {
                    if (zip.length() == 6) {
                        String result = updateLatLon(_patID, zip);
                        out.println("Latitude(" + result + ") is updated for " + _patID);
                    }
                }
            }
        } catch (Exception e) {
            out.println(e.getLocalizedMessage());
        }
        return mobNo;
    }

    /*Lat Long for Patient*/
    public String updateLatLon(String _patID, String _zip) throws Exception {
        HttpSession appSession = (HttpSession) getCurrentInstance().getExternalContext().getSession(false);
        int loginid = parseInt(appSession.getAttribute("loginid").toString());
        String latLongs[] = getLatLongPositions(_zip);
        String lat = "12.12";
        String lon = "78.12";
        lat = latLongs[0];
        lon = latLongs[1];
        String latlon = lat + "," + lon;
        out.println("Latitude: " + latLongs[0] + " and Longitude: " + latLongs[1]);
        Connection con = null;
        try {
            con = getLDBConnection();
            PreparedStatement ps = con.prepareStatement("update CC_Patient_Registration set loginid = ?, Lat1 = ?, Lon1=?, Lat2=?, Lon2=?, cccardno = 0, uhid = 0, Gender= ?, Age= ? where patientID = ?");
            ps.setInt(1, loginid);
            ps.setString(2, lat);
            ps.setString(3, lon);
            ps.setString(4, lat);
            ps.setString(5, lon);
            ps.setString(6, "M");
            ps.setString(7, "35");
            ps.setString(8, _patID);
            ps.executeUpdate();
        } catch (Exception e) {
            out.println(e.getLocalizedMessage());
        }
        return latlon;
    }

    /*Get LAT/LONG from Pincode*/
    public static String[] getLatLongPositions(String address) throws Exception {
        int responseCode = 0;
        String api = "http://maps.googleapis.com/maps/api/geocode/xml?address=" + encode(address, "UTF-8") + "&sensor=true";
        URL url = new URL(api);
        HttpURLConnection httpConnection = (HttpURLConnection) url.openConnection();
        httpConnection.connect();
        responseCode = httpConnection.getResponseCode();
        if (responseCode == 200) {
            DocumentBuilder builder = new DocumentBuilder() {
                @Override
                public Document parse(InputSource is) throws SAXException, IOException {
                    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                }

                @Override
                public boolean isNamespaceAware() {
                    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                }

                @Override
                public boolean isValidating() {
                    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                }

                @Override
                public void setEntityResolver(EntityResolver er) {
                    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                }

                @Override
                public void setErrorHandler(ErrorHandler eh) {
                    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                }

                @Override
                public Document newDocument() {
                    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                }

                @Override
                public DOMImplementation getDOMImplementation() {
                    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                }
            };
            Document document = builder.parse(httpConnection.getInputStream());
            XPathFactory xPathfactory = newInstance();
            XPath xpath = xPathfactory.newXPath();
            XPathExpression expr = xpath.compile("/GeocodeResponse/status");
            String status = (String) expr.evaluate(document, STRING);
            if (status.equals("OK")) {
                expr = xpath.compile("//geometry/location/lat");
                String latitude = (String) expr.evaluate(document, STRING);
                expr = xpath.compile("//geometry/location/lng");
                String longitude = (String) expr.evaluate(document, STRING);
                return new String[]{latitude, longitude};
            } else {
                throw new Exception("Error from the API - response status: " + status);
            }
        }
        return new String[]{"12.5", "78.5"};
    }

    /*Comments on Pre-PUSH Orders*/
    public String Addcomment() {
        HttpSession appSession = (HttpSession) getCurrentInstance().getExternalContext().getSession(false);
        String AgentID = appSession.getAttribute("loginid").toString();
        FacesContext fc = getCurrentInstance();
        Map<String, String> params = fc.getExternalContext().getRequestParameterMap();
        String ordno = params.get("ordNo").trim();
        String vendName = params.get("vendorName").trim();
        addAuditLog(ordno, AgentID, vendName, CommentData);
        return "Comments Added Successfully";
    }

    public void addAuditLog(String _ordno, String _AgentID, String _vendorName, String _comment) {
        OrderComment ordComment = new OrderComment();
        try {
            Connection con = getLDBConnection();
            ordComment.addCommenttoOrder(con, _ordno, _comment, Integer.parseInt(_AgentID), _vendorName);
            con.close();
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
        }
    }

    /*Comments for selected Order*/
    public List<comments> listComments(String _ordNo, String _vendorName) {
        try {
            Connection con = getLDBConnection();
            OrderComment ordComment = new OrderComment();
            thisOrderComment = ordComment.listAllComments(con, _ordNo, _vendorName);
            con.close();
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
        }
        return thisOrderComment;
    }

    /*Inventory*/
    public List<invAvailability> showInventory() {
        invAvail.clear();
        StringBuilder sb = new StringBuilder();
        for (myCart c : cart) {
            sb.append(c.getArtcode()).append(",");
        }
        String art = sb.toString().substring(0, sb.length() - 1);
        System.out.println(art);
        String url = "http://172.16.2.251:8085/WSS/mapp/inventoryCheck/" + shopid + "/" + art;
        Client restClient = create();
        WebResource webResource = restClient.resource(url);
        ClientResponse resp = webResource.accept("application/json")
                .get(ClientResponse.class);
        if (resp.getStatus() != 200) {
            err.println("Unable to connect to the server");
        }
        String output = resp.getEntity(String.class);
        System.out.println("response: " + output);
        try {
            JSONArray jsonArray = new JSONArray(output);
            int count = jsonArray.length();
            for (int i = 0; i < count; i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                String invartCode = jsonObject.getString("artCode");
                int invQty = jsonObject.getInt("qoh");
                double invmrp = jsonObject.getDouble("mrp");
                String exp = jsonObject.getString("expDate");
                invAvail.add(new invAvailability(invartCode, invQty, invmrp, exp));
            }
        } catch (JSONException e) {
            System.out.println(e.getLocalizedMessage());
        }
        return invAvail;
    }

    public void onResize(ResizeEvent event) {
        //FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Image resized", "Width:" + event.getWidth() + ",Height:" + event.getHeight());

        //FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    /*==================================================================================================================*/
    public void setCart(List<myCart> cart) {
        this.cart = cart;
    }

    public double getGrandTot() {
        return grandTot;
    }

    public void setGrandTot(double grandTot) {
        this.grandTot = grandTot;
    }

    public double getSubTot() {
        return subTot;
    }

    public void setSubTot(double subTot) {
        this.subTot = subTot;
    }

    public double getTax() {
        return tax;
    }

    public void setTax(double tax) {
        this.tax = tax;
    }

    public double getDisc() {
        return disc;
    }

    public void setDisc(double disc) {
        this.disc = disc;
    }

    public List<shoptimizeOrderDetail> getOrdDet() {
        return ordDet;
    }

}
