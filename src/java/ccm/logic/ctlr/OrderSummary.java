/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ccm.logic.ctlr;

import static ccm.dao.bean.connectivity.getLDBConnection;
import ccm.logic.bean.OrderSummarybean;
import static java.lang.System.out;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import static java.util.Calendar.DAY_OF_MONTH;
import static java.util.Calendar.MONTH;
import static java.util.Calendar.YEAR;
import static java.util.Calendar.getInstance;
import java.util.Date;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Lenovo
 */
@ManagedBean(name = "ordSummary")
@ViewScoped
public class OrderSummary {

    /**
     * Creates a new instance of dashboard
     */
    private Date date1, date2;

    public Date getDate1() {
        return date1;
    }

    public void setDate1(Date date1) {
        this.date1 = date1;
    }

    public Date getDate2() {
        return date2;
    }

    public void setDate2(Date date2) {
        this.date2 = date2;
    }

    public OrderSummary() {
        ordSum();
    }

    List<OrderSummarybean> osum = new ArrayList<OrderSummarybean>();

    public List ordSum() {
        osum.clear();
        Connection con = null;
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");

        Calendar c = getInstance();
        int year = c.get(YEAR);
        int month = c.get(MONTH);
        int day = 1;
        c.set(year, month, day);
        int numOfDaysInMonth = c.getActualMaximum(DAY_OF_MONTH);
        String d1 = sdf.format(c.getTime());
        c.add(DAY_OF_MONTH, numOfDaysInMonth - 1);
        String d2 = sdf.format(c.getTime());
        try {
            con = getLDBConnection();
            //PreparedStatement ps = con.prepareStatement("select ordersource,count(*),status  from cc_ordhead where orddate >='01-Nov-2017' and orddate <= '30-Nov-2017'  group by ordersource,status order by ordersource");
            PreparedStatement psc = con.prepareStatement("select dc_site from openquery\n"
                    + "		(CMS,'select dc_site from web..cms_sites group by dc_site order by dc_site')");
            ResultSet rsc = psc.executeQuery();
            while (rsc.next()) {
                String rscdc_site = rsc.getString(1);
                String qry = "select \n"
                        + "	(select city from openquery\n"
                        + " (CMS,'select city from web..cms_sites where dc_site = ''" + rscdc_site + "'' group by city')) as city,\n"
                        + "	ordersource,count(*),status  from cc_ordhead where \n"
                        + "	orddate >= '" + d1 + "' and orddate <= '" + d2 + "'  \n"
                        + "	and siteid in (select siteid from openquery\n"
                        + "	(CMS,'select siteid from web..cms_sites where dc_site = ''" + rscdc_site + "'' group by siteid'))\n"
                        + "	group by ordersource,status order by ordersource,status asc";
                PreparedStatement ps = con.prepareStatement(qry);
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    int status = rs.getInt(4);
                    String Reg = rs.getString(1).toString().toUpperCase();
                    int Count = rs.getInt(3);
                    if (status == 0) {
                        osum.add(new OrderSummarybean(rs.getString(1), rs.getString(2), rs.getString(3), "Order Initiated", rs.getString(4)));
                    } else if (status == 3) {
                        osum.add(new OrderSummarybean(rs.getString(1), rs.getString(2), rs.getString(3), "Delivered", rs.getString(4)));
                    } else if (status == 2) {
                        osum.add(new OrderSummarybean(rs.getString(1), rs.getString(2), rs.getString(3), "Cancelled", rs.getString(4)));
                    } else if (status == 1) {
                        osum.add(new OrderSummarybean(rs.getString(1), rs.getString(2), rs.getString(3), "Billed", rs.getString(4)));
                    }
                }
            }
        } catch (Exception e) {
            out.println(e.getLocalizedMessage());
        }
        return osum;
    }

    public List<OrderSummarybean> getOsum() {
        return osum;
    }

}
