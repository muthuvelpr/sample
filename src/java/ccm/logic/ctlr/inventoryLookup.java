/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ccm.logic.ctlr;

import static ccm.dao.bean.connectivity.getAXConnection;
import static ccm.dao.bean.connectivity.getCMSDBConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

/**
 *
 * @author Pitchu
 */
public class inventoryLookup {

    public List checkInventory(String siteid, String artCode) {
        Connection con = null;
        try {
            con = getCMSDBConnection();
            PreparedStatement ps = con.prepareStatement("select server from dcmaster where dccode = (select dc from WEB_LOGIN_MASTER where UserID = ?)");
            ps.setString(1, siteid);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                con.close();
                ps = null;
                String server = rs.getString(1);
                if (server.equals("MASTER")) {
                    con = getAXConnection();
                } else if (server.equals("SLAVE")) {
                    con = getAXConnection();
                }
                ps = con.prepareStatement("  select SUM(AVAILPHYSICAL) ONHAND from INVENTSUM A, INVENTDIM B ,inventsite c WHERE b.inventsiteid = ? and a.itemid IN (?) AND CLOSED=0 AND A.INVENTDIMID=B.INVENTDIMID  and c.siteid=b.INVENTSITEID and c.ACXSITERUNNINGON=2 AND A.DATAAREAID = 'AHEL' AND A.PARTITION = '5637144576'  AND B.DATAAREAID = 'AHEL' AND B.PARTITION = '5637144576'   AND C.DATAAREAID = 'AHEL' AND C.PARTITION = '5637144576'  GROUP BY A.ITEMID, B.INVENTSITEID having SUM(AVAILPHYSICAL) >0");
                ps.setString(1, siteid);
                ps.setString(2, artCode);
            }
        } catch (Exception e) {

        }
        return null;
    }
}
