/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ccm.logic.ctlr;

/**
 *
 * @author Lenovo
 */
import static ccm.dao.bean.connectivity.getLDBConnection;
import static java.lang.System.arraycopy;
import static java.lang.System.out;
import java.sql.Connection;
import java.sql.PreparedStatement;

import javax.faces.application.FacesMessage;
import static javax.faces.application.FacesMessage.SEVERITY_ERROR;
import static javax.faces.application.FacesMessage.SEVERITY_INFO;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import static javax.faces.context.FacesContext.getCurrentInstance;

import org.primefaces.model.UploadedFile;

@ManagedBean
@RequestScoped
public class ImageStoreBean {

    private UploadedFile uploaded_image;

    // Store file in the database
    public void storeImage() {
        // Create connection
        try {
            byte[] file = new byte[uploaded_image.getContents().length];
            arraycopy(uploaded_image.getContents(), 0, file, 0, uploaded_image.getContents().length);
            out.println(file);
            try {
                Connection con = getLDBConnection();
                PreparedStatement ps = con.prepareStatement("insert into CC_UploadedPrescriptions (ordno,prescription) values(?,?)");
                ps.setInt(1, 2);
                ps.setBytes(2, file);
                ps.execute();

            } catch (Exception e) {
                out.println(e.getLocalizedMessage());
            }

            FacesMessage msg = new FacesMessage(SEVERITY_INFO, "Upload success", uploaded_image.getFileName() + " is uploaded.");
            getCurrentInstance().addMessage(null, msg);

        } catch (Exception e) {
            e.printStackTrace();

            // Add error message
            FacesMessage errorMsg = new FacesMessage(SEVERITY_ERROR, "Upload error", e.getMessage());
            getCurrentInstance().addMessage(null, errorMsg);
        }

    }

    // Getter method
    public UploadedFile getUploaded_image() {
        return uploaded_image;
    }

    public void setUploaded_image(UploadedFile uploaded_image) {
        this.uploaded_image = uploaded_image;
    }

}
