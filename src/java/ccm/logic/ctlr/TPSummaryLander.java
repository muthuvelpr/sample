/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ccm.logic.ctlr;

import static ccm.dao.bean.connectivity.getLDBConnection;
import ccm.logic.bean.OrderSummarybean;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author pitchaiah_m
 */
@ManagedBean(name = "TPSum")
@ViewScoped
public class TPSummaryLander implements Serializable {

    /**
     * Creates a new instance of TPSummaryLander
     */
    public TPSummaryLander() {
        showOrderSummarN();
    }

    List<OrderSummarybean> orderSummaryN = new ArrayList<>();

    /*Show Order Summary Count*/
    public List<OrderSummarybean> showOrderSummarN() {
        orderSummaryN.clear();
        try {
            Connection con = null;
            con = getLDBConnection();
            PreparedStatement ps = con.prepareStatement("select VendorName,is_medicine,OrderStatus,count(OrderId) from ThirdPartyOrderHeader where createddatetime >='01-Dec-2018' and OrderStatus = 0 group by VendorName,is_medicine,OrderStatus order by 1,3 asc");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                String ordType = rs.getString(2);

                if ((ordType.length()) <= 1) {
                    ordType = "PRE-Orders";
                }
                orderSummaryN.add(new OrderSummarybean(rs.getString(1), ordType, rs.getString(3), rs.getString(4)));
            }
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
        }
        return orderSummaryN;
    }

    public List<OrderSummarybean> getOrderSummaryN() {
        return orderSummaryN;
    }

}
