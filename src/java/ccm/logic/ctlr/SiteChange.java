/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ccm.logic.ctlr;

import ccm.dao.bean.connectivity;
import static ccm.dao.bean.connectivity.getLDBConnection;
import ccm.logic.model.ordSiteChange;
import java.io.Serializable;
import static java.lang.System.out;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Lenovo
 */
@ManagedBean(name = "siteChange")
@ViewScoped
public class SiteChange implements Serializable{

    String ordNo, oSiteid, dSiteid, ordStatus;

    public String getOrdNo() {
        return ordNo;
    }

    public void setOrdNo(String ordNo) {
        this.ordNo = ordNo;
    }

    public String getOrdStatus() {
        return ordStatus;
    }

    public void setOrdStatus(String ordStatus) {
        this.ordStatus = ordStatus;
    }

    public String getoSiteid() {
        return oSiteid;
    }

    public void setoSiteid(String oSiteid) {
        this.oSiteid = oSiteid;
    }

    public String getdSiteid() {
        return dSiteid;
    }

    public void setdSiteid(String dSiteid) {
        this.dSiteid = dSiteid;
    }

    public String orderSiteChange() {
        ordSiteChange agent = new ordSiteChange();
        boolean readyforChange = ordSiteChange.checkforSiteChange(ordNo);
        if (readyforChange) {
            //siteChangeDataContract sitechange = createDataReq(ordNo, ordStatus, dSiteid);
            /*siteChangeDataContract sitechange = new siteChangeDataContract();
            sitechange.setOrdno(ordNo);
            sitechange.setOrderStatus("PHARMACY_CHANGE");
            sitechange.setNewSiteID(dSiteid);
            Gson gson = new Gson();
            String reqData = gson.toJson(sitechange);
            ClientResponse resp = agent.siteChange(ordNo, reqData);*/
            String ReferenceNumber, status, message;
            try {
                JSONArray jsonArray = new JSONArray();
                int count = jsonArray.length();
                for (int i = 0; i < count; i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    ReferenceNumber = jsonObject.getString("ReferenceNumber");
                    status = jsonObject.getString("OrderStatus");
                    if (status.equals("FAILURE")) {
                        message = jsonObject.getString("message");
                    } else if (status.equals("SUCCESS")) {
                        message = "SUCCESS";
                        try {
                            Connection con = connectivity.getLDBConnection();
                            PreparedStatement ps = con.prepareCall("{call SiteChange(?,?,?,?)}");
                            ps.setString(1, ordNo);
                            ps.setString(2, oSiteid);
                            ps.setString(3, dSiteid);
                            ps.setString(4, "");
                            ps.executeUpdate();
                        } catch (Exception e) {

                        }
                    }
                }
            } catch (JSONException e) {
                out.println(e.getLocalizedMessage());
            }
            return "SUCCESS";
        } else {
            return "FAILURE";
        }
    }

    public List<String> complete(String query) {
        List<String> results = new ArrayList<>();
        Connection con = null;
        try {
            con = getLDBConnection();
            PreparedStatement ps = con.prepareStatement("select ordno from cc_ordhead where ordersouce = 'PRACTO' and status in (0,5)  ordno like '%" + query + "%' group by ordno");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                results.add(rs.getString(1));
            }
        } catch (Exception e) {
        }
        return results;
    }
}
