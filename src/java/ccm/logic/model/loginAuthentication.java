/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ccm.logic.model;

import static ccm.dao.bean.connectivity.close;
import static ccm.dao.bean.connectivity.getLDBConnection;
import static ccm.logic.bean.Util.getSession;
import static java.lang.System.out;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Pitchu
 *
 */
public class loginAuthentication {

    public static boolean loginAuth(String un, String pw) {
        HttpSession myLoginSession = getSession();
//        if (!myLoginSession.isNew()) {
//            return true;
//        } else {
        Connection con = null;
        try {
            con = getLDBConnection();
            PreparedStatement ps = con.prepareStatement("select * from CClogin where usr = ? and pwd = ? and isactive = 1");
            ps.setString(1, un);
            ps.setString(2, pw);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                myLoginSession.setAttribute("loginid", rs.getString(1));
                myLoginSession.setAttribute("usr", rs.getString(2));
                myLoginSession.setAttribute("username", rs.getString(3));
                myLoginSession.setAttribute("reg_id", rs.getString(5));
                myLoginSession.setAttribute("reg_name", rs.getString(6));
                myLoginSession.setAttribute("usr_group", rs.getString(7));
                myLoginSession.setAttribute("isactive", rs.getString(9));

//                if (rs.getString(15).equals("0")) {
                return true;
//                } else {
//                    return false;
//                }
            } else {
                return false;
            }
        } catch (Exception e) {
            out.println(e.getLocalizedMessage());
            return false;
        } finally {
            close(con);
        }

//        }
    }

    public static boolean changeLoginStatus(String un, String pw) {
        {
            HttpSession myLoginSession = getSession();
            Connection con = null;
            try {
                con = getLDBConnection();
                PreparedStatement ps = con.prepareStatement("update CClogin set loggedin = '1' where usr = ? and pwd = ? ");
                ps.setString(1, un);
                ps.setString(2, pw);
                if (ps.executeUpdate() > 0) {
                    myLoginSession.setAttribute("isloggedin", "1");
                    return true;
                }
//            } catch (SQLException ex | ClassNotFoundException) {
            } catch (Exception ex) {
                out.println(ex.toString());
            } finally {
                close(con);
            }
            return false;
        }

    }

    public static boolean changeLogoutStatus() {
        {
            HttpSession myLoginSession = getSession();
            Connection con = null;
            try {
                con = getLDBConnection();
                PreparedStatement ps = con.prepareStatement("update CClogin set loggedin = '0' where usr = ? ");
                ps.setString(1, myLoginSession.getAttribute("usr").toString());
                if (ps.executeUpdate() > 0) {
                    return true;
                }
            } catch (Exception ex) {
                out.println(ex.toString());
            } finally {
                close(con);
            }
            return false;
        }

    }
}
