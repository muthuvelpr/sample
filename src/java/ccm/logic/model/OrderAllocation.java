/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ccm.logic.model;

import ccm.dao.bean.connectivity;
import ccm.logic.bean.lookup;
import ccm.logic.bean.myCart;
import ccm.logic.bean.payLoad;
import com.google.gson.Gson;
import com.logic.order.allocation.model.AllocationAuditLog;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import com.util.POSTHandler;

/**
 *
 * @author Administrator
 */
public class OrderAllocation {

    public String GetRoutingInfo(String _ordNo, String _vendName, List<myCart> _cart, String _AgentID) {
        String response = "";
        try {
            Connection con = connectivity.getLDBConnection();
            //PreparedStatement ps = con.prepareStatement("select d.itemid,d.Qty,td.POSTAL_CODE,th.is_medicine from ThirdPartyOrderHeader th, thirdpartyorderdetails d, TPDELADDUPDATE td where d.orderid = td.orderid and th.orderid = td.orderid and th.orderstatus = 0 and th.vendorname = ? and th.orderid = ?");
            PreparedStatement ps = con.prepareStatement("select td.POSTAL_CODE,th.is_medicine from ThirdPartyOrderHeader th, TPDELADDUPDATE td where th.orderid = td.orderid and \n"
                    + "th.orderstatus in (0,7)and th.orderid = ? and th.vendorname = ?");
            ps.setString(1, _ordNo);
            ps.setString(2, _vendName);
            ResultSet rs = ps.executeQuery();
            payLoad pl = new payLoad();
            String ordType = "";
            String postal = "";
            List<lookup> lp = new ArrayList<>();
            if (rs.next()) {
                ordType = rs.getString(2);
                postal = rs.getString(1);
            }
            for (myCart c : _cart) {
                double qty = (c.getQoh() * c.getPack());
                if (c.getArtcode() != "ESH0002") {
                    lp.add(new lookup(c.getArtcode(), (int) qty));
                } else {

                }
            }
            pl.setOrdertype(ordType);
            pl.setPostalcode(postal);
            pl.setLookup(lp);
            Gson g = new Gson();
            String data = g.toJson(pl);
            System.out.println(data);
            AllocationAuditLog aal = new AllocationAuditLog();
            aal.createAuditPayLoadLog(con, _vendName, _ordNo, data, _AgentID);
            POSTHandler postH = new POSTHandler();
            response = postH.POSTRequest(data);
            aal.createAuditResponseLog(con, _vendName, _ordNo, response, _AgentID);
            con.clearWarnings();
            con.close();
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
        }
        return response;
    }
}
