/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ccm.logic.model;

import ccm.dao.bean.connectivity;
import static ccm.dao.bean.connectivity.getLDBConnection;
import static java.lang.System.out;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;

/**
 * @created for APOLLO PHARMACY CRM
 * @author PITCHU
 * @created Apr 5, 2017 6:25:46 AM
 */
public class findPreviousOrder {

    public static boolean checkLastOrder(int _patientID) {
        Connection con = null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
            String curDate = sdf.format(new java.util.Date());
            con = getLDBConnection();
            PreparedStatement ps = con.prepareStatement("select * from cc_ordhead where patid = ? and ordDate >= ?");
            ps.setInt(1, _patientID);
            ps.setString(2, curDate);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return false;
            } else {
                return true;
            }
        } catch (Exception e) {
            out.println(e.getLocalizedMessage());
        }
        return false;
    }
}
