/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ccm.logic.model;

import ccm.dao.bean.connectivity;
import static ccm.dao.bean.connectivity.close;
import static ccm.dao.bean.connectivity.getHSPConnection;
import static ccm.dao.bean.connectivity.getLDBConnection;
import static java.lang.System.out;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 *
 * @author Pitchu
 * @created 29 Oct, 2014 6:01:28 PM
 *
 */
public class customerAvailability {

    public static boolean checkcustomerinLocalDB(long mobNo) {
        Connection con = null;
        try {
            con = getLDBConnection();
            PreparedStatement ps = con.prepareStatement("select MobileNo from CC_Patient_Registration where Mobileno = ?");
            ps.setLong(1, mobNo);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            out.println(e.getLocalizedMessage());
            return false;
        } finally {
            close(con);
        }
    }

    public static boolean checkuhidcustomerinLocalDB(String uhid) {
        Connection con = null;
        try {
            con = getLDBConnection();
            PreparedStatement ps = con.prepareStatement("select MobileNo from CC_Patient_Registration where UHID = ?");
            ps.setString(1, uhid);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            out.println(e.getLocalizedMessage());
            return false;
        } finally {
            close(con);
        }
    }

    public static boolean checkcccustomerinLocalDB(long cc) {
        Connection con = null;
        try {
            con = getLDBConnection();
            PreparedStatement ps = con.prepareStatement("select MobileNo from CC_Patient_Registration where cccardno = ?");
            ps.setLong(1, cc);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            out.println(e.getLocalizedMessage());
            return false;
        } finally {
            close(con);
        }
    }

    public static boolean checkcccinHYDDB(long ccn) {
        Connection con = null;
        try {
            con = getLDBConnection();
            //PreparedStatement ps = con.prepareStatement("select CCCARDNO from CC_Patient_Registration where CCCARDNO = ?");
            //PreparedStatement ps = con.prepareStatement("select cust_name firstname,cust_title lastname,sex gender,age,phone1 mobileno,addr1,addr2,city,zipcode zip,getdate() as registereddt,'2' loginid,cust_no cccardno,''uhid from openrowset('SQLOLEDB','SERVER=172.16.200.100;UID=sa;Pwd=hyd','select * from atmchron..chronic_reg  where cust_no =''"++"'') where phone1 in(select phone1 mobileno from openrowset('SQLOLEDB','SERVER=172.16.200.100;UID=sa;Pwd=hyd','select * from atmchron..chronic_reg  where cust_no =''"++"'')except select mobileno from dbo.CC_Patient_Registration)");
            Statement stmt = con.createStatement();
            String sql = "select cust_name firstname,cust_title lastname,sex gender,age,phone1 mobileno,addr1,addr2,city,zipcode zip,getdate() "
                    + "as registereddt,2 loginid,cust_no cccardno,'' uhid from openrowset('SQLOLEDB','SERVER=172.16.200.100;UID=sa;Pwd=hyd',"
                    + "'select * from atmchron..chronic_reg  where cust_no =''" + ccn + "''') where phone1 in(select phone1 mobileno from "
                    + "openrowset('SQLOLEDB','SERVER=172.16.200.100;UID=sa;Pwd=hyd','select * from atmchron..chronic_reg  where cust_no =''" + ccn + "'''))";
            out.println(sql);
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            out.println(e.getLocalizedMessage());
        } finally {
            close(con);
        }
        return false;
    }

    public static boolean checkUHIDinHSPDB(String uhid) {
        Connection con = null;
        try {
            con = getHSPConnection();
            Statement stmt = con.createStatement();
            String sql = "select REGISTRATION_NUMBER as uhid,patientname firstname,'Mr./Mrs.'lastname,gender, age,contact mobileno,address1 addr1,address2 addr2, city,pincode zip from OPENQUERY (EHISCHNSEP,'select P.UHID AS REGISTRATION_NUMBER, (P.FIRSTNAME || '' '' || P.MIDDLENAME || '' '' || P.LASTNAME) AS PATIENTNAME, (SELECT UPPER(GM.GENDERTYPE) FROM EHIS.GENDERMASTER GM WHERE GM.GENDERID = P.GENDER) AS GENDER,        FLOOR(MONTHS_BETWEEN(SYSDATE, P.BIRTHDATE) / 12) AS AGE, (SELECT UPPER(CM.CITYNAME) FROM EHIS.citymaster CM WHERE CM.cityID = adm.city) AS CITY, ADM.address1,ADM.address2,ADM.pincode,locationid, substr(COALESCE(ADM.MOBILENUMBER, ADM.EMERGENCYNUMBER, ADM.CONTACTNUMBER1),4,10) CONTACT,UPPER(ADM.PRIMARYEMAIL) AS EMAIL, (select max(TO_CHAR(IM.DATEOFADMISSION,''YYYY-MM-DD HH:MM:SS'')) from adt.inpatientmaster im where im.uhid=p.uhid) AS DATEOFADMISSION, (select max(TO_CHAR(IM.DISCHARGEDATE,''YYYY-MM-DD HH:MM:SS'')) from adt.inpatientmaster im where im.uhid=p.uhid) AS DISCHARGEDATE, (select max(IM.INPATIENTNO) from adt.inpatientmaster im where im.uhid=p.uhid) AS INPATIENTNO,case when(select max(IM.INPATIENTNO) from adt.inpatientmaster im where im.uhid=p.uhid) is null then ''OP'' else ''IP'' end Type, TO_CHAR(p.LOCATIONID) REGSLOCATION,(SELECT UPPER(ED.LEVELDETAILNAME) FROM EHIS.COA_STRUCT_DETAILS ED WHERE ED.CHARTID = P.LOCATIONID) AS REGISTERPLACE from REGISTRATION.PATIENT P inner join REGISTRATION.ADDRESSMASTER adm on ADM.REGISTRATIONID = P.REGISTRATIONID AND ADM.ADDRESSTYPEID = 2 and  P.UHID in(''" + uhid + "'')')";
            out.println(sql);
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            out.println(e.getLocalizedMessage());
        } finally {
            close(con);
        }
     
        return false;
    }
}
