/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ccm.logic.model;

import ccm.dao.bean.connectivity;
import static ccm.dao.bean.connectivity.close;
import static ccm.dao.bean.connectivity.getLDBConnection;
import ccm.logic.bean.shops;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Pitchu
 * @created 30 Oct, 2014 11:11:25 AM
 */
@ManagedBean(name = "sl")
@SessionScoped
public class shopList {

    private String region;
    private String shopid;

    public String getShopid() {
        return shopid;
    }

    public void setShopid(String shopid) {
        this.shopid = shopid;
    }
    List<shops> shop = new ArrayList<shops>();

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public List shops() {
        Connection con = null;
        shop.clear();
        try {
            con = getLDBConnection();
            PreparedStatement ps = con.prepareStatement("select siteid,sitename from cc_apollo_site where city = ? group by siteid, sitename order by sitename");
            ps.setString(1, region);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                shop.add(new shops(rs.getInt(1), rs.getString(2).toUpperCase()));
            }
        } catch (Exception e) {
        } finally {
            close(con);
        }
        return shop;
    }

    public List<shops> getShop() {
        return shop;
    }
}
