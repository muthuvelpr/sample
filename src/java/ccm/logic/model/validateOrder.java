/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ccm.logic.model;

import ccm.dao.bean.connectivity;
import static ccm.dao.bean.connectivity.getLDBConnection;
import static java.lang.System.out;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author Lenovo
 */
public class validateOrder {

    public static boolean checkforTPOrderduplicate(String _VendordID) {
        Connection con = null;
        try {
            con = getLDBConnection();
            PreparedStatement ps = con.prepareStatement("select orderstatus from thirdpartyorderheader where orderid = ?");
            ps.setString(1, _VendordID);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                String status = rs.getString(1);
                if (status.equals("0")) {
                    try {
                        PreparedStatement ps1 = con.prepareStatement("select count(*) from cc_tporderinfo where tpordid = ? ");
                        ps1.setString(1, _VendordID);
                        ResultSet rs1 = ps1.executeQuery();
                        if (rs1.next()) {
                            int count = rs1.getInt(1);
                            if (count > 1) {
                                return false;
                            } else {
                                return true;
                            }
                        }
                    } catch (Exception e) {
                        out.println(e.getLocalizedMessage());
                    }
                } else {
                    return false;
                }
            }
        } catch (Exception e) {
            out.println(e.getLocalizedMessage());
        }
        return false;
    }
}
