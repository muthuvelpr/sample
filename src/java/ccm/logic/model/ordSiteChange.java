/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ccm.logic.model;

import ccm.dao.bean.connectivity;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import static java.lang.System.err;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.apache.commons.codec.digest.DigestUtils;

/**
 *
 * @author Lenovo
 */
public class ordSiteChange {

    public ClientResponse siteChange(String _OrdNo, String _data) {

        String SecreteKey = "omwsdpppDFSDFWEGsdhhhfsdf2312335fduer876axcnkDSFSDWEREWfdsjpkesd";
        //String SecreteKey = "tkljlkjhislkjltkeyvalklk2332jljlkue";
        String encReqData = SecreteKey + _OrdNo;
        String encdata = DigestUtils.sha512Hex(encReqData);
        System.out.println(encdata);
        String url = "https://dose.practo.com/api/v1/vendors/orders/updates";
        //String url = "https://dose-ordera.practodev.com/api/v1/vendors/orders/updates";
        Client client = Client.create();
        WebResource webResource = client.resource(url);

        ClientResponse resp = null;
        resp = webResource.header("Content-Type", "application/json;charset=UTF-8")
                .header("Authorization", "Auth-Token," + encdata)
                .post(ClientResponse.class);
        webResource.setProperty(_data, url);
        if (resp.getStatus() != 200) {
            err.println("Unable to connect to the server");
        }
        String output = resp.getEntity(String.class);
        System.out.println("response: " + output);

        return resp;
    }

    public static boolean checkforSiteChange(String _ordNo) {
        Connection con = null;
        try {
            con = connectivity.getLDBConnection();
            PreparedStatement ps = con.prepareStatement("select * from cc_ordhead where ordno = ? and status in (0,5)");
            ps.setString(1, _ordNo);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {

        }
        return false;
    }

}
