/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ccm.logic.model;

import ccm.dao.bean.connectivity;
import static ccm.dao.bean.connectivity.getLDBConnection;
import static java.lang.System.out;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * @created for APOLLO PHARMACY DPAPP
 * @author PITCHU
 * @created Apr 5, 2017 7:21:51 AM
 */
public class orderProcessCheck {

    public static boolean checkOrderStatus(String _ordNo) {
        Connection con = null;
        try {
            con = getLDBConnection();
            PreparedStatement ps = con.prepareStatement("select * from CC_OrdProcessLog where ordno = ?");
            ps.setString(1, _ordNo);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            out.println(e.getLocalizedMessage());
        }
        return true;
    }
}
