/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ccm.dao.bean;

import static java.lang.Class.forName;
import static java.lang.System.out;
import java.sql.Connection;
import static java.sql.DriverManager.getConnection;
import java.sql.SQLException;

/**
 *
 * @author Pitchu
 */
public class connectivity {

    public static Connection getLDBConnection() throws SQLException, ClassNotFoundException {
        try {
            forName("net.sourceforge.jtds.jdbc.Driver");
            //Connection Ccon = getConnection("jdbc:jtds:sqlserver://172.16.2.250:1433/CCDB;user=web;password=Apollo@8901n");
//            Connection Ccon = getConnection("jdbc:jtds:sqlserver://172.16.2.250:1433/CCDB;user=sa;password=");
            Connection Ccon = getConnection("jdbc:jtds:sqlserver://172.16.2.241:1433/WEB;user=web;password=(M5W36");
            //Connection Ccon = getConnection("jdbc:jtds:sqlserver://localhost:1433/CCDB;user=sa;password=");
            return Ccon;
        } catch (SQLException e) {
            out.println(e.getLocalizedMessage());
            return null;
        } catch (ClassNotFoundException e) {
            out.println(e.getLocalizedMessage());
            return null;
        }
    }

    public static Connection getCMSDBConnection() throws SQLException, ClassNotFoundException {
        try {
            forName("net.sourceforge.jtds.jdbc.Driver");
            //Connection Ccon = DriverManager.getConnection("jdbc:jtds:sqlserver://127.0.0.1:1433/CCDB;user=sa;password=");
            Connection Ccon = getConnection("jdbc:jtds:sqlserver://172.16.2.241:1433/WEB;user=web;password=(M5W36");
//            Connection Ccon = DriverManager.getConnection("jdbc:jtds:sqlserver://172.16.2.250:1433/CCDB;user=sa;password=");
            //Connection Ccon = DriverManager.getConnection("jdbc:jtds:sqlserver://192.168.13.147:1433/CCDB;user=sa;password=");
            return Ccon;
        } catch (SQLException e) {
            out.println(e.getLocalizedMessage());
            return null;
        } catch (ClassNotFoundException e) {
            out.println(e.getLocalizedMessage());
            return null;
        }
    }

//    public static Connection getGDBConnection() throws SQLException, ClassNotFoundException {
//        try {
//            forName("net.sourceforge.jtds.jdbc.Driver");
//            //Connection Ccon = DriverManager.getConnection("jdbc:jtds:sqlserver://localhost:1433/CCDB;user=sa;password=");
//            Connection Ccon = getConnection("jdbc:jtds:sqlserver://192.168.0.137:1433/GOLDDB;user=sa;password=");
//            return Ccon;
//        } catch (SQLException e) {
//            out.println(e.getLocalizedMessage());
//            return null;
//        } catch (ClassNotFoundException e) {
//            out.println(e.getLocalizedMessage());
//            return null;
//        }
//    }
//    public static Connection getHHWConnection() throws SQLException, ClassNotFoundException {
//        try {
//            forName("net.sourceforge.jtds.jdbc.Driver");
//            //Connection Ccon = DriverManager.getConnection("jdbc:jtds:sqlserver://localhost:1433/Healthhiway;user=sa;password=");
//            Connection Ccon = getConnection("jdbc:jtds:sqlserver://172.16.2.241:1433/WEB;user=web;password=(M5W36");
//            return Ccon;
//        } catch (SQLException e) {
//            out.println(e.getLocalizedMessage());
//            return null;
//        } catch (ClassNotFoundException e) {
//            out.println(e.getLocalizedMessage());
//            return null;
//        }
//    }
    public static Connection getHSPConnection() throws SQLException, ClassNotFoundException {
        try {
            forName("net.sourceforge.jtds.jdbc.Driver");
            //Connection Ccon = DriverManager.getConnection("jdbc:jtds:sqlserver://localhost:1433/Healthhiway;user=sa;password=");
            Connection con = getConnection("jdbc:jtds:sqlserver://172.16.48.44:1433/AHISAPOLLO;user=sa;password=");
            return con;
        } catch (SQLException e) {
            out.println(e.getLocalizedMessage());
            return null;
        } catch (ClassNotFoundException e) {
            out.println(e.getLocalizedMessage());
            return null;
        }
    }

    public static Connection getSSOOTPConnection() throws SQLException, ClassNotFoundException {
        try {
            forName("net.sourceforge.jtds.jdbc.Driver");
            //Connection Ccon = DriverManager.getConnection("jdbc:jtds:sqlserver://localhost:1433/Healthhiway;user=sa;password=");
            Connection con = getConnection("jdbc:jtds:sqlserver://172.16.1.178:1433/POSDB;user=apposcr;password=2#06A9a");
            return con;
        } catch (SQLException e) {
            out.println(e.getLocalizedMessage());
            return null;
        } catch (ClassNotFoundException e) {
            out.println(e.getLocalizedMessage());
            return null;
        }
    }
//    public static Connection getProdServerGOLDConnection() throws SQLException, ClassNotFoundException {
//        try {
//            forName("oracle.jdbc.OracleDriver");
//            String url = "jdbc:oracle:thin:@172.16.1.100:1521:goldprod";
//            String usr = "reports";
//            String pwd = "stroper";
//            Connection Gcon = getConnection(url, usr, pwd);
//            return Gcon;
//        } catch (SQLException e) {
//            out.println(e.getLocalizedMessage());
//            return null;
//        } catch (ClassNotFoundException e) {
//            out.println(e.getLocalizedMessage());
//            return null;
//        }
//    }
//    public static Connection getGOLDDWHServerGOLDConnection() throws SQLException, ClassNotFoundException {
//        try {
//            forName("oracle.jdbc.OracleDriver");
//            String url = "jdbc:oracle:thin:@172.16.1.108:1521:golddwh";
//            String usr = "golddwh";
//            String pwd = "golddwh";
//            Connection Gcon = getConnection(url, usr, pwd);
//            return Gcon;
//        } catch (SQLException e) {
//            out.println(e.getLocalizedMessage());
//            return null;
//        } catch (ClassNotFoundException e) {
//            out.println(e.getLocalizedMessage());
//            return null;
//        }
//    }

    public static Connection getMISServerGOLDConnection() throws SQLException, ClassNotFoundException {
        try {
            forName("oracle.jdbc.OracleDriver");
            String url = "jdbc:oracle:thin:@172.16.1.108:1521:golddwh";
            String usr = "apollomis";
            String pwd = "apollomis";
            Connection Gcon = getConnection(url, usr, pwd);
            return Gcon;
        } catch (SQLException e) {
            out.println(e.getLocalizedMessage());
            return null;
        } catch (ClassNotFoundException e) {
            out.println(e.getLocalizedMessage());
            return null;
        }
    }

    public static Connection getAXConnection() {
        try {
            forName("net.sourceforge.jtds.jdbc.Driver");
            //Connection con = DriverManager.getConnection("jdbc:jtds:sqlserver://10.0.1.5:1433/WEB;user=sa;password=");
            //Connection con = getConnection("jdbc:jtds:sqlserver://10.4.2.16:1433/MicrosoftDynamicsAX_Prod;user=devusr;password=apollo@123");
            //Connection con = DriverManager.getConnection("jdbc:jtds:sqlserver://172.16.2.241:1433/WEB;user=sa;password=");
            Connection con = getConnection("jdbc:jtds:sqlserver://dbpr1.apollopharmacy.org:1433/MicrosoftDynamicsAX_Prod;user=devusr;password=apollo@123");
            return con;
        } catch (Exception ex) {
            out.println("Database.getConnection() Error -->" + ex.getMessage());
            return null;
        }
    }

    public static Connection getAX2Connection() {
        try {
            forName("net.sourceforge.jtds.jdbc.Driver");
            //Connection con = DriverManager.getConnection("jdbc:jtds:sqlserver://10.0.1.5:1433/WEB;user=sa;password=");
            //Connection con = getConnection("jdbc:jtds:sqlserver://10.4.2.16:1433/MicrosoftDynamicsAX_Prod;user=devusr;password=apollo@123");
            //Connection con = DriverManager.getConnection("jdbc:jtds:sqlserver://172.16.2.241:1433/WEB;user=sa;password=");
            Connection con = getConnection("jdbc:jtds:sqlserver://dbpr2.apollopharmacy.org:1433/MicrosoftDynamicsAXPR2;user=devusr;password=apollo@123");
            return con;
        } catch (Exception ex) {
            out.println("Database.getConnection() Error -->" + ex.getMessage());
            return null;
        }
    }

    public static Connection getMISDB1Connection() {
        try {
            forName("net.sourceforge.jtds.jdbc.Driver");
            Connection con = getConnection("jdbc:jtds:sqlserver://dbpr1.apollopharmacy.org:1433/MISDB;user=devusr;password=apollo@123");
            return con;
        } catch (Exception ex) {
            out.println("Database.getConnection() Error -->" + ex.getMessage());
            return null;
        }
    }

    public static Connection getMISDB2Connection() {
        try {
            forName("net.sourceforge.jtds.jdbc.Driver");
            Connection con = getConnection("jdbc:jtds:sqlserver://dbpr2.apollopharmacy.org:1433/MISDBPR2;user=devusr;password=apollo@123");
            return con;
        } catch (Exception ex) {
            out.println("Database.getConnection() Error -->" + ex.getMessage());
            return null;
        }
    }

    public static Connection getAzureInterfaceConnection() {
        try {
            forName("net.sourceforge.jtds.jdbc.Driver");
            Connection con = getConnection("jdbc:jtds:sqlserver://10.4.14.6:1433/HCA;user=wserv;password=Apollo@8901n");
            return con;
        } catch (Exception ex) {
            out.println("Database.getConnection() Error -->" + ex.getMessage());
            return null;
        }
    }

//    public static Connection getAXSlaveConnection() {
//        try {
//            forName("net.sourceforge.jtds.jdbc.Driver");
//            //Connection con = DriverManager.getConnection("jdbc:jtds:sqlserver://10.0.1.5:1433/WEB;user=sa;password=");
//            //Connection con = getConnection("jdbc:jtds:sqlserver://10.4.2.16:1433/MicrosoftDynamicsAXPR2;user=devusr;password=apollo@123");
//            Connection con = getConnection("jdbc:jtds:sqlserver://dbpr2.apollopharmacy.org:1433/MicrosoftDynamicsAXPR2;user=devusr;password=apollo@123");
//            //Connection con = DriverManager.getConnection("jdbc:jtds:sqlserver://172.16.2.241:1433/WEB;user=sa;password=");
//            return con;
//        } catch (Exception ex) {
//            out.println("Database.getConnection() Error -->" + ex.getMessage());
//            return null;
//        }
//    }
    public static void close(Connection con) {
        try {
            con.close();
        } catch (Exception ex) {
        }
    }
}
