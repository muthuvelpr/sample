/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Pitchu
 * @created 8 Nov, 2014 5:23:26 PM
 */
public class orderSummary {

    public String region, vendor, status;
    public int count;

    public orderSummary(String region, String vendor, String status, int count) {
        this.region = region;
        this.vendor = vendor;
        this.status = status;
        this.count = count;
    }

    public orderSummary(String vendor, String status, int count) {
        this.vendor = vendor;
        this.status = status;
        this.count = count;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

}
