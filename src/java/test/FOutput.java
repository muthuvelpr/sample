/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import java.util.List;

/**
 *
 * @author Administrator
 */
public class FOutput {

    private String Region;
    private List<Vendorwise> vend;

    public FOutput(String Region, List<Vendorwise> vend) {
        this.Region = Region;
        this.vend = vend;
    }

    public String getRegion() {
        return Region;
    }

    public void setRegion(String Region) {
        this.Region = Region;
    }

    public List<Vendorwise> getVend() {
        return vend;
    }

    public void setVend(List<Vendorwise> vend) {
        this.vend = vend;
    }

}
