/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

/**
 *
 * @author MUTHUVEL
 */
public class OAuthTest {

    private static final String URL_API
            = "https://www.martjack.com/developerapi/Order/BulkOrderCreation";

    public static void main(String[] ar) {
        Client client = Client.create();

        // Create a resource to be used to make SmugMug API calls
        WebResource resource = client.resource(URL_API);
        ClientResponse respons = resource.type("application/json").post(ClientResponse.class);
        String response = respons.getEntity(String.class);
        System.out.println(response);

    }
}
