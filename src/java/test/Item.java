package test;

import java.math.BigDecimal;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Lenovo
 */
public class Item {

    private String OrderSource;
    private int count;
    private String status;
    private String region;

    public Item(String OrderSource, int count, String status, String region) {
        this.OrderSource = OrderSource;
        this.count = count;
        this.status = status;
        this.region = region;
    }

    public Item(String status, int count) {
        this.count = count;
        this.status = status;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getOrderSource() {
        return OrderSource;
    }

    public void setOrderSource(String OrderSource) {
        this.OrderSource = OrderSource;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

}
