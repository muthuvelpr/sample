package test;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Lenovo
 */
import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import static java.util.stream.Collectors.groupingBy;

public class Java8Example2 {

    public static void main(String[] args) {

        //3 apple, 2 banana, others 1
        List<Item> items = Arrays.asList(
                new Item("APOLLOSTORE", 1, "Order Initiated", "Chennai"),
                new Item("APOLLOSTORE", 1, "Billed", "Chennai"),
                new Item("APOLLOSTORE", 1, "Delivered", "Chennai"),
                new Item("AskApollo", 29, "Order Initiated", "Chennai"),
                new Item("AskApollo", 3, "Billed", "Chennai"),
                new Item("AskApollo", 15, "Delivered", "Chennai"),
                new Item("MediAssist", 11, "Order Initiated", "Chennai"),
                new Item("MediAssist", 1, "Billed", "Chennai"),
                new Item("MediAssist", 5, "Delivered", "Chennai"),
                new Item("PRACTO", 46, "Order Initiated", "Chennai"),
                new Item("PRACTO", 14, "Billed", "Chennai"),
                //new Item("PRACTO", 28, "Cancelled", "Chennai"),
                new Item("PRACTO", 151, "Delivered", "Chennai"),
                new Item("APOLLOSTORE", 15, "Order Initiated", "Hyderabad"),
                new Item("APOLLOSTORE", 1, "Billed", "Hyderabad"),
                new Item("APOLLOSTORE", 10, "Delivered", "Hyderabad"),
                new Item("AskApollo", 29, "Order Initiated", "Hyderabad"),
                new Item("AskApollo", 3, "Billed", "Hyderabad"),
                new Item("AskApollo", 15, "Delivered", "Hyderabad"),
                new Item("MediAssist", 11, "Order Initiated", "Hyderabad"),
                new Item("MediAssist", 1, "Billed", "Hyderabad"),
                new Item("MediAssist", 5, "Delivered", "Hyderabad"),
                new Item("PRACTO", 46, "Order Initiated", "Hyderabad"),
                new Item("PRACTO", 14, "Billed", "Hyderabad"),
                //new Item("PRACTO", 28, "Cancelled", "Hyderabad"),
                new Item("PRACTO", 151, "Delivered", "Hyderabad"),
                new Item("APOLLOSTORE", 15, "Order Initiated", "Bangalore"),
                new Item("APOLLOSTORE", 1, "Billed", "Bangalore"),
                new Item("APOLLOSTORE", 10, "Delivered", "Bangalore"),
                new Item("AskApollo", 29, "Order Initiated", "Bangalore"),
                new Item("AskApollo", 3, "Billed", "Bangalore"),
                new Item("AskApollo", 15, "Delivered", "Bangalore"),
                new Item("MediAssist", 11, "Order Initiated", "Bangalore"),
                new Item("MediAssist", 1, "Billed", "Bangalore"),
                new Item("MediAssist", 5, "Delivered", "Bangalore"),
                new Item("PRACTO", 46, "Order Initiated", "Bangalore"),
                new Item("PRACTO", 14, "Billed", "Bangalore"),
                //new Item("PRACTO", 28, "Cancelled", "Bangalore"),
                new Item("PRACTO", 151, "Delivered", "Bangalore")
        );

        //group by price
        Map<String, List<Item>> groupByPriceMap
                = items.stream().collect(Collectors.groupingBy(Item::getStatus));

        //System.out.println(groupByPriceMap);
        // group by price, uses 'mapping' to convert List<Item> to Set<String>
        Map<String, Long> result = items.stream().collect(
                Collectors.groupingBy(Item::getStatus, Collectors.counting()));

        System.out.println(result);
        Map<String, Long> sum = items.stream().collect(
                Collectors.groupingBy(Item::getOrderSource, Collectors.counting()));

        System.out.println(sum);

        Map<String, Long> regSum = items.stream().collect(
                Collectors.groupingBy(Item::getRegion, Collectors.counting()));
        //Collectors.groupingBy(Item::getRegion, Collectors.summingInt(Item::getQty)));
        System.out.println(regSum);

        List<FOutput> output = new ArrayList<>();
        List<Vendorwise> voutput = new ArrayList<>();
        List<Item> ioutput = new ArrayList<>();
        Map<String, Map<String, List<Item>>> personsByCountryAndCity = items.stream().collect(
                groupingBy(Item::getRegion,
                        groupingBy(Item::getOrderSource)
                )
        );
        //Map<String, String> map = getMapStream().collect(Collectors.toList( -> x.getName(), x -> x.getCode()));
        for (Map.Entry<String, Map<String, List<Item>>> entry : personsByCountryAndCity.entrySet()) {
            // System.out.println("Region = " + entry.getKey()                    + ", Value = " + entry.getValue());
            System.out.println(entry.getKey());
            voutput.clear();
            for (Map.Entry<String, List<Item>> v : entry.getValue().entrySet()) {
                //System.out.println(v.getKey());
                ioutput.clear();
                for (Item i : v.getValue()) {
                    //System.out.println(i.getStatus() + " - " + i.getCount());
                    ioutput.add(new Item(i.getStatus(), i.getCount()));
                }
                voutput.add(new Vendorwise(v.getKey(), ioutput));

            }
            output.add(new FOutput(entry.getKey(), voutput));

            Gson g = new Gson();
            System.out.println(g.toJson(output));
            System.out.println("\n");
            //voutput.add(new Vendorwise(entry.getKey(), entry.getValue()));
        }

        //System.out.println (personsByCountryAndCity);
        //System.out.println("Persons living in London: " + personsByCountryAndCity.get("UK").get("London").size());
//for(Item p: personsByCountryAndCity)
    }

}
