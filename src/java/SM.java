/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import static java.lang.System.getProperties;
import java.util.Properties;
import javax.mail.Message;
import static javax.mail.Message.RecipientType.TO;
import javax.mail.Session;
import static javax.mail.Session.getDefaultInstance;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Administrator
 */
public class SM extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        res.setContentType("text/html");
        PrintWriter out = res.getWriter();
        String ordNo = req.getParameter("ordNo").toString();
        String ForThis = req.getParameter("ForThis").toString();
        try {
            String host = "smtp.gmail.com",
                    user = "itd@apollopharmacy.org",
                    pass = "apollo@741";

            String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";

            String mailid = req.getParameter("to");
            String[] to = mailid.split(",");
            if (mailid == null && ForThis.equals("SHOP")) {
                res.sendRedirect("mail.jsp?ordNo=" + ordNo + "&ForThis=Customer");
            } else if (mailid == null && ForThis.equals("Customer")) {
                out.println("<script type=text/javascript>{alert(\"Order No (" + ordNo + ") has been raised\");window.location = \"hoLander.jsf\";}</script>");
            } else {
                String from = "itd@apollopharmacy.org";
                String subject = req.getParameter("subject").toString();
                String messageText = req.getParameter("message").toString();
                //String mailidcc = "balaji_c@apollohospitals.com";
                //String[] cc = mailidcc.split(",");
                boolean sessionDebug = true;
                Properties props = getProperties();
                props.put("mail.host", host);
                props.put("mail.transport.protocol.", "smtp");
                props.put("mail.smtp.auth", "true");
                props.put("mail.smtp.", "true");
                props.put("mail.smtp.port", "465");
                props.put("mail.smtp.socketFactory.fallback", "false");
                props.put("mail.smtp.socketFactory.class", SSL_FACTORY);
                Session mailSession = getDefaultInstance(props, null);
                mailSession.setDebug(sessionDebug);
                Message msg = new MimeMessage(mailSession);
                msg.setFrom(new InternetAddress(from));
                for (String item : to) {
                    InternetAddress toa = new InternetAddress(item);
                    msg.addRecipient(TO, toa);
                }
                msg.setSubject(subject);
                msg.setContent(messageText, "text/html");
                Transport transport = mailSession.getTransport("smtp");
                transport.connect(host, user, pass);
                try {
                    transport.sendMessage(msg, msg.getAllRecipients());
                } catch (Exception err) {
                    out.println("Error" + err.getMessage());
                }
                transport.close();
            }
        } catch (Exception e) {
            out.println(e.getLocalizedMessage());
        }

        if (ForThis.equals(
                "SHOP")) {
            //res.sendRedirect("mail.jsp?ordNo=" + ordNo + "&ForThis=Customer");
            out.println("<script type=text/javascript>{alert(\"Order No (" + ordNo + ") has been raised\");window.location = \"hoLander.jsf\";}</script>");
        } else if (ForThis.equals(
                "Customer")) {
            out.println("<script type=text/javascript>{alert(\"Order No (" + ordNo + ") has been raised\");window.location = \"hoLander.jsf\";}</script>");
        }
    }
}
