import javax.mail.*;
import javax.mail.internet.*;
import javax.activation.*;
import java.util.*;
import java.io.*;

class SendAdvancedEmail
{
	final String emailInfo = "EmailInfo.properties";
	Properties properties = new Properties();

	public static void main(String args[])
	{
		SendAdvancedEmail sendAdvancedEmail = new SendAdvancedEmail ();
		sendAdvancedEmail.sendEmail();
	}

	private void sendEmail()
	{
		try{
			//This is required to load all the properties
			FileInputStream fileInputStream = new FileInputStream(emailInfo);
			properties.load(fileInputStream);
			fileInputStream.close();
		}catch(IOException ioe)
		{
			//throw IOException of your choice.
			//can end here
		}
		System.out.println("Email properties read successfully.");

		String smtpAddress = properties.getProperty("smtpAddress");
		String fromAddress = properties.getProperty("fromAddress");
		String toAddress = properties.getProperty("toAddress");
		String emailSubject = properties.getProperty("emailSubject");
		String emailBody = properties.getProperty("emailBody");

		Properties props = new Properties();
		props.put("mail.smtp.host", smtpAddress);
		props.put("mail.from", fromAddress);
		Session session = Session.getInstance(props, null);

		try
		{
			MimeMessage mimeMessage = new MimeMessage(session);
			mimeMessage.setRecipients(Message.RecipientType.TO,toAddress);
			mimeMessage.setSentDate(new Date());
			mimeMessage.setSubject(emailSubject);
			//The following is required to add attachments
			MimeMultipart multipart = new MimeMultipart();
			//Creating the text part of the email message
			BodyPart bodyPart = new MimeBodyPart();
			//The type is set to "text/plain"
			bodyPart.setText(emailBody);
			//Creating the attachment part of the email message
			BodyPart attachment = new MimeBodyPart();
			DataSource source = new FileDataSource(emailInfo);
			attachment.setDataHandler(new DataHandler(source));
			attachment.setFileName(emailInfo);
			//attachment.setContent(properties, "text/html");
			//Now combining the email message and the attachment
			multipart.addBodyPart(bodyPart);
			multipart.addBodyPart(attachment);
			//Setting the message with the multipart just created
			mimeMessage.setContent(multipart);

			System.out.println("Sending e-mail...");
			Transport.send(mimeMessage);
			System.out.println("e-mail sent.");
		}
		catch(MessagingException me)
		{
			System.out.println("e-mail send failed."+me);
			me.getMessage();
		}
	}
}