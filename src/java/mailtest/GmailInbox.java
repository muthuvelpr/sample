/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mailtest;

/**
 *
 * @author Lenovo
 */
import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Store;
import com.sun.mail.util.MailSSLSocketFactory;

public class GmailInbox {

    public static void main(String[] args) {

        GmailInbox gmail = new GmailInbox();
        gmail.read();

    }

    public void read() {

        Properties props = new Properties();

        try {
            props.load(new FileInputStream(new File("D:\\smtp.properties")));
            MailSSLSocketFactory sf = new MailSSLSocketFactory();
            sf.setTrustAllHosts(true);
            props.put("mail.imap.ssl.trust", "smtp.gmail.com");
            props.put("mail.imap.ssl.socketFactory", sf);
            Session session = Session.getDefaultInstance(props, null);

            Store store = session.getStore("imaps");
            store.connect("smtp.gmail.com", "mmpitchu@gmail.com", "pitchup@ssw0rd5.");

            Folder inbox = store.getFolder("inbox");
            inbox.open(Folder.READ_ONLY);
            int messageCount = inbox.getMessageCount();

            System.out.println("Total Messages:- " + messageCount);

            Message[] messages = inbox.getMessages();
            System.out.println("------------------------------");

            for (int i = 0; i < 10; i++) {
                System.out.println("Mail Subject:- " + messages[i].getSubject());
            }

            inbox.close(true);
            store.close();

        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
        }
    }

}
