/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Pitchu
 * @created 11 Nov, 2014 5:53:48 PM
 */
import ccm.dao.bean.connectivity;
import static ccm.dao.bean.connectivity.getLDBConnection;
import java.io.Serializable;
import static java.lang.System.out;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.faces.bean.ManagedBean;

import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import static org.primefaces.model.chart.AxisType.X;
import static org.primefaces.model.chart.AxisType.Y;
import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.HorizontalBarChartModel;

@ManagedBean
public class ChartView implements Serializable {

    private BarChartModel myPANIndiaAnalytics;
    private BarChartModel thisMonthTransactions;

    public BarChartModel getThisMonthTransactions() {
        return thisMonthTransactions;
    }

    public BarChartModel getMyPANIndiaAnalytics() {
        return myPANIndiaAnalytics;
    }

    public ChartView() {
        createHorizontalBarModel();
    }
    ChartSeries createdOrder = new ChartSeries();
    ChartSeries processingOrder = new ChartSeries();
    ChartSeries deliveredOrder = new ChartSeries();

    private void createHorizontalBarModel() {
        myPANIndiaAnalytics = new HorizontalBarChartModel();
        Connection con = null;
        try {
            con = getLDBConnection();
            PreparedStatement ps = con.prepareStatement("select city,sum(ordcount) ordcount,0 status from(select dc_site region,city,count(ordno) ordcount,status from cc_apollo_site o left outer join cc_ordhead h on o.siteid=h.siteid and status=0 group by dc_site,status,city) a group by city union all select city,sum(ordcount) ordcount,1 status from(select dc_site region,city,count(ordno) ordcount,status from cc_apollo_site o left outer join cc_ordhead h on o.siteid=h.siteid and status=1 group by dc_site,status,city) a group by city union all select city,sum(ordcount) ordcount,2 status from(select dc_site region,city,count(ordno) ordcount,status from cc_apollo_site o left outer join cc_ordhead h on o.siteid=h.siteid and status=2 group by dc_site,status,city) a group by city order by 3");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int status = rs.getInt(3);
                String Reg = rs.getString(1).toString().toUpperCase();
                int Count = rs.getInt(2);
                if (status == 0) {
                    createdOrder.set(Reg, Count);
                    out.println("created - " + Reg + " , " + Count);
                } else if (status == 1) {
                    processingOrder.set(Reg, Count);
                    out.println("processing - " + Reg + " , " + Count);
                } else if (status == 2) {
                    deliveredOrder.set(Reg, Count);
                    out.println("delivered - " + Reg + " , " + Count);
                }
            }
        } catch (Exception e) {
        }
        createdOrder.setLabel("CREATED ORDERS");
        processingOrder.setLabel("PROCESSING ORDERS");
        deliveredOrder.setLabel("DELIVERED ORDERS");
        //myPANIndiaAnalytics.setBarPadding(1);
        //myPANIndiaAnalytics.setBarMargin(1);
        myPANIndiaAnalytics.setLegendCols(2);
        myPANIndiaAnalytics.setLegendRows(2);

        myPANIndiaAnalytics.addSeries(createdOrder);
        myPANIndiaAnalytics.addSeries(processingOrder);
        myPANIndiaAnalytics.addSeries(deliveredOrder);

        myPANIndiaAnalytics.setAnimate(true);
        myPANIndiaAnalytics.setTitle("PAN India Analytics");
        myPANIndiaAnalytics.setLegendPosition("ne");
        myPANIndiaAnalytics.setStacked(true);

        Axis xAxis = myPANIndiaAnalytics.getAxis(X);
        xAxis.setLabel("Orders");

        Axis yAxis = myPANIndiaAnalytics.getAxis(Y);
        yAxis.setLabel("Regions");
//        yAxis.setMin(0);
//        yAxis.setMax(350);
    }

}
