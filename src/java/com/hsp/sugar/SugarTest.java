/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hsp.sugar;

import com.sun.jersey.api.client.Client;
import static com.sun.jersey.api.client.Client.create;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import static java.lang.System.err;

/**
 *
 * @author pitchaiah_m
 */
public class SugarTest {

    public static void main(String args[]) {
        String url = "http://220.225.218.48:8395/api/Surgery/SurgeryDetails";
        Client restClient = create();
        WebResource webResource = restClient.resource(url);
        String input = "{\"RegionID\": 1,\"FromDate\": \"22-Oct-2018 10:00:00\",\"ToDate\":\"23-Oct-2018 20:00:00\"}";

        ClientResponse resp = webResource
                .header("AuthKey", "881A4A97-3E56-4CFB-A866-550C4D8A9ED8")
                .accept("application/json")
                .post(ClientResponse.class, input);
        if (resp.getStatus() != 200) {
            err.println("Unable to connect to the server");
        }
        String output = resp.getEntity(String.class);
        System.out.println("response: " + output);

    }
}
