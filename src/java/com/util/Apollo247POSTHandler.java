/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.util;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import java.io.IOException;

/**
 *
 * @author Administrator
 */
public class Apollo247POSTHandler {

    String Apollo247_BASE_URL = "https://api.apollo247.com/";

    public String POSTRequest(String _payLoad) throws IOException {
        String response = "";
        String token = "3d1833da7020e0602165529446587434";
        try {
            Client client = Client.create();
            WebResource webResource = client.resource(Apollo247_BASE_URL);
            ClientResponse respons = webResource.type("application/json")
                    .header("authorization", "Bearer " + token)
                    .post(ClientResponse.class, _payLoad);
            response = respons.getEntity(String.class);

        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
        }
        return response;
    }
}
