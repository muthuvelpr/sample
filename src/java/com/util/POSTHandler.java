/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 *
 * @author Administrator
 */
public class POSTHandler {

    public String POSTRequest(String _payLoad) throws IOException {
        String response = "";
        try {
            URL url = new URL("http://172.16.2.251:8083/IEngine/webresources/Inventory/getDeliveryDate");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Authentication", "YXV0aF91c2VyOnN1cGVyc2VjcmV0X3Rhd");
            OutputStream os = conn.getOutputStream();
            os.write(_payLoad.getBytes());
            os.flush();
            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));
            String output;
            StringBuffer sb = new StringBuffer();
            while ((output = br.readLine()) != null) {
                response = sb.append(output).toString();
            }
            conn.disconnect();
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
        }
        return response;
    }
}
