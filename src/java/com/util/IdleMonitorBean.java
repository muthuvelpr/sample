/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.util;

import static ccm.logic.bean.Util.getSession;
import ccm.logic.ctlr.appLander;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Administrator
 */
@ManagedBean
@RequestScoped
public class IdleMonitorBean {

    public void welcomeListener() {
        FacesContext.getCurrentInstance().addMessage(
                null,
                new FacesMessage(FacesMessage.SEVERITY_WARN, "Welcome Back",
                        "Continue your works."));

    }

    public String logoutListener() {
        FacesContext.getCurrentInstance().addMessage(
                null,
                new FacesMessage(FacesMessage.SEVERITY_WARN,
                        "You Have Logged Out!",
                        "Thank you for using abc Online Financial Services"));

        HttpSession session = getSession();
        session.invalidate();
        return "login?faces-redirect=true";
        // invalidate session, and redirect to other pages
    }
}
