/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.util;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import java.io.IOException;

/**
 *
 * @author MUTHUVEL
 */
public class OnlinePOSTHandler {

    String Online_BASE_URL = "https://www.apollopharmacy.in/rest/V1/SomsReverseIntegration/";

    public String OnlineReverseEngineering(String _payLoad, String _action) throws IOException {
        String response = "";
        try {
            Client client = Client.create();
            WebResource webResource = client.resource(Online_BASE_URL + _action);
            ClientResponse respons = webResource.type("application/json").post(ClientResponse.class, _payLoad);
            response = respons.getEntity(String.class);

        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
        }
        return response;
    }

}
