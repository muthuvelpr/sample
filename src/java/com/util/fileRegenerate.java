/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.util;

import ccm.dao.bean.connectivity;
import static ccm.dao.bean.connectivity.getLDBConnection;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import static java.lang.System.out;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.apache.commons.net.ftp.FTPClient;

/**
 *
 * @author Lenovo
 */
public class fileRegenerate {

    public static void main(String args[]) {
        int OrdNo = 1000006501;
        String _vendName = "AskApollo";
        String shopid = "16306";
        String _VendordID = "201802061319286714";
        String _shipping = "STOREPICKUP";
        String _payment = "COD";
        String origin = "V";
        int patid = 35889;
        ordInitFTPProcess(OrdNo, shopid, _vendName, _VendordID, _shipping, _payment, origin, patid);
    }

    public static void ordInitFTPProcess(int OrdNo, String shopid, String _vendName, String _VendordID, String _shipping, String _payment, String origin, int patid) {
        FTPClient client = new FTPClient();
        FileInputStream fis = null;
        if (_VendordID == "" || _VendordID == null) {
            _VendordID = "0";
        } else if (_shipping == "" || _shipping == null) {
            _shipping = "0";
        } else if (_payment == "" || _payment == null) {
            _payment = "";
        } else if (_vendName == "" || _vendName == null) {
            _vendName = "";
        }
        try {
            client.connect("172.16.1.16");
            client.login("goldprod", "G$pr0d@p0110");
            //client.login("posuser", "p0sUs3r");
            try {
                try {
                    client.changeWorkingDirectory("data/shop/" + shopid);
                    FileWriter fstream = new FileWriter(origin + OrdNo + "_" + shopid + ".AP");
                    BufferedWriter outr = new BufferedWriter(fstream);
                    try {
                        Connection conn = null;
                        conn = getLDBConnection();
                        PreparedStatement ps = conn.prepareStatement("SELECT ORDNO,ORDDATE,FIRSTNAME CUSTNAME,MOBILENO,GENDER,AGE,COMMADDR,siteID,CCCARDNO,UHID FROM CC_ORDHEAD,CC_Patient_Registration WHERE patId=PATIENTID and patID = ? AND ordNo = ?");
                        ps.setInt(1, patid);
                        ps.setInt(2, OrdNo);
                        ResultSet rs0 = ps.executeQuery();
                        Connection con = getLDBConnection();
                        PreparedStatement pst = con.prepareStatement("SELECT A.ORDNO,artCode,artName,reqqoh,MRP FROM CC_ORDDET A,CC_ORDHEAD B,CC_Patient_Registration WHERE A.ordNo= ? AND  patId=PATIENTID AND A.ordNo=B.ORDNO");
                        pst.setInt(1, OrdNo);
                        out.println("Writing data to the Text File");
//                        System.out.println("<br>");
                        ResultSet rst = pst.executeQuery();
                        while (rs0.next()) {
                            if (origin.equalsIgnoreCase("C")) {
                                outr.write(origin + rs0.getString(1) + "|"
                                        + rs0.getString(2) + "|" + rs0.getString(3) + "|"
                                        + rs0.getString(4) + "|" + rs0.getString(5) + "|"
                                        + rs0.getString(6) + "|" + rs0.getString(7) + ","
                                        + "" + "|" + "Apollo" + "|"
                                        + "" + "|" + "" + "|" + "0" + "|" + "0" + "|" + "0" + "|" + "" + "|" + rs0.getString(8) + "|" + ""
                                        + "|" + "5" + "|" + "" + "|" + rs0.getString(9)
                                        + "\n");
                            } else if (origin.equalsIgnoreCase("U")) {
                                outr.write(origin + rs0.getString(1) + "|"
                                        + rs0.getString(2) + "|" + rs0.getString(3) + "|"
                                        + rs0.getString(4) + "|" + rs0.getString(5) + "|"
                                        + rs0.getString(6) + "|" + rs0.getString(7) + ","
                                        + "" + "|" + "Apollo" + "|"
                                        + "" + "|" + "" + "|" + "0" + "|" + "0" + "|" + "0" + "|" + "" + "|" + rs0.getString(8) + "|" + ""
                                        + "|" + "5" + "|" + "" + "|" + rs0.getString(10)
                                        + "\n");
                            } else if (origin.equalsIgnoreCase("P")) {
                                outr.write(origin + rs0.getString(1) + "|"
                                        + rs0.getString(2) + "|" + rs0.getString(3) + "|"
                                        + rs0.getString(4) + "|" + rs0.getString(5) + "|"
                                        + rs0.getString(6) + "|" + rs0.getString(7) + ","
                                        + "" + "|" + "Apollo" + "|"
                                        + "" + "|" + "" + "|" + "0" + "|" + "0" + "|" + "0" + "|" + "" + "|" + rs0.getString(8) + "|" + ""
                                        + "|" + "5" + "|" + "" + "|" + "0"
                                        + "\n");
                            } else if (origin.equalsIgnoreCase("V")) {
                                outr.write(origin + rs0.getString(1) + "|"
                                        + rs0.getString(2) + "|" + rs0.getString(3) + "|"
                                        + rs0.getString(4) + "|" + rs0.getString(5) + "|"
                                        + rs0.getString(6) + "|" + rs0.getString(7) + ","
                                        + "" + "|" + "Apollo" + "|"
                                        + "" + "|" + "" + "|" + _VendordID + "|" + _vendName + "|" + _payment + "|" + "" + "|" + rs0.getString(8) + "|" + ""
                                        + "|" + "5" + "|" + "" + "|" + "0"
                                        + "\n");
                                /*System.out.println(origin + rs0.getString(1) + "|"
                                                + rs0.getString(2) + "|" + rs0.getString(3) + "|"
                                                + rs0.getString(4) + "|" + rs0.getString(5) + "|"
                                                + rs0.getString(6) + "|" + rs0.getString(7) + ","
                                                + "" + "|" + "Apollo" + "|"
                                                + "" + "|" + "" + "|" + "0" + "|" + "0" + "|" + "0" + "|" + "" + "|" + rs0.getString(8) + "|" + ""
                                                + "|" + "5" + "|" + "" + "|" + "0"
                                                + "\n");*/
                            }
                            /*System.out.println("CP" + rs0.getString(1) + "|"
                                           + rs0.getString(2) + " " + rs0.getString(3) + "|"
                                           + rs0.getString(4) + "|" + rs0.getString(5) + "|"
                                           + rs0.getString(6) + "|" + rs0.getString(7) + ","
                                           + "" + "|" + "" + "|"
                                           + "" + "|" + "0" + "|"
                                           + "0" + "|" + "0" + "|" + "" + "|" + rs0.getString(8) + "|" + ""
                                           + "|" + "5" + "|" + "" + "|" + "0"
                                           + "\n");*/
                        }
                        int i = 1;
                        while (rst.next()) {
                            outr.write(origin + rst.getString(1) + "|" + i + "|"
                                    + "" + "|" + rst.getString(2) + "|"
                                    + rst.getString(3) + "|" + rst.getString(4) + "|"
                                    + "" + "|" + "" + "|"
                                    + "" + "|"
                                    + "\n");
                            out.println("CP" + rst.getString(1) + "|" + i + "|"
                                    + rst.getString(2) + "|" + rst.getString(2) + "|"
                                    + rst.getString(3) + "|" + rst.getString(4) + "|"
                                    + "" + "|" + "" + "|"
                                    + "" + "|"
                                    + "\n");
                            i++;
                        }
                        outr.close();
                    } catch (Exception e) {
                        out.println("Hi i'm here" + e.getLocalizedMessage());
                    }
                    out.println("File Updated with Data");
                    String filename = origin + OrdNo + "_" + shopid + ".AP";
                    fis = new FileInputStream(filename);
                    try {
                        client.enterLocalPassiveMode();
                        client.storeFile(filename, fis);
                    } catch (Exception e) {
                        out.println("e1" + e.getLocalizedMessage());
                    }
                    client.logout();
                } catch (Exception fe) {
                    out.println("er" + fe.getLocalizedMessage());
                    out.println(fe.getStackTrace() + "/n");

                }
            } catch (Exception e) {
                out.println("Error: " + e.getLocalizedMessage());
                out.println(e.getStackTrace());
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (fis != null) {
                    fis.close();
                    //System.out.println("<br>");
                }
                client.disconnect();
            } catch (Exception e) {
                out.println(e.getLocalizedMessage());
            }
        }
        makeTRG(OrdNo, origin, shopid);
    }

    public static void makeTRG(int OrdNo, String origin, String shopid) {
        FTPClient client = new FTPClient();
        FileInputStream fis = null;
        try {
            client.connect("172.16.1.16");
            client.login("goldprod", "G$pr0d@p0110");
            try {
                try {
                    client.changeWorkingDirectory("data/shop/" + shopid);
                    FileWriter fstream = new FileWriter(origin + OrdNo + "_" + shopid + ".TRG");
                    BufferedWriter outr = new BufferedWriter(fstream);
                    //System.out.println("File Updated with Data");
                    String filename = origin + OrdNo + "_" + shopid + ".TRG";
                    fis = new FileInputStream(filename);
                    try {
                        client.enterLocalPassiveMode();
                        client.storeFile(filename, fis);
                    } catch (Exception e) {
                        out.println("e1" + e.getLocalizedMessage());
                    }
                    client.logout();
                } catch (Exception fe) {
                    out.println("er" + fe.getLocalizedMessage());
                    out.println(fe.getStackTrace() + "/n");

                }
            } catch (Exception e) {
                out.println("Error: " + e.getLocalizedMessage());
                out.println(e.getStackTrace());
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (fis != null) {
                    fis.close();
                    //System.out.println("<br>");
                }
                client.disconnect();
            } catch (Exception e) {
                out.println(e.getLocalizedMessage());
            }
        }
    }

}
