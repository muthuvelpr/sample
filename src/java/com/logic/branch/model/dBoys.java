/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.logic.branch.model;

/**
 *
 * @author Pitchu
 * @created 27 Nov, 2014 6:18:40 PM
 *
 */
public class dBoys {

    private String dboyname, dboyid;

    public dBoys(String dboyname, String dboyid) {
        this.dboyname = dboyname;
        this.dboyid = dboyid;
    }

    public String getDboyid() {
        return dboyid;
    }

    public void setDboyid(String dboyid) {
        this.dboyid = dboyid;
    }

    public String getDboyname() {
        return dboyname;
    }

    public void setDboyname(String dboyname) {
        this.dboyname = dboyname;
    }
}
