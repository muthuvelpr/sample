/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.logic.branch.model;

/**
 *
 * @author Pitchu
 * @created 27 Nov, 2014 4:30:20 PM
 *
 */
public class dpMaintainance {

    private String dpName;
    private int shopid;

    public dpMaintainance(String dpName, int shopid) {
        this.dpName = dpName;
        this.shopid = shopid;
    }

    public String getDpName() {
        return dpName;
    }

    public void setDpName(String dpName) {
        this.dpName = dpName;
    }

    public int getShopid() {
        return shopid;
    }

    public void setShopid(int shopid) {
        this.shopid = shopid;
    }
}
