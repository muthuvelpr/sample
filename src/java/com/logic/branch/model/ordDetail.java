/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.logic.branch.model;

/**
 *
 * @author Pitchu
 * @created 20 Nov, 2014 11:05:16 AM
 *
 */
public class ordDetail {

    private String artcode, artname, qoh, orddt;

    public ordDetail(String artcode, String artname, String qoh, String orddt) {
        this.artcode = artcode;
        this.artname = artname;
        this.qoh = qoh;
        this.orddt = orddt;
    }

    public String getArtcode() {
        return artcode;
    }

    public void setArtcode(String artcode) {
        this.artcode = artcode;
    }

    public String getArtname() {
        return artname;
    }

    public void setArtname(String artname) {
        this.artname = artname;
    }

    public String getOrddt() {
        return orddt;
    }

    public void setOrddt(String orddt) {
        this.orddt = orddt;
    }

    public String getQoh() {
        return qoh;
    }

    public void setQoh(String qoh) {
        this.qoh = qoh;
    }
}
