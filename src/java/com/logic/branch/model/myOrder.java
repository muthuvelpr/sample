/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.logic.branch.model;

/**
 *
 * @author Pitchu
 * @created 15 Nov, 2014 6:07:19 PM
 *
 */
public class myOrder {

    private String ordNo, ordDate, patName, gender, mobno, cccardno, uhid, lat1, lon1, lat2, lon2, deliverymode, paymentmode, remarks, orderSource, commAddr;

    public myOrder(String ordNo, String ordDate, String patName, String gender, String mobno, String cccardno, String uhid, String lat1, String lon1, String lat2, String lon2, String deliverymode, String paymentmode, String remarks, String orderSource, String commAddr) {
        this.ordNo = ordNo;
        this.ordDate = ordDate;
        this.patName = patName;
        this.gender = gender;
        this.mobno = mobno;
        this.cccardno = cccardno;
        this.uhid = uhid;
        this.lat1 = lat1;
        this.lon1 = lon1;
        this.lat2 = lat2;
        this.lon2 = lon2;
        this.deliverymode = deliverymode;
        this.paymentmode = paymentmode;
        this.remarks = remarks;
        this.orderSource = orderSource;
        this.commAddr = commAddr;
    }

    public String getCommAddr() {
        return commAddr;
    }

    public void setCommAddr(String commAddr) {
        this.commAddr = commAddr;
    }

    public String getOrderSource() {
        return orderSource;
    }

    public void setOrderSource(String orderSource) {
        this.orderSource = orderSource;
    }

    public String getDeliverymode() {
        return deliverymode;
    }

    public void setDeliverymode(String deliverymode) {
        this.deliverymode = deliverymode;
    }

    public String getPaymentmode() {
        return paymentmode;
    }

    public void setPaymentmode(String paymentmode) {
        this.paymentmode = paymentmode;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getCccardno() {
        return cccardno;
    }

    public void setCccardno(String cccardno) {
        this.cccardno = cccardno;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getLat1() {
        return lat1;
    }

    public void setLat1(String lat1) {
        this.lat1 = lat1;
    }

    public String getLat2() {
        return lat2;
    }

    public void setLat2(String lat2) {
        this.lat2 = lat2;
    }

    public String getLon1() {
        return lon1;
    }

    public void setLon1(String lon1) {
        this.lon1 = lon1;
    }

    public String getLon2() {
        return lon2;
    }

    public void setLon2(String lon2) {
        this.lon2 = lon2;
    }

    public String getMobno() {
        return mobno;
    }

    public void setMobno(String mobno) {
        this.mobno = mobno;
    }

    public String getOrdDate() {
        return ordDate;
    }

    public void setOrdDate(String ordDate) {
        this.ordDate = ordDate;
    }

    public String getOrdNo() {
        return ordNo;
    }

    public void setOrdNo(String ordNo) {
        this.ordNo = ordNo;
    }

    public String getPatName() {
        return patName;
    }

    public void setPatName(String patName) {
        this.patName = patName;
    }

    public String getUhid() {
        return uhid;
    }

    public void setUhid(String uhid) {
        this.uhid = uhid;
    }
}
