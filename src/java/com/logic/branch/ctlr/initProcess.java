/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.logic.branch.ctlr;

import ccm.dao.bean.connectivity;
import static ccm.dao.bean.connectivity.close;
import static ccm.dao.bean.connectivity.getLDBConnection;
import static java.lang.Integer.parseInt;
import static java.lang.System.out;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Random;

/**
 *
 * @author Pitchu
 * @created 28 Nov, 2014 10:24:15 AM
 *
 */
public class initProcess {

    int randomInt;

    public void doInitialization(String ordNo, String edt, String dboy) {
        out.println(ordNo);
        Connection con = null;
        Random randomGenerator = new Random();
        for (int randompin = 1; randompin <= 1; ++randompin) {
            randomInt = randomGenerator.nextInt(10000);
        }
        try {
            con = getLDBConnection();
            PreparedStatement ps = con.prepareStatement("update CC_OrdHead set status = 1, edt = ?, initDate = getDate(),secreteCode = ?,delboyid=? where ordno = ?");
            ps.setString(1, edt);
            ps.setInt(2, randomInt);
            ps.setInt(3, parseInt(dboy));
            ps.setString(4, ordNo);
            ResultSet rs = ps.executeQuery();
        } catch (Exception e) {
            out.println(e.getLocalizedMessage());
        } finally {
            close(con);
        }
    }
}
