/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.logic.branch.ctlr;

import ccm.dao.bean.connectivity;
import static ccm.dao.bean.connectivity.close;
import static ccm.dao.bean.connectivity.getLDBConnection;
import static java.lang.System.out;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author Pitchu
 * @created Aug 29, 2016 5:56:36 PM
 */
public class initCancel {

     public void doorderCancel(String _ordNo, String _canRemarks) {
          out.println(_ordNo);
          Connection con = null;
          try {
               con = getLDBConnection();
               PreparedStatement ps = con.prepareStatement("update CC_OrdHead set status = 2, cancelDate = getDate(), cancelRemarks=? where ordno = ?");
               ps.setString(1, _canRemarks);
               ps.setString(2, _ordNo);
               ResultSet rs = ps.executeQuery();
          } catch (Exception e) {
               out.println(e.getLocalizedMessage());
          } finally {
               close(con);
          }
     }
}
