/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.logic.branch.ctlr;

import ccm.dao.bean.connectivity;
import static ccm.dao.bean.connectivity.getLDBConnection;
import static java.lang.System.out;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author Pitchu
 * @created 27 Nov, 2014 7:02:51 PM
 *
 */
public class initValidator {

    public static boolean checkOrderStatusbeforeOrdInit(String ordNo) {
        Connection con = null;
        try {
            con = getLDBConnection();
            PreparedStatement ps = con.prepareStatement("select status from cc_ordhead where ordno = ?");
            ps.setString(1, ordNo);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                if (rs.getString(1).toString().equals("0")) {
                    return true;
                } else {
                    return false;
                }
            }
        } catch (Exception e) {
            out.println(e.getLocalizedMessage());
        } finally {
        }
        return false;
    }

    public static boolean checkOrderStatusbeforeOrdCancel(String ordNo) {
        Connection con = null;
        try {
            con = getLDBConnection();
            PreparedStatement ps = con.prepareStatement("select status from cc_ordhead where ordno = ?");
            ps.setString(1, ordNo);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                if (rs.getString(1).toString().equals("0") || rs.getString(1).toString().equals("1")) {
                    return true;
                } else if (rs.getString(1).toString().equals("3") || rs.getString(1).toString().equals("2")) {
                    return false;
                }
            }
        } catch (Exception e) {
            out.println(e.getLocalizedMessage());
        } finally {
        }
        return false;
    }

    public static boolean checkOrderStatusbeforeclosureInit(String ordNo) {
        Connection con = null;
        try {
            con = getLDBConnection();
            PreparedStatement ps = con.prepareStatement("select status from cc_ordhead where ordno = ?");
            ps.setString(1, ordNo);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                String status = rs.getString(1);
                if (status.equals("1") || status.equals("0")) {
                    return true;
                } else {
                    return false;
                }
            }
        } catch (Exception e) {
            out.println(e.getLocalizedMessage());
        } finally {
        }
        return false;
    }
}
