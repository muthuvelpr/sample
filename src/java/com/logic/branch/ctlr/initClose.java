/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.logic.branch.ctlr;

import ccm.dao.bean.connectivity;
import static ccm.dao.bean.connectivity.close;
import static ccm.dao.bean.connectivity.getLDBConnection;
import static java.lang.System.out;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author Pitchu
 * @created 3 Dec, 2014 1:14:20 PM
 *
 */
public class initClose {

    public void doorderDelivered(String ordNo, String secCode) {
        out.println(ordNo);
        Connection con = null;
        try {
            con = getLDBConnection();
            PreparedStatement ps = con.prepareStatement("update CC_OrdHead set status = 3, delDate = getDate() where ordno = ? and secreteCode = ?");
            ps.setString(1, ordNo);
            ps.setString(2, secCode);
            ResultSet rs = ps.executeQuery();
        } catch (Exception e) {
            out.println(e.getLocalizedMessage());
        } finally {
            close(con);
        }
    }

    public void doorderDeliveredwithoutPin(String ordNo) {
        out.println(ordNo);
        Connection con = null;
        try {
            con = getLDBConnection();
            PreparedStatement ps = con.prepareStatement("update CC_OrdHead set status = 3, delDate = getDate() where ordno = ?");
            ps.setString(1, ordNo);
            ResultSet rs = ps.executeQuery();
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
        } finally {
            close(con);
        }
    }
}
