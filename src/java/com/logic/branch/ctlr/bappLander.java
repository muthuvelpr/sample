package com.logic.branch.ctlr;

import ccm.dao.bean.connectivity;
import static ccm.dao.bean.connectivity.close;
import static ccm.dao.bean.connectivity.getLDBConnection;
import ccm.logic.bean.Util;
import static ccm.logic.bean.Util.getSession;
import ccm.logic.ctlr.sms;
import ccm.logic.bean.ordHistItem;
import static com.logic.branch.ctlr.initValidator.checkOrderStatusbeforeOrdCancel;
import static com.logic.branch.ctlr.initValidator.checkOrderStatusbeforeOrdInit;
import com.logic.branch.model.dBoys;
import com.logic.branch.model.myOrder;
import static java.lang.Integer.parseInt;
import static java.lang.System.out;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.faces.application.FacesMessage;
import static javax.faces.application.FacesMessage.SEVERITY_WARN;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import static javax.faces.context.FacesContext.getCurrentInstance;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Pitchu
 * @created 15 Nov, 2014 4:19:27 PM
 */
@ManagedBean
@ViewScoped
public class bappLander {
    
    private String url;
    private String lat1;
    private String lat2;
    private String lon1;
    private String lon2;
    private String dBoy, deliverymode, paymentmode, remarks, actRemarks, orderSource;
    private Date edt = new Date();
    SimpleDateFormat sd = new SimpleDateFormat("yyyy/MM/dd hh:MM");
    
    public String getOrderSource() {
        return orderSource;
    }
    
    public void setOrderSource(String orderSource) {
        this.orderSource = orderSource;
    }
    
    public String getActRemarks() {
        return actRemarks;
    }
    
    public void setActRemarks(String actRemarks) {
        this.actRemarks = actRemarks;
    }
    
    public String getDeliverymode() {
        return deliverymode;
    }
    
    public void setDeliverymode(String deliverymode) {
        this.deliverymode = deliverymode;
    }
    
    public String getPaymentmode() {
        return paymentmode;
    }
    
    public void setPaymentmode(String paymentmode) {
        this.paymentmode = paymentmode;
    }
    
    public String getRemarks() {
        return remarks;
    }
    
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }
    
    public Date getEdt() {
        return edt;
    }
    
    public void setEdt(Date edt) {
        this.edt = edt;
    }
    
    public String getdBoy() {
        return dBoy;
    }
    
    public void setdBoy(String dBoy) {
        this.dBoy = dBoy;
    }
    
    public String getUrl() {
        return url;
    }
    
    public void setUrl(String url) {
        this.url = url;
    }
    
    public String getLat1() {
        return lat1;
    }
    
    public void setLat1(String lat1) {
        this.lat1 = lat1;
    }
    
    public String getLat2() {
        return lat2;
    }
    
    public void setLat2(String lat2) {
        this.lat2 = lat2;
    }
    
    public String getLon1() {
        return lon1;
    }
    
    public void setLon1(String lon1) {
        this.lon1 = lon1;
    }
    
    public String getLon2() {
        return lon2;
    }
    
    public void setLon2(String lon2) {
        this.lon2 = lon2;
    }
    List<myOrder> myOrd = new ArrayList<myOrder>();
    List<ordHistItem> myOrdDet = new ArrayList<ordHistItem>();
    List<dBoys> dbn = new ArrayList<dBoys>();
    private myOrder selectedrow;
    
    public myOrder getSelectedrow() {
        return selectedrow;
    }
    
    public void setSelectedrow(myOrder selectedrow) {
        this.selectedrow = selectedrow;
    }
    
    public bappLander() {
        myOrders();
    }
    
    public List myOrders() {
        HttpSession sess = getSession();
        String shopid = sess.getAttribute("usr").toString();
        myOrd.clear();
        Connection con = null;
        try {
            con = getLDBConnection();
            PreparedStatement ps = con.prepareStatement("select ordno,orddate,siteid,(select sitename from CC_Apollo_Site where siteid=a.siteid) as sitename,(select FirstName + ' ' + LastName from dbo.CC_Patient_Registration where patientid=patid) as Patname,(select gender from dbo.CC_Patient_Registration where patientid=patid) as Gender, (select mobileno from dbo.CC_Patient_Registration where patientid=patid) as Patmobileno, (select cccardno from dbo.CC_Patient_Registration where patientid=patid) as cccardno, (select uhid from dbo.CC_Patient_Registration where patientid=patid) as uhid,status, (select lat from dbo.CClogin where ltrim(usr)= ?) as blat,(select long from dbo.CClogin where ltrim(usr)= ?) as blon, (select lat2 from dbo.CC_Patient_Registration where patientid=patid)lat ,(select lon2 from dbo.CC_Patient_Registration where patientid=patid) lon,deliverymode,paymentmode,remarks,orderSource,(select CommAddr from dbo.CC_Patient_Registration where patientid=patid) as commAddr from cc_ordhead a where siteid = ? and status = 0");
            ps.setString(1, shopid);
            ps.setString(2, shopid);
            ps.setString(3, shopid);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                String ordDate = rs.getString(2).toString().substring(0, 16);
                myOrd.add(new myOrder(rs.getString(1), ordDate, rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9), rs.getString(11), rs.getString(12), rs.getString(13), rs.getString(14), rs.getString(15), rs.getString(16), rs.getString(17), rs.getString(18), rs.getString(19)));
            }
        } catch (Exception e) {
        } finally {
        }
        return myOrd;
    }
    
    public List<myOrder> getMyOrd() {
        return myOrd;
    }
    
    public List myOrderDetail() {
        Connection con = null;
        showDeliveryBoy();
        FacesContext fc = getCurrentInstance();
        Map<String, String> params = fc.getExternalContext().getRequestParameterMap();
        String ordNo = params.get("ordNo");
        myOrdDet.clear();
        try {
            con = getLDBConnection();
            PreparedStatement ps = con.prepareStatement("select artcode,artname,reqqoh,mrp from cc_orddet where ordno = ?");
            ps.setString(1, ordNo);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                String h_artcode = rs.getString(1);
                String h_artname = rs.getString(2);
                int h_qty = rs.getInt(3);
                double mrp = rs.getDouble(4);
                myOrdDet.add(new ordHistItem(h_artcode, h_artname, h_qty, parseInt(ordNo), mrp));
                out.println(h_artcode + ", " + h_artname + ", " + h_qty);
            }
        } catch (Exception e) {
            out.println(e.getLocalizedMessage());
        } finally {
            close(con);
        }
        return myOrdDet;
    }
    
    public List<ordHistItem> getMyOrdDet() {
        return myOrdDet;
    }
    
    public List showDeliveryBoy() {
        List<String> results = new ArrayList<String>();
        HttpSession sess = getSession();
        String shopid = sess.getAttribute("usr").toString();
        Connection con = null;
        dbn.clear();
        try {
            con = getLDBConnection();
            PreparedStatement ps = con.prepareStatement("select dpname,vechicleid from cc_dpmaintain where shopid = ?");
            ps.setString(1, shopid);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                dbn.add(new dBoys(rs.getString(1), rs.getString(2)));
            }
        } catch (Exception e) {
            out.println(e.getLocalizedMessage());
        }
        return dbn;
    }
    
    public List<dBoys> getDbn() {
        return dbn;
    }
    
    @SuppressWarnings("element-type-mismatch")
    public void deliveryInit() {
        FacesContext fc = getCurrentInstance();
        Map<String, String> params = fc.getExternalContext().getRequestParameterMap();
        String ordNo = params.get("ordNo");
        String ordSRC = params.get("ordSrc");
        boolean result = checkOrderStatusbeforeOrdInit(ordNo);
        if (result) {
            initProcess s1 = new initProcess();
            s1.doInitialization(ordNo, sd.format(edt).toString(), dBoy);
            if (ordSRC.equalsIgnoreCase("HAPP")||ordSRC.equalsIgnoreCase("PRACTO")) {
            } else {
                doInitializationSMS(ordNo);
            }
            myOrd.remove(selectedrow);
        } else {
            getCurrentInstance().addMessage(null, new FacesMessage(SEVERITY_WARN, "Order no: " + ordNo + " already Initiated!!!", ""));
        }
    }
    
    public void cancelOrderInit() {
        FacesContext fc = getCurrentInstance();
        Map<String, String> params = fc.getExternalContext().getRequestParameterMap();
        String ordNo = params.get("ordNo");
        String cancelFrom = params.get("cancelFrom");
        String ordSRC = params.get("ordSrc");
        boolean result = checkOrderStatusbeforeOrdCancel(ordNo);
        if (result) {
            if (cancelFrom.equals("InitOrd")) {
                initCancel can = new initCancel();
                can.doorderCancel(ordNo, actRemarks);
                if (ordSRC.equalsIgnoreCase("HAPP") || ordSRC.equals("PRACTO")) {
                } else {
                    try {
                        Connection con = null;
                        con = getLDBConnection();
                        String qry = "select mobileno from cc_patient_registration where patientid = (select patid from cc_ordhead where ordno = ?)";
                        PreparedStatement ps = con.prepareStatement(qry);
                        ps.setString(1, ordNo);
                        ResultSet rs = ps.executeQuery();
                        while (rs.next()) {
                            String MobNo = rs.getString(1);
                            if (MobNo.length() == 10) {
                                sms pushSMStoManager = new sms();
                                pushSMStoManager.sendSMSOrderCancel(MobNo, ordNo, actRemarks);
                            }
                        }
                    } catch (Exception e) {
                    }
                }
                myOrd.remove(selectedrow);
                getCurrentInstance().addMessage(null, new FacesMessage(SEVERITY_WARN, "Order no: " + ordNo + " cancelled!!!", ""));
            }
        } else {
            getCurrentInstance().addMessage(null, new FacesMessage(SEVERITY_WARN, "Could not cancel Order no: " + ordNo + "!!!", ""));
        }
    }
    
    public void doInitializationSMS(String ordNo) {
        try {
            //ccm.sms.Medical service = new ccm.sms.Medical();
            //ccm.sms.MedicalSoap port = service.getMedicalSoap12();

            Connection con = null;
            try {
                HttpSession sess = getSession();
                String shopid = sess.getAttribute("usr").toString();
                con = getLDBConnection();
                PreparedStatement ps = con.prepareStatement("select mobileno,  firstname+' '+lastname, (select  username from cclogin where usr = ?),secreteCode from cc_patient_registration a, cc_ordhead b where b.ordno = ? and a.patientid = b.patid");
                ps.setString(1, shopid);
                ps.setString(2, ordNo);
                ResultSet rs = ps.executeQuery();
                if (rs.next()) {
                    java.lang.String mobileNo = rs.getString(1);
                    java.lang.String orderNo = ordNo;
                    java.lang.String name = dBoy;
                    java.lang.String outletName = rs.getString(3);
                    java.lang.String phoneNo = "";
                    java.lang.String secreteCode = rs.getString(4);
                    java.lang.String clickHere = "clickhere";
                    sms _sms = new sms();
                    _sms.sendSMSOrderDeliveryInitimation(mobileNo, name, orderNo, name);
                    //System.out.println("Result = " + result);
                }
            } catch (Exception e) {
                out.println(e.getLocalizedMessage());
            } finally {
                close(con);
            }
        } catch (Exception ex) {
            out.println(ex.getLocalizedMessage());
        }
    }
    
    public String showRoute() {
        FacesContext fc = getCurrentInstance();
        Map<String, String> params = fc.getExternalContext().getRequestParameterMap();
        lat1 = params.get("lat1");
        lat2 = params.get("lat2");
        lon1 = params.get("lon1");
        lon2 = params.get("lon2");
        url = "http://172.16.2.251:89/Order/Direction.aspx?Lat1=" + lat1 + "&Lon1=" + lon1 + "&Lat2=" + lat2 + "&Lon2=" + lon2 + "";
        return url;
    }
}
