/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.logic.branch.ctlr;

import ccm.dao.bean.connectivity;
import static ccm.dao.bean.connectivity.close;
import static ccm.dao.bean.connectivity.getLDBConnection;
import ccm.logic.bean.Util;
import static ccm.logic.bean.Util.getSession;
import ccm.logic.ctlr.sms;
import ccm.logic.bean.ordHistItem;
import static com.logic.branch.ctlr.initValidator.checkOrderStatusbeforeOrdCancel;
import static com.logic.branch.ctlr.initValidator.checkOrderStatusbeforeclosureInit;
import com.logic.branch.model.myOrder;
import static java.lang.Integer.parseInt;
import static java.lang.System.out;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.faces.application.FacesMessage;
import static javax.faces.application.FacesMessage.SEVERITY_WARN;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import static javax.faces.context.FacesContext.getCurrentInstance;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Pitchu
 * @created 3 Dec, 2014 11:55:41 AM
 *
 */
@ManagedBean(name = "bac")
@ViewScoped
public class bappClose {

    List<myOrder> myInitiatedOrd = new ArrayList<myOrder>();
    List<ordHistItem> myOrdDet = new ArrayList<ordHistItem>();
    private myOrder selectedrow;
    private String securityPin;
    String actRemarks, orderSource;

    public String getOrderSource() {
        return orderSource;
    }

    public void setOrderSource(String orderSource) {
        this.orderSource = orderSource;
    }

    public String getActRemarks() {
        return actRemarks;
    }

    public void setActRemarks(String actRemarks) {
        this.actRemarks = actRemarks;
    }

    public String getSecurityPin() {
        return securityPin;
    }

    public void setSecurityPin(String securityPin) {
        this.securityPin = securityPin;
    }

    public myOrder getSelectedrow() {
        return selectedrow;
    }

    public void setSelectedrow(myOrder selectedrow) {
        this.selectedrow = selectedrow;
    }

    public bappClose() {
        myInitiatedOrders();
    }

    public List myInitiatedOrders() {
        HttpSession sess = getSession();
        String shopid = sess.getAttribute("usr").toString();
        myInitiatedOrd.clear();
        Connection con = null;
        try {
            con = getLDBConnection();
            PreparedStatement ps = con.prepareStatement("select ordno,orddate,siteid,(select sitename from CC_Apollo_Site where siteid=a.siteid) as sitename,(select FirstName + ' ' + LastName from dbo.CC_Patient_Registration where patientid=patid) as Patname,(select gender from dbo.CC_Patient_Registration where patientid=patid) as Gender, (select mobileno from dbo.CC_Patient_Registration where patientid=patid) as Patmobileno, (select cccardno from dbo.CC_Patient_Registration where patientid=patid) as cccardno, (select uhid from dbo.CC_Patient_Registration where patientid=patid) as uhid,status, (select lat from dbo.CClogin where ltrim(usr)= ?) as blat,(select long from dbo.CClogin where ltrim(usr)= ?) as blon, (select lat2 from dbo.CC_Patient_Registration where patientid=patid)lat ,(select lon2 from dbo.CC_Patient_Registration where patientid=patid) lon,deliverymode,paymentmode,remarks,orderSource,(select CommAddr from dbo.CC_Patient_Registration where patientid=patid) as commAddr from cc_ordhead a where siteid = ? and status in(1)");
            ps.setString(1, shopid);
            ps.setString(2, shopid);
            ps.setString(3, shopid);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                myInitiatedOrd.add(new myOrder(rs.getString(1), rs.getString(2).substring(0, 16), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9), rs.getString(11), rs.getString(12), rs.getString(13), rs.getString(14), rs.getString(15), rs.getString(16), rs.getString(17), rs.getString(18), rs.getString(19)));
            }
        } catch (Exception e) {
        } finally {
        }
        return myInitiatedOrd;
    }

    public List<myOrder> getMyInitiatedOrd() {
        return myInitiatedOrd;
    }

    public List myOrderDetail() {
        Connection con = null;

        FacesContext fc = getCurrentInstance();
        Map<String, String> params = fc.getExternalContext().getRequestParameterMap();
        String ordNo = params.get("ordNo");
        myOrdDet.clear();
        try {
            con = getLDBConnection();
            PreparedStatement ps = con.prepareStatement("select artcode,artname,reqqoh,mrp from cc_orddet where ordno = ?");
            ps.setString(1, ordNo);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                String h_artcode = rs.getString(1);
                String h_artname = rs.getString(2);
                int h_qty = rs.getInt(3);
                double mrp = rs.getDouble(4);
                myOrdDet.add(new ordHistItem(h_artcode, h_artname, h_qty, parseInt(ordNo), mrp));
                out.println(h_artcode + ", " + h_artname + ", " + h_qty);
            }
        } catch (Exception e) {
            out.println(e.getLocalizedMessage());
        } finally {
            close(con);
        }
        return myOrdDet;
    }

    public List<ordHistItem> getMyOrdDet() {
        return myOrdDet;
    }

    public void ordClosureInit() {
        FacesContext fc = getCurrentInstance();
        Map<String, String> params = fc.getExternalContext().getRequestParameterMap();
        String ordNo = params.get("ordNo");
        String security = params.get("security");
        String ordSRC = params.get("ordSrc");
        boolean result = checkOrderStatusbeforeclosureInit(ordNo);
        if (result) {
            if (security.equalsIgnoreCase("yes")) {
                boolean chk = checkSecurityPin(ordNo, securityPin);
                if (chk) {
                    initClose s1 = new initClose();
                    s1.doorderDelivered(ordNo, securityPin);
                    myInitiatedOrd.remove(selectedrow);
                    if (ordSRC.equalsIgnoreCase("Whatsapp") || ordSRC.equalsIgnoreCase("SMS") || ordSRC.equalsIgnoreCase("Call") || ordSRC.equalsIgnoreCase("TEST") || ordSRC.equalsIgnoreCase("AskApollo")) {
                        ordDelSMSInit(ordNo);
                    }
                    getCurrentInstance().addMessage(null, new FacesMessage(SEVERITY_WARN, "Order no: " + ordNo + " Delivered!!!", ""));
                } else {
                    getCurrentInstance().addMessage(null, new FacesMessage(SEVERITY_WARN, "Invalid Pin Please check the pin!!!", ""));
                }
            } else if (security.equalsIgnoreCase("no")) {
                initClose s1 = new initClose();
                s1.doorderDeliveredwithoutPin(ordNo);
                myInitiatedOrd.remove(selectedrow);
                if (ordSRC.equalsIgnoreCase("Whatsapp") || ordSRC.equalsIgnoreCase("SMS") || ordSRC.equalsIgnoreCase("Call") || ordSRC.equalsIgnoreCase("TEST") || ordSRC.equalsIgnoreCase("AskApollo")) {
                    ordDelSMSInit(ordNo);
                }
                getCurrentInstance().addMessage(null, new FacesMessage(SEVERITY_WARN, "Order no: " + ordNo + " Delivered!!!", ""));
            }
        } else {
            getCurrentInstance().addMessage(null, new FacesMessage(SEVERITY_WARN, "Order no: " + ordNo + " already Initiated!!!", ""));
        }
    }

    public static boolean checkSecurityPin(String ordNo, String securityPin) {
        Connection con = null;
        try {
            con = getLDBConnection();
            PreparedStatement ps = con.prepareStatement("select secreteCode from CC_OrdHead where ordno = ? and secreteCode = ?");
            ps.setString(1, ordNo);
            ps.setString(2, securityPin);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            out.println(e.getLocalizedMessage());
        }
        return false;
    }

    public void cancelOrderInit() {
        FacesContext fc = getCurrentInstance();
        Map<String, String> params = fc.getExternalContext().getRequestParameterMap();
        String ordNo = params.get("ordNo");
        String cancelFrom = params.get("cancelFrom");
        String ordSRC = params.get("ordSrc");
        boolean result = checkOrderStatusbeforeOrdCancel(ordNo);
        if (result) {
            if (cancelFrom.equals("RFDOrd")) {
                initCancel can = new initCancel();
                can.doorderCancel(ordNo, actRemarks);
                if (ordSRC.equalsIgnoreCase("Whatsapp") || ordSRC.equalsIgnoreCase("SMS") || ordSRC.equalsIgnoreCase("Call")) {
                    try {
                        Connection con = null;
                        con = getLDBConnection();
                        String qry = "select mobileno from cc_patient_registration where patientid = (select patid from cc_ordhead where ordno = ?)";
                        PreparedStatement ps = con.prepareStatement(qry);
                        ps.setString(1, ordNo);
                        ResultSet rs = ps.executeQuery();
                        while (rs.next()) {
                            String MobNo = rs.getString(1).toString();
                            if (MobNo.length() == 10) {
                                sms pushSMStoManager = new sms();
                                pushSMStoManager.sendSMSOrderCancel(MobNo, ordNo, actRemarks);
                            }
                        }
                    } catch (Exception e) {
                    }
                }
                myInitiatedOrd.remove(selectedrow);
                getCurrentInstance().addMessage(null, new FacesMessage(SEVERITY_WARN, "Order no: " + ordNo + " cancelled!!!", ""));
            }
        } else {
            getCurrentInstance().addMessage(null, new FacesMessage(SEVERITY_WARN, "Could not cancel Order no: " + ordNo + "!!!", ""));
        }
    }

    public void ordDelSMSInit(String ordNo) {

        try {
            //ccm.sms.Medical service = new ccm.sms.Medical();
            //ccm.sms.MedicalSoap port = service.getMedicalSoap12();
            Connection con = null;
            try {
                con = getLDBConnection();
                PreparedStatement ps = con.prepareStatement("select mobileno, FirstName +' '+ LastName from cc_patient_registration where patientid = (select patid from cc_ordhead where ordno = ?)");
                ps.setString(1, ordNo);
                ResultSet rs = ps.executeQuery();
                if (rs.next()) {
                    java.lang.String mobileNo = rs.getString(1);
                    java.lang.String name = rs.getString(2);
                    java.lang.String orderNo = ordNo;
                    if (mobileNo.length() == 10) {
                        sms _sms = new sms();
                        _sms.sendSMSOrderDelivered(mobileNo, name, orderNo, name);
                        //java.lang.String result = port.sendSMSThanksSMS(mobileNo, name, orderNo);
                        //System.out.println("Result = " + result);
                    } else {
                        out.println("Invalid Mobile No");
                    }
                }
            } catch (Exception e) {
                out.println(e.getLocalizedMessage());
            } finally {
                close(con);
            }
        } catch (Exception ex) {
            out.println(ex.getLocalizedMessage());
        }
    }
}
