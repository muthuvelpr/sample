/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.logic.lms.model;

import ccm.dao.bean.connectivity;
import com.logic.lms.bean.SiteSugession;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author pitchaiah_m
 */
public class SiteSugessionModel {

    List<SiteSugession> siteList = new ArrayList<>();

    public List<SiteSugession> ListSitesforsugession(String _postalCode) {
        try {
            siteList.clear();
            Connection con = connectivity.getAzureInterfaceConnection();
            PreparedStatement ps = con.prepareStatement("select * from CC_InventoryLookupMaster where PINCODE = ?");
            ps.setString(1, _postalCode);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                siteList.add(new SiteSugession(rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(8), rs.getString(12)));
            }
        } catch (Exception e) {

        }
        return siteList;
    }

}
