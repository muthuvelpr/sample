/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.logic.lms.model;

import com.logic.lms.bean.xbees;
import com.logic.lms.bean.xbeesitems;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author pitchaiah_m
 */
public class xbeesDataModel {

    List<xbees> xbeesData = new ArrayList<>();
    List<xbeesitems> xbeeitem = new ArrayList<>();

    public List<xbees> getAllOrderReadyforDelivery(Connection _con) {
        try {
            PreparedStatement ps = _con.prepareStatement("SELECT H.SITEID,TPD.patname,(CASE WHEN (LEN(tpd.del_add)< 5) THEN (select addr1+' '+addr2+' '+CommAddr+' ' from dbo.CC_Patient_Registration where patientid = h.patId) ELSE tpd.del_add END) as del_add,(CASE WHEN (tpd.PRICONTACT is null) THEN (select Mobileno from dbo.CC_Patient_Registration where patientid = h.patId) ELSE tpd.PRICONTACT END) as primob, tpd.SECCONTACT,h.ordno,h.PaymentMode,h.DeliveryMode,TH.is_medicine,h.invValue, ld.Name,ld.Location,ld.City,ld.ContactNumber,h.orderSource,h.ordDate,h.initDate,oi.APBILLNO,oi.CASHVALUE, oi.CREDITVALUE,oi.TOTALAMT FROM CC_ORDHEAD H, TPDELADDUPDATE TPD, CC_TPOrderInfo oi, CC_SUN_LocationDetails ld, THIRDPARTYORDERHEADER TH WHERE H.SITEID IN ('91079','16012','16335','15132','91905','15497','91803','16097','15175','15813','16469','14007','14790','16136','91858','91011','15195','16651','16303','15683','15478','16353','15815','16708','15920','15489','15554','15311','91017','15648','15524','15257','14036','15371','14045','91055','15689','16611','14189','15039','14819','15894','14617','14653','14654','16473','16495','15905','15508','14613','16095','15410','15360','15969','14514','16016','15835','16494','15993','16008','16324','16587','16311','14651','16100','14884','14800','15448','14603','16007','16456','15679','15799','15806','16388','15773') AND H.STATUS = 1 and cast( h.ordno as nvarchar(20)) = oi.APORDID and oi.TPORDID = tpd.orderid  and h.siteID = ld.SiteId AND oi.TPORDID = th.OrderId and h.orderSource in ('AskApollo','PRACTO','MediAssist')");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                String items = getAllSKUs(_con, rs.getString(6));
                //  siteid,  custName,  custAddr,  custPMobNo,  custSMobNo,  ordno,  items,  payment,  shipping,  orderType,  invVal,  siteName,  siteAddr,  City,  siteContact,  orderSource,  ordDate,  billDate,  billNo,  cash,  credit,  total
                xbeesData.add(new xbees(rs.getString(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6),
                        items,
                        rs.getString(7),
                        rs.getString(8),
                        rs.getString(9),
                        rs.getString(10),
                        rs.getString(11),
                        rs.getString(12),
                        rs.getString(13),
                        rs.getString(14),
                        rs.getString(15),
                        rs.getString(16),
                        rs.getString(17),
                        rs.getString(18),
                        rs.getString(19),
                        rs.getString(20),
                        rs.getString(21)
                ));
            }
        } catch (Exception e) {

        }
        return xbeesData;
    }

    public String getAllSKUs(Connection _con, String _ordno) {
        String items = "";
        try {
            PreparedStatement ps = _con.prepareStatement("select artcode from CC_Itemdet where ordno = ? group by artcode");
            ps.setString(1, _ordno);
            ResultSet rs = ps.executeQuery();
            StringBuffer sb = new StringBuffer();
            while (rs.next()) {
                sb.append(",").append(rs.getString(1));
            }
            items = sb.toString().replaceFirst(",", "");
        } catch (Exception e) {

        }
        return items;
    }
}
