/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.logic.lms.ctlr;

import ccm.dao.bean.connectivity;
import com.logic.lms.bean.xbees;
import com.logic.lms.model.xbeesDataModel;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author pitchaiah_m
 */
@ManagedBean(name = "xbees")
@ViewScoped
public class xbeesLMSDataController {

    List<xbees> xbeesOrders = new ArrayList<xbees>();

    /**
     * Creates a new instance of xbeesLMSDataController
     */
    public xbeesLMSDataController() {

    }

    public List<xbees> getAllOrdersReadyforLogistics() {
        Connection con = null;
        try {
            xbeesOrders.clear();
            con = connectivity.getLDBConnection();
            xbeesDataModel data = new xbeesDataModel();
            xbeesOrders = data.getAllOrderReadyforDelivery(con);
            for (xbees order : xbeesOrders) {
                System.out.println(order.getSiteid() + " , " + order.getCustName() + " , " + order.getCustAddr() + " , " + order.getCustPMobNo() + " , " + order.getCustSMobNo() + " , " + order.getOrdno() + " , " + order.getPayment() + " , " + order.getShipping() + " , " + order.getInvVal() + " , " + order.getSiteName() + " , " + order.getSiteAddr() + " , " + order.getCity() + " , " + order.getSiteContact() + " , "
                        + order.getOrdDate() + " , " + order.getBillDate() + " , " + order.getBillNo() + " , " + order.getCash() + " , " + order.getCredit() + " , " + order.getTotal());
            }
        } catch (Exception e) {

        }
        return xbeesOrders;
    }

    public List<xbees> getXbeesOrders() {
        return xbeesOrders;
    }

}
