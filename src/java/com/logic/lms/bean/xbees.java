/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.logic.lms.bean;

import java.util.List;

/**
 *
 * @author pitchaiah_m
 */
public class xbees {

    private String siteid, custName, custAddr, custPMobNo, custSMobNo, ordno, payment, shipping, orderType, invVal, siteName, siteAddr, City, siteContact, ordDate, billDate, billNo, cash, credit, total, items, orderSource;

    public xbees(String siteid, String custName, String custAddr, String custPMobNo, String custSMobNo, String ordno, String items, String payment, String shipping, String orderType, String invVal, String siteName, String siteAddr, String City, String siteContact, String orderSource, String ordDate, String billDate, String billNo, String cash, String credit, String total) {
        this.siteid = siteid;
        this.custName = custName;
        this.custAddr = custAddr;
        this.custPMobNo = custPMobNo;
        this.custSMobNo = custSMobNo;
        this.ordno = ordno;
        this.items = items;
        this.payment = payment;
        this.shipping = shipping;
        this.orderType = orderType;
        this.invVal = invVal;
        this.siteName = siteName;
        this.siteAddr = siteAddr;
        this.City = City;
        this.siteContact = siteContact;
        this.orderSource = orderSource;
        this.ordDate = ordDate;
        this.billDate = billDate;
        this.billNo = billNo;
        this.cash = cash;
        this.credit = credit;
        this.total = total;

    }

    public String getSiteid() {
        return siteid;
    }

    public void setSiteid(String siteid) {
        this.siteid = siteid;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getCustAddr() {
        return custAddr;
    }

    public void setCustAddr(String custAddr) {
        this.custAddr = custAddr;
    }

    public String getCustPMobNo() {
        return custPMobNo;
    }

    public void setCustPMobNo(String custPMobNo) {
        this.custPMobNo = custPMobNo;
    }

    public String getCustSMobNo() {
        return custSMobNo;
    }

    public void setCustSMobNo(String custSMobNo) {
        this.custSMobNo = custSMobNo;
    }

    public String getOrdno() {
        return ordno;
    }

    public void setOrdno(String ordno) {
        this.ordno = ordno;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public String getShipping() {
        return shipping;
    }

    public void setShipping(String shipping) {
        this.shipping = shipping;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getInvVal() {
        return invVal;
    }

    public void setInvVal(String invVal) {
        this.invVal = invVal;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public String getSiteAddr() {
        return siteAddr;
    }

    public void setSiteAddr(String siteAddr) {
        this.siteAddr = siteAddr;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String City) {
        this.City = City;
    }

    public String getSiteContact() {
        return siteContact;
    }

    public void setSiteContact(String siteContact) {
        this.siteContact = siteContact;
    }

    public String getOrdDate() {
        return ordDate;
    }

    public void setOrdDate(String ordDate) {
        this.ordDate = ordDate;
    }

    public String getBillDate() {
        return billDate;
    }

    public void setBillDate(String billDate) {
        this.billDate = billDate;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getCash() {
        return cash;
    }

    public void setCash(String cash) {
        this.cash = cash;
    }

    public String getCredit() {
        return credit;
    }

    public void setCredit(String credit) {
        this.credit = credit;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getItems() {
        return items;
    }

    public void setItems(String items) {
        this.items = items;
    }

    public String getOrderSource() {
        return orderSource;
    }

    public void setOrderSource(String orderSource) {
        this.orderSource = orderSource;
    }
}
