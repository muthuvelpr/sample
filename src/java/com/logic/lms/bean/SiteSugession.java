/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.logic.lms.bean;

/**
 *
 * @author pitchaiah_m
 */
public class SiteSugession {

    private String siteid, siteName, distance, city, state;

    public SiteSugession(String siteid, String siteName, String distance, String city, String state) {
        this.siteid = siteid;
        this.siteName = siteName;
        this.distance = distance;
        this.city = city;
        this.state = state;
    }

    public String getSiteid() {
        return siteid;
    }

    public void setSiteid(String siteid) {
        this.siteid = siteid;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

}
