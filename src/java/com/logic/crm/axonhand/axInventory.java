/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.logic.crm.axonhand;

import ccm.dao.bean.connectivity;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author Lenovo
 */
public class axInventory {

    public void gatherOrderDetails() {
        Connection con = null;
        String ordno = "";
        String artCodes = "";
        try {
            con = connectivity.getLDBConnection();
            PreparedStatement psListofOrders = con.prepareStatement("select h.ordno, h.siteid, count(d.artcode) from cc_ordhead h, cc_orddet d where  h.status in(0,5)  and d.status is null and h.ordno = d.ordno group by h.ordno, h.siteid");
            ResultSet rsListofOrders = psListofOrders.executeQuery();
            while (rsListofOrders.next()) {
                ordno = rsListofOrders.getString(1);
                String siteid = rsListofOrders.getString(2);
                StringBuilder sb = new StringBuilder();
                PreparedStatement psOrderDetail = con.prepareStatement("select artcode from cc_orddet where ordno = ?");
                psOrderDetail.setString(1, ordno);
                ResultSet rsOrderDetail = psOrderDetail.executeQuery();
                while (rsOrderDetail.next()) {
                    sb.append(",'" + rsOrderDetail.getString(1) + "'");
                }
                artCodes = sb.toString().replaceFirst(",", "");
                getServer(ordno, siteid, artCodes);
            }
        } catch (Exception e) {

        }
    }

    public void getServer(String ordno, String siteid, String artCodes) {
        Connection con = null;
        try {
            con = connectivity.getCMSDBConnection();
            PreparedStatement psPreOH = con.prepareStatement("select server from dcmaster where dccode = (select dc from WEB_LOGIN_MASTER where UserID = ?)");
            psPreOH.setString(1, siteid);
            ResultSet rsPreOH = psPreOH.executeQuery();
            if (rsPreOH.next()) {
                String server = rsPreOH.getString(1);
                getAXData(ordno, siteid, artCodes, server);
                try {
                    con = connectivity.getLDBConnection();
                    PreparedStatement ps = con.prepareStatement("update cc_orddet set onhandupdate=getdate(),status=1 where ordno = ?");
                    ps.setString(1, ordno);
                    ps.executeUpdate();
                    updateInventoryStatus(con, ordno);
                } catch (Exception e) {
                    System.out.println(e.getLocalizedMessage());
                }
            }
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
        }
    }

    public void getAXData(String ordno, String siteid, String artCodes, String server) {
        Connection con = null;
        PreparedStatement psAX = null;
        try {
            if (server.equals("MASTER")) {
                con = connectivity.getAXConnection();
            } else if (server.equals("SLAVE")) {
                con = connectivity.getAX2Connection();
            }
            psAX = con.prepareStatement("select a.itemid, SUM(AVAILPHYSICAL) ONHAND from INVENTSUM A, INVENTDIM B ,inventsite c WHERE b.inventsiteid = ? and a.itemid IN (" + artCodes + ") AND CLOSED=0 AND A.INVENTDIMID=B.INVENTDIMID  and c.siteid=b.INVENTSITEID and c.ACXSITERUNNINGON=2 AND A.DATAAREAID = 'AHEL' AND A.PARTITION = '5637144576'  AND B.DATAAREAID = 'AHEL' AND B.PARTITION = '5637144576'   AND C.DATAAREAID = 'AHEL' AND C.PARTITION = '5637144576'  GROUP BY A.ITEMID, B.INVENTSITEID having SUM(AVAILPHYSICAL) >0");
            psAX.setString(1, siteid);
            ResultSet rsAX = psAX.executeQuery();
            while (rsAX.next()) {
                String onHandartCode = rsAX.getString(1);
                int onHand = rsAX.getInt(2);
                System.out.println("Batch Started!!!");
                try {
                    Connection conUpdate = connectivity.getLDBConnection();
                    System.out.println(ordno + " " + onHandartCode + "        " + onHand);
                    PreparedStatement psUpdate = conUpdate.prepareStatement("update cc_orddet set onhand = ? where ordno = ? and artcode = ?");
                    psUpdate.setInt(1, onHand);
                    psUpdate.setString(2, ordno);
                    psUpdate.setString(3, onHandartCode);
                    psUpdate.executeUpdate();
                    updateInventoryStatus(conUpdate, ordno);
                } catch (Exception e) {
                    System.out.println(e.getLocalizedMessage());
                }
                System.out.println("Batch Completed!!!");
            }
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
        }
    }

    public void updateInventoryStatus(Connection _con, String ordno) {
        StringBuilder availability = new StringBuilder();
        try {
            String status = "";
            PreparedStatement ps = _con.prepareStatement("select (ISNULL(CONVERT(int, onhand),'0')-reqqoh) from CC_Orddet where ordno = ? and status = 1");
            ps.setString(1, ordno);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                if (rs.getInt(1) >= 0) {
                    availability.append("1-");
                } else if (rs.getInt(1) < 0) {
                    availability.append("0-");
                }
            }
            if (availability.toString().contains("0") && availability.length() > 1) {
                status = "Inventory Un-Availabile";
            } else if (availability.toString().contains("1") && availability.length() > 1) {
                status = "Inventory Available";
            }
            PreparedStatement ps1 = _con.prepareStatement("update cc_tporderinfo set InventoryStatus = ? where APORDID = ?");
            ps1.setString(1, status);
            ps1.setString(2, ordno);
            ps1.executeUpdate();
            System.out.println(ordno + " - " + status);
        } catch (Exception e) {
            System.out.println(ordno + " - " + e.getLocalizedMessage());
        }
    }
}
