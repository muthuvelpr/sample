/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.logic.order.model;

import ccm.dao.bean.connectivity;
import ccm.logic.bean.myCart;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author pitchaiah_m
 */
public class EstimationModel {

    List<myCart> cart = new ArrayList<>();

    public String SendEstimationtoMDIndia(String payload) {
        String response = "";
        String resp = "";
        try {
//            URL url = new URL("http://mdiuatsail.mdindia.com:8098/api/ApolloSail/SendDigiPrescription");
            URL url = new URL("http://mdiapollo.mdindia.com:8113/api/ApolloSail/SendDigiPrescription");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Token", "bWRpbmRpYQ==");

            OutputStream os = conn.getOutputStream();
            os.write(payload.getBytes());
            os.flush();
            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));

            while ((response = br.readLine()) != null) {
                resp = response;
                System.out.println(response);

            }
            conn.disconnect();
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
        }
        return resp;
    }

    public String SendEstimationtoApollo247(String payload) {
        String response = "";
        String resp = "";
        try {
            URL url = new URL("");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");

            OutputStream os = conn.getOutputStream();
            os.write(payload.getBytes());
            os.flush();
            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));

            while ((response = br.readLine()) != null) {
                resp = response;
                System.out.println(response);

            }
            conn.disconnect();
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
        }
        return resp;
    }

    public void EstimationReqLog(String ordNo, String vendName, int agent, String PayLoad, String _sid) {
        try {
            Connection con = connectivity.getLDBConnection();
            PreparedStatement ps = con.prepareStatement("insert into cc_vendorestimation (ORDERID,VENDORNAME,AGENT,PAYLOAD,REQDT,REFID) values (?,?,?,?,getDate(),?)");
            ps.setString(1, ordNo);
            ps.setString(2, vendName);
            ps.setInt(3, agent);
            ps.setString(4, PayLoad);
            ps.setString(5, _sid);
            ps.execute();
        } catch (Exception e) {

        }
    }

    public void EstimationResLog(String ordNo, String vendName, String Response, String _sid) {
        try {
            Connection con = connectivity.getLDBConnection();
            PreparedStatement ps = con.prepareStatement("update cc_vendorestimation set RESPONSE = ?,respdt = getDate() where orderid = ? and vendorname = ? and REFID=?");
            ps.setString(1, Response);
            ps.setString(2, ordNo);
            ps.setString(3, vendName);
            ps.setString(4, _sid);
            ps.execute();
        } catch (Exception e) {

        }
    }

    public boolean checkEstimation(String _ordno, String _vendName) {
        try {
            Connection con = connectivity.getLDBConnection();
            PreparedStatement ps = con.prepareStatement("select * from cc_vendorestimation where orderid = ? and vendorname = ?");
            ps.setString(1, _ordno);
            ps.setString(2, _vendName);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return false;
            } else {
                return true;
            }
        } catch (Exception e) {

        }
        return false;
    }

    public void updateTPOrderDetail(List<myCart> c, String _siteid, String _vendName, String _ordno) {
        try {
            Connection con = connectivity.getLDBConnection();
            PreparedStatement ps = con.prepareStatement("select ItemID,ItemName from ThirdpartyOrderDetails where orderid= ?");
            ps.setString(1, _ordno);
            ResultSet rs = ps.executeQuery();
//            ps.close();
            if (rs.next()) {

                ps = con.prepareStatement("delete from ThirdpartyOrderDetails where orderid= ?");
                ps.setString(1, _ordno);
                ps.executeUpdate();
                ps.close();
            }

//            ps = con.prepareStatement("insert into ThirdpartyOrderDetails(orderid,ItemID,ItemName,Qty,filestatus,price_mrp,site_ID,inserteddatetime,vendorname,APLineComment,pack_Size,subtotal) values (?,?,?,?,0,?,?,getDate(),?,?,?,?)");
            ps = con.prepareStatement("insert into ThirdpartyOrderDetails(orderid,ItemID,ItemName,Qty,filestatus,price_mrp,inserteddatetime,vendorname,APLineComment,pack_Size,subtotal,site_ID) values(?,?,?,?,0,?,getDate(),?,?,?,?,?)");
            for (myCart cart : c) {
                ps.setString(1, _ordno);
                ps.setString(2, cart.getArtcode());
                ps.setString(3, cart.getArtname());
                ps.setString(4, Integer.toString(cart.getReqQTY()));
                ps.setString(5, Double.toString(cart.getMrp()));
                ps.setString(6, _vendName);
                ps.setString(7, cart.getAplinecomment());
                ps.setString(8, Double.toString(cart.getPack()));
                int reqQty = cart.getQoh();
                double mrp = cart.getMrp();

                Double subtotal = reqQty * mrp;
                BigDecimal bdsubtotal = new BigDecimal(subtotal).setScale(2, RoundingMode.HALF_UP);
                double pre_subtotal = bdsubtotal.doubleValue();
                ps.setString(9, Double.toString(pre_subtotal));
                ps.setString(10, "True");
                ps.execute();
            }
        } catch (Exception e) {
            System.out.println("com.logic.order.model.EstimationModel.updateTPOrderDetail()" + e.getLocalizedMessage());
        }
    }

    public void updateTPOrderHeader(String _siteid, String _vendName, String _ordno) {
        try {
            Connection con = connectivity.getLDBConnection();
            PreparedStatement ps = con.prepareStatement("update ThirdpartyOrderHeader set OrderStatus = '12', site_ID = ? where orderid = ? and vendorname =  ?");
            ps.setString(1, _siteid);
            ps.setString(2, _ordno);
            ps.setString(3, _vendName);
            ps.execute();
        } catch (Exception e) {
        }
    }

    public void updateDigitalItems(List<myCart> c, String _siteid, String _vendName, String _ordno) {
        try {
            Connection con = connectivity.getLDBConnection();
            PreparedStatement ps = con.prepareStatement("insert into CC_DigitialItems(ORDERID,VENDORNAME,ITEMID,ITEMNAME,QTY,REMARKS,COMMENTBY) values (?,?,?,?,?,?,?)");
            for (myCart cart : c) {
                ps.setString(1, _ordno);
                ps.setString(2, _vendName);
                ps.setString(3, cart.getArtcode());
                ps.setString(4, cart.getArtname());
                ps.setString(5, Integer.toString(cart.getReqQTY()));
                ps.setString(6, cart.getAplinecomment());
                ps.setString(7, "Agent");
                ps.execute();
            }
        } catch (Exception e) {
            System.out.println("com.logic.order.model.EstimationModel.updateDigitalItems()" + e.getLocalizedMessage());
        }
    }

    public void updateOrderAmount(String _amount, String _vendName, String _ordno) {
        try {
            Connection con = connectivity.getLDBConnection();
            PreparedStatement ps = con.prepareStatement("update TPDELADDUPDATE set ORDERAMOUNT = ? where orderid = ? and vendorname =  ?");
            ps.setString(1, _amount);
            ps.setString(2, _ordno);
            ps.setString(3, _vendName);
            ps.execute();
        } catch (Exception e) {
        }
    }

}
