/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.logic.order.model;

import static ccm.dao.bean.connectivity.getLDBConnection;
import com.google.gson.Gson;
import com.logic.order.bean.ItemDetail;
import com.logic.order.bean.MDIndiaCancelRequest;
import com.logic.order.bean.OrderConfirmRequest;
import com.logic.thirdparty.ordercancellation.model.Apollo247OrderCancellationPL;
import com.logic.thirdparty.ordercancellation.model.orderCancelInput;
import com.logic.thirdparty.ordercancellation.model.variables;
import com.logic.thirdparty.orderconfirmation.model.Apollo247OrderConfirmationPL;
import com.logic.thirdparty.orderconfirmation.model.orderConfirmationInput;
import com.logic.thirdparty.orderconfirmation.model.ordersResult;
import com.util.Apollo247POSTHandler;
import com.util.MDIndiaPOSTHandler;
import com.util.OnlinePOSTHandler;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import static java.lang.System.out;
import java.net.HttpURLConnection;
import static java.net.HttpURLConnection.HTTP_OK;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.UUID;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

/**
 *
 * @author pitchaiah_m
 */
public class TPOrderStatusUpdate {

    public void updateOrderConfirmationtoVendor(String _VendordID, String _genOrdno, String _siteID, String _VendorName) {
        if (_VendorName.equalsIgnoreCase("MediAssist")) {
            MAOrderUpdate(_VendordID, _genOrdno, _siteID);
        } else if (_VendorName.equalsIgnoreCase("APOLLO247")) {
            APOLLO247OrderConfirmationUpdate(_VendordID, _genOrdno);
        } else if (_VendorName.equalsIgnoreCase("Online")) {
            OnlineOrderConfirmationUpdate(_VendorName, _genOrdno);
        }
    }

    public String updateOrderCancellationtoVendor(String _VendordID, String _VendorName, String _cancelRemarksCode) {
        String status = "";
        if (_VendorName.equalsIgnoreCase("APOLLO247")) {
            status = APOLLO247OrderCancellationUpdate(Integer.parseInt(_VendordID), _VendorName, _cancelRemarksCode
            );
        }

        if (_VendorName.equalsIgnoreCase("Online")) {
            //status = OnlineOrderCancellationUpdate(Integer.parseInt(_VendordID), _VendorName, _cancelRemarksCode);
            status = "true";
        }

        if (_VendorName.equalsIgnoreCase("MDIndia")) {
            status = MdindiaOrderCancellationUpdate(_VendordID, _VendorName, _cancelRemarksCode
            );
        }
        return status;
    }

    public static String OnlineOrderConfirmationUpdate(String _VendorName, String _genOrdno) {
        String status = "failure";
        try {
            OrderConfirmRequest oc = new OrderConfirmRequest();
            ArrayList<ItemDetail> itemList = new ArrayList<ItemDetail>();
            Gson gson = new Gson();
            Connection con = null;
            con = getLDBConnection();
            OnlineOrderUpdateLog oLog = new OnlineOrderUpdateLog();
            OnlinePOSTHandler oPH = new OnlinePOSTHandler();

            String query = "select ti.tpordid as magentoOrderID , td.patname, td.pricontact,td.del_add,td.POSTAL_CODE,h.ordno as fulfillmentID from cc_ordhead h, cc_tporderinfo ti, tpdeladdupdate td where cast(h.ordno as nvarchar) = ti.apordid and td.orderid = ti.tpordid  and h.ordno = ? and h.orderSource = 'Online'";
            PreparedStatement ps = con.prepareStatement(query);
            ps.setString(1, _genOrdno);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                oc.setMagentoOrderID(rs.getString(1));
                oc.setCustomername(rs.getString(2));
                oc.setCustomerMobileNumber(rs.getString(3));
                oc.setCustomerAddress(rs.getString(4));
                oc.setPostalCode(rs.getString(5));
                oc.setFullfilmentID(rs.getString(6));

            }
            String itemQuery = "select artcode as sku, artName as itemname, reqqoh as qty from cc_orddet where ordno = ? ";
            PreparedStatement ps1 = con.prepareStatement(itemQuery);
            ps1.setString(1, _genOrdno);
            ResultSet rs1 = ps1.executeQuery();
            while (rs1.next()) {
                itemList.add(new ItemDetail(rs1.getString(1), rs1.getString(2), rs1.getInt(3)));
            }
            oc.setItemDetail(itemList);
            String reqStr = gson.toJson(oc);
            UUID uuid = UUID.randomUUID();
            String sid = uuid.toString();
            oLog.ReqLog(con, _genOrdno, _VendorName, reqStr, sid);
            String res = oPH.OnlineReverseEngineering(reqStr, "OrderConfirmation");
            if (res.contains("success")) {
                status = "success";
            }
            oLog.ResLog(con, _genOrdno, status, _VendorName, res, sid);
            ps.close();
            ps1.close();
            rs.close();
            rs1.close();
            con.close();

        } catch (Exception ex) {
            System.out.println("com.logic.order.model.TPOrderStatusUpdate.OnlineOrderConfirmationUpdate()" + ex.getLocalizedMessage());
        }

        return status;
    }

    public String APOLLO247OrderConfirmationUpdate(String _VendordID, String _genOrdno) {
        String status = "false";
        try {
            Connection con = null;
            Apollo247OrderConfirmationPL a247c = new Apollo247OrderConfirmationPL();
            orderConfirmationInput oci = new orderConfirmationInput();
            com.logic.thirdparty.orderconfirmation.model.variables var = new com.logic.thirdparty.orderconfirmation.model.variables();
            ordersResult or = new ordersResult();
            or.setMessage("Order verified and confirmed");
            or.setApOrderNo(_genOrdno);
            or.setOrderNo(Integer.parseInt(_VendordID));
            oci.setOrdersResult(or);
            var.setOrderConfirmationInput(oci);
            a247c.setQuery("mutation($orderConfirmationInput:OrderConfirmationInput!){saveOrderConfirmation(orderConfirmationInput:$orderConfirmationInput){requestStatus requestMessage}}");
            a247c.setVariables(var);
            Gson g = new Gson();
            String pl = g.toJson(a247c);
            Apollo247POSTHandler ap247ph = new Apollo247POSTHandler();
            TPOrderUpdateLog tpoul = new TPOrderUpdateLog();
            UUID uuid = UUID.randomUUID();
            String sid = uuid.toString();
            con = getLDBConnection();
            tpoul.ReqLog(con, _genOrdno, "APOLLO247", pl, sid);
            String response = ap247ph.POSTRequest(pl);
            if (response.contains("true")) {
                status = "true";
            }
            tpoul.ResLog(con, _genOrdno, status, "APOLLO247", response, sid);
            con.clearWarnings();
            con.close();
        } catch (Exception e) {
            System.out.println("com.logic.order.model.TPOrderStatusUpdate.APOLLO247OrderConfirmationUpdate()" + e.getLocalizedMessage());
        }
        return status;
    }

    public String APOLLO247OrderCancellationUpdate(int _vendOrdNo, String _vendorName, String _cancelRemarksCode) {
        String status = "false";
        try {
            Connection con = null;
            Apollo247OrderCancellationPL a247can = new Apollo247OrderCancellationPL();
            HttpSession appSession = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
            int username = Integer.parseInt(appSession.getAttribute("loginid").toString());
            orderCancelInput oci = new orderCancelInput();
            oci.setOrderNo(_vendOrdNo);
            oci.setRemarksCode(_cancelRemarksCode);
            variables var = new variables();
            var.setOrderCancelInput(oci);
            a247can.setQuery("mutation($orderCancelInput:OrderCancelInput!){ saveOrderCancelStatus(orderCancelInput:$orderCancelInput){requestStatus requestMessage}}");
            a247can.setVariables(var);
            Gson g = new Gson();
            String pl = g.toJson(a247can);
            Apollo247POSTHandler ap247ph = new Apollo247POSTHandler();
            TPOrderUpdateLog tpoul = new TPOrderUpdateLog();
            UUID uuid = UUID.randomUUID();
            String sid = uuid.toString();
            con = getLDBConnection();
            tpoul.ReqLog(con, String.valueOf(_vendOrdNo), "APOLLO247", pl, sid);
            String response = ap247ph.POSTRequest(pl);

            if (response.contains("true")) {
                status = "true";
                OrderCancellation cancellation = new OrderCancellation();
                cancellation.processCancellation(_cancelRemarksCode, username, String.valueOf(_vendOrdNo), _vendorName);
            }
            tpoul.ResLog(con, String.valueOf(_vendOrdNo), status, "APOLLO247", response, sid);
            con.clearWarnings();
            con.close();
        } catch (Exception e) {
            System.out.println("com.logic.order.model.APOLLO247Update Error : " + e.getLocalizedMessage());
        }
        return status;
    }

    public String OnlineOrderCancellationUpdate(int _vendOrdNo, String _vendorName, String _cancelRemarksCode) {
        String status = "false";
        try {
            Connection con = null;
            Apollo247OrderCancellationPL a247can = new Apollo247OrderCancellationPL();
            HttpSession appSession = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
            int username = Integer.parseInt(appSession.getAttribute("loginid").toString());
            orderCancelInput oci = new orderCancelInput();
            oci.setOrderNo(_vendOrdNo);
            oci.setRemarksCode(_cancelRemarksCode);
            variables var = new variables();
            var.setOrderCancelInput(oci);
            a247can.setQuery("mutation($orderCancelInput:OrderCancelInput!){ saveOrderCancelStatus(orderCancelInput:$orderCancelInput){requestStatus requestMessage}}");
            a247can.setVariables(var);
            Gson g = new Gson();
            String pl = g.toJson(a247can);
            Apollo247POSTHandler ap247ph = new Apollo247POSTHandler();
            TPOrderUpdateLog tpoul = new TPOrderUpdateLog();
            UUID uuid = UUID.randomUUID();
            String sid = uuid.toString();
            con = getLDBConnection();
            tpoul.ReqLog(con, String.valueOf(_vendOrdNo), "Online", pl, sid);
            String response = ap247ph.POSTRequest(pl);

            if (response.contains("true")) {
                status = "true";
                OrderCancellation cancellation = new OrderCancellation();
                cancellation.processCancellation(_cancelRemarksCode, username, String.valueOf(_vendOrdNo), _vendorName);
            }
            tpoul.ResLog(con, String.valueOf(_vendOrdNo), status, "APOLLO247", response, sid);
            con.clearWarnings();
            con.close();
        } catch (Exception e) {
            System.out.println("com.logic.order.model.APOLLO247Update Error : " + e.getLocalizedMessage());
        }
        return status;
    }

    public String MdindiaOrderCancellationUpdate(String _vendOrdNo, String _vendorName, String _cancelRemarksCode) {
        String status = "false";
        try {
            Connection con = null;
            HttpSession appSession = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
            int username = Integer.parseInt(appSession.getAttribute("loginid").toString());
            MDIndiaCancelRequest mdcr = new MDIndiaCancelRequest();

            mdcr.setMDIOrderNo(_vendOrdNo);
            mdcr.setApolloOrderNo("");
            mdcr.setCanceledBy("Apollo");
            mdcr.setRemark(_cancelRemarksCode);

            Gson g = new Gson();
            String pl = g.toJson(mdcr);

            MDIndiaPOSTHandler mdindiaph = new MDIndiaPOSTHandler();
            TPOrderUpdateLog tpoul = new TPOrderUpdateLog();
            UUID uuid = UUID.randomUUID();
            String sid = uuid.toString();
            con = getLDBConnection();
            tpoul.ReqLog(con, _vendOrdNo, "MDIndia", pl, sid);
            String response = mdindiaph.POSTRequest(pl);

            if (response.contains("Accepted") || response.contains("ACCEPTED")) {
                status = "true";
                OrderCancellation cancellation = new OrderCancellation();
                cancellation.processCancellation(_cancelRemarksCode, username, String.valueOf(_vendOrdNo), _vendorName);
            }
            tpoul.ResLog(con, _vendOrdNo, status, "MDIndia", response, sid);
            con.clearWarnings();
            con.close();
        } catch (Exception e) {
            System.out.println("com.logic.order.model.TPOrderStatusUpdate.MdindiaOrderCancellationUpdate()" + e.getLocalizedMessage());
        }
        return status;
    }

    public void MAOrderUpdate(String _VendordID, String _genOrdno, String _siteID) {
        try {
            final String USER_AGENT = "Mozilla/5.0";
            final String GET_URL = "http://172.16.2.251:84/MediAssist_pos.aspx?MA_Orderid=" + _VendordID + "&AP_orderid=" + _genOrdno + "&siteid=" + _siteID + "&Action=orderupdate";
            URL obj = new URL(GET_URL);
            out.println(GET_URL);
            HttpURLConnection smscon = (HttpURLConnection) obj.openConnection();
            smscon.setRequestMethod("GET");
            smscon.setRequestProperty("User-Agent", USER_AGENT);
            int responseCode = smscon.getResponseCode();
            out.println("GET Response Code :: " + responseCode);
            if (responseCode == HTTP_OK) { // success
                BufferedReader in = new BufferedReader(new InputStreamReader(
                        smscon.getInputStream()));
                String inputLine;
                StringBuilder response = new StringBuilder();
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
                out.println(response.toString());
            } else {
                out.println("GET request not worked");
            }
        } catch (IOException ex) {
            out.println(ex.getLocalizedMessage());
        }
    }
}
