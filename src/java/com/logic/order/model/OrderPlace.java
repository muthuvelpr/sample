/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.logic.order.model;

import ccm.logic.bean.myCart;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.util.List;

/**
 *
 * @author pitchaiah_m
 */
public class OrderPlace {

    public boolean checkOrder(Connection _con, String _vendorName, String _vendOrdNo) throws SQLException {
        try {
            PreparedStatement ps = _con.prepareStatement("select orderstatus from thirdpartyorderheader where orderid = ? and vendorname = ? and orderstatus in (0,7)");
            ps.setString(1, _vendOrdNo);
            ps.setString(2, _vendorName);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            System.out.println(new java.util.Date() + " com.logic.order.model.OrderPlace.checkOrder - " + e.getLocalizedMessage());
        }
        return false;
    }

    public String getNewOrder(Connection _con, String _vendorName, String _vendOrdno, String _orderStatus, int _SiteID, String _TAT) throws SQLException {
        String ordno = "";
        boolean canProcessOrder = checkOrder(_con, _vendorName, _vendOrdno);
        if (canProcessOrder) {
            try {
                PreparedStatement ps = _con.prepareCall("{call CC_GetOrderNo(?,?,?,?,?)}");
                ps.setString(1, _vendorName);
                ps.setString(2, _vendOrdno);
                ps.setString(3, _orderStatus);
                ps.setInt(4, _SiteID);
                ps.setString(5, _TAT);
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    ordno = rs.getString(1);
                }
            } catch (Exception e) {
                System.out.println(new java.util.Date() + " com.logic.order.model.OrderPlace.getNewOrder - " + e.getLocalizedMessage());
            }
        } else {
            ordno = "";
        }

        return ordno;
    }

    public void createOrderHeader(Connection _con, String _vendorOrderNo, int _siteid, String _agentID, String _remarks, String _ordNo, String _shipping, String _vendorName) {
        try {
            PreparedStatement ps = _con.prepareStatement("insert into cc_ordhead (OrdNo,patid,siteID,ordDate,loginid,status,deliverymode,paymentmode,Remarks,orderSource) (select '" + _ordNo + "', h.patientid,'" + _siteid + "', getdate(),'" + _agentID + "',5,'" + _shipping + "',h.PaymentMethod,'" + _remarks + "',h.VendorName from ThirdPartyOrderHeader h, TPDELADDUPDATE tpd where h.OrderId = tpd.orderid and h.VendorName = tpd.vendorname and h.orderid = ? and h.vendorname = ?)");
            ps.setString(1, _vendorOrderNo);
            ps.setString(2, _vendorName);
            ps.execute();
        } catch (Exception e) {
            System.out.println(new java.util.Date() + " - com.logic.order.model.OrderPlace.createOrderHeader - " + e.getLocalizedMessage());
        }
    }

    public void createOrderLine(Connection _con, String _ordNo, String _vendorOrderNo, String _vendorName, List<myCart> cart, int siteid) {
        for (myCart p : cart) {
            String cartartcode = p.getArtcode();
            String cartartname = p.getArtname();
            Double cartmrp = p.getMrp();
            int cartreqQTY = p.getReqQTY();
            double cartpack = p.getPack();
            String cartcomment = p.getAplinecomment();
            try {
                PreparedStatement ps = _con.prepareStatement("{call CreateOrderLineNew(?,?,?,?,?,?,?,?,?,?)}");
                ps.setString(1, _ordNo);
                ps.setString(2, _vendorOrderNo);
                ps.setString(3, _vendorName);
                ps.setString(4, cartartcode);
                ps.setString(5, cartartname);
                ps.setInt(6, cartreqQTY);
                ps.setDouble(7, cartmrp);
                ps.setDouble(8, cartpack);
                ps.setInt(9, siteid);
                ps.setString(10, cartcomment);
                ps.execute();
            } catch (Exception e) {
                System.out.println(new java.util.Date() + " - com.logic.order.model.OrderPlace.createOrderLine - " + e.getLocalizedMessage());
            }

        }
    }

}
