/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.logic.order.model;

import static ccm.dao.bean.connectivity.getLDBConnection;
import ccm.logic.bean.CancelRemarks;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author pitchaiah_m
 */
public class OrderCancellation {

    public boolean checkCancellation(String _vendorName, String _ordNo) throws SQLException {
        Connection con = null;
        try {
            con = getLDBConnection();
            PreparedStatement ps = con.prepareStatement("select orderstatus from thirdpartyorderheader where orderid = ? and vendorname = ? AND VENDORNAME IN ('AskApollo','Online','Apollo247','MDIndia')");
            ps.setString(1, _ordNo);
            ps.setString(2, _vendorName);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                String curStatus = rs.getString(1);
                if (curStatus.equals("0") || curStatus.equals("11")) {
                    return true;
                } else {
                    return false;
                }
            }
            con.clearWarnings();
            con.close();
        } catch (Exception e) {
            System.out.println(new java.util.Date() + " com.logic.order.model.OrderCancellation.checkCancellation - " + e.getLocalizedMessage());
        } finally {
            con.close();
        }
        return false;
    }

    public void processCancellation(String _cancelRemarks, int _cancelBy, String _ordNo, String _vendorName) throws SQLException {
        Connection con = null;
        try {
            con = getLDBConnection();
            PreparedStatement ps = con.prepareStatement("update ThirdPartyOrderHeader set OrderStatus=2, CancelDatetime=getdate(), CancelRemarks=?, CancelBy=? where OrderId = ? and VendorName = ?");
            ps.setString(1, _cancelRemarks);
            ps.setInt(2, _cancelBy);
            ps.setString(3, _ordNo);
            ps.setString(4, _vendorName);
            ps.executeUpdate();
            con.clearWarnings();
            con.close();
        } catch (Exception e) {
            System.out.println(new java.util.Date() + " com.logic.order.model.OrderCancellation.processCancellation - " + e.getLocalizedMessage());
        } finally {
            con.close();
        }
    }

    List<String> cancelBase = new ArrayList<>();

    public List<String> listCancelBase(String _vendname) {
        cancelBase.clear();
        try {
            Connection con = getLDBConnection();
            PreparedStatement ps1 = con.prepareStatement("select reason_base from CC_OrderCancelRemarks where status = 1 and VENDOR = ? group by REASON_BASE order by 1 asc");
            ps1.setString(1, _vendname);
            ResultSet rs1 = ps1.executeQuery();
            while (rs1.next()) {
                cancelBase.add(rs1.getString(1));
            }
        } catch (Exception e) {
            System.out.println(new java.util.Date() + " com.logic.order.model.Cancellation.listCancelRemarks - " + e.getLocalizedMessage());
        }
        return cancelBase;
    }

    public List<String> getCancelBase() {
        return cancelBase;
    }

    List<CancelRemarks> CanRem = new ArrayList<>();

    public List<CancelRemarks> listCancelRemarks(String _vendname, String _reasonBase) {
        CanRem.clear();
        try {
            Connection con = getLDBConnection();
            PreparedStatement ps1 = con.prepareStatement("select REASON_CODE,REASON_DESC from CC_OrderCancelRemarks where vendor = ? and REASON_BASE=? and status = 1 order by REASON_CODE asc");
            ps1.setString(1, _vendname);
            ps1.setString(2, _reasonBase);
            ResultSet rs1 = ps1.executeQuery();
            while (rs1.next()) {
                CanRem.add(new CancelRemarks(rs1.getString(1), rs1.getString(2)));
            }
        } catch (Exception e) {
            System.out.println(new java.util.Date() + " com.logic.order.model.Cancellation.listCancelRemarks - " + e.getLocalizedMessage());
        }
        return CanRem;
    }

    public List<CancelRemarks> getCanRem() {
        return CanRem;
    }

    public boolean canCancelfromSOMS(String _vendName)
            throws SQLException {
        Connection con = null;
        try {
            con = getLDBConnection();
            PreparedStatement ps = con.prepareStatement("select omscancel from OMS_Cancel_Control where vendorname = ?");
            ps.setString(1, _vendName);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                boolean bool;
                if (rs.getString(1).equals("1")) {
                    return true;
                }
                return false;
            }
        } catch (Exception e) {
            System.out.println(new java.util.Date() + " com.logic.order.model.OrderCancellation.canCancelfromSOMS - " + e.getLocalizedMessage());
        } finally {
            con.close();
        }
        return false;
    }

    public String getCancelRemarks(String _canCode) {
        String reason = "";
        Connection con = null;
        try {
            con = getLDBConnection();
            PreparedStatement ps1 = con.prepareStatement("select REASON_DESC from CC_OrderCancelRemarks where REASON_CODE=?");
            ps1.setString(1, _canCode);
            ResultSet rs1 = ps1.executeQuery();
            while (rs1.next()) {
                reason = rs1.getString(1);
            }
            con.close();

        } catch (Exception e) {
            System.out.println("com.logic.order.model.OrderCancellation.getCancelRemarks()" + e);
        }
        return reason;
    }

}
