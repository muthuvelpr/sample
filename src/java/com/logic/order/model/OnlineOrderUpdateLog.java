/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.logic.order.model;

import java.sql.Connection;
import java.sql.PreparedStatement;

/**
 *
 * @author Administrator
 */
public class OnlineOrderUpdateLog {

    public void ReqLog(Connection _con, String _ordNo, String _vendName, String _reqPayLoad, String _sid) {
        try {
            PreparedStatement ps = _con.prepareStatement("insert into tpordupdate (APORDID,TPVNAME,ORDREQDATA,REQDATE,seqno) VALUES (?,?,?,getDate(), ?)");
            ps.setString(1, _ordNo);
            ps.setString(2, _vendName);
            ps.setString(3, _reqPayLoad);
            ps.setString(4, _sid);
            ps.execute();
        } catch (Exception e) {
            System.out.println("com.logic.order.model.OnlineOrderUpdateLog.ReqLog()" + e.getLocalizedMessage());
        }
    }

    public void ResLog(Connection _con, String _ordNo, String _status, String _vendName, String _response, String _sid) {
        try {
            PreparedStatement ps = _con.prepareStatement("update tpordupdate set ordrespdata = ?, respdate = getdate(),ordstatus = ? where apordid = ? and TPVName = ? and seqno = ? ");
            ps.setString(1, _response);
            ps.setString(2, _status);
            ps.setString(3, _ordNo);
            ps.setString(4, _vendName);
            ps.setString(5, _sid);
            ps.executeUpdate();
        } catch (Exception e) {
            System.out.println("com.logic.order.model.OnlineOrderUpdateLog.ResLog()" + e.getLocalizedMessage());

        }
    }
}
