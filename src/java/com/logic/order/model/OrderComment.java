/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.logic.order.model;

import com.logic.order.bean.comments;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author pitchaiah_m
 */
public class OrderComment {

    public void addCommenttoOrder(Connection _con, String _orderID, String _commentData, int _AgentID, String _vendorName) {
        try {
            PreparedStatement ps = _con.prepareStatement("insert into CC_OrderComments values (?,?,?,getDate(),?)");
            ps.setString(1, _orderID);
            ps.setString(2, _commentData);
            ps.setInt(3, _AgentID);
            ps.setString(4, _vendorName);
            ps.execute();
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
        }
    }

    public List<comments> listAllComments(Connection _con, String _orderID, String _vendorName) {
        List<comments> listComments = new ArrayList<>();
        try {
            listComments.clear();
            PreparedStatement ps = _con.prepareStatement("select  c.ordno,c.commentdata,c.agentid,c.commenton,c.ordersource, ccl.username from CC_OrderComments c, CClogin ccl where c.ordno = ? and c.ordersource = ? and c.AgentID = ccl.id");
            ps.setString(1, _orderID);
            ps.setString(2, _vendorName);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                listComments.add(new comments(rs.getString(1), rs.getString(2), rs.getString(6), rs.getString(4)));
            }
        } catch (SQLException e) {
            System.out.println(e.getLocalizedMessage());
        }
        return listComments;
    }
}
