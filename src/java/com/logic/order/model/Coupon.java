/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.logic.order.model;

import static ccm.dao.bean.connectivity.getLDBConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author Administrator
 */
public class Coupon {

    public boolean checkCouponAllocation(String _mobNo, String _orderSource) {
        boolean allocated = false;
        int alreadyIssued = 0;
        try {
            Connection con = getLDBConnection();
            PreparedStatement ps = con.prepareStatement("select count(*) from CC_CouponLog where mobileno = ? and ordersource = ?  and orderdate>='30-Sep-2019'");
            ps.setString(1, _mobNo);
            ps.setString(2, _orderSource);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                alreadyIssued = rs.getInt(1);
                if (alreadyIssued < 2) {
                    allocated = true;
                } else {
                    allocated = false;
                }
            } else {
                allocated = true;
            }
            con.clearWarnings();
            con.close();
        } catch (Exception e) {
            System.out.println(new java.util.Date() + " - Error on com.logic.order.model.Coupon.checkCouponAllocation - " + e.getLocalizedMessage());
        }
        return allocated;
    }

    public void AllocateCoupon(String _orderid, String _mobNo, int _AgentID, String _CouponCode, String _Remarks1, String _Remarks2, String _OrderSource) {
        try {
            Connection con = getLDBConnection();
            PreparedStatement ps = con.prepareStatement("{call CC_CouponAllocation(?,?,?,?,?,?,?)}");
            ps.setString(1, _orderid);
            ps.setString(2, _mobNo);
            ps.setInt(3, _AgentID);
            ps.setString(4, _CouponCode);
            ps.setString(5, _Remarks1);
            ps.setString(6, _Remarks2);
            ps.setString(7, _OrderSource);
            ps.execute();
            con.clearWarnings();
            con.close();
        } catch (Exception e) {
            System.out.println(new java.util.Date() + " - Error on com.logic.order.model.Coupon.AllocateCoupon - " + e.getLocalizedMessage());
        }
    }
}
