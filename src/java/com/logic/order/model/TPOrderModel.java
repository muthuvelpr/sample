/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.logic.order.model;

import static ccm.dao.bean.connectivity.getAX2Connection;
import static ccm.dao.bean.connectivity.getAXConnection;
import static ccm.dao.bean.connectivity.getLDBConnection;
import ccm.logic.bean.UploadedPrescDocs;
import ccm.logic.bean.itemDetails;
import ccm.logic.bean.myCart;
import ccm.report.model.invAvailability;
import com.logic.order.bean.MDIndiaOrderSummary;
import com.logic.order.bean.TPOrder;
import com.logic.order.bean.TPOrderSummary;
import java.io.Serializable;
import static java.lang.System.out;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author pitchaiah_m
 */
@ManagedBean(name = "tpordmod")
@RequestScoped
public class TPOrderModel implements Serializable {

    List<MDIndiaOrderSummary> orderSummaryList = new ArrayList<>();
    List<TPOrder> tpordsum = new ArrayList<>();
    List<TPOrderSummary> tpordsummary = new ArrayList<>();
    List<UploadedPrescDocs> prescriptionURLs = new ArrayList<>();
    List<myCart> cart = new ArrayList<>();
    List<itemDetails> itemdetail = new ArrayList<>();
    List<invAvailability> invAvail = new ArrayList<>();
    double total = 0.0;

    public List<TPOrder> getAllOrders(String _DisplayData) {
        Connection con = null;
        System.out.println(new java.util.Date());
        tpordsum.clear();
        try {
            con = getLDBConnection();
            PreparedStatement ps = null;
            if (_DisplayData.equals("ThirdParty")) {
                ps = con.prepareStatement("select b.orderid, U.patname AS CUSTNAME,C.MOBILENO, b.createddatetime, b.is_medicine, b.shippingmethod, b.paymentmethod,b.patientid, B.VendorName,ISNULL(u.ORDERAMOUNT ,'-') as ordAmount, ISNULL(u.POSTAL_CODE ,'-') as POSTAL, ISNULL(u.CITY,'-') as CITY, isnull(u.PSTATUS,'-') as TXNStatus, isnull(u.PORDERID,'-') as PayTMOrderID,u.del_add,b.site_ID,isnull(b.wod,'0') as WOD from ThirdPartyOrderHeader b,tpdeladdupdate U,CC_Patient_Registration C  where  b.PATIENTID = C.PATIENTID AND B.ORDERID=U.ORDERID and b.orderstatus in (0,7) and b.VendorName in ('MediAssist') group by b.orderid, U.patname, C.MOBILENO, b.createddatetime, b.is_medicine, b.shippingmethod, b.paymentmethod,b.patientid,B.VendorName,U.ORDERAMOUNT,u.POSTAL_CODE,u.CITY,u.PSTATUS,u.PORDERID,u.del_add,b.site_ID,b.WOD order by b.createddatetime desc");
            } else if (_DisplayData.equalsIgnoreCase("AskApollo") || _DisplayData.equalsIgnoreCase("MAskApollo")) {
                ps = con.prepareStatement("select b.orderid, U.patname AS CUSTNAME,C.MOBILENO, b.createddatetime, b.is_medicine, b.shippingmethod, b.paymentmethod,b.patientid, B.VendorName,ISNULL(u.ORDERAMOUNT ,'-') as ordAmount, ISNULL(u.POSTAL_CODE ,'-') as POSTAL, ISNULL(u.CITY,'-') as CITY, isnull(u.PSTATUS,'-') as TXNStatus, isnull(u.PORDERID,'-') as PayTMOrderID,u.del_add,b.site_ID,isnull(b.wod,'0') as WOD from ThirdPartyOrderHeader b,tpdeladdupdate U,CC_Patient_Registration C  where  b.PATIENTID = C.PATIENTID AND B.ORDERID=U.ORDERID and b.orderstatus in (0,7,11) and b.VendorName in ('AskApollo') and pstatus in ('approved','Txn Success','APPROVAL','Success') and len(u.postal_code) = 6 group by b.orderid, U.patname, C.MOBILENO, b.createddatetime, b.is_medicine, b.shippingmethod, b.paymentmethod,b.patientid,B.VendorName,U.ORDERAMOUNT,u.POSTAL_CODE,u.CITY,u.PSTATUS,u.PORDERID,u.del_add,b.site_ID,b.WOD union all select b.orderid, U.patname AS CUSTNAME,C.MOBILENO, b.createddatetime, b.is_medicine, b.shippingmethod, b.paymentmethod,b.patientid, B.VendorName,ISNULL(u.ORDERAMOUNT ,'-') as ordAmount, ISNULL(u.POSTAL_CODE ,'-') as POSTAL, ISNULL(u.CITY,'-') as CITY, isnull(u.PSTATUS,'-') as TXNStatus, isnull(u.PORDERID,'-') as PayTMOrderID,u.del_add,b.site_ID,isnull(b.wod,'0') as WOD from ThirdPartyOrderHeader b,tpdeladdupdate U,CC_Patient_Registration C  where  b.PATIENTID = C.PATIENTID AND B.ORDERID=U.ORDERID and b.orderstatus in (0,7,11) and b.VendorName in ('AskApollo') and b.paymentmethod = 'COD' and len(u.postal_code) = 6 group by b.orderid, U.patname, C.MOBILENO, b.createddatetime, b.is_medicine, b.shippingmethod, b.paymentmethod,b.patientid,B.VendorName,U.ORDERAMOUNT,u.POSTAL_CODE,u.CITY,u.PSTATUS,u.PORDERID,u.del_add,b.site_ID,b.WOD order by b.createddatetime desc");
            } else if (_DisplayData.equalsIgnoreCase("Apollo247")) {
                ps = con.prepareStatement("select b.orderid, U.patname AS CUSTNAME,C.MOBILENO, b.createddatetime, b.is_medicine, b.shippingmethod, b.paymentmethod,b.patientid, B.VendorName,ISNULL(u.ORDERAMOUNT ,'-') as ordAmount, ISNULL(u.POSTAL_CODE ,'-') as POSTAL, ISNULL(u.CITY,'-') as CITY, isnull(u.PSTATUS,'-') as TXNStatus, isnull(u.PORDERID,'-') as PayTMOrderID,u.del_add,b.site_ID,isnull(b.wod,'0') as WOD from ThirdPartyOrderHeader b,tpdeladdupdate U,CC_Patient_Registration C  where  b.PATIENTID = C.PATIENTID AND B.ORDERID=U.ORDERID and b.orderstatus in (0,7,11) and b.VendorName in ('Apollo247') and pstatus in ('approved','Txn Success','APPROVAL','Success','TXN_SUCCESS') and len(u.postal_code) = 6 group by b.orderid, U.patname, C.MOBILENO, b.createddatetime, b.is_medicine, b.shippingmethod, b.paymentmethod,b.patientid,B.VendorName,U.ORDERAMOUNT,u.POSTAL_CODE,u.CITY,u.PSTATUS,u.PORDERID,u.del_add,b.site_ID,b.WOD union all select b.orderid, U.patname AS CUSTNAME,C.MOBILENO, b.createddatetime, b.is_medicine, b.shippingmethod, b.paymentmethod,b.patientid, B.VendorName,ISNULL(u.ORDERAMOUNT ,'-') as ordAmount, ISNULL(u.POSTAL_CODE ,'-') as POSTAL, ISNULL(u.CITY,'-') as CITY, isnull(u.PSTATUS,'-') as TXNStatus, isnull(u.PORDERID,'-') as PayTMOrderID,u.del_add,b.site_ID,isnull(b.wod,'0') as WOD from ThirdPartyOrderHeader b,tpdeladdupdate U,CC_Patient_Registration C  where  b.PATIENTID = C.PATIENTID AND B.ORDERID=U.ORDERID and b.orderstatus in (0,7) and b.VendorName in ('Apollo247') and b.paymentmethod = 'COD' and len(u.postal_code) = 6 group by b.orderid, U.patname, C.MOBILENO, b.createddatetime, b.is_medicine, b.shippingmethod, b.paymentmethod,b.patientid,B.VendorName,U.ORDERAMOUNT,u.POSTAL_CODE,u.CITY,u.PSTATUS,u.PORDERID,u.del_add,b.site_ID,b.WOD order by b.createddatetime desc");
            } else if (_DisplayData.equalsIgnoreCase("OnlinePaid")) {
                ps = con.prepareStatement("select b.orderid, U.patname AS CUSTNAME,C.MOBILENO, b.createddatetime, b.is_medicine, b.shippingmethod, b.paymentmethod,b.patientid, B.VendorName,ISNULL(u.ORDERAMOUNT ,'-') as ordAmount, ISNULL(u.POSTAL_CODE ,'-') as POSTAL, ISNULL(u.CITY,'-') as CITY, isnull(u.PSTATUS,'-') as TXNStatus, isnull(u.PORDERID,'-') as PayTMOrderID,u.del_add,b.site_ID,isnull(b.wod,'0') as WOD from ThirdPartyOrderHeader b,tpdeladdupdate U,CC_Patient_Registration C  where  b.PATIENTID = C.PATIENTID AND B.ORDERID=U.ORDERID and b.orderstatus in (0,7,11) and b.VendorName in ('Online') and pstatus in ('approved','Txn Success','APPROVAL','Success') group by b.orderid, U.patname, C.MOBILENO, b.createddatetime, b.is_medicine, b.shippingmethod, b.paymentmethod,b.patientid,B.VendorName,U.ORDERAMOUNT,u.POSTAL_CODE,u.CITY,u.PSTATUS,u.PORDERID,u.del_add,b.site_ID,b.WOD union all select b.orderid, U.patname AS CUSTNAME,C.MOBILENO, b.createddatetime, b.is_medicine, b.shippingmethod, b.paymentmethod,b.patientid, B.VendorName,ISNULL(u.ORDERAMOUNT ,'-') as ordAmount, ISNULL(u.POSTAL_CODE ,'-') as POSTAL, ISNULL(u.CITY,'-') as CITY, isnull(u.PSTATUS,'-') as TXNStatus, isnull(u.PORDERID,'-') as PayTMOrderID,u.del_add,b.site_ID,isnull(b.wod,'0') as WOD from ThirdPartyOrderHeader b,tpdeladdupdate U,CC_Patient_Registration C  where  b.PATIENTID = C.PATIENTID AND B.ORDERID=U.ORDERID and b.orderstatus in (0,7,11) and b.VendorName in ('Online') and b.paymentmethod = 'COD' group by b.orderid, U.patname, C.MOBILENO, b.createddatetime, b.is_medicine, b.shippingmethod, b.paymentmethod,b.patientid,B.VendorName,U.ORDERAMOUNT,u.POSTAL_CODE,u.CITY,u.PSTATUS,u.PORDERID,u.del_add,b.site_ID,b.WOD order by b.createddatetime desc");
            } else if (_DisplayData.equalsIgnoreCase("CommerceNonPaid")) {
                ps = con.prepareStatement("select b.orderid, U.patname AS CUSTNAME,C.MOBILENO, b.createddatetime, b.is_medicine, b.shippingmethod, b.paymentmethod,b.patientid, B.VendorName,ISNULL(u.ORDERAMOUNT ,'-') as ordAmount, ISNULL(u.POSTAL_CODE ,'-') as POSTAL, ISNULL(u.CITY,'-') as CITY, isnull(u.PSTATUS,'-') as TXNStatus, isnull(u.PORDERID,'-') as PayTMOrderID,u.del_add,b.site_ID,isnull(b.wod,'0') as WOD from ThirdPartyOrderHeader b,tpdeladdupdate U,CC_Patient_Registration C  where  b.PATIENTID = C.PATIENTID AND B.ORDERID=U.ORDERID and b.orderstatus in (0,7,11) and b.VendorName in ('Online','AskApollo','Apollo247') and pstatus not in ('approved','Txn Success','APPROVAL','Success','TXN_SUCCESS') and b.paymentmethod != 'COD' group by b.orderid, U.patname, C.MOBILENO, b.createddatetime, b.is_medicine, b.shippingmethod, b.paymentmethod,b.patientid,B.VendorName,U.ORDERAMOUNT,u.POSTAL_CODE,u.CITY,u.PSTATUS,u.PORDERID,u.del_add,b.site_ID,b.WOD order by b.createddatetime desc");
            } else if (_DisplayData.equals("EstimationOrders")) {
                ps = con.prepareStatement("select b.orderid, U.patname AS CUSTNAME,u.PRICONTACT, b.createddatetime, b.is_medicine, b.shippingmethod, b.paymentmethod,b.patientid, B.VendorName,ISNULL(u.ORDERAMOUNT ,'-') as ordAmount, ISNULL(u.POSTAL_CODE ,'-') as POSTAL, ISNULL(u.CITY,'-') as CITY, isnull(u.PSTATUS,'-') as TXNStatus, isnull(u.PORDERID,'-') as PayTMOrderID,u.del_add,b.site_ID,isnull(b.wod,'0') as WOD from ThirdPartyOrderHeader b,tpdeladdupdate U,CC_Patient_Registration C  where  b.PATIENTID = C.PATIENTID AND B.ORDERID=U.ORDERID and b.orderstatus in (11) and b.VendorName in ('MDIndia') group by b.orderid, U.patname, u.PRICONTACT, b.createddatetime, b.is_medicine, b.shippingmethod, b.paymentmethod,b.patientid,B.VendorName,U.ORDERAMOUNT,u.POSTAL_CODE,u.CITY,u.PSTATUS,u.PORDERID,u.del_add,b.site_ID,b.WOD order by b.createddatetime desc");
            } else if (_DisplayData.equals("EstimationInProcess")) {
                ps = con.prepareStatement("select b.orderid, U.patname AS CUSTNAME,u.PRICONTACT, b.createddatetime, b.is_medicine, b.shippingmethod, b.paymentmethod,b.patientid, B.VendorName,ISNULL(u.ORDERAMOUNT ,'-') as ordAmount, ISNULL(u.POSTAL_CODE ,'-') as POSTAL, ISNULL(u.CITY,'-') as CITY, isnull(u.PSTATUS,'-') as TXNStatus, isnull(u.PORDERID,'-') as PayTMOrderID,u.del_add,b.site_ID,isnull(b.wod,'0') as WOD from ThirdPartyOrderHeader b,tpdeladdupdate U,CC_Patient_Registration C  where  b.PATIENTID = C.PATIENTID AND B.ORDERID=U.ORDERID and b.orderstatus in (12) and b.VendorName in ('MDIndia') group by b.orderid, U.patname, u.PRICONTACT, b.createddatetime, b.is_medicine, b.shippingmethod, b.paymentmethod,b.patientid,B.VendorName,U.ORDERAMOUNT,u.POSTAL_CODE,u.CITY,u.PSTATUS,u.PORDERID,u.del_add,b.site_ID,b.WOD order by b.createddatetime desc");
            } else if (_DisplayData.equals("EstimationReview")) {
                ps = con.prepareStatement("select b.orderid, U.patname AS CUSTNAME,u.PRICONTACT, b.createddatetime, b.is_medicine, b.shippingmethod, b.paymentmethod,b.patientid, B.VendorName,ISNULL(u.ORDERAMOUNT ,'-') as ordAmount, ISNULL(u.POSTAL_CODE ,'-') as POSTAL, ISNULL(u.CITY,'-') as CITY, isnull(u.PSTATUS,'-') as TXNStatus, isnull(u.PORDERID,'-') as PayTMOrderID,u.del_add,b.site_ID,isnull(b.wod,'0') as WOD from ThirdPartyOrderHeader b,tpdeladdupdate U,CC_Patient_Registration C  where  b.PATIENTID = C.PATIENTID AND B.ORDERID=U.ORDERID and b.orderstatus in (13) and b.VendorName in ('MDIndia') group by b.orderid, U.patname, u.PRICONTACT, b.createddatetime, b.is_medicine, b.shippingmethod, b.paymentmethod,b.patientid,B.VendorName,U.ORDERAMOUNT,u.POSTAL_CODE,u.CITY,u.PSTATUS,u.PORDERID,u.del_add,b.site_ID,b.WOD order by b.createddatetime desc");
            } else if (_DisplayData.equalsIgnoreCase("Apollo247NonCartEstimationOrders")) {
                ps = con.prepareStatement("select b.orderid, U.patname AS CUSTNAME,C.MOBILENO, b.createddatetime, b.is_medicine, b.shippingmethod, b.paymentmethod,b.patientid, B.VendorName,ISNULL(u.ORDERAMOUNT ,'-') as ordAmount, ISNULL(u.POSTAL_CODE ,'-') as POSTAL, ISNULL(u.CITY,'-') as CITY, isnull(u.PSTATUS,'-') as TXNStatus, isnull(u.PORDERID,'-') as PayTMOrderID,u.del_add,b.site_ID,isnull(b.wod,'0') as WOD from ThirdPartyOrderHeader b,tpdeladdupdate U,CC_Patient_Registration C  where  b.PATIENTID = C.PATIENTID AND B.ORDERID=U.ORDERID and b.orderstatus in (0,7,11) and b.VendorName in ('Apollo247') and pstatus in ('approved','Txn Success','APPROVAL','Success','TXN_SUCCESS') and len(u.postal_code) = 6 group by b.orderid, U.patname, C.MOBILENO, b.createddatetime, b.is_medicine, b.shippingmethod, b.paymentmethod,b.patientid,B.VendorName,U.ORDERAMOUNT,u.POSTAL_CODE,u.CITY,u.PSTATUS,u.PORDERID,u.del_add,b.site_ID,b.WOD union all select b.orderid, U.patname AS CUSTNAME,C.MOBILENO, b.createddatetime, b.is_medicine, b.shippingmethod, b.paymentmethod,b.patientid, B.VendorName,ISNULL(u.ORDERAMOUNT ,'-') as ordAmount, ISNULL(u.POSTAL_CODE ,'-') as POSTAL, ISNULL(u.CITY,'-') as CITY, isnull(u.PSTATUS,'-') as TXNStatus, isnull(u.PORDERID,'-') as PayTMOrderID,u.del_add,b.site_ID,isnull(b.wod,'0') as WOD from ThirdPartyOrderHeader b,tpdeladdupdate U,CC_Patient_Registration C  where  b.PATIENTID = C.PATIENTID AND B.ORDERID=U.ORDERID and b.orderstatus in (11) and b.VendorName in ('Apollo247') and b.paymentmethod = 'COD' and len(u.postal_code) = 6 group by b.orderid, U.patname, C.MOBILENO, b.createddatetime, b.is_medicine, b.shippingmethod, b.paymentmethod,b.patientid,B.VendorName,U.ORDERAMOUNT,u.POSTAL_CODE,u.CITY,u.PSTATUS,u.PORDERID,u.del_add,b.site_ID,b.WOD order by b.createddatetime desc");
            }
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                SimpleDateFormat sd = new SimpleDateFormat("dd-MMM-yy hh:mm");
                String ordDt = rs.getString(4).substring(0, 16);
                String shippingmethod = rs.getString(6).toUpperCase();
                String Postal = rs.getString(11);
                String City = rs.getString(12).toUpperCase();
                String cartType = "Non-Cart";
                if (rs.getInt(17) >= 1) {
                    cartType = "Cart";
                }
                tpordsum.add(new TPOrder(rs.getString(1), rs.getString(2).toUpperCase(), rs.getString(3), ordDt, rs.getString(5).toUpperCase(), shippingmethod, rs.getString(7).toUpperCase(), rs.getString(8), rs.getString(9), rs.getString(10), Postal, City, rs.getString(13), rs.getString(14), rs.getString(15) + " " + City + " " + Postal, rs.getString(16), cartType));
            }
            System.out.println(new java.util.Date());
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
        }
        return tpordsum;
    }

    public List<TPOrderSummary> getSummaryData() {
        Connection con = null;
        tpordsummary.clear();
        try {
            con = getLDBConnection();
            PreparedStatement ps = con.prepareStatement("select vendorname Vendor,sum(isnull(cartpharma,0)) CartPharma,sum(isnull(cartfmcg,0)) CartFmcg,sum(isnull(CartBlank,0)) CartBlank,sum(isnull(CartBoth,0)) CartBoth,sum(isnull(cartpharma,0))+sum(isnull(cartfmcg,0))+sum(isnull(CartBlank,0))+sum(isnull(CartBoth,0)) Totalcart,sum(isnull(noncartpharma,0)) NonCartPharma,sum(isnull(noncartfmcg,0)) NonCartFmcg,sum(isnull(noncartblank,0)) NonCartBlank,sum(isnull(noncartboth,0))  NonCartBoth,sum(isnull(noncartpharma,0))+sum(isnull(noncartfmcg,0))+sum(isnull(noncartblank,0))+sum(isnull(noncartboth,0)) TotalNoncart from(select b.vendorname,b.pharma CartPharma,b.fmcg CartFmcg,b.blank CartBlank,b.both CartBoth,'' NonCartpharma, '' NonCartfmcg,'' NonCartBlank, '' NonCartboth from(select vendorname,[Pharma],[FMCG],[BLANK] as 'BLANK',[BOTH]   from(select VendorName,case when is_medicine='' then'BLANK' else is_medicine end TYPE,count(OrderId)coid,(CASE WHEN (WOD >=1) THEN ('CART') ELSE 'NONCART' END) as cnc  from ThirdPartyOrderHeader where wod>=1 and createddatetime >='01-Dec-2017' and OrderStatus in (0,7,11) group by VendorName,is_medicine,(CASE WHEN (WOD >=1) THEN ('CART') ELSE 'NONCART' END))tab1 pivot (sum(coid) for type in([Pharma],[FMCG],[BLANK],[BOTH])) as a) b union all select b.vendorname,'' CartPharma,'' CartFmcg,'' Cartblank,'' CartBoth,b.pharma,b.fmcg,b.blank,b.both from(select vendorname,[Pharma],[FMCG],[BLANK] as 'BLANK',[BOTH]   from(select VendorName,case when is_medicine='' then'BLANK' else is_medicine end TYPE,count(OrderId)coid,(CASE WHEN (WOD >=1) THEN ('CART') ELSE 'NONCART' END) as cnc  from ThirdPartyOrderHeader where wod<1 and createddatetime >='01-Dec-2017' and OrderStatus in (0,7,11) group by VendorName,is_medicine, (CASE WHEN (WOD >=1) THEN ('CART') ELSE 'NONCART' END))tab1 pivot (sum(coid) for type in([Pharma],[FMCG],[BLANK],[BOTH])) as a) b) final group by vendorname union all select 'Grand Total',sum([CartPharma]),sum([CartFmcg]),sum([CartBlank]),sum([CartBoth]),sum([Totalcart]),sum([NonCartPharma]),sum([NonCartFmcg]),sum([NonCartBlank]),sum([NonCartBoth]),sum([TotalNoncart]) from (select vendorname Vendor,sum(isnull(cartpharma,0)) CartPharma,sum(isnull(cartfmcg,0)) CartFmcg,sum(isnull(CartBlank,0)) CartBlank,sum(isnull(CartBoth,0)) CartBoth, sum(isnull(cartpharma,0))+sum(isnull(cartfmcg,0))+sum(isnull(CartBlank,0))+sum(isnull(CartBoth,0)) Totalcart,sum(isnull(noncartpharma,0)) NonCartPharma,sum(isnull(noncartfmcg,0)) NonCartFmcg,sum(isnull(noncartblank,0)) NonCartBlank,sum(isnull(noncartboth,0)) NonCartBoth, sum(isnull(noncartpharma,0))+sum(isnull(noncartfmcg,0))+sum(isnull(noncartblank,0))+sum(isnull(noncartboth,0)) TotalNoncart  from(select b.vendorname,b.pharma CartPharma,b.fmcg CartFmcg,b.blank CartBlank,b.both CartBoth,'' NonCartpharma, '' NonCartfmcg,'' NonCartBlank,'' NonCartBoth from(select vendorname,[Pharma],[FMCG],[BLANK] as 'BLANK',[BOTH]   from(select VendorName,case when is_medicine='' then'BLANK' else is_medicine end TYPE,count(OrderId)coid,(CASE WHEN (WOD >=1) THEN ('CART') ELSE 'NONCART' END) as cnc  from ThirdPartyOrderHeader where wod>=1 and createddatetime >='01-Dec-2017' and OrderStatus in (0,7,11) group by VendorName,is_medicine,(CASE WHEN (WOD >=1) THEN ('CART') ELSE 'NONCART' END))tab1 pivot (sum(coid) for type in([Pharma],[FMCG],[BLANK],[BOTH])) as a) b union all select b.vendorname,'' CartPharma,'' CartFmcg,'' Cartblank,'' Cartboth,b.pharma,b.fmcg,b.blank,b.both from(select vendorname,[Pharma],[FMCG],[BLANK] as 'BLANK',[BOTH]   from(select VendorName,case when is_medicine='' then'BLANK' else is_medicine end TYPE,count(OrderId)coid,(CASE WHEN (WOD >=1) THEN ('CART') ELSE 'NONCART' END) as cnc  from ThirdPartyOrderHeader where wod<1 and createddatetime >='01-Dec-2017' and OrderStatus in (0,7,11) group by VendorName,is_medicine,(CASE WHEN (WOD >=1) THEN ('CART') ELSE 'NONCART' END))tab1 pivot (sum(coid) for type in([Pharma],[FMCG],[BLANK],[BOTH])) as a) b) final group by vendorname) a");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                tpordsummary.add(new TPOrderSummary(rs.getString(1).toUpperCase(), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9), rs.getString(10), rs.getString(11)));
            }
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
        }
        return tpordsummary;
    }

    public List<MDIndiaOrderSummary> getMDIndiaSummaryData() {
        Connection con = null;
        orderSummaryList.clear();
        try {
            con = getLDBConnection();
            PreparedStatement ps = con.prepareStatement("select OrderStatus,count(orderid) from ThirdPartyOrderHeader where vendorname in ('MDIndia') and orderstatus in (11,12,13) group by OrderStatus");
            ResultSet rs = ps.executeQuery();
            String status = "";
            while (rs.next()) {
                status = rs.getString(1);
                if (status.equals("11")) {
                    status = "Estimations";
                } else if (status.equals("12")) {
                    status = "In Process";
                } else if (status.equals("13")) {
                    status = "In Review";
                }
                orderSummaryList.add(new MDIndiaOrderSummary(status, rs.getString(2)));
            }
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
        }
        return orderSummaryList;
    }

    public List<UploadedPrescDocs> showPrescriptions(Connection _con, String _ordNo, String _vendorName) {

        prescriptionURLs.clear();

        String pres_img_url;
        try {
            PreparedStatement ps = _con.prepareStatement("select presc_image,vendorName from ThirdPartyOrderHeader where orderID = ? and vendorname = ?");
            ps.setString(1, _ordNo);
            ps.setString(2, _vendorName);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                String vendName = rs.getString(2);
                if (vendName.equalsIgnoreCase("AskApollo") || vendName.equalsIgnoreCase("SIDBIE") || vendName.equalsIgnoreCase("Kaizala") || vendName.equalsIgnoreCase("FHPL")) {
                    pres_img_url = rs.getString(1).replace("[", "").replace("]", "").replace("\\/", "/").replace("\"", "/").replace("E:\\AppDoc\\", "http://172.16.2.251:8085/IA/Images/");
                } else {
                    pres_img_url = rs.getString(1).replace("[", "").replace("]", "").replace("\\/", "/").replace("\"", "");
                }
                String strArray[] = pres_img_url.split(",");
                String extension = "";
                if (pres_img_url.contains(".")) {
                    extension = pres_img_url.replace(" ", "%20").substring(pres_img_url.lastIndexOf("."));
                    if (extension.contains("jpg") || extension.contains("jpeg") || extension.contains("png") || extension.contains("gif")) {
                        for (int i = 0; i < strArray.length; i++) {
                            prescriptionURLs.add(new UploadedPrescDocs("Prescription" + (i + 1), "<img src=" + strArray[i].replace(" ", "%20") + " height=\"500px\" width=\"500px\"></img>"));

                        }
                    } else {
                        for (int i = 0; i < strArray.length; i++) {
                            prescriptionURLs.add(new UploadedPrescDocs("Prescription" + (i + 1), "<a href=" + strArray[i].replace(" ", "%20") + " target=\"_blank\">" + "Prescription" + (i + 1) + "</a>"));
                        }
                    }
                }
            }
        } catch (Exception e) {
            out.println(e.getLocalizedMessage());
        }
        return prescriptionURLs;
    }

    public List<myCart> ItemDetails(Connection _con, String _ordNo, String _vendorName) {
        cart.clear();
        //Issue in this Page
        try {
            PreparedStatement ps = null;
            if (_vendorName.equalsIgnoreCase("AskApollo")) {
                ps = _con.prepareStatement("select d.ItemID,d.ItemName,d.Qty,case when vendorname in('AskApollo') then pm.MOU else pack_size end pack_Size,subtotal,d.APLineComment,d.TPLineComment from ThirdPartyOrderDetails d,CC_EComPriceMaster pm where d.orderid = ? and d.vendorname = ? and d.filestatus = 0 and d.ItemID = pm.ARTCODE union all select d.ItemID,d.ItemName,d.Qty,pack_size,subtotal,d.APLineComment,d.TPLineComment from ThirdPartyOrderDetails d where d.orderid = ? and d.vendorname in ('AskApollo') and d.filestatus = 0 and d.itemid = 'ESH0002' and d.vendorname = ?");
                ps.setString(1, _ordNo);
                ps.setString(2, _vendorName);
                ps.setString(3, _ordNo);
                ps.setString(4, _vendorName);
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    String h_artcode = rs.getString(1);
                    String h_artname = rs.getString(2);
                    int h_qty = rs.getInt(3);
                    int pack = Integer.parseInt(rs.getString(4));
                    double mrp = rs.getDouble(5);
                    String apLC = rs.getString(6);
                    String tpLC = rs.getString(7);
                    cart.add(new myCart(h_artcode, h_artname, h_qty, (h_qty * pack), mrp, 0.0, pack, apLC, tpLC));
                }
            } else if (_vendorName.equalsIgnoreCase("MediAssist")) {
                ps = _con.prepareStatement("select ItemID,ItemName,Qty,pack_Size,price_mrp,subtotal,APLineComment,TPLineComment from ThirdPartyOrderDetails  where orderid = ? and vendorname = ? and filestatus = 0");
                ps.setString(1, _ordNo);
                ps.setString(2, _vendorName);
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    String h_artcode = rs.getString(1);
                    String h_artname = rs.getString(2);
                    int h_qty = rs.getInt(3);
                    double packsize = rs.getDouble(4);
                    int pack = (int) packsize;
                    double h_mrp = rs.getDouble(5);
                    double subTotal = rs.getDouble(6);
                    String apLC = rs.getString(7);
                    String tpLC = rs.getString(8);
                    boolean art_exists = false;
                    for (int i = 0; i < cart.size(); i++) {
                        if (h_artcode.equalsIgnoreCase(cart.get(i).getArtcode())) {
                            art_exists = true;
                            int qoh = h_qty + cart.get(i).getQoh();
                            int reqqty = (h_qty * pack) + cart.get(i).getReqQTY();
                            double dubtot = subTotal + cart.get(i).getSubtotal();
                            cart.set(i, new myCart(h_artcode, h_artname, qoh, reqqty, h_mrp, dubtot, pack, apLC, tpLC));
                            break;
                        }
                    }
                    if (!art_exists) {
                        cart.add(new myCart(h_artcode, h_artname, h_qty, h_qty, h_mrp, 0.0, pack, apLC, tpLC));
                    }
                }
            } else {
                ps = _con.prepareStatement("select ItemID,ItemName,Qty,pack_Size,price_mrp,subtotal,APLineComment,TPLineComment from ThirdPartyOrderDetails  where orderid = ? and vendorname = ? and filestatus = 0 ");
                ps.setString(1, _ordNo);
                ps.setString(2, _vendorName);
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    String h_artcode = rs.getString(1);
                    String h_artname = rs.getString(2);
                    int h_qty = rs.getInt(3);
                    double packsize = rs.getDouble(4);
                    int pack = (int) packsize;
                    double h_mrp = rs.getDouble(5);
                    double subTotal = rs.getDouble(6);
                    String apLC = rs.getString(7);
                    String tpLC = rs.getString(8);
                    boolean art_exists = false;
                    //qoh = pack, reqQTY = hqty
                    if (_vendorName.equalsIgnoreCase("Online") || _vendorName.equalsIgnoreCase("Apollo247")) {
                        cart.add(new myCart(h_artcode, h_artname, pack, h_qty, subTotal, subTotal, (h_qty / pack), apLC, tpLC));
                    } else if (_vendorName.equalsIgnoreCase("MDIndia")) {
//                        cart.add(new myCart(h_artcode, h_artname, (h_qty / pack), h_qty, h_mrp, subTotal, pack, apLC, tpLC));

                        for (int i = 0; i < cart.size(); i++) {
                            if (h_artcode.equalsIgnoreCase(cart.get(i).getArtcode())) {
                                art_exists = true;
                                int qoh = h_qty + cart.get(i).getQoh();
                                int reqqty = (h_qty * pack) + cart.get(i).getReqQTY();
                                double dubtot = subTotal + cart.get(i).getSubtotal();
                                cart.set(i, new myCart(h_artcode, h_artname, qoh, reqqty, h_mrp, dubtot, pack, apLC, tpLC));
                                break;
                            }
                        }

                        if (!art_exists) {
                            cart.add(new myCart(h_artcode, h_artname, (h_qty / pack), h_qty, h_mrp, subTotal, pack, apLC, tpLC));
                        }

                    }
//                    else {
//
//                        for (int i = 0; i < cart.size(); i++) {
//                            if (h_artcode.equalsIgnoreCase(cart.get(i).getArtcode())) {
//                                art_exists = true;
//                                int qoh = h_qty + cart.get(i).getQoh();
//                                int reqqty = (h_qty * pack) + cart.get(i).getReqQTY();
//                                double dubtot = subTotal + cart.get(i).getSubtotal();
//                                cart.set(i, new myCart(h_artcode, h_artname, qoh, reqqty, h_mrp, dubtot, pack, apLC, tpLC));
//                                break;
//                            }
//                        }
//
//                        if (!art_exists) {
//                            cart.add(new myCart(h_artcode, h_artname, h_qty, h_qty, h_mrp, 0.0, pack, apLC, tpLC));
//                        }
//
//                    }
                }
            }
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
        }
        return cart;
    }

    public List<String> collectMedicineList(String query) {
        List<String> results = new ArrayList<>();
        Connection con = null;
        try {
            con = getAXConnection();
            PreparedStatement ps = con.prepareStatement("select itemid,acxitemdescription from inventtable where acxitemdescription like '%" + query + "%'");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                results.add(rs.getString(1) + "-" + rs.getString(2).toUpperCase());
            }
        } catch (SQLException e) {
        }
        return results;
    }

    public List<itemDetails> collectSelectedMedicineDetails(String _newArticle) {
        Connection con = null;
        itemdetail.clear();
        String string = _newArticle.trim();
        String[] art = string.split("-");
        String artcode = art[0].trim(); // 004
        ResultSet rs = null;
        PreparedStatement ps = null;
        try {
            con = getAXConnection();
            ps = con.prepareStatement("select itemid,(select acxitemname from inventtable where itemid=a.itemid)itemname,ISNULL(acxmaximumretailprice_in,'0') mrp, ISNULL(proddate,'-') lastcreated,(select multipleqty from inventiteminventsetup where itemid = a.itemid and inventdimid = 'AllBlank' and partition = 5637144576 and dataareaid = 'ahel')  from inventbatch a where proddate in(select max(proddate) from inventbatch where a.itemid=itemid) and partition ='5637144576' and dataareaid='ahel' and itemid = ? group by itemid,acxmaximumretailprice_in,proddate ORDER BY 1 ASC");
            ps.setString(1, artcode);
            rs = ps.executeQuery();
            if (rs.next()) {
                String gart = rs.getString(1).toUpperCase();
                String gname = rs.getString(2).toUpperCase();
                //int gqoh = 5;
                double mrp = rs.getDouble(3);
                int pack = rs.getInt(5);
                itemdetail.add(new itemDetails(gart, gname, pack, mrp, pack));
            } else {
                con.close();
                con = getAX2Connection();
                ps = con.prepareStatement("select itemid,(select acxitemname from inventtable where itemid=a.itemid)itemname,ISNULL(acxmaximumretailprice_in,'0') mrp, ISNULL(proddate,'-') lastcreated,(select multipleqty from inventiteminventsetup where itemid = a.itemid and inventdimid = 'AllBlank' and partition = 5637144576 and dataareaid = 'ahel')  from inventbatch a where proddate in(select max(proddate) from inventbatch where a.itemid=itemid) and partition ='5637144576' and dataareaid='ahel' and itemid = ? group by itemid,acxmaximumretailprice_in,proddate ORDER BY 1 ASC");
                ps.setString(1, artcode);
                rs = ps.executeQuery();
                if (rs.next()) {
                    String gart = rs.getString(1).toUpperCase();
                    String gname = rs.getString(2).toUpperCase();
                    //int gqoh = 5;
                    double mrp = rs.getDouble(3);
                    int pack = rs.getInt(5);

                    itemdetail.add(new itemDetails(gart, gname, pack, mrp, pack));
                }
            }
        } catch (SQLException e) {
            System.out.println(e.getLocalizedMessage());
        } finally {
        }
        return itemdetail;
    }

    public List<String> complete(String query) {
        List<String> results = new ArrayList<>();
        Connection con = null;
        try {
            con = getLDBConnection();
            PreparedStatement ps = con.prepareStatement("select siteid,name,HS from cc_sun_locationdetails where name like '%" + query + "%' or siteid like '" + query + "%' group by name,siteid,HS order by name, siteid");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                results.add(rs.getString(1) + " " + rs.getString(2).toUpperCase() + " - " + rs.getString(3));
            }
        } catch (Exception e) {
        }
        return results;
    }
}
