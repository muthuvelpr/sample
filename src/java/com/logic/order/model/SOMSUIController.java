/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.logic.order.model;

import static ccm.dao.bean.connectivity.getLDBConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author pitchaiah_m
 */
public class SOMSUIController {

    public boolean canCancelfromSOMS(String _vendName) throws SQLException {
        Connection con = null;
        try {
            con = getLDBConnection();
            PreparedStatement ps = con.prepareStatement("select omscancel from CC_OMSUI_Control where vendorname = ?");
            ps.setString(1, _vendName);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                if (rs.getString(1).equals("1")) {
                    return true;
                } else {
                    return false;
                }
            }
        } catch (Exception e) {
            System.out.println(new java.util.Date() + " com.logic.order.model.OrderCancellation.canCancelfromSOMS - " + e.getLocalizedMessage());
        } finally {
            con.close();
        }
        return false;
    }
    
     public boolean canEstimateEligible(String _vendName) throws SQLException {
        Connection con = null;
        try {
            con = getLDBConnection();
            PreparedStatement ps = con.prepareStatement("select omsestimate from CC_OMSUI_Control where vendorname = ?");
            ps.setString(1, _vendName);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                if (rs.getString(1).equals("1")) {
                    return true;
                } else {
                    return false;
                }
            }
        } catch (Exception e) {
            System.out.println(new java.util.Date() + " com.logic.order.model.OrderCancellation.canCancelfromSOMS - " + e.getLocalizedMessage());
        } finally {
            con.close();
        }
        return false;
    }
}
