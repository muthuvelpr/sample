/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.logic.order.bean;

import static ccm.dao.bean.connectivity.getLDBConnection;
import com.google.gson.Gson;
import com.logic.order.model.OnlineOrderUpdateLog;
import com.util.OnlinePOSTHandler;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.UUID;

/**
 *
 * @author MUTHUVEL
 */
public class test {

    public static void main(String args[]) {
        String resp = OnlineOrderConfirmationUpdate("Online", "8000000034");
        System.out.println(resp);
//        String res = "{ \"tat\" : \"Expected Delivery date : 20-Sep-2019  20:00 PM from 14057\"}";
//        System.out.println(test.findAllottedSiteId(res));

    }

    public static String OnlineOrderConfirmationUpdate(String _VendorName, String _genOrdno) {
        String status = "failure";
        try {
            OrderConfirmRequest oc = new OrderConfirmRequest();
            ArrayList<ItemDetail> itemList = new ArrayList<ItemDetail>();
            Gson gson = new Gson();
            Connection con = null;
            con = getLDBConnection();
            OnlineOrderUpdateLog oLog = new OnlineOrderUpdateLog();
            OnlinePOSTHandler oPH = new OnlinePOSTHandler();

            String query = "select ti.tpordid as magentoOrderID , td.patname, td.pricontact,td.del_add,td.POSTAL_CODE,h.ordno as fulfillmentID from cc_ordhead h, cc_tporderinfo ti, tpdeladdupdate td where cast(h.ordno as nvarchar) = ti.apordid and td.orderid = ti.tpordid  and h.ordno = ? and h.orderSource = 'Online'";
            PreparedStatement ps = con.prepareStatement(query);
            ps.setString(1, _genOrdno);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                oc.setMagentoOrderID(rs.getString(1));
                oc.setCustomername(rs.getString(2));
                oc.setCustomerMobileNumber(rs.getString(3));
                oc.setCustomerAddress(rs.getString(4));
                oc.setPostalCode(rs.getString(5));
                oc.setFullfilmentID(rs.getString(6));

            }
            String itemQuery = "select artcode as sku, artName as itemname, reqqoh as qty from cc_orddet where ordno = ? ";
            PreparedStatement ps1 = con.prepareStatement(itemQuery);
            ps1.setString(1, _genOrdno);
            ResultSet rs1 = ps1.executeQuery();
            while (rs1.next()) {
                itemList.add(new ItemDetail(rs1.getString(1), rs1.getString(2), rs1.getInt(3)));
            }
            oc.setItemDetail(itemList);
            String reqStr = gson.toJson(oc);
            UUID uuid = UUID.randomUUID();
            String sid = uuid.toString();
            oLog.ReqLog(con, _genOrdno, _VendorName, reqStr, sid);
            String res = oPH.OnlineReverseEngineering(reqStr, "OrderConfirmation");
            if (res.contains("success")) {
                status = "success";
            }
            oLog.ResLog(con, _genOrdno, status, _VendorName, res, sid);
            ps.close();
            ps1.close();
            rs.close();
            rs1.close();
            con.close();

        } catch (Exception ex) {
            System.out.println("com.logic.order.model.TPOrderStatusUpdate.OnlineOrderConfirmationUpdate()" + ex.getLocalizedMessage());
        }

        return status;
    }

    public static String findAllottedSiteId(String resp) {

        String fin = resp.replaceAll("[^0-9]", "");

        return fin.substring(fin.length() - 5);

    }

}
