/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.logic.order.bean;

/**
 *
 * @author MUTHUVEL
 */
public class MDIndiaOrderSummary {

    String status, statusCount;

    public MDIndiaOrderSummary(String status, String statusCount) {
        this.status = status;
        this.statusCount = statusCount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusCount() {
        return statusCount;
    }

    public void setStatusCount(String statusCount) {
        this.statusCount = statusCount;
    }

}
