/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.logic.order.bean;

/**
 *
 * @author MUTHUVEL
 */
public class ItemDetail {

    String sku;
    String itemname;
    int qty;

    public ItemDetail() {
    }

    public ItemDetail(String sku, String itemname, int qty) {
        this.sku = sku;
        this.itemname = itemname;
        this.qty = qty;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getItemname() {
        return itemname;
    }

    public void setItemname(String itemname) {
        this.itemname = itemname;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

}
