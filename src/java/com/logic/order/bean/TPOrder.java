/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.logic.order.bean;

/**
 *
 * @author PITCHU
 */
public class TPOrder {

    String ordNo, custName, mobNo, ordDT, is_med, shipping, payment, patID, vendorName, receivedCash, postalCode, City, TXNStatus, TXNID, delAddress, prefferedSiteID, WOD;
    boolean is_pendingPayment;

    public TPOrder(String ordNo, String custName, String mobNo, String ordDT, String is_med, String shipping, String payment, String patID, String vendorName, String receivedCash, String postalCode, String City, String TXNStatus, String TXNID, String delAddress, String prefferedSiteID, String WOD) {
        this.ordNo = ordNo;
        this.custName = custName;
        this.mobNo = mobNo;
        this.ordDT = ordDT;
        this.is_med = is_med;
        this.shipping = shipping;
        this.payment = payment;
        this.patID = patID;
        this.vendorName = vendorName;
        this.receivedCash = receivedCash;
        this.postalCode = postalCode;
        this.City = City;
        this.TXNStatus = TXNStatus;
        this.TXNID = TXNID;
        this.delAddress = delAddress;
        this.prefferedSiteID = prefferedSiteID;
        this.WOD = WOD;
    }

    public String getPrefferedSiteID() {
        return prefferedSiteID;
    }

    public void setPrefferedSiteID(String prefferedSiteID) {
        this.prefferedSiteID = prefferedSiteID;
    }

    public String getDelAddress() {
        return delAddress;
    }

    public void setDelAddress(String delAddress) {
        this.delAddress = delAddress;
    }

    public String getTXNStatus() {
        return TXNStatus;
    }

    public void setTXNStatus(String TXNStatus) {
        this.TXNStatus = TXNStatus;
    }

    public String getTXNID() {
        return TXNID;
    }

    public void setTXNID(String TXNID) {
        this.TXNID = TXNID;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String City) {
        this.City = City;
    }

    public String getReceivedCash() {
        return receivedCash;
    }

    public void setReceivedCash(String receivedCash) {
        this.receivedCash = receivedCash;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getPatID() {
        return patID;
    }

    public void setPatID(String patID) {
        this.patID = patID;
    }

    public String getOrdNo() {
        return ordNo;
    }

    public void setOrdNo(String ordNo) {
        this.ordNo = ordNo;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getMobNo() {
        return mobNo;
    }

    public void setMobNo(String mobNo) {
        this.mobNo = mobNo;
    }

    public String getOrdDT() {
        return ordDT;
    }

    public void setOrdDT(String ordDT) {
        this.ordDT = ordDT;
    }

    public String getIs_med() {
        return is_med;
    }

    public void setIs_med(String is_med) {
        this.is_med = is_med;
    }

    public String getShipping() {
        return shipping;
    }

    public void setShipping(String shipping) {
        this.shipping = shipping;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public String getWOD() {
        return WOD;
    }

    public void setWOD(String WOD) {
        this.WOD = WOD;
    }

}
