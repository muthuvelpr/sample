/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.logic.order.bean;

/**
 *
 * @author MUTHUVEL
 */
public class MDIndiaCancelRequest {

    String MDIOrderNo, ApolloOrderNo, CanceledBy, Remark;

    public MDIndiaCancelRequest() {
    }

    public MDIndiaCancelRequest(String MDIOrderNo, String ApolloOrderNo, String CanceledBy, String Remark) {
        this.MDIOrderNo = MDIOrderNo;
        this.ApolloOrderNo = ApolloOrderNo;
        this.CanceledBy = CanceledBy;
        this.Remark = Remark;
    }

    public String getMDIOrderNo() {
        return MDIOrderNo;
    }

    public void setMDIOrderNo(String MDIOrderNo) {
        this.MDIOrderNo = MDIOrderNo;
    }

    public String getApolloOrderNo() {
        return ApolloOrderNo;
    }

    public void setApolloOrderNo(String ApolloOrderNo) {
        this.ApolloOrderNo = ApolloOrderNo;
    }

    public String getCanceledBy() {
        return CanceledBy;
    }

    public void setCanceledBy(String CanceledBy) {
        this.CanceledBy = CanceledBy;
    }

    public String getRemark() {
        return Remark;
    }

    public void setRemark(String Remark) {
        this.Remark = Remark;
    }

}
