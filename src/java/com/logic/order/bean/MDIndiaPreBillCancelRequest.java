/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.logic.order.bean;

/**
 *
 * @author MUTHUVEL
 */
public class MDIndiaPreBillCancelRequest {

    String OrderNo, Remarks, VendorName;

    public MDIndiaPreBillCancelRequest() {
    }

    public MDIndiaPreBillCancelRequest(String OrderNo, String Remarks, String VendorName) {
        this.OrderNo = OrderNo;
        this.Remarks = Remarks;
        this.VendorName = VendorName;
    }

    public String getOrderNo() {
        return OrderNo;
    }

    public void setOrderNo(String OrderNo) {
        this.OrderNo = OrderNo;
    }

    public String getRemarks() {
        return Remarks;
    }

    public void setRemarks(String Remarks) {
        this.Remarks = Remarks;
    }

    public String getVendorName() {
        return VendorName;
    }

    public void setVendorName(String VendorName) {
        this.VendorName = VendorName;
    }

}
