/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.logic.order.bean;

import java.util.ArrayList;

/**
 *
 * @author MUTHUVEL
 */
public class OrderConfirmRequest {

    String magentoOrderID;
    String customername;
    String customerMobileNumber;
    String customerAddress;
    String postalCode;
    String fullfilmentID;
    ArrayList<ItemDetail> itemDetail;

    public OrderConfirmRequest() {
    }

    public OrderConfirmRequest(String magentoOrderID, String customername, String customerMobileNumber, String customerAddress, String postalCode, String fullfilmentID, ArrayList<ItemDetail> itemDetList) {
        this.magentoOrderID = magentoOrderID;
        this.customername = customername;
        this.customerMobileNumber = customerMobileNumber;
        this.customerAddress = customerAddress;
        this.postalCode = postalCode;
        this.fullfilmentID = fullfilmentID;
        this.itemDetail = itemDetList;
    }

    public String getMagentoOrderID() {
        return magentoOrderID;
    }

    public void setMagentoOrderID(String magentoOrderID) {
        this.magentoOrderID = magentoOrderID;
    }

    public String getCustomername() {
        return customername;
    }

    public void setCustomername(String customername) {
        this.customername = customername;
    }

    public String getCustomerMobileNumber() {
        return customerMobileNumber;
    }

    public void setCustomerMobileNumber(String customerMobileNumber) {
        this.customerMobileNumber = customerMobileNumber;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getFullfilmentID() {
        return fullfilmentID;
    }

    public void setFullfilmentID(String fullfilmentID) {
        this.fullfilmentID = fullfilmentID;
    }

    public ArrayList<ItemDetail> getItemDetail() {
        return itemDetail;
    }

    public void setItemDetail(ArrayList<ItemDetail> itemDetail) {
        this.itemDetail = itemDetail;
    }

}
