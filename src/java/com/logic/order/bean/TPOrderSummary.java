/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.logic.order.bean;

/**
 *
 * @author Administrator
 */
public class TPOrderSummary {

    private String Vendor, CPharma, CFMCG, CBlank, CBoth, CTotal, NCPharma, NCFMCG, NCBlank, NCBoth, NCTotal;

    public TPOrderSummary(String Vendor, String CPharma, String CFMCG, String CBlank, String CBoth, String CTotal, String NCPharma, String NCFMCG, String NCBlank, String NCBoth, String NCTotal) {
        this.Vendor = Vendor;
        this.CPharma = CPharma;
        this.CFMCG = CFMCG;
        this.CBlank = CBlank;
        this.CBoth = CBoth;
        this.CTotal = CTotal;
        this.NCPharma = NCPharma;
        this.NCFMCG = NCFMCG;
        this.NCBlank = NCBlank;
        this.NCBoth = NCBoth;
        this.NCTotal = NCTotal;
    }

    public String getVendor() {
        return Vendor;
    }

    public void setVendor(String Vendor) {
        this.Vendor = Vendor;
    }

    public String getCPharma() {
        return CPharma;
    }

    public void setCPharma(String CPharma) {
        this.CPharma = CPharma;
    }

    public String getCFMCG() {
        return CFMCG;
    }

    public void setCFMCG(String CFMCG) {
        this.CFMCG = CFMCG;
    }

    public String getCBlank() {
        return CBlank;
    }

    public void setCBlank(String CBlank) {
        this.CBlank = CBlank;
    }

    public String getCBoth() {
        return CBoth;
    }

    public void setCBoth(String CBoth) {
        this.CBoth = CBoth;
    }

    public String getCTotal() {
        return CTotal;
    }

    public void setCTotal(String CTotal) {
        this.CTotal = CTotal;
    }

    public String getNCPharma() {
        return NCPharma;
    }

    public void setNCPharma(String NCPharma) {
        this.NCPharma = NCPharma;
    }

    public String getNCFMCG() {
        return NCFMCG;
    }

    public void setNCFMCG(String NCFMCG) {
        this.NCFMCG = NCFMCG;
    }

    public String getNCBlank() {
        return NCBlank;
    }

    public void setNCBlank(String NCBlank) {
        this.NCBlank = NCBlank;
    }

    public String getNCBoth() {
        return NCBoth;
    }

    public void setNCBoth(String NCBoth) {
        this.NCBoth = NCBoth;
    }

    public String getNCTotal() {
        return NCTotal;
    }

    public void setNCTotal(String NCTotal) {
        this.NCTotal = NCTotal;
    }

}
