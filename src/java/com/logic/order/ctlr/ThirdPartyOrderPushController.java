/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.logic.order.ctlr;

import ccm.dao.bean.connectivity;
import ccm.logic.bean.CancelRemarks;
import ccm.logic.bean.UploadedPrescDocs;
import com.logic.order.bean.comments;
import ccm.logic.bean.itemDetails;
import ccm.logic.bean.myCart;
import ccm.logic.ctlr.sms;
import ccm.logic.model.OrderAllocation;
import ccm.report.model.invAvailability;
import com.google.gson.Gson;
import com.logic.order.bean.MDIndiaOrderSummary;
import com.logic.order.bean.TPOrder;
import com.logic.order.bean.TPOrderSummary;
import com.logic.order.estimation.bean.Apollo247EstimationItems;
import com.logic.order.estimation.bean.Apollo247EstimationSchema;
import com.logic.order.estimation.bean.Apollo247MedOrdInput;
import com.logic.order.estimation.bean.Apollo247Variables;
import com.logic.order.estimation.bean.MDIEstimationItems;
import com.logic.order.estimation.bean.MDIndiaEstimationSchema;
import com.logic.order.model.EstimationModel;
import com.logic.order.model.OrderCancellation;
import com.logic.order.model.OrderComment;
import com.logic.order.model.OrderPlace;
import com.logic.order.model.TPOrderModel;
import com.logic.order.model.TPOrderStatusUpdate;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.primefaces.event.RowEditEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.ToggleEvent;
import org.primefaces.event.UnselectEvent;

/**
 *
 * @author pitchaiah_m
 */
@ManagedBean(name = "tporder")
@ViewScoped
public class ThirdPartyOrderPushController implements Serializable {

    List<TPOrder> tpordsum = new ArrayList();
    List<TPOrderSummary> tpordsummary = new ArrayList();
    List<TPOrder> filteredOrder;
    private TPOrder selectedOrder;
    List<UploadedPrescDocs> prescriptionURLs = new ArrayList();
    List<myCart> cart = new ArrayList();
    List<itemDetails> itemdetail = new ArrayList();
    List<invAvailability> invAvail = new ArrayList();
    List<CancelRemarks> CanRem = new ArrayList();
    List<String> cancelBase = new ArrayList<>();
    List<comments> thisOrderComment = new ArrayList();
    List<MDIEstimationItems> mdiesil = new ArrayList<>();
    List<MDIndiaEstimationSchema> mdis = new ArrayList<>();
    List<Apollo247EstimationItems> ap247esil = new ArrayList<>();
//    List<Apollo247EstimationSchema> ap247is = new ArrayList<>();

    String pageName = "";

    private myCart selectedItem;
    private int reqQTY;
    private int pack;
    private double mrp;
    private String AddNewArticle;
    private String artname;
    private String artcode;
    private String siteid = "16001";
    private Date delDT = new Date();
    private String remarks;
    private String shipping;
    private String payment;
    private int selecetedOrderRowNum = 0;
    private String cancelRemarksCode;
    private String cancelTabDisable = "true";
    private String commentData = "";
    private String aplinecomment;

    private String cancelBaseReason = "";
    TPOrderModel tpOrderModel = new TPOrderModel();
    List<MDIndiaOrderSummary> mdSummaryList = new ArrayList<>();

    public ThirdPartyOrderPushController() {
        try {
            FacesContext ctx = FacesContext.getCurrentInstance();
            pageName = ctx.getViewRoot().getViewId().replace("/", "").replace(".xhtml", "");
            showAllOrdersfromAllvendorsSummary();
            if (pageName.equalsIgnoreCase("TPOrders")) {
                showAllOrdersfromAllvendors("ThirdParty");
            } else if (pageName.equalsIgnoreCase("PreVerificationOrders")) {
                showAllOrdersfromAllvendors("AskApollo");
            } else if (pageName.equalsIgnoreCase("AskApollo")) {
                showAllOrdersfromAllvendors("MAskApollo");
            } else if (pageName.equalsIgnoreCase("Apollo247") || pageName.equalsIgnoreCase("PreVerificationOrders247")) {
                showAllOrdersfromAllvendors("Apollo247");
            } else if (pageName.equalsIgnoreCase("Online") || pageName.equalsIgnoreCase("OnlineAuto")) {
                showAllOrdersfromAllvendors("OnlinePaid");
            } else if (pageName.equalsIgnoreCase("CommerceNonPaid")) {
                showAllOrdersfromAllvendors("CommerceNonPaid");
            } else if (pageName.equalsIgnoreCase("EstimationOrders")) {
                showAllOrdersfromAllvendors("EstimationOrders");
                showMDIndiaSummary();
            } else if (pageName.equalsIgnoreCase("EstimationReview")) {
                showAllOrdersfromAllvendors("EstimationReview");
                showMDIndiaSummary();
            } else if (pageName.equalsIgnoreCase("EstimationInProcess")) {
                showAllOrdersfromAllvendors("EstimationInProcess");
                showMDIndiaSummary();
            } else if (pageName.equalsIgnoreCase("Apollo247NonCartEstimationOrders")) {
                showAllOrdersfromAllvendors("Apollo247NonCartEstimationOrders");
            }
            this.selectedOrder = ((TPOrder) this.tpordsum.get(0));
            OrderCancellation cancel = new OrderCancellation();
            boolean cancelAllowed = cancel.canCancelfromSOMS(this.selectedOrder.getVendorName());
            if (cancelAllowed) {
                this.cancelBase = cancel.listCancelBase(this.selectedOrder.getVendorName());
                //this.CanRem = cancel.listCancelRemarks(this.selectedOrder.getVendorName());
                this.cancelTabDisable = "false";
            } else {
                this.cancelTabDisable = "true";
            }
        } catch (Exception ex) {
            Logger.getLogger(ThirdPartyOrderPushController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<TPOrder> getFilteredOrder() {
        return this.filteredOrder;
    }

    public String getPageName() {
        return pageName;
    }

    public void setPageName(String pageName) {
        this.pageName = pageName;
    }

    public void setFilteredOrder(List<TPOrder> filteredOrder) {
        this.filteredOrder = filteredOrder;
    }

    public String getCancelBaseReason() {
        return cancelBaseReason;
    }

    public void setCancelBaseReason(String cancelBaseReason) {
        this.cancelBaseReason = cancelBaseReason;
    }

    public String getCommentData() {
        return this.commentData;
    }

    public void setCommentData(String commentData) {
        this.commentData = commentData;
    }

    public String getCancelTabDisable() {
        return this.cancelTabDisable;
    }

    public void setCancelTabDisable(String cancelTabDisable) {
        this.cancelTabDisable = cancelTabDisable;
    }

    public String getCancelRemarksCode() {
        return this.cancelRemarksCode;
    }

    public void setCancelRemarksCode(String cancelRemarksCode) {
        this.cancelRemarksCode = cancelRemarksCode;
    }

    public int getSelecetedOrderRowNum() {
        return this.selecetedOrderRowNum;
    }

    public void setSelecetedOrderRowNum(int selecetedOrderRowNum) {
        this.selecetedOrderRowNum = selecetedOrderRowNum;
    }

    public String getPayment() {
        return this.payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public String getShipping() {
        return this.shipping;
    }

    public void setShipping(String shipping) {
        this.shipping = shipping;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Date getDelDT() {
        return this.delDT;
    }

    public void setDelDT(Date delDT) {
        this.delDT = delDT;
    }

    public String getAddNewArticle() {
        return this.AddNewArticle;
    }

    public String getSiteid() {
        return this.siteid;
    }

    public void setSiteid(String siteid) {
        this.siteid = siteid.substring(0, 5);
    }

    public void setAddNewArticle(String AddNewArticle) {
        this.AddNewArticle = AddNewArticle;
    }

    public double getMrp() {
        return this.mrp;
    }

    public void setMrp(double mrp) {
        this.mrp = mrp;
    }

    public int getPack() {
        return pack;
    }

    public void setPack(int pack) {
        this.pack = pack;
    }

    public String getArtname() {
        return this.artname;
    }

    public void setArtname(String artname) {
        this.artname = artname;
    }

    public String getArtcode() {
        return this.artcode;
    }

    public void setArtcode(String artcode) {
        this.artcode = artcode;
    }

    public myCart getSelectedItem() {
        return this.selectedItem;
    }

    public void setSelectedItem(myCart selectedItem) {
        this.selectedItem = selectedItem;
    }

    public int getReqQTY() {
        return this.reqQTY;
    }

    public void setReqQTY(int reqQTY) {
        this.reqQTY = reqQTY;
    }

    public TPOrder getSelectedOrder() {
        return this.selectedOrder;
    }

    public void setSelectedOrder(TPOrder selectedOrder) {
        this.invAvail.clear();
        this.selectedOrder = selectedOrder;
        String ordNo = selectedOrder.getOrdNo();
        String vendorName = selectedOrder.getVendorName();
        listComments(ordNo, vendorName);
    }

    public List<MDIndiaOrderSummary> getMdSummaryList() {
        return mdSummaryList;
    }

    public void setMdSummaryList(List<MDIndiaOrderSummary> mdSummaryList) {
        this.mdSummaryList = mdSummaryList;
    }

    public final List<TPOrder> showAllOrdersfromAllvendors(String DisplayData) {
        this.tpordsum = this.tpOrderModel.getAllOrders(DisplayData);
        return this.tpordsum;
    }

    public List<TPOrder> getTpordsum() {
        return this.tpordsum;
    }

    public final List<TPOrderSummary> showAllOrdersfromAllvendorsSummary() {
        this.tpordsummary = this.tpOrderModel.getSummaryData();
        return this.tpordsummary;
    }

    public final List<MDIndiaOrderSummary> showMDIndiaSummary() {
        this.mdSummaryList = this.tpOrderModel.getMDIndiaSummaryData();
        return this.mdSummaryList;

    }

    public List<TPOrderSummary> getTpordsummary() {
        return tpordsummary;
    }

    public void onRowSelect(SelectEvent event) {
        this.selecetedOrderRowNum = this.tpordsum.indexOf((TPOrder) event.getObject());
        try {
            Connection con = null;
            con = connectivity.getLDBConnection();
            String _ordNo = ((TPOrder) event.getObject()).getOrdNo();
            String _vendorName = ((TPOrder) event.getObject()).getVendorName();
            this.prescriptionURLs = this.tpOrderModel.showPrescriptions(con, _ordNo, _vendorName);
            this.cart = this.tpOrderModel.ItemDetails(con, _ordNo, _vendorName);
            if (_vendorName.equalsIgnoreCase("MDIndia")) {
                siteid = selectedOrder.getPrefferedSiteID();
            }
            OrderCancellation cancel = new OrderCancellation();
            boolean cancelAllowed = cancel.canCancelfromSOMS(_vendorName);
            if (cancelAllowed) {
                this.cancelBase = cancel.listCancelBase(this.selectedOrder.getVendorName());
                this.cancelTabDisable = "false";
            } else {
                this.cancelTabDisable = "true";
            }
            con.close();
        } catch (Exception e) {
            System.out.println(new Date() + " com.logic.order.ctlr.ThirdPartyOrderPushController.onRowSelect - " + e.getLocalizedMessage());
        }
    }

    public List<CancelRemarks> updateCancelRemarks() {
        OrderCancellation cancel = new OrderCancellation();
        CanRem = cancel.listCancelRemarks(this.selectedOrder.getVendorName(), cancelBaseReason);
        return CanRem;
    }

    public List<UploadedPrescDocs> getPrescriptionURLs() {
        return this.prescriptionURLs;
    }

    public List<myCart> getCart() {
        return this.cart;
    }

    public void addItem() {

        //        FacesContext fc = FacesContext.getCurrentInstance();
//        Map<String, String> params = fc.getExternalContext().getRequestParameterMap();
        boolean art_exists = false;
        String _artCode = artcode;
        String _artName = artname;
        double _item_mrp = mrp;
        int _pack = pack;
        String _lineComment = aplinecomment;

        if (_artCode.equals("")) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Please Select ItemName!!!", "Please Select ItemName!!!"));
        } else if (_artName.equals("")) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Please Enter Artname", "Please Enter Artname"));
        } else if (this.reqQTY <= 0) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Please give QTY!!!", "Please give QTY!!!"));
        } else {
            //public myCart(String artcode, String artname, int qoh, int reqQTY, double mrp, double subtotal, int pack) {
            double mrp = (_pack * _item_mrp);
            BigDecimal bdmrp = new BigDecimal(mrp).setScale(2, RoundingMode.HALF_UP);
            double pre_mrp = bdmrp.doubleValue();
            double subtotal = (reqQTY * _item_mrp * _pack);
            BigDecimal bdsubtotal = new BigDecimal(subtotal).setScale(2, RoundingMode.HALF_UP);
            double pre_subtotal = bdsubtotal.doubleValue();
            HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
            String url = request.getRequestURL().toString();
            String uri = request.getRequestURI();
            //String artcode, String artname, int qoh, int reqQTY, double mrp, double subtotal, double pack, String aplinecomment, String tplinecomment

            for (int i = 0; i < cart.size(); i++) {
                if (_artCode.equalsIgnoreCase(cart.get(i).getArtcode())) {
                    art_exists = true;
                    int qoh = reqQTY + cart.get(i).getQoh();
                    int reqqty = (reqQTY * _pack) + cart.get(i).getReqQTY();
                    double dubtot = pre_subtotal + cart.get(i).getSubtotal();
                    this.cart.set(i, new myCart(_artCode, _artName, qoh, reqqty, pre_mrp, dubtot, _pack, _lineComment, ""));
                    break;
                }
            }

            if (!art_exists) {
                this.cart.add(new myCart(_artCode, _artName, this.reqQTY, (this.reqQTY * _pack), pre_mrp, pre_subtotal, _pack, _lineComment, ""));
            }

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, _artCode + " Added to Order", ""));
        }
//        this.artcode = "";
//        this.artname = "";
//        this.mrp = 0.0D;
//        this.reqQTY = 0;
//        this.pack = 0;
//        this.aplinecomment = "";
//        setAddNewArticle("");
    }

    public void deleteAction(myCart item) {
        this.cart.remove(item);
        FacesMessage msg = new FacesMessage("Item Deleted " + item.getArtcode().toUpperCase(), "");
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onRowEditing(RowEditEvent event) {
        int qty = this.selectedItem.getQoh();
        if (qty <= 0) {
            int req_qty = this.selectedItem.getReqQTY();
            double dpack = this.selectedItem.getPack();
            Double npack = dpack;
            int packsize = npack.intValue();
            int qoh = req_qty / packsize;
            double sTot = this.selectedItem.getMrp() * qoh;
            this.selectedItem.setQoh(qoh);
            this.selectedItem.setReqQTY(qoh * packsize);
            this.selectedItem.setSubtotal(sTot);
//            onRowCancel(event);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "The Item " + this.selectedItem.getArtname().toUpperCase() + " has Invalid Qty!!!", "Invalid Qty!!!"));
        } else {
            double price = this.selectedItem.getMrp() * this.selectedItem.getQoh();
            double dpack = this.selectedItem.getPack();
            Double npack = dpack;
            int packsize = npack.intValue();
            this.selectedItem.setReqQTY(qty * packsize);
            this.selectedItem.setSubtotal(price);
            FacesMessage msg = new FacesMessage("Edit performed for " + this.selectedItem.getArtname().toUpperCase() + price, "");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }

    public void onRowCancel(RowEditEvent event) {
        FacesMessage msg = new FacesMessage("Edit Cancelled for " + this.selectedItem.getArtname().toUpperCase(), "");
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void deleteItem() {
        this.cart.remove(this.selectedItem);
        this.selectedItem = null;
    }

    public void onRowUnselect(UnselectEvent event) {
    }

    public List<String> collectMedicineList(String query) {
        List<String> listArticles = new ArrayList();
        listArticles = this.tpOrderModel.collectMedicineList(query);
        return listArticles;
    }

    public void collectSelectedMedicineDetails() {
        this.itemdetail = this.tpOrderModel.collectSelectedMedicineDetails(this.AddNewArticle);
        for (itemDetails i : this.itemdetail) {
            this.artcode = i.getArtcode();
            this.artname = i.getArtname();
            this.mrp = i.getMrp();
            this.pack = i.getPack();
        }
    }

    public List<MDIndiaEstimationSchema> GenerateMDIndiaEstimation() {
        double total = 0.0;
        mdiesil.clear();
        mdis.clear();
        if (siteid.equals("16001")) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Please select different site!!!", "Please select different site!!!"));
        } else {

            for (myCart c : this.cart) {
                total += c.getSubtotal();
                BigDecimal bdsubtotal = new BigDecimal(c.getSubtotal()).setScale(2, RoundingMode.HALF_UP);
                double pre_subtotal = bdsubtotal.doubleValue();
                mdiesil.add(new MDIEstimationItems(c.getArtcode(), c.getArtname(), c.getReqQTY(), c.getMrp(), pre_subtotal, c.getAplinecomment()));
            }
            BigDecimal bdsubtotal = new BigDecimal(total).setScale(2, RoundingMode.HALF_UP);
            double pre_subtotal = bdsubtotal.doubleValue();
            mdis.add(new MDIndiaEstimationSchema(selectedOrder.getOrdNo(), siteid, pre_subtotal, mdiesil));

        }
        for (int i = 0; i < mdis.size(); i++) {
            System.out.println("com.logic.order.ctlr.ThirdPartyOrderPushController.GenerateMDIndiaEstimation()" + mdis.get(i).getItemdetails().get(i).getItemID());
        }

        return mdis;
    }

    public List<MDIndiaEstimationSchema> getMdis() {
        return mdis;
    }

    public String sendEstimation() {
        String mdIndiaPayLoad = "";
        double total = 0.0;
        mdiesil.clear();
        String ordno = selectedOrder.getOrdNo();
        String vendName = selectedOrder.getVendorName();
        HttpSession appSession = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        FacesContext fc = FacesContext.getCurrentInstance();
        Map<String, String> params = fc.getExternalContext().getRequestParameterMap();
        String smsStatus = (String) params.get("sendSms");
        int AgentID = Integer.parseInt(appSession.getAttribute("loginid").toString());
        if (siteid.equals("16001")) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Please select different site!!!", "Please select different site!!!"));
        } else {
            for (myCart c : this.cart) {
                total += c.getSubtotal();

                mdiesil.add(new MDIEstimationItems(c.getArtcode(), c.getArtname(), c.getReqQTY(), c.getMrp(), c.getSubtotal(), c.getAplinecomment()));
            }

            BigDecimal bdsubtotal = new BigDecimal(total).setScale(2, RoundingMode.HALF_UP);
            double pre_subtotal = bdsubtotal.doubleValue();

            MDIndiaEstimationSchema mds = new MDIndiaEstimationSchema();
            mds.setOrderid(selectedOrder.getOrdNo());
            mds.setShopid(siteid);
            mds.setEstimatedAmount(total);
            mds.setItemdetails(mdiesil);

            Gson gson = new Gson();
            mdIndiaPayLoad = gson.toJson(mds);
            System.out.println(mdIndiaPayLoad);
            EstimationModel em = new EstimationModel();
//            boolean canSendEstimate = em.checkEstimation(ordno, vendName);
            boolean canSendEstimate = true;
            if (canSendEstimate) {
                UUID uuid = UUID.randomUUID();
                String sid = uuid.toString();
                em.EstimationReqLog(ordno, vendName, AgentID, mdIndiaPayLoad, sid);
//                String response = em.SendEstimationtoMDIndia(mdIndiaPayLoad);
                String response = "Accept";
                em.EstimationResLog(ordno, vendName, response, sid);
                if (response.contains("Accept")) {
                    em.updateTPOrderDetail(cart, siteid, vendName, ordno);
                    em.updateTPOrderHeader(siteid, vendName, ordno);
                    em.updateDigitalItems(cart, siteid, vendName, ordno);
                    em.updateOrderAmount(String.valueOf(pre_subtotal), vendName, ordno);

                    if (smsStatus.equals("true")) {
                        sms _sms = new sms();
//                        _sms.sendDigitalizedSMS(this.selectedOrder.getMobNo(), this.selectedOrder.getOrdNo());
                    }

                    this.tpordsum.remove(this.selectedOrder);
                    this.cart.clear();
                    this.prescriptionURLs.clear();
                    this.remarks = "";
                    this.selectedOrder = ((TPOrder) this.tpordsum.get(this.selecetedOrderRowNum));

                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Estimation Send Successfully!!!", "Estimation Send Successfully!!!"));
                }
            } else {
                this.tpordsum.remove(this.selectedOrder);
                this.cart.clear();
                this.prescriptionURLs.clear();
                this.remarks = "";
                this.selectedOrder = ((TPOrder) this.tpordsum.get(this.selecetedOrderRowNum));
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Estimation already sent!!!", "Estimation already sent!!!"));
            }
        }
        return null;
    }

    public String sendApollo247Estimation() {
        String apollo247PayLoad = "";
        double total = 0.0;
        ap247esil.clear();
        String ordno = selectedOrder.getOrdNo();
        String vendName = selectedOrder.getVendorName();
        HttpSession appSession = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        int AgentID = Integer.parseInt(appSession.getAttribute("loginid").toString());
        if (siteid.equals("16001")) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Please select different site!!!", "Please select different site!!!"));
        } else {
            for (myCart c : this.cart) {
                total += c.getSubtotal();
//                mdiesil.add(new MDIEstimationItems(c.getArtcode(), c.getArtname(), c.getReqQTY(), c.getMrp(), c.getSubtotal(), c.getAplinecomment()));
//                ap247esil.add(new Apollo247EstimationItems((c.getArtcode(), c.getArtname(), c.getMrp(), c.getReqQTY(), c.getMrp(), c.))           );
            }
            Apollo247MedOrdInput ami = new Apollo247MedOrdInput();
            ami.setQuoteId(AgentID);
            ami.setShopId(siteid);
            ami.setEstimatedAmount(total);
            ami.setItems(ap247esil);

            Apollo247Variables av = new Apollo247Variables();
            av.setMedicineOrderInput(ami);

            Apollo247EstimationSchema aes = new Apollo247EstimationSchema();
            aes.setQuery("mutation getDigitizedPrescription($MedicineOrderInput:MedicineOrderInput!){getDigitizedPrescription(MedicineOrderInput:$MedicineOrderInput) { status errorCodeerrorMessage }}");
            aes.setVariables(av);

            Gson gson = new Gson();
            apollo247PayLoad = gson.toJson(aes);
            System.out.println(apollo247PayLoad);
            EstimationModel em = new EstimationModel();
//            boolean canSendEstimate = em.checkEstimation(ordno, vendName);
            boolean canSendEstimate = true;
            if (canSendEstimate) {
                UUID uuid = UUID.randomUUID();
                String sid = uuid.toString();
                em.EstimationReqLog(ordno, vendName, AgentID, apollo247PayLoad, sid);
                String response = em.SendEstimationtoApollo247(apollo247PayLoad);
//                String response = "Accept";
                em.EstimationResLog(ordno, vendName, response, sid);
                if (response.contains("Accept")) {
                    em.updateTPOrderDetail(cart, siteid, vendName, ordno);
                    em.updateTPOrderHeader(siteid, vendName, ordno);
                    em.updateDigitalItems(cart, siteid, vendName, ordno);
                    this.tpordsum.remove(this.selectedOrder);
                    this.cart.clear();
                    this.prescriptionURLs.clear();
                    this.remarks = "";
                    this.selectedOrder = ((TPOrder) this.tpordsum.get(this.selecetedOrderRowNum));
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Estimation Send Successfully!!!", "Estimation Send Successfully!!!"));
                }
            } else {
                this.tpordsum.remove(this.selectedOrder);
                this.cart.clear();
                this.prescriptionURLs.clear();
                this.remarks = "";
                this.selectedOrder = ((TPOrder) this.tpordsum.get(this.selecetedOrderRowNum));
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Estimation already sent!!!", "Estimation already sent!!!"));
            }
        }
        return null;
    }

    public List<invAvailability> showInventory() {
        this.invAvail.clear();
        if (this.siteid.contains("16001")) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Please select different site!!!", "Please select different site!!!"));
        } else {
            StringBuilder sb = new StringBuilder();
            for (myCart c : this.cart) {
                sb.append(c.getArtcode()).append(",");
            }
            String art = sb.toString().substring(0, sb.length() - 1);
            System.out.println(art);
            String url = "http://172.16.2.251:8085/WSS/mapp/inventoryCheck/" + this.siteid + "/" + art;
            Client restClient = Client.create();
            WebResource webResource = restClient.resource(url);

            ClientResponse resp = (ClientResponse) webResource.accept(new String[]{"application/json"}).get(ClientResponse.class);
            if (resp.getStatus() != 200) {
                System.err.println("Unable to connect to the server");
            }
            String output = (String) resp.getEntity(String.class);
            System.out.println("response: " + output);

            try {
                JSONObject obj = new JSONObject(output);
                JSONArray jsonArray = obj.getJSONArray("item");
                int count = jsonArray.length();
                for (int i = 0; i < count; i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    String invartCode = jsonObject.getString("artCode");
                    int invQty = jsonObject.getInt("qoh");
                    double invmrp = jsonObject.getDouble("mrp");
                    String exp = jsonObject.getString("expDate");
                    this.invAvail.add(new invAvailability(invartCode, invQty, invmrp, exp));
                }
            } catch (JSONException e) {
                System.out.println(e.getLocalizedMessage());
            }
        }
        return this.invAvail;
    }

    public List<invAvailability> getInvAvail() {
        return this.invAvail;
    }

    public List<itemDetails> getItemdetail() {
        return this.itemdetail;
    }

    public String createOrder() {
        String apolloordno = "";
        FacesContext fc = FacesContext.getCurrentInstance();
        Map<String, String> params = fc.getExternalContext().getRequestParameterMap();
        String splitStatus = (String) params.get("SplitStatus");
//        if (this.siteid.equals("16001")) {
//            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Please change the site 16001 is not a operating Outlet!!!", "Please change the site 16001 is not a operating Outlet!!!"));
//        } else {
        if (this.cart.size() <= 0) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Add Item to this Order!!!", "Add Item to this Order!!!"));
        } else {
            OrderPlace createOrder = new OrderPlace();
            HttpSession appSession = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
            String AgentID = appSession.getAttribute("loginid").toString();
            Connection con = null;
            try {
                con = connectivity.getLDBConnection();
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
                String tat = sdf.format(delDT);
                apolloordno = createOrder.getNewOrder(con, this.selectedOrder.getVendorName(), this.selectedOrder.getOrdNo(), splitStatus, Integer.parseInt(this.siteid), tat);
                if (apolloordno.length() <= 7) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Error Processing Order", ""));
                } else {
                    createOrder.createOrderHeader(con, this.selectedOrder.getOrdNo(), Integer.parseInt(this.siteid), AgentID, this.remarks, apolloordno, this.shipping, this.selectedOrder.getVendorName());
                    createOrder.createOrderLine(con, apolloordno, this.selectedOrder.getOrdNo(), this.selectedOrder.getVendorName(), this.cart, Integer.parseInt(this.siteid));
                    /*if (this.selectedOrder.getVendorName().equalsIgnoreCase("AskApollo") && this.selectedOrder.getWOD().equals("0")) {
                        Coupon coupon = new Coupon();
                        boolean CheckforCouponEligibility = coupon.checkCouponAllocation(this.selectedOrder.getMobNo(), this.selectedOrder.getVendorName());
                        if (CheckforCouponEligibility) {
                            coupon.AllocateCoupon(this.selectedOrder.getOrdNo(), this.selectedOrder.getMobNo(), Integer.parseInt(AgentID), "AA-10-40-15-C200", "40", "200", this.selectedOrder.getVendorName());
                        }
                    }*/
                    if (this.selectedOrder.getVendorName().equals("MediAssist") || this.selectedOrder.getVendorName().equalsIgnoreCase("APOLLO247") || this.selectedOrder.getVendorName().equalsIgnoreCase("Online")) {
                        TPOrderStatusUpdate vendorUpdate = new TPOrderStatusUpdate();
                        vendorUpdate.updateOrderConfirmationtoVendor(this.selectedOrder.getOrdNo(), apolloordno, this.siteid, this.selectedOrder.getVendorName());
                    }
                    if (this.selectedOrder.getVendorName().equalsIgnoreCase("Online")) {
                        sms _sms = new sms();
                        _sms.sendSMSOrderCreation(this.selectedOrder.getMobNo(), this.selectedOrder.getOrdNo());
                    } else {
                        sms _sms = new sms();
                        _sms.sendSMSOrderCreation(this.selectedOrder.getMobNo(), apolloordno);
                    }
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, apolloordno + " created for " + this.selectedOrder.getOrdNo() + " (" + this.selectedOrder.getVendorName() + ")", "Add Item to this Order!!!"));
                    if (splitStatus.equals("1")) {
                        this.tpordsum.remove(this.selectedOrder);
                        this.cart.clear();
                        this.prescriptionURLs.clear();
                        this.remarks = "";
                        this.selectedOrder = ((TPOrder) this.tpordsum.get(this.selecetedOrderRowNum));
                    } else if (splitStatus.equals("7")) {
                        this.cart.clear();
                        this.reqQTY = 0;
                        this.remarks = "";
                        this.selectedOrder = ((TPOrder) this.tpordsum.get(this.selecetedOrderRowNum));
                        this.cart = this.tpOrderModel.ItemDetails(con, this.selectedOrder.getOrdNo(), this.selectedOrder.getVendorName());
                    }
                }
            } catch (Exception e) {
                System.out.println(new Date() + " com.logic.order.ctlr.ThirdPartyOrderPushController.createOrder - " + e.getLocalizedMessage());
            }
        }
        //}
        return apolloordno;
    }

    public String verifyOrder() {
        String apolloordno = "";
        String tat = "";
        int siteID = 16001;
        FacesContext fc = FacesContext.getCurrentInstance();
        Map<String, String> params = fc.getExternalContext().getRequestParameterMap();
        String VendName = selectedOrder.getVendorName();
        String TPOrderNo = selectedOrder.getOrdNo();
        HttpSession appSession = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        String AgentID = appSession.getAttribute("loginid").toString();
        if (this.cart.size() <= 0) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Add Item to this Order!!!", "Add Item to this Order!!!"));
//        } else if (selectedOrder.getShipping().equalsIgnoreCase("Store Pickup") && !selectedOrder.getPrefferedSiteID().equals("16001")) {
//            siteID = Integer.parseInt(selectedOrder.getPrefferedSiteID());
//
        } else {
            OrderAllocation ordAllocate = new OrderAllocation();
            String TATresponse = ordAllocate.GetRoutingInfo(TPOrderNo, VendName, cart, AgentID);
            if (TATresponse.contains("from")) {
                Matcher TAT = Pattern.compile(Pattern.quote("date : ") + "(.*?)" + Pattern.quote(" from")).matcher(TATresponse);
                Matcher Site = Pattern.compile(Pattern.quote(" from ") + "(.*?)" + Pattern.quote("\"")).matcher(TATresponse);
                TAT.find();
                Site.find();
                System.out.println(TATresponse);
                System.out.println(TAT.group(1));
                System.out.println(Site.group(1));
                siteID = Integer.parseInt(Site.group(1));
                tat = TAT.group(1);
                //apolloordno = "TAT : " + TAT.group(1) + " - Allotted Site : " + Site.group(1);
                if (siteID != 16001) {
                    OrderPlace createOrder = new OrderPlace();
                    Connection con = null;
                    try {
                        con = connectivity.getLDBConnection();
                        apolloordno = createOrder.getNewOrder(con, this.selectedOrder.getVendorName(), this.selectedOrder.getOrdNo(), "1", siteID, tat);
                        if (apolloordno.length() <= 7) {
                            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Error Processing Order", ""));
                        } else {
                            createOrder.createOrderHeader(con, this.selectedOrder.getOrdNo(), siteID, AgentID, this.remarks, apolloordno, this.shipping, this.selectedOrder.getVendorName());
                            createOrder.createOrderLine(con, apolloordno, this.selectedOrder.getOrdNo(), this.selectedOrder.getVendorName(), this.cart, siteID);
                            /*AskApollo 50% offer Coupon code.
                if (this.selectedOrder.getVendorName().equalsIgnoreCase("AskApollo") && this.selectedOrder.getWOD().equals("0")) {
                            Coupon coupon = new Coupon();
                            boolean CheckforCouponEligibility = coupon.checkCouponAllocation(this.selectedOrder.getMobNo(), this.selectedOrder.getVendorName());
                            if (CheckforCouponEligibility) {
                                coupon.AllocateCoupon(this.selectedOrder.getOrdNo(), this.selectedOrder.getMobNo(), Integer.parseInt(AgentID), "AA-10-40-15-C200", "40", "200", this.selectedOrder.getVendorName());
                            }
                        }*/
                            if (this.selectedOrder.getVendorName().equalsIgnoreCase("MediAssist") || this.selectedOrder.getVendorName().equalsIgnoreCase("APOLLO247") || this.selectedOrder.getVendorName().equalsIgnoreCase("Online")) {
                                TPOrderStatusUpdate vendorUpdate = new TPOrderStatusUpdate();
                                vendorUpdate.updateOrderConfirmationtoVendor(this.selectedOrder.getOrdNo(), apolloordno, this.siteid, this.selectedOrder.getVendorName());
                            }
                            if (this.selectedOrder.getVendorName().equalsIgnoreCase("Online")) {
                                sms _sms = new sms();
                                _sms.sendSMSOrderCreation(this.selectedOrder.getMobNo(), this.selectedOrder.getOrdNo());
                            } else {
                                sms _sms = new sms();
                                _sms.sendSMSOrderCreation(this.selectedOrder.getMobNo(), apolloordno);
                            }
                            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, apolloordno + " created for " + this.selectedOrder.getOrdNo() + " (" + this.selectedOrder.getVendorName() + ") Order Allotted to " + siteID + " with TAT of " + tat, "Order created!!!"));
                            this.tpordsum.remove(this.selectedOrder);
                            this.cart.clear();
                            this.prescriptionURLs.clear();
                            this.remarks = "";
                            this.selectedOrder = ((TPOrder) this.tpordsum.get(this.selecetedOrderRowNum));
                        }
                    } catch (Exception e) {
                        System.out.println(new Date() + " com.logic.order.ctlr.ThirdPartyOrderPushController.createOrder - " + e.getLocalizedMessage());
                    }
                } else {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Please try again later", "Please try again later"));
                }
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, TATresponse, TATresponse));
            }
        }
        return apolloordno;
    }

    public String sendOnHoldSMS() {
        String smsACK = "On-HOLD SMS Send successfully to " + this.selectedOrder.getMobNo() + " for Order number : " + this.selectedOrder.getOrdNo();
        sms _sms = new sms();
        _sms.sendOnHoldSMS(this.selectedOrder.getMobNo(), this.selectedOrder.getOrdNo());
        HttpSession appSession = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        String AgentID = appSession.getAttribute("loginid").toString();
        String ordno = this.selectedOrder.getOrdNo();
        String vendorName = this.selectedOrder.getVendorName();
        addAuditLog(ordno, AgentID, vendorName, "On-HOLD-SMS");
        return smsACK;
    }

    public List<CancelRemarks> onRowToggle(ToggleEvent event) {
        return this.CanRem;
    }

    public List<CancelRemarks> getCanRem() {
        return this.CanRem;
    }

    public List<String> getCancelBase() {
        return cancelBase;
    }

    public void doCancellation() {
        String L_ordNo = this.selectedOrder.getOrdNo();
        String L_vendorName = this.selectedOrder.getVendorName();
        try {
            HttpSession appSession = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
            int username = Integer.parseInt(appSession.getAttribute("loginid").toString());
            OrderCancellation cancellation = new OrderCancellation();
            boolean cancancel = cancellation.checkCancellation(L_vendorName, L_ordNo);

            if (cancancel && this.cancelRemarksCode != null) {
                if (L_vendorName.equalsIgnoreCase("APOLLO247") || L_vendorName.equalsIgnoreCase("MDIndia")) {
                    TPOrderStatusUpdate statusUpdate = new TPOrderStatusUpdate();
                    String upStatus = statusUpdate.updateOrderCancellationtoVendor(L_ordNo, this.selectedOrder.getVendorName(), this.cancelRemarksCode);
                    if (upStatus.equalsIgnoreCase("true")) {
                        if (L_vendorName.equalsIgnoreCase("MDIndia")) {
                            String _reason = cancellation.getCancelRemarks(this.cancelRemarksCode);
                            sms _sms = new sms();
                            _sms.sendSMSMDIndiaOrderCancel(this.selectedOrder.getMobNo(), L_ordNo, _reason);
                        }
                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Order Cancelled successfully!!!", ""));
                        this.tpordsum.remove(this.selectedOrder);
                        this.selectedOrder = ((TPOrder) this.tpordsum.get(this.selecetedOrderRowNum));

                    } else {
                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Order can't be cancelled!!!", ""));
                    }

                } else if (L_vendorName.equalsIgnoreCase("ASKAPOLLO") || L_vendorName.equalsIgnoreCase("Online")) {
                    cancellation.processCancellation(this.cancelRemarksCode, username, L_ordNo, L_vendorName);
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Order Cancelled successfully!!!", ""));
                    this.tpordsum.remove(this.selectedOrder);
                    this.selectedOrder = ((TPOrder) this.tpordsum.get(this.selecetedOrderRowNum));
                }

            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Order can't be cancelled!!!", ""));
            }
        } catch (Exception ex) {
            System.out.println("com.logic.order.ctlr.ThirdPartyOrderPushController.doCancellation -> " + ex.getLocalizedMessage());
        }
    }

    public String Addcomment() {
        HttpSession appSession = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        String AgentID = appSession.getAttribute("loginid").toString();
        String ordno = this.selectedOrder.getOrdNo();
        String vendorName = this.selectedOrder.getVendorName();
        addAuditLog(ordno, AgentID, vendorName, this.commentData);
        return "Comments Added Successfully";
    }

    public void addAuditLog(String _ordno, String _AgentID, String _vendorName, String _comment) {
        OrderComment ordComment = new OrderComment();
        try {
            Connection con = connectivity.getLDBConnection();
            ordComment.addCommenttoOrder(con, _ordno, _comment, Integer.parseInt(_AgentID), _vendorName);
            con.close();
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
        }
    }

    public List<comments> listComments(String _ordNo, String _vendorName) {
        try {
            Connection con = connectivity.getLDBConnection();
            OrderComment ordComment = new OrderComment();
            this.thisOrderComment = ordComment.listAllComments(con, _ordNo, _vendorName);
            con.close();
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
        }
        return this.thisOrderComment;
    }

    public List<comments> getThisOrderComment() {
        return this.thisOrderComment;
    }

    public String getAplinecomment() {
        return aplinecomment;
    }

    public void setAplinecomment(String aplinecomment) {
        this.aplinecomment = aplinecomment;
    }

    public List<MDIEstimationItems> getMdiesil() {
        return mdiesil;
    }

    public void setMdiesil(List<MDIEstimationItems> mdiesil) {
        this.mdiesil = mdiesil;
    }

    public TPOrderModel getTpOrderModel() {
        return tpOrderModel;
    }

    public void setTpOrderModel(TPOrderModel tpOrderModel) {
        this.tpOrderModel = tpOrderModel;
    }

}
