/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.logic.order.allocation.model;

import java.sql.Connection;
import java.sql.PreparedStatement;

/**
 *
 * @author Administrator
 */
public class AllocationAuditLog {

    public void createAuditPayLoadLog(Connection _con, String _vendName, String _vendOrderNo, String _payLoad, String _AgentID) {
        try {
            PreparedStatement ps = _con.prepareStatement("insert into CC_OrderAllocationLog (orderid,vendorName,AgentID,reqPayLoad,reqDateTime) values (?,?,?,?,getDate())");
            ps.setString(1, _vendOrderNo);
            ps.setString(2, _vendName);
            ps.setString(3, _AgentID);
            ps.setString(4, _payLoad);
            ps.execute();
        } catch (Exception e) {
            System.out.println("Error at -> com.logic.order.allocation.model.AllocationAuditLog.createAuditLog -> " + e.getLocalizedMessage());
        }
    }

    public void createAuditResponseLog(Connection _con, String _vendName, String _vendOrderNo, String _response, String _AgentID) {
        try {
            PreparedStatement ps = _con.prepareStatement("update CC_OrderAllocationLog set response = ?, resDateTime = getDate() where orderid = ? and vendorName = ? and AgentID = ?");
            ps.setString(1, _response);
            ps.setString(2, _vendOrderNo);
            ps.setString(3, _vendName);
            ps.setString(4, _AgentID);
            ps.execute();
        } catch (Exception e) {
            System.out.println("Error at -> com.logic.order.allocation.model.AllocationAuditLog.createAuditLog -> " + e.getLocalizedMessage());
        }
    }
}
