/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.logic.order.estimation.bean;

import java.util.List;

/**
 *
 * @author pitchaiah_m
 */
public class MDIndiaEstimationSchema {

    private String orderid;
    private String shopid;
    private Double estimatedAmount;
    List<MDIEstimationItems> itemdetails;

    public MDIndiaEstimationSchema(String orderid, String shopid, Double estimatedAmount, List<MDIEstimationItems> itemdetails) {
        this.orderid = orderid;
        this.shopid = shopid;
        this.estimatedAmount = estimatedAmount;
        this.itemdetails = itemdetails;
    }

    public MDIndiaEstimationSchema() {
    }

    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

    public String getShopid() {
        return shopid;
    }

    public void setShopid(String shopid) {
        this.shopid = shopid;
    }

    public Double getEstimatedAmount() {
        return estimatedAmount;
    }

    public void setEstimatedAmount(Double estimatedAmount) {
        this.estimatedAmount = estimatedAmount;
    }

    public List<MDIEstimationItems> getItemdetails() {
        return itemdetails;
    }

    public void setItemdetails(List<MDIEstimationItems> itemdetails) {
        this.itemdetails = itemdetails;
    }

}
