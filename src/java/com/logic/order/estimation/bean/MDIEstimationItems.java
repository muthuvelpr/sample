/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.logic.order.estimation.bean;

import java.io.Serializable;

/**
 *
 * @author pitchaiah_m
 */
public class MDIEstimationItems implements Serializable {

    public String itemID;
    public String itemName;
    public Integer qty;
    public Double mrp;
    public Double price;
    public String ItemRemark;

    public MDIEstimationItems(String itemID, String itemName, Integer qty, Double mrp, Double price, String ItemRemark) {
        this.itemID = itemID;
        this.itemName = itemName;
        this.qty = qty;
        this.mrp = mrp;
        this.price = price;
        this.ItemRemark = ItemRemark;
    }

    public String getItemID() {
        return itemID;
    }

    public void setItemID(String itemID) {
        this.itemID = itemID;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public Double getMrp() {
        return mrp;
    }

    public void setMrp(Double mrp) {
        this.mrp = mrp;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getItemRemark() {
        return ItemRemark;
    }

    public void setItemRemark(String ItemRemark) {
        this.ItemRemark = ItemRemark;
    }

}
