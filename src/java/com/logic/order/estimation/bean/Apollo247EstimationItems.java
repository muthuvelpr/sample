/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.logic.order.estimation.bean;

/**
 *
 * @author MUTHUVEL
 */
public class Apollo247EstimationItems {

    private String medicineSKU;
    private String medicineName;
    private double price;
    private int quantity;
    private double mrp;
    private int pack;
    private int mou;

    public Apollo247EstimationItems(String medicineSKU, String medicineName, double price, int quantity, double mrp, int pack, int mou) {
        this.medicineSKU = medicineSKU;
        this.medicineName = medicineName;
        this.price = price;
        this.quantity = quantity;
        this.mrp = mrp;
        this.pack = pack;
        this.mou = mou;
    }

    public String getMedicineSKU() {
        return medicineSKU;
    }

    public void setMedicineSKU(String medicineSKU) {
        this.medicineSKU = medicineSKU;
    }

    public String getMedicineName() {
        return medicineName;
    }

    public void setMedicineName(String medicineName) {
        this.medicineName = medicineName;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getMrp() {
        return mrp;
    }

    public void setMrp(double mrp) {
        this.mrp = mrp;
    }

    public int getPack() {
        return pack;
    }

    public void setPack(int pack) {
        this.pack = pack;
    }

    public int getMou() {
        return mou;
    }

    public void setMou(int mou) {
        this.mou = mou;
    }

}
