/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.logic.order.estimation.bean;

/**
 *
 * @author MUTHUVEL
 */
public class Apollo247EstimationSchema {

    private String query;
    private Apollo247Variables variables;

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public Apollo247Variables getVariables() {
        return variables;
    }

    public void setVariables(Apollo247Variables variables) {
        this.variables = variables;
    }

}
