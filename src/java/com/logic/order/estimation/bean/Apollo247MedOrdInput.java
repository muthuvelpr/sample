/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.logic.order.estimation.bean;

import java.util.List;

/**
 *
 * @author MUTHUVEL
 */
public class Apollo247MedOrdInput {

    private int quoteId;
    private String shopId;
    private double estimatedAmount;
    private List<Apollo247EstimationItems> items = null;

    public Apollo247MedOrdInput(int quoteId, String shopId, double estimatedAmount) {
        this.quoteId = quoteId;
        this.shopId = shopId;
        this.estimatedAmount = estimatedAmount;
    }

    public Apollo247MedOrdInput() {
    }

    public int getQuoteId() {
        return quoteId;
    }

    public void setQuoteId(int quoteId) {
        this.quoteId = quoteId;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public double getEstimatedAmount() {
        return estimatedAmount;
    }

    public void setEstimatedAmount(double estimatedAmount) {
        this.estimatedAmount = estimatedAmount;
    }

    public List<Apollo247EstimationItems> getItems() {
        return items;
    }

    public void setItems(List<Apollo247EstimationItems> items) {
        this.items = items;
    }

}
