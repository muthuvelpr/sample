/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.logic.thirdparty.model;

/**
 *
 * @author PITCHU
 */
public class shoptimizeOrderHeader {

    String shippingmethod, paymentmethod, site_id, presc_image_url, is_medicine;

    public shoptimizeOrderHeader(String shippingmethod, String paymentmethod, String site_id, String presc_image_url, String is_medicine) {
        this.shippingmethod = shippingmethod;
        this.paymentmethod = paymentmethod;
        this.site_id = site_id;
        this.presc_image_url = presc_image_url;
        this.is_medicine = is_medicine;
    }

    public String getIs_medicine() {
        return is_medicine;
    }

    public void setIs_medicine(String is_medicine) {
        this.is_medicine = is_medicine;
    }

    public String getPaymentmethod() {
        return paymentmethod;
    }

    public void setPaymentmethod(String paymentmethod) {
        this.paymentmethod = paymentmethod;
    }

    public String getPresc_image_url() {
        return presc_image_url;
    }

    public void setPresc_image_url(String presc_image_url) {
        this.presc_image_url = presc_image_url;
    }

    public String getShippingmethod() {
        return shippingmethod;
    }

    public void setShippingmethod(String shippingmethod) {
        this.shippingmethod = shippingmethod;
    }

    public String getSite_id() {
        return site_id;
    }

    public void setSite_id(String site_id) {
        this.site_id = site_id;
    }
}
