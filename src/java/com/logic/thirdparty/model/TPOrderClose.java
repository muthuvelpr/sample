/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.logic.thirdparty.model;

/**
 *
 * @author Pitchu
 * @created 31 Oct, 2014 5:23:40 PM
 *
 */
public class TPOrderClose {

    private int ord, siteid, tpord;
    private String mobileno, region, patname, orderby, gender, orddate, initdate, deldate, statusurl, statusval, orderSource, cancelDate, cancelRemarks, paymentMode, commAddr1;

    public TPOrderClose(int ord, int siteid, String orddate, String initdate, String deldate, String region, String patname, String mobileno, String gender, String statusurl, String orderby, String orderSource, String cancelDate, String cancelRemarks, String paymentMode, String commAddr1, int tpord) {
        this.ord = ord;
        this.siteid = siteid;
        this.mobileno = mobileno;
        this.region = region;
        this.patname = patname;
        this.orderby = orderby;
        this.gender = gender;
        this.orddate = orddate;
        this.initdate = initdate;
        this.deldate = deldate;
        this.statusurl = statusurl;
        this.statusval = statusval;
        this.orderSource = orderSource;
        this.cancelDate = cancelDate;
        this.cancelRemarks = cancelRemarks;
        this.paymentMode = paymentMode;
        this.commAddr1 = commAddr1;
        this.tpord = tpord;
    }

    public int getTpord() {
        return tpord;
    }

    public void setTpord(int tpord) {
        this.tpord = tpord;
    }

    public String getCommAddr1() {
        return commAddr1;
    }

    public void setCommAddr1(String commAddr1) {
        this.commAddr1 = commAddr1;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public String getCancelDate() {
        return cancelDate;
    }

    public void setCancelDate(String cancelDate) {
        this.cancelDate = cancelDate;
    }

    public String getCancelRemarks() {
        return cancelRemarks;
    }

    public void setCancelRemarks(String cancelRemarks) {
        this.cancelRemarks = cancelRemarks;
    }

    public String getOrderSource() {
        return orderSource;
    }

    public void setOrderSource(String orderSource) {
        this.orderSource = orderSource;
    }

    public String getDeldate() {
        return deldate;
    }

    public void setDeldate(String deldate) {
        this.deldate = deldate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getInitdate() {
        return initdate;
    }

    public void setInitdate(String initdate) {
        this.initdate = initdate;
    }

    public String getMobileno() {
        return mobileno;
    }

    public void setMobileno(String mobileno) {
        this.mobileno = mobileno;
    }

    public int getOrd() {
        return ord;
    }

    public void setOrd(int ord) {
        this.ord = ord;
    }

    public String getOrddate() {
        return orddate;
    }

    public void setOrddate(String orddate) {
        this.orddate = orddate;
    }

    public String getOrderby() {
        return orderby;
    }

    public void setOrderby(String orderby) {
        this.orderby = orderby;
    }

    public String getPatname() {
        return patname;
    }

    public void setPatname(String patname) {
        this.patname = patname;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public int getSiteid() {
        return siteid;
    }

    public void setSiteid(int siteid) {
        this.siteid = siteid;
    }

    public String getStatusurl() {
        return statusurl;
    }

    public void setStatusurl(String statusurl) {
        this.statusurl = statusurl;
    }

    public String getStatusval() {
        return statusval;
    }

    public void setStatusval(String statusval) {
        this.statusval = statusval;
    }
}
