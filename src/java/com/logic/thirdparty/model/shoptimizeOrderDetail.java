/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.logic.thirdparty.model;

/**
 *
 * @author PITCHU
 */
public class shoptimizeOrderDetail {

     String itemId, itemName;
     double qty, price_MRP;

     public shoptimizeOrderDetail(String itemId, String itemName, double qty, double price_MRP) {
          this.itemId = itemId;
          this.itemName = itemName;
          this.qty = qty;
          this.price_MRP = price_MRP;
     }

     public String getItemId() {
          return itemId;
     }

     public void setItemId(String itemId) {
          this.itemId = itemId;
     }

     public String getItemName() {
          return itemName;
     }

     public void setItemName(String itemName) {
          this.itemName = itemName;
     }

     public double getQty() {
          return qty;
     }

     public void setQty(double qty) {
          this.qty = qty;
     }

     public double getPrice_MRP() {
          return price_MRP;
     }

     public void setPrice_MRP(double price_MRP) {
          this.price_MRP = price_MRP;
     }

}
