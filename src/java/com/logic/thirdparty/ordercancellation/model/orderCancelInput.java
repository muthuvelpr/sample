/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.logic.thirdparty.ordercancellation.model;

/**
 *
 * @author Administrator
 */
public class orderCancelInput {

    private int orderNo;
    private String remarksCode;

    public int getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(int orderNo) {
        this.orderNo = orderNo;
    }

    public String getRemarksCode() {
        return remarksCode;
    }

    public void setRemarksCode(String remarksCode) {
        this.remarksCode = remarksCode;
    }

}
