/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.logic.thirdparty.orderconfirmation.model;

import com.google.gson.Gson;
import java.util.UUID;

/**
 *
 * @author Administrator
 */
public class testPload {

    public static void main(String args[]) {
        String pl = APOLLO247Update("3", "2342");
        System.out.println(pl);
        UUID uuid = UUID.randomUUID();
        System.out.println("UUID=" + uuid.toString() );
    }

    public static String APOLLO247Update(String _VendordID, String _genOrdno) {
        Apollo247OrderConfirmationPL a247c = new Apollo247OrderConfirmationPL();
        orderConfirmationInput oci = new orderConfirmationInput();
        variables var = new variables();
        ordersResult or = new ordersResult();
        or.setMessage("Order verified and confirmed");
        or.setApOrderNo(_genOrdno);
        or.setOrderNo(Integer.parseInt(_VendordID));
        oci.setOrdersResult(or);
        a247c.setQuery("mutation($orderConfirmationInput:OrderConfirmationInput!){saveOrderConfirmation(orderConfirmationInput:$orderConfirmationInput){requestStatus requestMessage}}");
        var.setOrderConfirmationInput(oci);
        a247c.setVariables(var);
        Gson g = new Gson();
        String pl = g.toJson(a247c);

        return pl;
    }
}
