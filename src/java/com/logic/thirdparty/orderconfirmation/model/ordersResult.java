/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.logic.thirdparty.orderconfirmation.model;

/**
 *
 * @author Administrator
 */
public class ordersResult {

    private String message, apOrderNo;
    private int orderNo;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getApOrderNo() {
        return apOrderNo;
    }

    public void setApOrderNo(String apOrderNo) {
        this.apOrderNo = apOrderNo;
    }

    public int getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(int orderNo) {
        this.orderNo = orderNo;
    }

}
