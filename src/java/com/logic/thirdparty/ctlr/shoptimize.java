/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.logic.thirdparty.ctlr;

import static ccm.dao.bean.connectivity.close;
import static ccm.dao.bean.connectivity.getAXConnection;
import static ccm.dao.bean.connectivity.getLDBConnection;
import com.logic.order.model.OrderComment;
import ccm.logic.bean.CancelRemarks;
import static ccm.logic.bean.Util.getSession;
import ccm.logic.bean.UploadedPrescDocs;
import ccm.logic.ctlr.orderCreation;
import com.logic.order.bean.comments;
import ccm.logic.bean.customerInfo;
import ccm.logic.bean.itemDetails;
import ccm.logic.bean.myCart;
import ccm.report.model.invAvailability;
import com.logic.order.bean.TPOrder;
import com.logic.thirdparty.model.shoptimizeOrderDetail;
import com.sun.jersey.api.client.Client;
import static com.sun.jersey.api.client.Client.create;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import java.io.IOException;
import java.io.Serializable;
import static java.lang.Double.parseDouble;
import static java.lang.Integer.parseInt;
import static java.lang.Long.parseLong;
import static java.lang.System.err;
import static java.lang.System.out;
import java.net.HttpURLConnection;
import java.net.URL;
import static java.net.URLEncoder.encode;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.faces.application.FacesMessage;
import static javax.faces.application.FacesMessage.SEVERITY_INFO;
import static javax.faces.application.FacesMessage.SEVERITY_WARN;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import static javax.faces.context.FacesContext.getCurrentInstance;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.xpath.XPath;
import static javax.xml.xpath.XPathConstants.STRING;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;
import static javax.xml.xpath.XPathFactory.newInstance;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.primefaces.event.ResizeEvent;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.xml.sax.EntityResolver;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author PITCHU
 */
@ManagedBean
@ViewScoped
public class shoptimize implements Serializable {

//    int testCounter = 0;
//    String shopid;
//    String CommentData = "";
//    List<comments> thisOrderComment = new ArrayList<>();
//    List<invAvailability> invAvail = new ArrayList<>();
//
//    public String getCommentData() {
//        return CommentData;
//    }
//
//    public void setCommentData(String CommentData) {
//        this.CommentData = CommentData;
//    }
//
//    public List<invAvailability> getInvAvail() {
//        return invAvail;
//    }
//
//    public void setInvAvail(List<invAvailability> invAvail) {
//        this.invAvail = invAvail;
//    }
//
//    List<CancelRemarks> cancelRemarks = new ArrayList<>();
//
//    public int getTestCounter() {
//        return testCounter;
//    }
//
//    public void setTestCounter(int testCounter) {
//        this.testCounter = testCounter;
//    }
//
//    public String getShopid() {
//        return shopid;
//    }
//
//    public void setShopid(String shopid) {
//        this.shopid = shopid;
//    }
//
//    public shoptimize() {
//        this.ordSum = new ArrayList<TPOrderSummary>();
//        showShoptimizeordersummary();
//    }
//
//    List<TPOrderSummary> ordSum;
//    private TPOrder selectedOrder;
//    List<TPOrderSummary> filterOrdSum;
//    List<shoptimizeOrderDetail> ordDet = new ArrayList<>();
//
//    public void displayDialog() {
//
//    }
//
//    public List<TPOrderSummary> getFilterOrdSum() {
//        return filterOrdSum;
//    }
//
//    public void setFilterOrdSum(List<TPOrderSummary> filterOrdSum) {
//        this.filterOrdSum = filterOrdSum;
//    }
//
//    public TPOrder getSelectedOrder() {
//        return selectedOrder;
//    }
//
//    public void setSelectedOrder(TPOrder selectedOrder) {
//        this.selectedOrder = selectedOrder;
//        String ordNo = selectedOrder.getOrdNo();
//        String vendorName = selectedOrder.getVendorName();
//        listComments(ordNo, vendorName);
//    }
//
//    /*--------------------------------------------------*/
//    List<itemDetails> itemdetail = new ArrayList<>();
//    private String artcode;
//    private int reqQTY;
//
//    public String getArtcode() {
//        return artcode;
//    }
//
//    public void setArtcode(String artcode) {
//        this.artcode = artcode;
//    }
//
//    public int getReqQTY() {
//        return reqQTY;
//    }
//
//    public void setReqQTY(int reqQTY) {
//        this.reqQTY = reqQTY;
//    }
//
//    public List collectSelectedMedicineDetails() {
//        Connection con = null;
//        itemdetail.clear();
//
//        String string = artcode;
//        String[] art = string.split("-");
//        String artcode = art[0]; // 004
//        String artname = art[1];
//        try {
//            //Statement stmt = con.createStatement();
//            //ResultSet rs = stmt.executeQuery("SELECT pkstrucobj.get_desc (stocinl, 'GB') artname,pkartrac.get_artcexr (pkartul.getcinr (stocinl)) artcode,SUM (goldprod.sitestock.get_cinlstock (site, stocinl)) qty FROM (SELECT DISTINCT site, NAME STORE, stocinl FROM goldprod.stocouch, goldprod.shops WHERE stosite = site AND site = '" + shopid + "' AND stocinl IN (SELECT arvcinv FROM goldprod.artuv WHERE arvcexr = UPPER ('" + artcode.toUpperCase() + "'))) a GROUP BY site, STORE, pkstrucobj.get_desc (stocinl, 'GB'), pkartrac.get_artcexr (pkartul.getcinr (stocinl)) HAVING SUM (goldprod.sitestock.get_cinlstock (site, stocinl)) > 0 ORDER BY site");
//            //PreparedStatement ps = con.prepareStatement("select art_desc artname,art_code artcode,mrp from cc_itemmaster where art_code = ?");
//            con = getAXConnection();
//            PreparedStatement ps = con.prepareStatement("select itemid,acxitemdescription from inventtable where itemid = ?");
//            ps.setString(1, artcode);
//            ResultSet rs = ps.executeQuery();
//            if (rs.next()) {
//                String gart = rs.getString(1).toUpperCase();
//                String gname = rs.getString(2).toUpperCase();
//                int gqoh = 5;
//                double mrp = 0.0;
//                itemdetail.add(new itemDetails(gart, gname, gqoh, mrp));
//            }
//        } catch (Exception e) {
//            System.out.println(e.getLocalizedMessage());
//        } finally {
//        }
//        return itemdetail;
//    }
//
//    public List<itemDetails> getItemdetail() {
//        return itemdetail;
//    }
//
//    public void deleteAction(myCart item) {
//
//        FacesContext fc = getCurrentInstance();
//        Map<String, String> params = fc.getExternalContext().getRequestParameterMap();
//        String ordno = params.get("ordno").trim();
//        String gartcode = params.get("gartcode").trim();
//        Connection con = null;
//        cart.remove(item);
//        try {
//            con = getLDBConnection();
//            Statement stmt = con.createStatement();
//            String delQRY = "delete from ThirdPartyOrderDetails where itemid = '" + gartcode + "' and orderid ='" + ordno + "'  ";
//            ResultSet rs = stmt.executeQuery(delQRY);
//        } catch (Exception e) {
//            out.println(e.getLocalizedMessage());
//        }
//
//    }
//
//
//    /*--------------------------------------------------*/
//    public List showShoptimizeordersummary() {
//
//        ordSum.clear();
//        Connection con = null;
//        try {
//            con = getLDBConnection();
//            //PreparedStatement ps = con.prepareStatement("select a.ordseq, count(a.itemid),a.orddt,a.patientid, b.is_medicine, b.shippingmethod,b.paymentmethod  from cc_itemdetails a, CC_Shoptimizeheader b where  a.ordseq = b.OrderId and filestatus=0 group by  a.ordseq,a.orddt,a.patientid, b.is_medicine, b.shippingmethod,b.paymentmethod order by a.ordDT asc");and b.is_medicine = 'PHARMA'
//            //PreparedStatement ps = con.prepareStatement("select a.ordseq, C.FIRSTNAME +' '+ C.LASTNAME AS CUSTNAME,C.MOBILENO, a.orddt, b.is_medicine, b.shippingmethod,b.paymentmethod,a.patientid  from cc_itemdetails a, CC_Shoptimizeheader b,CC_Patient_Registration C where A.PATIENTID = C.PATIENTID AND a.ordseq = b.OrderId and filestatus=0 group by  a.ordseq, C.FIRSTNAME +' '+ C.LASTNAME,C.MOBILENO, a.orddt,b.is_medicine, b.shippingmethod,b.paymentmethod,a.patientid order by a.ordDT desc");
//            //PreparedStatement ps = con.prepareStatement("select a.orderid, C.FIRSTNAME AS CUSTNAME,C.MOBILENO, a.orddt, b.is_medicine, b.shippingmethod, b.paymentmethod,a.patientid, B.VendorName  from ThirdPartyOrderDetails a, ThirdPartyOrderHeader b,CC_Patient_Registration C where  A.PATIENTID = C.PATIENTID AND b.orderid = a.OrderId and filestatus=0 group by a.orderid, C.FIRSTNAME ,C.MOBILENO, a.orddt, b.is_medicine, b.shippingmethod, b.paymentmethod,a.patientid,B.VendorName order by a.orderid asc");
//            //PreparedStatement ps = con.prepareStatement("select b.orderid, U.patname AS CUSTNAME,C.MOBILENO, b.createddatetime, b.is_medicine, b.shippingmethod, b.paymentmethod,b.patientid, B.VendorName,ISNULL(u.ORDERAMOUNT ,'-') from ThirdPartyOrderHeader b,tpdeladdupdate U,CC_Patient_Registration C  where  b.PATIENTID = C.PATIENTID AND B.ORDERID=U.ORDERID and b.orderstatus=0 group by b.orderid, U.patname, C.MOBILENO, b.createddatetime, b.is_medicine, b.shippingmethod, b.paymentmethod,b.patientid,B.VendorName,U.ORDERAMOUNT order by b.orderid asc");
//            PreparedStatement ps = con.prepareStatement("select top 10 b.orderid, U.patname AS CUSTNAME,C.MOBILENO, b.createddatetime, b.is_medicine, b.shippingmethod, b.paymentmethod,b.patientid, B.VendorName,ISNULL(u.ORDERAMOUNT ,'-') as ordAmount, ISNULL(u.POSTAL_CODE ,'-') as POSTAL, ISNULL(u.CITY,'-') as CITY, isnull(u.PSTATUS,'-') as TXNStatus, isnull(u.PORDERID,'-') as PayTMOrderID from ThirdPartyOrderHeader b,tpdeladdupdate U,CC_Patient_Registration C  where  b.PATIENTID = C.PATIENTID AND B.ORDERID=U.ORDERID and b.orderstatus=0 group by b.orderid, U.patname, C.MOBILENO, b.createddatetime, b.is_medicine, b.shippingmethod, b.paymentmethod,b.patientid,B.VendorName,U.ORDERAMOUNT,u.POSTAL_CODE,u.CITY,u.PSTATUS,u.PORDERID order by b.createddatetime desc");
//            ResultSet rs = ps.executeQuery();
//
//            while (rs.next()) {
//
//                SimpleDateFormat sd = new SimpleDateFormat("dd-MMM-yy hh:mm");
//                String ordDt = rs.getString(4).substring(0, 16);
//                String shippingmethod = rs.getString(6).toUpperCase();
//                String Postal = rs.getString(11);
//                String City = rs.getString(12).toUpperCase();
//                /*if (shippingmethod.equalsIgnoreCase("appflatrate_appflatrate")) {
//                    shippingmethod = "Store Pickup";
//                } else if (shippingmethod.equalsIgnoreCase("tablerate_bestway")) {
//                    shippingmethod = "Home Delivery";
//                } else {
//                    shippingmethod = "Store Pickup";
//                }*/
//                ordSum.add(new TPOrder(rs.getString(1), rs.getString(2).toUpperCase(), rs.getString(3), ordDt, rs.getString(5), shippingmethod, rs.getString(7), rs.getString(8), rs.getString(9), rs.getString(10), Postal, City, rs.getString(13), rs.getString(14)));
//            }
//        } catch (Exception e) {
//            out.println(e.getLocalizedMessage());
//        }
//        return ordSum;
//    }
//
//    public List<TPOrderSummary> getOrdSum() {
//        return ordSum;
//    }
//
//    public List<CancelRemarks> listCancelRemarks() {
//        FacesContext fc = getCurrentInstance();
//        Map<String, String> params = fc.getExternalContext().getRequestParameterMap();
//        String vendName = params.get("vendorName");
//        try {
//            cancelRemarks.clear();
//            Connection con = getLDBConnection();
//            PreparedStatement ps = con.prepareStatement("select * from CC_OrderCancelRemarks where VENDOR in (?)");
//            ps.setString(1, vendName);
//            ResultSet rs = ps.executeQuery();
//            while (rs.next()) {
//                cancelRemarks.add(new CancelRemarks(rs.getString(1), rs.getString(2)));
//            }
//        } catch (Exception e) {
//            System.out.println(e.getLocalizedMessage());
//        }
//        return cancelRemarks;
//    }
//
//    public List<CancelRemarks> getCancelRemarks() {
//        return cancelRemarks;
//    }
//
//    List<customerInfo> custInfo = new ArrayList<customerInfo>();
//
//    String shoptimize_site_id, shoptimize_ordertype, deliveryMode, paymentMode;
//    String shoptimize_pres_img_url = "https://upload.wikimedia.org/wikipedia/commons/a/ac/No_image_available.svg";
//
//    public String getShoptimize_ordertype() {
//        return shoptimize_ordertype;
//    }
//
//    public void setShoptimize_ordertype(String shoptimize_ordertype) {
//        this.shoptimize_ordertype = shoptimize_ordertype;
//    }
//
//    public String getShoptimize_pres_img_url() {
//        return shoptimize_pres_img_url;
//    }
//
//    public void setShoptimize_pres_img_url(String shoptimize_pres_img_url) {
//        this.shoptimize_pres_img_url = shoptimize_pres_img_url;
//    }
//
//    public String getShoptimize_site_id() {
//        return shoptimize_site_id;
//    }
//
//    public void setShoptimize_site_id(String shoptimize_site_id) {
//        this.shoptimize_site_id = shoptimize_site_id;
//    }
//
//    public String getDeliveryMode() {
//        return deliveryMode;
//    }
//
//    public void setDeliveryMode(String deliveryMode) {
//        this.deliveryMode = deliveryMode;
//    }
//
//    public String getPaymentMode() {
//        return paymentMode;
//    }
//
//    public void setPaymentMode(String paymentMode) {
//        this.paymentMode = paymentMode;
//    }
//
//    public void showShoptimizeOrderDetail() {
//
//        FacesContext fc = getCurrentInstance();
//        Map<String, String> params = fc.getExternalContext().getRequestParameterMap();
//        String ordNo = params.get("ordNo");
//        String patID = params.get("patID");
//        //boolean inProgress = orderProcessCheck.checkOrderStatus(ordNo);
//        //if (inProgress) {
//        //dialogWindow = "PF('myAlertDialog').show()";
//        //} else {
//        //  doLog(ordNo);
//        //updatePrice(ordNo);
//        /*orderCreation oc = new orderCreation();
//        cart = oc.moveShoptimizeItemDetailtoCart(ordNo);*/
//        //getOrderDetail(ordNo);
//        addItemtoVendor(ordNo);
//        String mobNo = getCustInfo(patID);
//        Long mobno = parseLong(mobNo);
//        customerInfo(mobno, ordNo);
//        shoptimizeheaderInfo(ordNo);
//        //dialogWindow = "PF('myDialog').show()";
//        //}
//        //return "VODetails?faces-redirect=true";
//    }
//
//    public void releaseReservation() {
//        Connection con = null;
//        HttpSession appSession = (HttpSession) getCurrentInstance().getExternalContext().getSession(false);
//        String username = appSession.getAttribute("loginid").toString();
//        FacesContext fc = getCurrentInstance();
//        Map<String, String> params = fc.getExternalContext().getRequestParameterMap();
//        String ordNo = params.get("ordNo");
//        try {
//            con = getLDBConnection();
//            PreparedStatement ps = con.prepareStatement("delete from CC_OrdProcessLog where ordNo=? and processby = ?)");
//            ps.setString(1, ordNo);
//            ps.setString(2, username);
//            ps.execute();
//        } catch (Exception e) {
//            out.println(e.getLocalizedMessage());
//        }
//    }
//
//    public void doLog(String _ordNo) {
//        Connection con = null;
//        HttpSession appSession = (HttpSession) getCurrentInstance().getExternalContext().getSession(false);
//        String username = appSession.getAttribute("loginid").toString();
//        try {
//            con = getLDBConnection();
//            PreparedStatement ps = con.prepareStatement("insert into CC_OrdProcessLog values(?,?,getDate())");
//            ps.setString(1, _ordNo);
//            ps.setString(2, username);
//            ps.execute();
//        } catch (Exception e) {
//            out.println(e.getLocalizedMessage());
//        }
//    }
//
//    public void updatePrice(String _ordNo) {
//        Connection con = null;
//        try {
//            con = getLDBConnection();
//            PreparedStatement ps = con.prepareStatement("update ThirdPartyOrderDetails set price_mrp=(select mrp from rvk_item1 where art_code=itemid), subtotal = qty * (select mrp from rvk_item1 where art_code=itemid) where orderid = ?");
//            ps.setString(1, _ordNo);
//
//            ps.execute();
//        } catch (Exception e) {
//            out.println(e.getLocalizedMessage());
//        }
//    }
//
//    List<uploadedPrescimages> upldprescimg = new ArrayList<>();
//
//    public List shoptimizeheaderInfo(String _ordNo) {
//        Connection con = null;
//        upldprescimg.clear();
//        try {
//            //ordHeader.clear();
//            con = getLDBConnection();
//            //PreparedStatement ps = con.prepareStatement("select shippingmethod, paymentmethod,site_id, presc_image,is_medicine from CC_Shoptimizeheader where orderID = ?");
//            PreparedStatement ps = con.prepareStatement("select shippingmethod, paymentmethod,site_id, presc_image,is_medicine,VendorName from ThirdPartyOrderHeader where orderID = ?");
//            ps.setString(1, _ordNo);
//            ResultSet rs = ps.executeQuery();
//            if (rs.next()) {
//                deliveryMode = rs.getString(1).toUpperCase();
//                paymentMode = rs.getString(2).toUpperCase();
//                shoptimize_site_id = rs.getString(3);
//                if (rs.getString(6).equalsIgnoreCase("AskApollo") || rs.getString(6).equalsIgnoreCase("SIDBIE") || rs.getString(6).equalsIgnoreCase("Kaizala") || rs.getString(6).equalsIgnoreCase("FHPL")) {
//                    shoptimize_pres_img_url = rs.getString(4).replace("[", "").replace("]", "").replace("\\/", "/").replace("\"", "/").replace("E:\\AppDoc\\", "http://172.16.2.251:8085/IA/Images/");
//                    //shoptimize_pres_img_url = rs.getString(4).replace("[", "").replace("]", "").replace("\\/", "/").replace("\"", "/").replace("E:\\AppDoc\\", "http://124.124.88.106:9090/IA/Images/");
//                } else {
//                    shoptimize_pres_img_url = rs.getString(4).replace("[", "").replace("]", "").replace("\\/", "/").replace("\"", "");
//                }
//                String strArray[] = shoptimize_pres_img_url.split(",");
//                out.println("String Array is : ");
//                for (int i = 0; i < strArray.length; i++) {
//                    upldprescimg.add(new UploadedPrescDocs("Prescription Data " + (i + 1), strArray[i]));
//                }
//                shoptimize_ordertype = rs.getString(5);
//                //ordHeader.add(new shoptimizeOrderHeader(rs.getString(1).toUpperCase(), rs.getString(2).toUpperCase(), rs.getString(3), rs.getString(4), rs.getString(5)));               
//            }
//        } catch (Exception e) {
//            out.println(e.getLocalizedMessage());
//        }
//        return upldprescimg;
//    }
//
//    public List<uploadedPrescimages> getUpldprescimg() {
//        return upldprescimg;
//    }
//
//    public List customerInfo(Long _mobNo, String _ordNo) {
//        HttpSession patientSession = getSession();
//        Connection con = null;
//        try {
//            custInfo.clear();
//            con = getLDBConnection();
//            //PreparedStatement ps = con.prepareStatement("select cr.PatientID,FirstName,Gender,Age, Addr1,City,Zip, a.del_add as CommAddr,MailID,LastName,Lat2,Lon2,cccardno, uhid from CC_Patient_Registration cr, deladdressupdate a where Mobileno = ? and cr.PatientID = a.patientid and a.orderid = ?");
//            PreparedStatement ps = con.prepareStatement("select tp.PatientID,tp.patname, cr.Gender ,cr.Age, tp.del_add as CommAddr,cr.MailID,tp.patname,cr.Lat2,cr.Lon2,cr.cccardno, cr.uhid from \n"
//                    + "TPDELADDUPDATE tp,CC_Patient_Registration cr where cr.Mobileno = ? and cr.PatientID = tp.patientid and tp.orderid = ?");
//            ps.setLong(1, _mobNo);
//            ps.setString(2, _ordNo);
//            ResultSet rs = ps.executeQuery();
//            if (rs.next()) {
//                String pid = rs.getString(1);
//                String fn = rs.getString(2).toUpperCase();
//                String custgender = "";
//                int custage = rs.getInt(4);
//                String commAdd = rs.getString(5);
//                String mailid = rs.getString(6);
//                String cccardno = rs.getString(10);
//                String custuhid = rs.getString(11);
//                patientSession.setAttribute("pid", pid);
//                patientSession.setAttribute("commAddr", commAdd);
//                patientSession.setAttribute("mailID", mailid);
//                patientSession.setAttribute("Lat", rs.getDouble(8));
//                patientSession.setAttribute("Lon", rs.getDouble(9));
//                custInfo.add(new customerInfo(pid, fn, custgender, custage, commAdd, commAdd, mailid, cccardno, custuhid));
//            }
//        } catch (Exception e) {
//            System.out.println(e.getLocalizedMessage());
//        } finally {
//            close(con);
//        }
//        return custInfo;
//    }
//
//    public List<customerInfo> getCustInfo() {
//        return custInfo;
//    }
//    double grandTot = 0.0;
//    double subTot = 0.0;
//    double tax = 0.0;
//    double disc = 0.0;
//
//    public List getOrderDetail(String _ordNo) {
//        Connection con = null;
//        ordDet.clear();
//        grandTot = 0.0;
//        orderCreation oc = new orderCreation();
//        try {
//            con = getLDBConnection();
//            //PreparedStatement ps = con.prepareStatement("select itemid,ItemName,Qty,tax_amount,discount_amount,grand_total,subtotal,price_mrp from cc_itemdetails where ordSeq = ?");
//            PreparedStatement ps = con.prepareStatement("select itemid,ItemName,Qty,tax_amount,discount_amount,grand_total,subtotal,price_mrp from ThirdPartyOrderDetails where orderid= ?");
//            ps.setString(1, _ordNo);
//            ResultSet rs = ps.executeQuery();
//
//            while (rs.next()) {
//                grandTot += rs.getDouble(7);
//                subTot = rs.getDouble(7);
//                tax = rs.getDouble(4);
//                disc = rs.getDouble(5);
//                ordDet.add(new shoptimizeOrderDetail(rs.getString(1), rs.getString(2), rs.getDouble(3), rs.getDouble(8)));
//
//            }
//        } catch (Exception e) {
//        }
//        return ordDet;
//    }
//
//    List<myCart> cart = new ArrayList<myCart>();
//
//    public List addItemtoVendor(String _ordNo) {
//        Connection con = null;
//        orderCreation oc = new orderCreation();
//
//        cart.clear();
//        try {
//            con = getLDBConnection();
//            PreparedStatement ps = con.prepareStatement("select ItemID,ItemName,Qty,subtotal from ThirdPartyOrderDetails where orderid = ?");
//            ps.setString(1, _ordNo);
//            ResultSet rs = ps.executeQuery();
//            while (rs.next()) {
//                String h_artcode = rs.getString(1);
//                String h_artname = rs.getString(2);
//                Double D = rs.getDouble(3);
//                int h_qty = D.intValue();
//                int tem = 0;
//                double mrp = rs.getDouble(4);
//                cart.add(new myCart(h_artcode, h_artname, tem, h_qty, mrp));
//                out.println(h_artcode + ", " + h_artname + ", " + h_qty);
//            }
//        } catch (Exception e) {
//            out.println(e.getLocalizedMessage());
//        } finally {
//            close(con);
//        }
//        return cart;
//    }
//
//    public void addCart() {
//        FacesContext fc = getCurrentInstance();
//        Map<String, String> params = fc.getExternalContext().getRequestParameterMap();
//        String item_artcode = params.get("gartcode");
//        String item_artname = params.get("gartname");
//        int item_qoh = parseInt(params.get("gqoh"));
//        double item_mrp = parseDouble(params.get("gmrp")) * reqQTY;
//        out.println(reqQTY);
//        if (reqQTY <= 0) {
//            getCurrentInstance().addMessage(null, new FacesMessage(SEVERITY_INFO, "Please give QTY!!!", "Please give QTY!!!"));
//        } else {
//            cart.add(new myCart(item_artcode, item_artname, item_qoh, reqQTY, item_mrp));
//            setReqQTY(0);
//            getCurrentInstance().addMessage(null, new FacesMessage(SEVERITY_WARN, item_artcode + " Added to Order", ""));
//        }
//    }
//
//    public List<myCart> getCart() {
//        return cart;
//    }
//
//    public void setCart(List<myCart> cart) {
//        this.cart = cart;
//    }
//
//    public double getGrandTot() {
//        return grandTot;
//    }
//
//    public void setGrandTot(double grandTot) {
//        this.grandTot = grandTot;
//    }
//
//    public double getSubTot() {
//        return subTot;
//    }
//
//    public void setSubTot(double subTot) {
//        this.subTot = subTot;
//    }
//
//    public double getTax() {
//        return tax;
//    }
//
//    public void setTax(double tax) {
//        this.tax = tax;
//    }
//
//    public double getDisc() {
//        return disc;
//    }
//
//    public void setDisc(double disc) {
//        this.disc = disc;
//    }
//
//    public List<shoptimizeOrderDetail> getOrdDet() {
//        return ordDet;
//    }
//
//    public String getCustInfo(String _patID) {
//        Connection con = null;
//        String mobNo = "";
//        try {
//            con = getLDBConnection();
//            PreparedStatement ps = con.prepareStatement("select * from CC_Patient_Registration where PatientID = ?");
//            ps.setString(1, _patID);
//            ResultSet rs = ps.executeQuery();
//            while (rs.next()) {
//                String lat = rs.getString(17);
//                String zip = rs.getString(10);
//                mobNo = rs.getString(6);
//                out.println(lat);
//                if (lat != null) {
//                    out.println("Latitude is not null");
//                } else {
//                    if (zip.length() == 6) {
//                        String result = updateLatLon(_patID, zip);
//                        out.println("Latitude(" + result + ") is updated for " + _patID);
//                    }
//                }
//            }
//        } catch (Exception e) {
//            out.println(e.getLocalizedMessage());
//        }
//        return mobNo;
//    }
//
//    public String updateLatLon(String _patID, String _zip) throws Exception {
//        HttpSession appSession = (HttpSession) getCurrentInstance().getExternalContext().getSession(false);
//        int loginid = parseInt(appSession.getAttribute("loginid").toString());
//        String latLongs[] = getLatLongPositions(_zip);
//        String lat = "12.12";
//        String lon = "78.12";
//        lat = latLongs[0];
//        lon = latLongs[1];
//        String latlon = lat + "," + lon;
//        out.println("Latitude: " + latLongs[0] + " and Longitude: " + latLongs[1]);
//        Connection con = null;
//        try {
//            con = getLDBConnection();
//            PreparedStatement ps = con.prepareStatement("update CC_Patient_Registration set loginid = ?, Lat1 = ?, Lon1=?, Lat2=?, Lon2=?, cccardno = 0, uhid = 0, Gender= ?, Age= ? where patientID = ?");
//            ps.setInt(1, loginid);
//            ps.setString(2, lat);
//            ps.setString(3, lon);
//            ps.setString(4, lat);
//            ps.setString(5, lon);
//            ps.setString(6, "M");
//            ps.setString(7, "35");
//            ps.setString(8, _patID);
//            ps.executeUpdate();
//        } catch (Exception e) {
//            out.println(e.getLocalizedMessage());
//        }
//        return latlon;
//    }
//
//    public static String[] getLatLongPositions(String address) throws Exception {
//        int responseCode = 0;
//        String api = "http://maps.googleapis.com/maps/api/geocode/xml?address=" + encode(address, "UTF-8") + "&sensor=true";
//        URL url = new URL(api);
//        HttpURLConnection httpConnection = (HttpURLConnection) url.openConnection();
//        httpConnection.connect();
//        responseCode = httpConnection.getResponseCode();
//        if (responseCode == 200) {
//            DocumentBuilder builder = new DocumentBuilder() {
//                @Override
//                public Document parse(InputSource is) throws SAXException, IOException {
//                    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//                }
//
//                @Override
//                public boolean isNamespaceAware() {
//                    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//                }
//
//                @Override
//                public boolean isValidating() {
//                    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//                }
//
//                @Override
//                public void setEntityResolver(EntityResolver er) {
//                    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//                }
//
//                @Override
//                public void setErrorHandler(ErrorHandler eh) {
//                    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//                }
//
//                @Override
//                public Document newDocument() {
//                    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//                }
//
//                @Override
//                public DOMImplementation getDOMImplementation() {
//                    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//                }
//            };
//            Document document = builder.parse(httpConnection.getInputStream());
//            XPathFactory xPathfactory = newInstance();
//            XPath xpath = xPathfactory.newXPath();
//            XPathExpression expr = xpath.compile("/GeocodeResponse/status");
//            String status = (String) expr.evaluate(document, STRING);
//            if (status.equals("OK")) {
//                expr = xpath.compile("//geometry/location/lat");
//                String latitude = (String) expr.evaluate(document, STRING);
//                expr = xpath.compile("//geometry/location/lng");
//                String longitude = (String) expr.evaluate(document, STRING);
//                return new String[]{latitude, longitude};
//            } else {
//                throw new Exception("Error from the API - response status: " + status);
//            }
//        }
//        return new String[]{"12.5", "78.5"};
//    }
//
//    public List<String> complete(String query) {
//        List<String> results = new ArrayList<String>();
//        Connection con = null;
//        try {
//            con = getLDBConnection();
//            PreparedStatement ps = con.prepareStatement("select siteid,name,HS from cc_sun_locationdetails where name like '%" + query + "%' or siteid like '" + query + "%' group by name,siteid,HS order by name, siteid");
//            ResultSet rs = ps.executeQuery();
//            while (rs.next()) {
//                results.add(rs.getString(1) + " " + rs.getString(2).toUpperCase() + " - " + rs.getString(3));
//            }
//        } catch (Exception e) {
//        }
//        return results;
//    }
//
//    public String Addcomment() {
//        HttpSession appSession = (HttpSession) getCurrentInstance().getExternalContext().getSession(false);
//        String AgentID = appSession.getAttribute("loginid").toString();
//        FacesContext fc = getCurrentInstance();
//        Map<String, String> params = fc.getExternalContext().getRequestParameterMap();
//        String ordno = params.get("ordNo").trim();
//        String vendorName = params.get("vendorName").trim();
//        OrderComment ordComment = new OrderComment();
//        try {
//            Connection con = getLDBConnection();
//            ordComment.addCommenttoOrder(con, ordno, CommentData, Integer.parseInt(AgentID), vendorName);
//            con.close();
//        } catch (Exception e) {
//            System.out.println(e.getLocalizedMessage());
//        }
//        return "Comments Added Successfully";
//    }
//
//    public List<comments> listComments(String _ordNo, String _vendorName) {
//        try {
//            Connection con = getLDBConnection();
//            OrderComment ordComment = new OrderComment();
//            thisOrderComment = ordComment.listAllComments(con, _ordNo, _vendorName);
//            con.close();
//        } catch (Exception e) {
//            System.out.println(e.getLocalizedMessage());
//        }
//        return thisOrderComment;
//    }
//
//    public List<comments> getThisOrderComment() {
//        return thisOrderComment;
//    }
//
//    public List<invAvailability> showInventory() {
//        StringBuilder sb = new StringBuilder();
//        for (myCart c : cart) {
//            sb.append(c.getArtcode()).append(",");
//        }
//        String art = sb.toString().substring(0, sb.length() - 1);
//        System.out.println(art);
//
//        String url = "http://172.16.2.251:8085/WSS/mapp/inventoryCheck/" + shopid + "/" + art;
//        Client restClient = create();
//        WebResource webResource = restClient.resource(url);
//        ClientResponse resp = webResource.accept("application/json")
//                .get(ClientResponse.class);
//        if (resp.getStatus() != 200) {
//            err.println("Unable to connect to the server");
//        }
//        String output = resp.getEntity(String.class);
//        System.out.println("response: " + output);
//        try {
//            JSONArray jsonArray = new JSONArray(output);
//            int count = jsonArray.length();
//            for (int i = 0; i < count; i++) {
//                JSONObject jsonObject = jsonArray.getJSONObject(i);
//                String invartCode = jsonObject.getString("artCode");
//                int invQty = jsonObject.getInt("qoh");
//                invAvail.add(new invAvailability(invartCode, invQty));
//            }
//
//        } catch (JSONException e) {
//            System.out.println(e.getLocalizedMessage());
//        }
//        return invAvail;
//    }
//
//    public void onResize(ResizeEvent event) {
//        //FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Image resized", "Width:" + event.getWidth() + ",Height:" + event.getHeight());
//
//        //FacesContext.getCurrentInstance().addMessage(null, msg);
//    }
}
