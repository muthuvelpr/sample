/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.logic.thirdparty.ctlr;

import ccm.dao.bean.connectivity;
import static ccm.dao.bean.connectivity.getLDBConnection;
import com.logic.branch.ctlr.initClose;
import com.logic.branch.ctlr.initValidator;
import static com.logic.branch.ctlr.initValidator.checkOrderStatusbeforeclosureInit;
import com.logic.thirdparty.model.TPOrderClose;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import static java.lang.Integer.parseInt;
import static java.lang.System.out;
import java.net.HttpURLConnection;
import static java.net.HttpURLConnection.HTTP_OK;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import static java.util.Calendar.getInstance;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.faces.application.FacesMessage;
import static javax.faces.application.FacesMessage.SEVERITY_WARN;
import javax.faces.context.FacesContext;
import static javax.faces.context.FacesContext.getCurrentInstance;

/**
 *
 * @author PITCHU
 */
public class tpDelivery {

    List<TPOrderClose> tpDeliverySum = new ArrayList<TPOrderClose>();
    private TPOrderClose tpcloseselectedOrder;
    Calendar c = getInstance();
    SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd");
    Date ordFDate = new java.util.Date();
    Date ordTDate = new java.util.Date();

    public TPOrderClose getTpcloseselectedOrder() {
        return tpcloseselectedOrder;
    }

    public void setTpcloseselectedOrder(TPOrderClose tpcloseselectedOrder) {
        this.tpcloseselectedOrder = tpcloseselectedOrder;
    }

    /**
     * Creates a new instance of tpDelivery
     */
    public tpDelivery() {
    }

    public List showInitiatedOrders() {
        FacesContext fc = getCurrentInstance();
        Map<String, String> params = fc.getExternalContext().getRequestParameterMap();
        String repFor = params.get("repFor");
        Connection con = null;
        PreparedStatement ps = null;
        tpDeliverySum.clear();
        try {
            con = getLDBConnection();

            ps = con.prepareStatement("select o.ordno,o.siteid,ISNULL(CONVERT(varchar, o.orddate),'-') ,ISNULL(CONVERT(varchar, o.initdate),'-') ,ISNULL(CONVERT(varchar, o.deldate),'-'),(select reg_name from dbo.CClogin where loginid=id) as regname,(select FirstName + ' ' + LastName from dbo.CC_Patient_Registration where patientid=patid) as Patname,(select mobileno from dbo.CC_Patient_Registration where patientid=patid) as Patmobileno,(select gender from dbo.CC_Patient_Registration where patientid=patid) as Gender, status,(select username from dbo.CClogin where loginid=id) as loginusername,ordersource,ISNULL(CONVERT(varchar, cancelDate),'-'),ISNULL(CONVERT(varchar, cancelRemarks),'-'),paymentmode,(select Addr1 from dbo.CC_Patient_Registration where patientid=patid) as PatAddr,(select maorderid from orderupdate where ordno = REPLACE (ap_orderid, 'V' , '')) as MAOrdID from cc_ordhead o where o.status in(1) and ordersource = 'MedAssist' and  CONVERT (DATE, ORDDATE)>=CONVERT (DATE, ?) AND CONVERT (DATE, ORDDATE) <=CONVERT (DATE, ?)");
            ps.setString(1, df.format(ordFDate));
            ps.setString(2, df.format(ordTDate));

            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                String delCanDate = rs.getString(5);
                String statusurl = "";

                //String orddt = rs.getString(2).substring(0, 16);
                String statusval = rs.getString(10);
                if (statusval.equals("0")) {
                    statusurl = "./yellow.jpg";
                } else if (statusval.equals("1")) {
                    statusurl = "./green.jpg";
                } else if (statusval.equals("2")) {
                    statusurl = "./redcan.jpg";
                } else if (statusval.equals("3")) {
                    statusurl = "./red.jpg";
                }

                if (delCanDate.equals("-")) {
                    delCanDate = rs.getString(13);
                }
                //summaryReport(int ord, int siteid, String orddate, String initdate, String deldate, String region, String patname, String mobileno, String gender, String statusurl, String orderby, String orderSource, String cancelDate, String cancelRemarks, String paymentMode, String commAddr1)
                tpDeliverySum.add(new TPOrderClose(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getString(4), delCanDate, rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9), statusurl, rs.getString(11), rs.getString(12), rs.getString(13), rs.getString(14), rs.getString(15), rs.getString(16), rs.getInt(17)));
            }
        } catch (Exception e) {
            out.println(e.getLocalizedMessage());
        } finally {
        }
        return tpDeliverySum;
    }

    public List<TPOrderClose> getTpSum() {
        return tpDeliverySum;
    }

    public void ordClosureInit() {
        FacesContext fc = getCurrentInstance();
        Map<String, String> params = fc.getExternalContext().getRequestParameterMap();
        String ordNo = params.get("ordNo");
        String ordSRC = params.get("ordSrc");
        boolean result = checkOrderStatusbeforeclosureInit(ordNo);
        if (result) {
            initClose s1 = new initClose();
            s1.doorderDeliveredwithoutPin(ordNo);
            List<TPOrderClose> tpSum = new ArrayList<TPOrderClose>();
            tpSum.remove(tpcloseselectedOrder);
            getDeliveryDetails(parseInt(ordNo));
            showInitiatedOrders();

            getCurrentInstance().addMessage(null, new FacesMessage(SEVERITY_WARN, "Order no: " + ordNo + " Delivered!!!", ""));
        } else {
            getCurrentInstance().addMessage(null, new FacesMessage(SEVERITY_WARN, "Order no: " + ordNo + " already Initiated!!!", ""));
        }
    }

    public void getDeliveryDetails(int _ordNo) {
        Connection con = null;
        try {
            String siteid = "";
            String tid = "";
            String docnum = "";
            String APOrderId = "";
            String invoicenum = "";
            String billvalue = "";
            String ordertype = "";
            String remarks = "";
            String status = "2";
            String cash = "";
            String credit = "";
            con = getLDBConnection();
            Statement stmt = con.createStatement();
            String qry = "select siteid,tid,posdocnum,aporderid,invoicenum,invoiceamount, (select paymentmode from cc_ordhead where ordno = " + _ordNo + "), cashvalue,creditvalue from MEDASSITTRANSACTIONS where aporderid like '%" + _ordNo + "' and invoicenum is not null ";
            ResultSet rs = stmt.executeQuery(qry);
            if (rs.next()) {
                siteid = rs.getString(1);
                tid = rs.getString(2);
                docnum = rs.getString(3);
                APOrderId = rs.getString(4);
                invoicenum = rs.getString(5);
                billvalue = rs.getString(6);
                ordertype = rs.getString(7);
                cash = rs.getString(8);
                credit = rs.getString(9);
                //TEMP Lock updateDeliveryStatustoVendor(siteid, tid, docnum, APOrderId, invoicenum, billvalue, ordertype, remarks, status, cash, credit);
            }
        } catch (Exception e) {
            out.println(e.getLocalizedMessage());
        }
    }

    public void updateDeliveryStatustoVendor(String _siteid, String _tid, String _docnum, String _APOrderId, String _invoicenum, String _billvalue, String _ordertype, String _remarks, String _status, String _cash, String _credit) {
        try {

            final String USER_AGENT = "Mozilla/5.0";
            //siteid=16001&tid=001&docnum=1151074&APOrderId=1000000532&invoicenum=123456&billvalue=100&remarks=CRO0001&ordertype=CREDIT&statusid=2&Cancelfrom=POS&Action=OrderStatus                
            final String GET_URL = "http://172.16.2.251:84/MediAssist_pos.aspx?"
                    + "siteid=" + _siteid + "&"
                    + "tid=" + _tid + "&"
                    + "docnum=" + _docnum + "&"
                    + "APOrderId=" + _APOrderId + "&"
                    + "invoicenum=" + _invoicenum + "&"
                    + "billvalue=" + _billvalue + "&"
                    + "remarks=" + _remarks + "&"
                    + "ordertype=" + _ordertype + "&"
                    + "statusid=" + _status + "&"
                    + "Cancelfrom=CRM&"
                    + "Cashvalue=" + _cash + "&"
                    + "Creditvalue=" + _credit + "&"
                    + "Action=OrderStatus";
            URL obj = new URL(GET_URL);
            out.println(GET_URL);
            HttpURLConnection smscon = (HttpURLConnection) obj.openConnection();
            smscon.setRequestMethod("GET");
            smscon.setRequestProperty("User-Agent", USER_AGENT);
            int responseCode = smscon.getResponseCode();
            out.println("GET Response Code :: " + responseCode);
            if (responseCode == HTTP_OK) { // success
                BufferedReader in = new BufferedReader(new InputStreamReader(
                        smscon.getInputStream()));
                String inputLine;
                StringBuilder response = new StringBuilder();
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
                out.println(response.toString());
            } else {
                out.println("GET request not worked");
            }
        } catch (IOException ex) {
            out.println(ex.getLocalizedMessage());
        }
    }
}
