/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.logic.cms.ctlr;

import ccm.dao.bean.connectivity;
import static ccm.dao.bean.connectivity.getLDBConnection;
import com.logic.cms.bean.crmOTP;
import com.logic.cms.bean.crmRegistration;
import com.logic.cms.model.allocation;
import com.logic.cms.model.ordSource;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import static javax.faces.application.FacesMessage.SEVERITY_INFO;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import static javax.faces.context.FacesContext.getCurrentInstance;
import javax.servlet.http.HttpSession;

/**
 * @created for APOLLO PHARMACY DPAPP
 * @author PITCHU
 * @created Sep 28, 2017 11:42:40 AM
 */
@ManagedBean(name = "cm")
@ViewScoped
public class callManager implements Serializable {

    private Map<String, Map<String, String>> data = new HashMap<String, Map<String, String>>();
    private String complaintType;
    private String forwardTo, siteid, orderSource;
    private Map<String, String> complaints;
    List<ordSource> ordSrc = new ArrayList<ordSource>();
    List<allocation> allott = new ArrayList<allocation>();
    private String custName, priContact, secContact, Location, addr, tracRef, compDet, remarks, mailid, otp;
    private String ticketNo;

    private boolean DialogVisible;

    public boolean isDialogVisible() {
        return DialogVisible;
    }

    public void setDialogVisible(boolean DialogVisible) {
        this.DialogVisible = DialogVisible;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getPriContact() {
        return priContact;
    }

    public void setPriContact(String priContact) {
        this.priContact = priContact;
    }

    public String getSecContact() {
        return secContact;
    }

    public void setSecContact(String secContact) {
        this.secContact = secContact;
    }

    public String getLocation() {
        return Location;
    }

    public void setLocation(String Location) {
        this.Location = Location;
    }

    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }

    public String getTracRef() {
        return tracRef;
    }

    public void setTracRef(String tracRef) {
        this.tracRef = tracRef;
    }

    public String getCompDet() {
        return compDet;
    }

    public void setCompDet(String compDet) {
        this.compDet = compDet;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getMailid() {
        return mailid;
    }

    public void setMailid(String mailid) {
        this.mailid = mailid;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getSiteid() {
        return siteid;
    }

    public void setSiteid(String siteid) {
        this.siteid = siteid;
    }

    public String getOrderSource() {
        return orderSource;
    }

    public void setOrderSource(String orderSource) {
        this.orderSource = orderSource;
    }

    public List<ordSource> getOrdSrc() {
        return ordSrc;
    }

    public Map<String, Map<String, String>> getData() {
        return data;
    }

    public String getComplaintType() {
        return complaintType;
    }

    public void setComplaintType(String complaintType) {
        this.complaintType = complaintType;
    }

    public String getForwardTo() {
        return forwardTo;
    }

    public void setForwardTo(String forwardTo) {
        this.forwardTo = forwardTo;
    }

    public Map<String, String> getComplaints() {
        return complaints;
    }

    public List<allocation> getAllott() {
        return allott;
    }

    public String getTicketNo() {
        return ticketNo;
    }

    public void setTicketNo(String ticketNo) {
        this.ticketNo = ticketNo;
    }

    @PostConstruct
    public void init() {
        complaints = new HashMap<String, String>();
        try {
            complaints.clear();
            Connection con = getLDBConnection();
            PreparedStatement ps = con.prepareStatement("select problemid, problemtype from crm_problemtypemaster");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                complaints.put(rs.getString(2), rs.getString(1));
            }
        } catch (Exception e) {

        }
        generateordSource();
    }

    public List generateordSource() {
        try {
            ordSrc.clear();
            Connection con = getLDBConnection();
            PreparedStatement ps = con.prepareStatement("select ORDERTYPE from cc_ordtype group by ordertype");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                ordSrc.add(new ordSource(rs.getString(1)));
            }
        } catch (Exception e) {

        }
        return ordSrc;
    }

    public List onPTChange() {
        try {
            allott.clear();
            Connection con = getLDBConnection();
            PreparedStatement ps = null;
            if (complaintType.equals("1")) {
                ps = con.prepareStatement("select usr,username from CClogin where usr_group = 3");
            } else {
                ps = con.prepareStatement("select usr,username from CClogin where usr_group = 1");
            }
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                allott.add(new allocation(rs.getString(1), rs.getString(2)));
            }
        } catch (Exception e) {

        }
        return allott;
    }

    public void doRegister() {
        String tktPrefix = "";
        SimpleDateFormat ticD = new SimpleDateFormat("yyMMdd");
        HttpSession appSession = (HttpSession) getCurrentInstance().getExternalContext().getSession(false);
        String username = appSession.getAttribute("loginid").toString();
        String action = "REGISTERED";
        if (siteid.length() >= 4) {
            siteid = siteid.substring(0, 5);
        } else {
            siteid = "0";
        }
        crmRegistration reg = new crmRegistration();
        if (complaintType.equals("1")) {
            tktPrefix = "COM" + ticD.format(new java.util.Date()) + "-";
        } else if (complaintType.equals("2")) {
            tktPrefix = "ENQ" + ticD.format(new java.util.Date()) + "-";
        } else if (complaintType.equals("3")) {
            tktPrefix = "INT" + ticD.format(new java.util.Date()) + "-";
        }
        ticketNo = reg.registration(complaintType, custName, priContact, secContact, Location, addr, orderSource, tracRef, compDet, siteid, forwardTo, remarks, tktPrefix, username, action, mailid);
        getCurrentInstance().addMessage(null, new FacesMessage(SEVERITY_INFO, "Complaint Registered Successfully!!!", ""));
    }

    public String getOTP() {
        HttpSession appSession = (HttpSession) getCurrentInstance().getExternalContext().getSession(false);
        String username = appSession.getAttribute("loginid").toString();
        siteid = (String) siteid.subSequence(0, 5);
        crmOTP posOTP = new crmOTP();
        otp = posOTP.getGeneratedPOSOTP(siteid.substring(0, 5), priContact);
        String otpalone = otp.replace("OTP for this transaction is : ", "");
        if (otp.length() >= 4) {
            posOTP.otpLog(siteid, priContact, otpalone, username);
        }
        return otp;
    }
}
