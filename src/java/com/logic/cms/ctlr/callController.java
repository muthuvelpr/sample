/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.logic.cms.ctlr;

import static ccm.dao.bean.connectivity.getLDBConnection;
import static com.logic.cms.bean.crmCheck.processCheck;
import com.logic.cms.bean.crmClose;
import com.logic.cms.bean.crmLogHistory;
import com.logic.cms.bean.crmProcess;
import com.logic.cms.bean.crmSendMail;
import com.logic.cms.model.allocation;
import com.logic.cms.model.callHistory;
import com.logic.cms.model.processingCalls;
import static java.lang.System.out;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.faces.application.FacesMessage;
import static javax.faces.application.FacesMessage.SEVERITY_ERROR;
import static javax.faces.application.FacesMessage.SEVERITY_WARN;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import static javax.faces.context.FacesContext.getCurrentInstance;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Lenovo
 */
@ManagedBean(name = "cc")
@ViewScoped
public class callController {

    /**
     * Creates a new instance of callController
     */
    List<processingCalls> pcc = new ArrayList<processingCalls>();
    List<processingCalls> cc = new ArrayList<processingCalls>();
    List<callHistory> ch = new ArrayList<callHistory>();
    List<allocation> allott = new ArrayList<allocation>();
    List<processingCalls> filteredprocessingCalls;

    String fwdTo = "";
    String remarks = "";
    String emailID = "";
    private processingCalls selectedProcessingCalls;

    public List<processingCalls> getFilteredprocessingCalls() {
        return filteredprocessingCalls;
    }

    public void setFilteredprocessingCalls(List<processingCalls> filteredprocessingCalls) {
        this.filteredprocessingCalls = filteredprocessingCalls;
    }

    public String getEmailID() {
        return emailID;
    }

    public void setEmailID(String emailID) {
        this.emailID = emailID;
    }

    public processingCalls getSelectedProcessingCalls() {
        return selectedProcessingCalls;
    }

    public String getFwdTo() {
        return fwdTo;
    }

    public void setFwdTo(String fwdTo) {
        this.fwdTo = fwdTo;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public void setSelectedProcessingCalls(processingCalls selectedProcessingCalls) {
        this.selectedProcessingCalls = selectedProcessingCalls;
        String ticketNo = selectedProcessingCalls.getTICKETNO();
        String probType = selectedProcessingCalls.getProbType();
        callHistory(ticketNo);
        PT(probType);
    }

    public callController() throws SQLException, ClassNotFoundException {
        List showProcessingCalls = showProcessingCalls();
        List showClosingCalls = showClosedCalls();
    }

    private List showProcessingCalls() throws SQLException, ClassNotFoundException {
        pcc.clear();
        Connection con = getLDBConnection();
        try {
            PreparedStatement ps = con.prepareStatement("select A.PROBLEMTYPE,A.CUSTNAME,A.PRICONTACTNO,A.SECCONTACTNO,A.LOCATION,A.ADDRESS,A.TRACKINGREFNO,A.ORDERSOURCE,A.PROBLEMDESCRIPTION,A.SITEID,A.TICKETNO,A.REGDATETIME,(SELECT USERNAME FROM CCLOGIN WHERE id = REGBY),isnull((SELECT NAME FROM cc_sun_locationdetails WHERE SITEID = A.SITEID),''), A.MAILID, A.STATUS FROM CRM_CMS_MASTER A WHERE STATUS IN (0,1) order by regdatetime desc");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                String regDate = rs.getString(12).substring(0, 16);
                String problemType = rs.getString(1);
                if (problemType.equals("1")) {
                    problemType = "Complaints";
                } else if (problemType.equals("2")) {
                    problemType = "Enquiry";
                } else if (problemType.equals("3")) {
                    problemType = "Internal";
                }
                pcc.add(new processingCalls(rs.getString(1), problemType, rs.getString(2).toUpperCase(), rs.getString(3), rs.getString(4), rs.getString(5).toUpperCase(), rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9).toUpperCase(), rs.getString(10), rs.getString(11), regDate, rs.getString(13), rs.getString(14).toUpperCase(), rs.getString(15), rs.getString(16)));
            }
        } catch (Exception e) {

        }
        return pcc;
    }

    public List<processingCalls> getPcc() {
        return pcc;
    }

    private List showClosedCalls() throws SQLException, ClassNotFoundException {
        cc.clear();
        Connection con = getLDBConnection();
        try {
            PreparedStatement ps = con.prepareStatement("select A.PROBLEMTYPE,A.CUSTNAME,A.PRICONTACTNO,A.SECCONTACTNO,A.LOCATION,A.ADDRESS,A.TRACKINGREFNO,A.ORDERSOURCE,A.PROBLEMDESCRIPTION,A.SITEID,A.TICKETNO,A.REGDATETIME,(SELECT USERNAME FROM CCLOGIN WHERE id = REGBY),isnull((SELECT NAME FROM cc_sun_locationdetails WHERE SITEID = A.SITEID),''), A.MAILID,STATUS FROM CRM_CMS_MASTER A  order by regdatetime desc");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                String regDate = rs.getString(12).substring(0, 16);
                String problemType = rs.getString(1);
                if (problemType.equals("1")) {
                    problemType = "Complaints";
                } else if (problemType.equals("2")) {
                    problemType = "Enquiry";
                } else if (problemType.equals("3")) {
                    problemType = "Internal";
                }
                cc.add(new processingCalls(rs.getString(1), problemType, rs.getString(2).toUpperCase(), rs.getString(3), rs.getString(4), rs.getString(5).toUpperCase(), rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9).toUpperCase(), rs.getString(10), rs.getString(11), regDate, rs.getString(13), rs.getString(14).toUpperCase(), rs.getString(15), rs.getString(16)));
            }
        } catch (Exception e) {

        }
        return cc;
    }

    public List<processingCalls> getCc() {
        return cc;
    }

    public List callHistory(String _ticketNo) {
        Connection con = null;
        ch.clear();
        try {
            con = getLDBConnection();
            PreparedStatement ps = con.prepareStatement("select ticketno,(SELECT USERNAME FROM CCLOGIN WHERE id = processby),processdatetime,(select username from cclogin where usr = allottedto), remarks,action from CRM_CMS_DTL where TICKETNO = ? order by PROCESSDATETIME asc");
            ps.setString(1, _ticketNo);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                //ticketno, regby,processdt,allottedto,remarks,action
                ch.add(new callHistory(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5).toUpperCase(), rs.getString(6)));
            }
        } catch (Exception e) {
            out.println(e.getLocalizedMessage());
            getCurrentInstance().addMessage(null, new FacesMessage(SEVERITY_ERROR, e.getLocalizedMessage(), "Error!"));
        }
        return ch;
    }

    public List<callHistory> getCh() {
        return ch;
    }

    public void sendMail(String _ticketNo) {

    }

    public List PT(String _probType) {
        try {
            allott.clear();
            Connection con = getLDBConnection();
            PreparedStatement ps = null;
            if (_probType.equals("1")) {
                ps = con.prepareStatement("select usr,username from CClogin where usr_group = 3");
            } else {
                ps = con.prepareStatement("select usr,username from CClogin where usr_group = 1");
            }
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                allott.add(new allocation(rs.getString(1), rs.getString(2)));
            }
        } catch (Exception e) {

        }
        return allott;
    }

    public List<allocation> getAllott() {
        return allott;
    }

    public void initProcess() {
        FacesContext context = getCurrentInstance();
        HttpServletRequest myRequest = (HttpServletRequest) context.getExternalContext().getRequest();
        String ticketNo = myRequest.getParameter("ticketNo");
        String action = myRequest.getParameter("action");
        boolean canProcess = processCheck(ticketNo);
        if (canProcess) {
            HttpSession appSession = (HttpSession) getCurrentInstance().getExternalContext().getSession(false);
            String username = appSession.getAttribute("loginid").toString();
            crmLogHistory log = new crmLogHistory();
            crmProcess process = new crmProcess();
            log.logHistory(ticketNo, username, fwdTo, remarks, action);
            process.crmCallProcess(ticketNo);
        } else {
            getCurrentInstance().addMessage(null, new FacesMessage(SEVERITY_WARN, "Call Already Closed!!!", ""));
        }
        if (action.equals("MAIL")) {
            crmSendMail mailFeature = new crmSendMail();
            mailFeature.sendMail(ticketNo, emailID);
        }
    }

    public void initClose() {
        FacesContext context = getCurrentInstance();
        HttpServletRequest myRequest = (HttpServletRequest) context.getExternalContext().getRequest();
        String ticketNo = myRequest.getParameter("ticketNo");
        String action = myRequest.getParameter("action");
        boolean canProcess = processCheck(ticketNo);
        if (canProcess) {
            HttpSession appSession = (HttpSession) getCurrentInstance().getExternalContext().getSession(false);
            String username = appSession.getAttribute("loginid").toString();
            crmLogHistory log = new crmLogHistory();
            crmClose close = new crmClose();
            log.logHistory(ticketNo, username, fwdTo, remarks, action);
            close.crmCallClose(ticketNo);
            pcc.remove(selectedProcessingCalls);
        } else {
            getCurrentInstance().addMessage(null, new FacesMessage(SEVERITY_WARN, "Call Already Closed!!!", ""));
        }
    }
}
