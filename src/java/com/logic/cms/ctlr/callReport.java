/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.logic.cms.ctlr;

import ccm.dao.bean.connectivity;
import static ccm.dao.bean.connectivity.getLDBConnection;
import com.logic.cms.model.cmsTracker;
import java.io.Serializable;
import static java.lang.System.out;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import static java.util.Calendar.DATE;
import static java.util.Calendar.getInstance;
import java.util.Date;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Pitchu
 */
@ManagedBean(name = "cr")
@ViewScoped
public class callReport implements Serializable {

    private String shopname, ticketNo, dept;
    private Date date1, date2;
    private String[] callStatus;
    private String[] reg_id;
    List<cmsTracker> cmsDetails = new ArrayList<cmsTracker>();

    public String getDept() {
        return dept;
    }

    public void setDept(String dept) {
        this.dept = dept;
    }

    public String[] getCallStatus() {
        return callStatus;
    }

    public void setCallStatus(String[] callStatus) {
        this.callStatus = callStatus;
    }

    public String[] getReg_id() {
        return reg_id;
    }

    public void setReg_id(String[] reg_id) {
        this.reg_id = reg_id;
    }

    public Date getDate1() {
        return date1;
    }

    public void setDate1(Date date1) {
        this.date1 = date1;
    }

    public Date getDate2() {
        return date2;
    }

    public void setDate2(Date date2) {
        this.date2 = date2;
    }

    public List cmsTrackDetailsByRegion() {
        Connection con = null;
        cmsDetails.clear();
        //filteredCalls.clear();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MMM-dd");
        SimpleDateFormat form = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String id1 = df.format(date1);

        Calendar c = getInstance();
        c.setTime(date2);
        c.add(DATE, 1);
        Date dt = c.getTime();
        String id2 = df.format(dt);
        StringBuilder reg_sb = new StringBuilder();
        StringBuilder callStatus_sb = new StringBuilder();
        String loopDelim = "";
        for (String s : reg_id) {
            reg_sb.append(loopDelim);
            reg_sb.append("'" + s + "'");
            loopDelim = ",";
        }
        String regions = reg_sb.toString();
        out.println(regions);
        loopDelim = "";
        for (String s : callStatus) {
            callStatus_sb.append(loopDelim);
            callStatus_sb.append("'" + s + "'");
            loopDelim = ",";
        }
        String call_Status = callStatus_sb.toString();
        out.println(call_Status);
        try {
            con = getLDBConnection();
            String regwiseQRY = "";
            PreparedStatement ps = null;
            ResultSet rs = null;

            if (call_Status.equals("'0'")) {
                regwiseQRY = "select branchname,ticketno,problemdrescription,regtime, '-' as closetime,staffname,'-'as process_by,'-'as remarks,status,'' AS at , '-' as tlevel from cms_master where "
                        + "REGION in(" + regions + ") and status in (0) and problemtype in ('" + dept + "') and regtime between '" + id1 + "' and '" + id2 + "'";
                ps = con.prepareStatement(regwiseQRY);
                /*ps.setString(1, dept);
                ps.setString(2, id1);
                ps.setString(3, id2);*/
                rs = ps.executeQuery();
            } else {
                regwiseQRY = "select a.branchname,a.ticketno,a.problemdrescription,a.regtime,b.DTTIME,a.staffname,(select empname from cms_login_master where empno = b.process_by) as processby,"
                        + "b.checkedwith,a.status,b.at,(select TLEVEL from cms_login_master where empno = b.process_by) as TLEVEL from cms_master a, cms_dtl b where REGION in (" + regions + ") and "
                        + "status in (" + call_Status + ") and a.ticketno=b.ticketno and problemtype in (?) and (a.ticketno) in (select ticketno from(select a.ticketno ticketno,max(b.dttime) dttime "
                        + "from cms_master a, cms_dtl b where REGION in(" + regions + ") and status in (" + call_Status + ") and a.ticketno=b.ticketno and problemtype in(?) and regtime between ? and ? "
                        + "group by a.ticketno)a)  and (b.dttime) in (select dttime from(select a.ticketno ticketno,max(b.dttime) dttime from cms_master a, cms_dtl b where REGION in (" + regions + ") and "
                        + "status in (" + call_Status + ") and a.ticketno=b.ticketno and problemtype in(?) and regtime between ? and ? group by a.ticketno)b) order by regtime desc";
                ps = con.prepareStatement(regwiseQRY);
                ps.setString(1, dept);
                ps.setString(2, dept);
                ps.setString(3, id1);
                ps.setString(4, id2);
                ps.setString(5, dept);
                ps.setString(6, id1);
                ps.setString(7, id2);
                rs = ps.executeQuery();
            }

            int sno = 0;
            while (rs.next()) {
                sno++;
                String rec = rs.getString(9);
                String regt, closet;
                long CallDiff, diff, sortDuration = 0, sortcallDuration = 0;
                long diffSeconds = 0;
                long diffMinutes = 0;
                long diffHours = 0;
                long diffDays = 0;
                long calldiffSeconds = 0;
                long calldiffMinutes = 0;
                long calldiffHours = 0;
                long calldiffDays = 0;
                if (rec.equals("0")) {
                    regt = rs.getString(4);
                    closet = rs.getString(5);
                } else {
                    regt = rs.getString(4).substring(0, 19);
                    closet = rs.getString(5).substring(0, 19);
                    Date d1 = form.parse(regt);
                    Date d2 = form.parse(closet);
                    Date d3 = new java.util.Date();
                    diff = d2.getTime() - d1.getTime();
                    CallDiff = d3.getTime() - d1.getTime();

                    sortDuration = diff;
                    sortcallDuration = CallDiff;

                    diffSeconds = diff / 1000 % 60;
                    diffMinutes = diff / (60 * 1000) % 60;
                    diffHours = diff / (60 * 60 * 1000) % 24;
                    diffDays = diff / (24 * 60 * 60 * 1000);

                    calldiffSeconds = CallDiff / 1000 % 60;
                    calldiffMinutes = CallDiff / (60 * 1000) % 60;
                    calldiffHours = CallDiff / (60 * 60 * 1000) % 24;
                    calldiffDays = CallDiff / (24 * 60 * 60 * 1000);

                }
                String status = "";

                String Duration = "-";
                String CallDuration = "-";
                String action = rs.getString(10);
                String tlevel = rs.getString(7) + "(" + rs.getString(11) + ")";
                if (rec.equals("0")) {
                    status = "NEW";
                    closet = "-";
                    Duration = calldiffDays + " Day(s), " + calldiffHours + " Hours,  " + calldiffMinutes + " Mins, " + calldiffSeconds + " sec.";
                    tlevel = "-";
                } else if (rec.equals("1")) {
                    status = "PROCESSING";
                    Duration = diffDays + " Day(s), " + diffHours + " Hours,  " + diffMinutes + " Mins, " + diffSeconds + " sec.";
                    CallDuration = calldiffDays + " Day(s), " + calldiffHours + " Hours,  " + calldiffMinutes + " Mins, " + calldiffSeconds + " sec.";
                } else if (rec.equals("2")) {
                    status = "CLOSED";
                    Duration = diffDays + " Day(s), " + diffHours + " Hours,  " + diffMinutes + " Mins, " + diffSeconds + " sec.";
                    CallDuration = Duration;
                } else if (rec.equals("3")) {
                    status = "L1 FWD";
                    Duration = diffDays + " Day(s), " + diffHours + " Hours,  " + diffMinutes + " Mins, " + diffSeconds + " sec.";
                    CallDuration = calldiffDays + " Day(s), " + calldiffHours + " Hours,  " + calldiffMinutes + " Mins, " + calldiffSeconds + " sec.";
                } else if (rec.equals("4")) {
                    status = "L2 FWD";
                    Duration = diffDays + " Day(s), " + diffHours + " Hours,  " + diffMinutes + " Mins, " + diffSeconds + " sec.";
                    CallDuration = calldiffDays + " Day(s), " + calldiffHours + " Hours,  " + calldiffMinutes + " Mins, " + calldiffSeconds + " sec.";
                } else if (rec.equals("5")) {
                    status = "L3 FWD";
                    Duration = diffDays + " Day(s), " + diffHours + " Hours,  " + diffMinutes + " Mins, " + diffSeconds + " sec.";
                    CallDuration = calldiffDays + " Day(s), " + calldiffHours + " Hours,  " + calldiffMinutes + " Mins, " + calldiffSeconds + " sec.";
                } else if (rec.equals("6")) {
                    status = "L4 FWD";
                    Duration = diffDays + " Day(s), " + diffHours + " Hours,  " + diffMinutes + " Mins, " + diffSeconds + " sec.";
                    CallDuration = calldiffDays + " Day(s), " + calldiffHours + " Hours,  " + calldiffMinutes + " Mins, " + calldiffSeconds + " sec.";
                } else if (rec.equals("7")) {
                    status = "RE-OPEN BY BRANCH";
                    Duration = diffDays + " Day(s), " + diffHours + " Hours,  " + diffMinutes + " Mins, " + diffSeconds + " sec.";
                    CallDuration = calldiffDays + " Day(s), " + calldiffHours + " Hours,  " + calldiffMinutes + " Mins, " + calldiffSeconds + " sec.";
                } else if (rec.equals("8")) {
                    status = "ACKNOWLEDGE BY BRANCH";
                    Duration = diffDays + " Day(s), " + diffHours + " Hours,  " + diffMinutes + " Mins, " + diffSeconds + " sec.";
                    CallDuration = calldiffDays + " Day(s), " + calldiffHours + " Hours,  " + calldiffMinutes + " Mins, " + calldiffSeconds + " sec.";
                }
                cmsDetails.add(new cmsTracker(rs.getString(1), rs.getString(2), rs.getString(3).trim().toUpperCase().replace("NULL, ", ""), regt, closet, rs.getString(6), tlevel, rs.getString(8), status, action, Duration, CallDuration, sortDuration, sortcallDuration, sno));
            }
        } catch (Exception e) {
            out.println("Error : " + e.getLocalizedMessage());
        }
        return cmsDetails;
    }

}
