/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.logic.cms.bean;

import ccm.dao.bean.connectivity;
import static ccm.dao.bean.connectivity.getLDBConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;

/**
 * @created for APOLLO PHARMACY DPAPP
 * @author PITCHU
 * @created Oct 17, 2017 3:23:17 AM
 */
public class crmLogHistory {

    public void logHistory(String _ticketNo, String _processBy, String _fwdTo, String _remarks, String _action) {
        Connection con = null;
        try {
            con = getLDBConnection();
            PreparedStatement ps = con.prepareStatement("insert into CRM_CMS_DTL values (?,?,getDate(),?,?,?)");
            ps.setString(1, _ticketNo);
            ps.setString(2, _processBy);
            ps.setString(3, _fwdTo);
            ps.setString(4, _remarks);
            ps.setString(5, _action);
            ps.executeQuery();
        } catch (Exception e) {

        }
    }
}
