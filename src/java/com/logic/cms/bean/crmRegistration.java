/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.logic.cms.bean;

import ccm.dao.bean.connectivity;
import static ccm.dao.bean.connectivity.getLDBConnection;
import static java.lang.Integer.parseInt;
import static java.lang.Integer.parseInt;
import static java.lang.System.out;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * @created for APOLLO PHARMACY DPAPP
 * @author PITCHU
 * @created Sep 28, 2017 6:19:56 PM
 */
public class crmRegistration {

    public String registration(String _probType, String _custName, String _priContNo, String _secContNo, String _location, String _compAddr, String _orderSource, String _trackRefNo, String _compDet, String _siteID, String _compAllottedTo, String _remarks, String _tktPrefix, String _regBy, String _action, String _mailid) {
        String ticket = "";
        try {
            Connection con = null;
            con = getLDBConnection();
            PreparedStatement ps = con.prepareStatement("SELECT MAX(TKT)+1 FROM CRM_TICKETMASTER WHERE PROBLEMID = ?");
            ps.setString(1, _probType);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                int tktNo = rs.getInt(1);
                PreparedStatement ps1 = con.prepareCall("{call CRMCALLLOG (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
                ps1.setInt(1, parseInt(_probType));
                ps1.setString(2, _custName);
                ps1.setString(3, _priContNo);
                ps1.setString(4, _secContNo);
                ps1.setString(5, _location);
                ps1.setString(6, _compAddr);
                ps1.setString(7, _trackRefNo);
                ps1.setString(8, _orderSource);
                ps1.setString(9, _compDet);
                ps1.setInt(10, parseInt(_siteID));
                ps1.setString(11, _compAllottedTo);
                ps1.setString(12, _tktPrefix + tktNo);
                ps1.setInt(13, tktNo);
                ps1.setString(14, _regBy);
                ps1.setString(15, _remarks);
                ps1.setString(16, _action);
                ps1.setString(17, _mailid);
                ResultSet rs1 = ps1.executeQuery();
                if (rs1.next()) {
                    ticket = _tktPrefix + tktNo;
                }
            }
        } catch (Exception e) {
            out.println(e.getLocalizedMessage());
        }
        return ticket;
    }
}
