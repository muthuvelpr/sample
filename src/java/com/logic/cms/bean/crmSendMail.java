/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.logic.cms.bean;

import static ccm.dao.bean.connectivity.getLDBConnection;
import static java.lang.System.getProperties;
import static java.lang.System.out;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import java.util.Properties;
import javax.mail.Message;
import static javax.mail.Message.RecipientType.BCC;
import static javax.mail.Message.RecipientType.TO;
import javax.mail.Session;
import static javax.mail.Session.getDefaultInstance;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * @created for APOLLO PHARMACY DPAPP
 * @author PITCHU
 * @created Oct 27, 2017 8:04:45 PM
 */
public class crmSendMail {

    public void sendMail(String _ticketNo, String mailid) {
        try {
            String host = "smtp.gmail.com",
                    user = "customerservice@apollopharmacy.org",
                    pass = "ap0ll0@9";
            String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";
            //mailid = "customerservice@apollopharmacy.org";
            String mailidbcc = "customerservice@apollopharmacy.org";
            /*String mailid = "cgbalaji@apollopharmacy.org";
                        String mailidbcc = "balaji_c@apollohospitals.com";*/
            String[] to = mailid.split(",");
            String[] bcc = mailidbcc.split(",");

            String from = "itd@apollopharmacy.org";
            String subject = "CRM Call Escalation for " + _ticketNo;
            StringBuilder htmlBuilder = new StringBuilder();
            htmlBuilder.append("<html>");
            htmlBuilder.append("<head><title>CRM Escalation</title></head>");
            Connection con = null;

            htmlBuilder.append("<h4>CRM Ticket No : " + _ticketNo + "</h4>");
            try {
                con = getLDBConnection();
//PreparedStatement ps = con.prepareStatement("SELECT b.ordforshopname,a.orderby, count(distinct(a.prescriptionid)) FROM CP_DTLPrescription a,CP_LoginMaster b  where a.orderby = b.ordforshopid and a.prescriptiondate <= getdate() and a.prescriptiondate >= getdate()-1 group by a.orderby,b.ordforshopname order by a.orderby desc");
                PreparedStatement ps = con.prepareStatement("SELECT (SELECT PROBLEMTYPE FROM CRM_PROBLEMTYPEMASTER WHERE PROBLEMID = M.PROBLEMTYPE) AS PROBLEMTYPE,CUSTNAME,PRICONTACTNO,SECCONTACTNO, LOCATION,TRACKINGREFNO, ORDERSOURCE,PROBLEMDESCRIPTION,SITEID,(SELECT NAME FROM cc_sun_locationdetails WHERE SITEID = M.SITEID),REGDATETIME   FROM CRM_CMS_MASTER M WHERE TICKETNO = ?");
                ps.setString(1, _ticketNo);
                ResultSet rs = ps.executeQuery();
                if (rs.next()) {

                    htmlBuilder.append("<table border=1 cellspacing=1 cellpadding=4>"
                            + "<tr style=\"background-color: #87ceeb;\">"
                            + "<td>QUERY TYPE</td><td><b>" + rs.getString(1) + "</b></td>"
                            + "</tr>");
                    htmlBuilder.append("<tr>"
                            + "<td>CUSTOMER NAME</td><td>" + rs.getString(2) + "</td>"
                            + "</tr>");
                    htmlBuilder.append("<tr>"
                            + "<td>CONTACT NUMBER</td><td>" + rs.getString(3) + " - " + rs.getString(4) + "</td>"
                            + "</tr>");
                    htmlBuilder.append("<tr>"
                            + "<td>LOCATION</td><td>" + rs.getString(5) + "</td>"
                            + "</tr>");
                    htmlBuilder.append("<tr>"
                            + "<td>TRACKING REF. NO.</td><td>" + rs.getString(6) + "</td>"
                            + "</tr>");
                    htmlBuilder.append("<tr>"
                            + "<td>SOURCE</td><td>" + rs.getString(7) + "</td>"
                            + "</tr>");
                    htmlBuilder.append("<tr>"
                            + "<td>PROBLEM DESC.</td><td>" + rs.getString(8) + "</td>"
                            + "</tr>");
                    htmlBuilder.append("<tr>"
                            + "<td>SITE</td><td>" + rs.getString(9) + " - " + rs.getString(10) + "</td>"
                            + "</tr>");
                    htmlBuilder.append("<tr>"
                            + "<td>REG. DATETIME</td><td>" + rs.getString(11) + "</td>"
                            + "</tr>");
                    htmlBuilder.append("</table>");

                    htmlBuilder.append("<br><hr><br>");
                    PreparedStatement ps1 = con.prepareStatement("select ticketno,(SELECT USERNAME FROM CCLOGIN WHERE id = processby),processdatetime,(select username from cclogin where usr = allottedto), remarks,action from CRM_CMS_DTL where TICKETNO = ? order by PROCESSDATETIME asc");
                    ps1.setString(1, _ticketNo);
                    ResultSet rs1 = ps1.executeQuery();
                    htmlBuilder.append("<table border=1 cellspacing=1 cellpadding=4>"
                            + "<tr style=\"background-color: #87ceeb;\"><td colspan=5><b>Call History</b></td></tr>"
                            + "<tr "
                            + "style=\"background-color: #87ceeb;"
                            + " font-color: #FFFFFF\">"
                            + "<td><b>PROCESS BY</b></td>"
                            + "<td><b>ALLOTTED or FORWADED</b></td>"
                            + "<td><b>PROCESSED DATETIME</b></td>"
                            + "<td><b>REMARKS</b></td>"
                            + "<td><b>ACTION</b></td>"
                            + "</tr>");

                    while (rs1.next()) {
                        htmlBuilder.append("<tr>"
                                + "<td>" + rs1.getString(2) + "</td>"
                                + "<td>" + rs1.getString(4) + "</td>"
                                + "<td>" + rs1.getString(3) + "</td>"
                                + "<td>" + rs1.getString(5) + "</td>"
                                + "<td>" + rs1.getString(6) + "</td>"
                                + "</tr>");
                    }
                    htmlBuilder.append("</table>");
                    htmlBuilder.append("<br><br>Thanks and Regards,<br>");
                    htmlBuilder.append("<b><i>Apollo Pharmacy - Customer Care,</i></b><br>");
                    htmlBuilder.append("1860-500-0101.");
                } else {

                }
            } catch (Exception e) {
                out.println(e.getLocalizedMessage());
            } finally {
                con.close();
            }

            htmlBuilder.append("</body>");
            htmlBuilder.append("</html>");
            String html = htmlBuilder.toString();
            String messageText = html;
            boolean sessionDebug = true;
            Properties props = getProperties();
            props.put("mail.host", host);
            props.put("mail.transport.protocol.", "smtp");
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.", "true");
            props.put("mail.smtp.port", "465");
            props.put("mail.smtp.socketFactory.fallback", "false");
            props.put("mail.smtp.socketFactory.class", SSL_FACTORY);
            Session mailSession = getDefaultInstance(props, null);
            mailSession.setDebug(sessionDebug);
            Message msg = new MimeMessage(mailSession);
            msg.setFrom(new InternetAddress(from));
            for (String item : to) {
                InternetAddress toa = new InternetAddress(item);
                msg.addRecipient(TO, toa);
            }
            for (String item : bcc) {
                InternetAddress toa = new InternetAddress(item);
                msg.addRecipient(BCC, toa);
            }
            msg.setSubject(subject);
            msg.setContent(messageText, "text/html");
            Transport transport = mailSession.getTransport("smtp");
            transport.connect(host, user, pass);
            try {
                transport.sendMessage(msg, msg.getAllRecipients());
            } catch (Exception err) {
                out.println("Error" + err.getMessage());
            }
            transport.close();
        } catch (Exception e) {
            out.println(e.getLocalizedMessage());
        }
    }
}
