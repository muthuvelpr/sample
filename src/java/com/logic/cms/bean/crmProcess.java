/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.logic.cms.bean;

import ccm.dao.bean.connectivity;
import static ccm.dao.bean.connectivity.getLDBConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;

/**
 * @created for APOLLO PHARMACY DPAPP
 * @author PITCHU
 * @created Oct 17, 2017 3:22:12 AM
 */
public class crmProcess {

    public void crmCallProcess(String _ticketNo) {
        Connection con = null;
        try {
            con = getLDBConnection();
            PreparedStatement ps = con.prepareStatement("update CRM_CMS_MASTER set status = 1 where ticketno = ?");
            ps.setString(1, _ticketNo);
            ps.executeUpdate();
        } catch (Exception e) {

        }
    }
}
