/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.logic.cms.bean;

import ccm.dao.bean.connectivity;
import static ccm.dao.bean.connectivity.getLDBConnection;
import static java.lang.System.out;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * @created for APOLLO PHARMACY DPAPP
 * @author PITCHU
 * @created Oct 28, 2017 5:27:26 PM
 */
public class crmOTP {

    public String getGeneratedPOSOTP(String _siteID, String _mobNo) {
        Connection con = null;
        String otpValue = "OTP for this transaction is : ";
        try {
            con = connectivity.getSSOOTPConnection();
            PreparedStatement ps = con.prepareStatement("SELECT siteid, mobileno, otpno FROM CC_OFFERDATA WHERE offertype <> 6 AND siteid = ? AND mobileno = ? AND status = '0' AND billdate IN (SELECT MAX (billdate)FROM CC_OFFERDATA  WHERE siteid = ? AND mobileno = ? AND status = '0')");
            ps.setString(1, _siteID);
            ps.setString(2, _mobNo);
            ps.setString(3, _siteID);
            ps.setString(4, _mobNo);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                otpValue += rs.getString(3);
            } else {
                otpValue += "-";
            }
        } catch (Exception e) {
            out.println(e.getLocalizedMessage());
        }
        return otpValue;
    }

    public void otpLog(String _siteID, String _mobNo, String _otp, String _loggedBy) {
        Connection con = null;
        try {
            con = getLDBConnection();
            PreparedStatement ps = con.prepareStatement("insert into CRM_OTPOFFERS_LOG (SITEID,CONTACTNO,OTP,AGENTID) values (?,?,?,?)");
            ps.setString(1, _siteID);
            ps.setString(2, _mobNo);
            ps.setString(3, _otp);
            ps.setString(4, _loggedBy);
            ps.executeUpdate();
        } catch (Exception e) {
            out.println(e.getLocalizedMessage());
        }
    }
}
