/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.logic.cms.model;

/**
 * @created for APOLLO PHARMACY DPAPP
 * @author PITCHU
 * @created Oct 17, 2017 1:23:00 AM
 */
public class processingCalls {

    private String probType, PROBLEMTYPE, CUSTNAME, PRICONTACTNO, SECCONTACTNO, LOCATION, ADDRESS, TRACKINGREFNO, ORDERSOURCE, PROBLEMDESCRIPTION, SITEID, TICKETNO, REGDATETIME, REGBY, SITENAME, MAILID, STATUS;

    public processingCalls(String probType, String PROBLEMTYPE, String CUSTNAME, String PRICONTACTNO, String SECCONTACTNO, String LOCATION, String ADDRESS, String TRACKINGREFNO, String ORDERSOURCE, String PROBLEMDESCRIPTION, String SITEID, String TICKETNO, String REGDATETIME, String REGBY, String SITENAME, String MAILID, String STATUS) {
        this.probType = probType;
        this.PROBLEMTYPE = PROBLEMTYPE;
        this.CUSTNAME = CUSTNAME;
        this.PRICONTACTNO = PRICONTACTNO;
        this.SECCONTACTNO = SECCONTACTNO;
        this.LOCATION = LOCATION;
        this.ADDRESS = ADDRESS;
        this.TRACKINGREFNO = TRACKINGREFNO;
        this.ORDERSOURCE = ORDERSOURCE;
        this.PROBLEMDESCRIPTION = PROBLEMDESCRIPTION;
        this.SITEID = SITEID;
        this.TICKETNO = TICKETNO;
        this.REGDATETIME = REGDATETIME;
        this.REGBY = REGBY;
        this.SITENAME = SITENAME;
        this.MAILID = MAILID;
        this.STATUS = STATUS;
    }

    public String getSTATUS() {
        return STATUS;
    }

    public void setSTATUS(String STATUS) {
        this.STATUS = STATUS;
    }

    public String getSITENAME() {
        return SITENAME;
    }

    public void setSITENAME(String SITENAME) {
        this.SITENAME = SITENAME;
    }

    public String getProbType() {
        return probType;
    }

    public void setProbType(String probType) {
        this.probType = probType;
    }

    public String getPROBLEMTYPE() {
        return PROBLEMTYPE;
    }

    public void setPROBLEMTYPE(String PROBLEMTYPE) {
        this.PROBLEMTYPE = PROBLEMTYPE;
    }

    public String getCUSTNAME() {
        return CUSTNAME;
    }

    public void setCUSTNAME(String CUSTNAME) {
        this.CUSTNAME = CUSTNAME;
    }

    public String getPRICONTACTNO() {
        return PRICONTACTNO;
    }

    public void setPRICONTACTNO(String PRICONTACTNO) {
        this.PRICONTACTNO = PRICONTACTNO;
    }

    public String getSECCONTACTNO() {
        return SECCONTACTNO;
    }

    public void setSECCONTACTNO(String SECCONTACTNO) {
        this.SECCONTACTNO = SECCONTACTNO;
    }

    public String getLOCATION() {
        return LOCATION;
    }

    public void setLOCATION(String LOCATION) {
        this.LOCATION = LOCATION;
    }

    public String getADDRESS() {
        return ADDRESS;
    }

    public void setADDRESS(String ADDRESS) {
        this.ADDRESS = ADDRESS;
    }

    public String getTRACKINGREFNO() {
        return TRACKINGREFNO;
    }

    public void setTRACKINGREFNO(String TRACKINGREFNO) {
        this.TRACKINGREFNO = TRACKINGREFNO;
    }

    public String getORDERSOURCE() {
        return ORDERSOURCE;
    }

    public void setORDERSOURCE(String ORDERSOURCE) {
        this.ORDERSOURCE = ORDERSOURCE;
    }

    public String getPROBLEMDESCRIPTION() {
        return PROBLEMDESCRIPTION;
    }

    public void setPROBLEMDESCRIPTION(String PROBLEMDESCRIPTION) {
        this.PROBLEMDESCRIPTION = PROBLEMDESCRIPTION;
    }

    public String getSITEID() {
        return SITEID;
    }

    public void setSITEID(String SITEID) {
        this.SITEID = SITEID;
    }

    public String getTICKETNO() {
        return TICKETNO;
    }

    public void setTICKETNO(String TICKETNO) {
        this.TICKETNO = TICKETNO;
    }

    public String getREGDATETIME() {
        return REGDATETIME;
    }

    public void setREGDATETIME(String REGDATETIME) {
        this.REGDATETIME = REGDATETIME;
    }

    public String getREGBY() {
        return REGBY;
    }

    public void setREGBY(String REGBY) {
        this.REGBY = REGBY;
    }

    public String getMAILID() {
        return MAILID;
    }

    public void setMAILID(String MAILID) {
        this.MAILID = MAILID;
    }

}
