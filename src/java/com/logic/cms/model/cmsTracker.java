/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.logic.cms.model;

/**
 *
 * @author Lenovo
 */
public class cmsTracker {

    private String bName, ticketNo, probDesc, regTime, closeTime, staffName, processBy, remarks, status, action, duration, callDuration;
    private long sortduration, sortcallDuration;
    private int sno;

    public cmsTracker(String bName, String ticketNo, String probDesc, String regTime, String closeTime, String staffName, String processBy, String remarks, String status, String action, String duration, String callDuration, long sortduration, long sortcallDuration, int sno) {
        this.bName = bName;
        this.ticketNo = ticketNo;
        this.probDesc = probDesc;
        this.regTime = regTime;
        this.closeTime = closeTime;
        this.staffName = staffName;
        this.processBy = processBy;
        this.remarks = remarks;
        this.status = status;
        this.action = action;
        this.duration = duration;
        this.callDuration = callDuration;
        this.sortduration = sortduration;
        this.sortcallDuration = sortcallDuration;
        this.sno = sno;
    }

    public cmsTracker(String bName, String ticketNo, String probDesc, String regTime, String closeTime, String staffName, String processBy, String remarks, String status, String action) {
        this.bName = bName;
        this.ticketNo = ticketNo;
        this.probDesc = probDesc;
        this.regTime = regTime;
        this.closeTime = closeTime;
        this.staffName = staffName;
        this.processBy = processBy;
        this.remarks = remarks;
        this.status = status;
        this.action = action;
    }

    public int getSno() {
        return sno;
    }

    public void setSno(int sno) {
        this.sno = sno;
    }

    public long getSortduration() {
        return sortduration;
    }

    public void setSortduration(long sortduration) {
        this.sortduration = sortduration;
    }

    public long getSortcallDuration() {
        return sortcallDuration;
    }

    public void setSortcallDuration(long sortcallDuration) {
        this.sortcallDuration = sortcallDuration;
    }

    public String getCallDuration() {
        return callDuration;
    }

    public void setCallDuration(String callDuration) {
        this.callDuration = callDuration;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getbName() {
        return bName;
    }

    public void setbName(String bName) {
        this.bName = bName;
    }

    public String getCloseTime() {
        return closeTime;
    }

    public void setCloseTime(String closeTime) {
        this.closeTime = closeTime;
    }

    public String getProbDesc() {
        return probDesc;
    }

    public void setProbDesc(String probDesc) {
        this.probDesc = probDesc;
    }

    public String getProcessBy() {
        return processBy;
    }

    public void setProcessBy(String processBy) {
        this.processBy = processBy;
    }

    public String getRegTime() {
        return regTime;
    }

    public void setRegTime(String regTime) {
        this.regTime = regTime;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getStaffName() {
        return staffName;
    }

    public void setStaffName(String staffName) {
        this.staffName = staffName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTicketNo() {
        return ticketNo;
    }

    public void setTicketNo(String ticketNo) {
        this.ticketNo = ticketNo;
    }
}
