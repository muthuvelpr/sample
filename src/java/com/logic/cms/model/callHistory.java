/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.logic.cms.model;

/**
 * @created for APOLLO PHARMACY DPAPP
 * @author PITCHU
 * @created Oct 17, 2017 2:02:46 AM
 */
public class callHistory {

    private String ticketno, regby, processdt, allottedto, remarks, action;

    public callHistory(String ticketno, String regby, String processdt, String allottedto, String remarks, String action) {
        this.ticketno = ticketno;
        this.regby = regby;
        this.processdt = processdt;
        this.allottedto = allottedto;
        this.remarks = remarks;
        this.action = action;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getTicketno() {
        return ticketno;
    }

    public void setTicketno(String ticketno) {
        this.ticketno = ticketno;
    }

    public String getRegby() {
        return regby;
    }

    public void setRegby(String regby) {
        this.regby = regby;
    }

    public String getProcessdt() {
        return processdt;
    }

    public void setProcessdt(String processdt) {
        this.processdt = processdt;
    }

    public String getAllottedto() {
        return allottedto;
    }

    public void setAllottedto(String allottedto) {
        this.allottedto = allottedto;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

}
