/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.logic.cms.model;

/**
 * @created for APOLLO PHARMACY DPAPP
 * @author PITCHU
 * @created Sep 28, 2017 3:23:56 PM
 */
public class allocation {

    private String allocationID, allottedMem;

    public allocation(String allocationID, String allottedMem) {
        this.allocationID = allocationID;
        this.allottedMem = allottedMem;
    }

    public String getAllocationID() {
        return allocationID;
    }

    public void setAllocationID(String allocationID) {
        this.allocationID = allocationID;
    }

    public String getAllottedMem() {
        return allottedMem;
    }

    public void setAllottedMem(String allottedMem) {
        this.allottedMem = allottedMem;
    }

}
