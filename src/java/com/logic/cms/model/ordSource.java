/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.logic.cms.model;

/**
 * @created for APOLLO PHARMACY DPAPP
 * @author PITCHU
 * @created Sep 28, 2017 5:28:05 PM
 */
public class ordSource {

    private String orderSource;

    public ordSource(String orderSource) {
        this.orderSource = orderSource;
    }

    public String getOrderSource() {
        return orderSource;
    }

    public void setOrderSource(String orderSource) {
        this.orderSource = orderSource;
    }

}
