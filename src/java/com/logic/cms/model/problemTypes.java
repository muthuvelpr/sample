/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.logic.cms.model;

/**
 * @created for APOLLO PHARMACY DPAPP
 * @author PITCHU
 * @created Sep 28, 2017 3:20:59 PM
 */
public class problemTypes {

    String probID, probType;

    public problemTypes(String probID, String probType) {
        this.probID = probID;
        this.probType = probType;
    }

    public String getProbID() {
        return probID;
    }

    public void setProbID(String probID) {
        this.probID = probID;
    }

    public String getProbType() {
        return probType;
    }

    public void setProbType(String probType) {
        this.probType = probType;
    }

}
