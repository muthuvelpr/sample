/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crm.cust.business.bean;

import java.util.List;

/**
 *
 * @author Lenovo
 */
public class custBillSummary {

    private String custName, billNo, billDate, total, disc, netTotal, id;

    public custBillSummary(String custName, String billNo, String billDate, String total, String disc, String netTotal, String id) {
        this.custName = custName;
        this.billNo = billNo;
        this.billDate = billDate;
        this.total = total;
        this.disc = disc;
        this.netTotal = netTotal;
        this.id = id;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getBillDate() {
        return billDate;
    }

    public void setBillDate(String billDate) {
        this.billDate = billDate;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getDisc() {
        return disc;
    }

    public void setDisc(String disc) {
        this.disc = disc;
    }

    public String getNetTotal() {
        return netTotal;
    }

    public void setNetTotal(String netTotal) {
        this.netTotal = netTotal;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
