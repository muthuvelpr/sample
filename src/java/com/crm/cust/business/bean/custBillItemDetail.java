/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crm.cust.business.bean;

/**
 *
 * @author Lenovo
 */
public class custBillItemDetail {

    private String artCode, artName, qty, mrp;

    public custBillItemDetail(String artCode, String artName, String qty, String mrp) {
        this.artCode = artCode;
        this.artName = artName;
        this.qty = qty;
        this.mrp = mrp;
    }

    public String getArtCode() {
        return artCode;
    }

    public void setArtCode(String artCode) {
        this.artCode = artCode;
    }

    public String getArtName() {
        return artName;
    }

    public void setArtName(String artName) {
        this.artName = artName;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getMrp() {
        return mrp;
    }

    public void setMrp(String mrp) {
        this.mrp = mrp;
    }

}
