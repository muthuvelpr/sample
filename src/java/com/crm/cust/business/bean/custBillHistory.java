/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crm.cust.business.bean;

/**
 *
 * @author Lenovo
 */
public class custBillHistory {

    private String siteID, siteName, partID, partName, noofBills;

    public custBillHistory(String siteID, String siteName, String partID, String partName, String noofBills) {
        this.siteID = siteID;
        this.siteName = siteName;
        this.partID = partID;
        this.partName = partName;
        this.noofBills = noofBills;
    }

    public String getSiteID() {
        return siteID;
    }

    public void setSiteID(String siteID) {
        this.siteID = siteID;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public String getPartID() {
        return partID;
    }

    public void setPartID(String partID) {
        this.partID = partID;
    }

    public String getPartName() {
        return partName;
    }

    public void setPartName(String partName) {
        this.partName = partName;
    }

    public String getNoofBills() {
        return noofBills;
    }

    public void setNoofBills(String noofBills) {
        this.noofBills = noofBills;
    }

}
