/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crm.cust.business.bean;

/**
 *
 * @author Lenovo
 */
public class Task {

    private String title;
    private String imagePath;
    private boolean period;

    public Task(String title, String imagePath, boolean period) {
        this.title = title;
        this.imagePath = imagePath;
        this.period = period;
    }

    public boolean isPeriod() {
        return period;
    }

    public void setPeriod(boolean period) {
        this.period = period;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

}
