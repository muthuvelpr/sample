/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crm.cust.business;

import ccm.dao.bean.connectivity;
import static ccm.dao.bean.connectivity.getLDBConnection;
import static ccm.dao.bean.connectivity.getMISServerGOLDConnection;
import com.crm.cust.business.bean.custBillHistory;
import com.crm.cust.business.bean.custBillItemDetail;
import com.crm.cust.business.bean.custBillSummary;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import static java.lang.System.out;
import java.net.HttpURLConnection;
import static java.net.HttpURLConnection.HTTP_OK;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import static java.util.Calendar.DATE;
import static java.util.Calendar.HOUR;
import static java.util.Calendar.MINUTE;
import static java.util.Calendar.MONTH;
import static java.util.Calendar.SECOND;
import static java.util.Calendar.YEAR;
import static java.util.Calendar.getInstance;
import java.util.Date;
import java.util.List;
import javax.faces.application.FacesMessage;
import static javax.faces.application.FacesMessage.SEVERITY_INFO;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import static javax.faces.context.FacesContext.getCurrentInstance;
import javax.servlet.http.HttpServletRequest;
import org.primefaces.event.timeline.TimelineSelectEvent;
import org.primefaces.model.timeline.TimelineEvent;
import org.primefaces.model.timeline.TimelineModel;

/**
 *
 * @author Lenovo
 */
@ManagedBean
@ViewScoped
public class c360 implements Serializable {

    private String mobNo = "";
    private final List<custBillHistory> billHistory = new ArrayList<custBillHistory>();
    private final List<custBillSummary> billSummary = new ArrayList<custBillSummary>();
    private List<custBillItemDetail> billItemDetail = new ArrayList<custBillItemDetail>();
    private TimelineModel model;
    private Date min;
    private Date max;
    private long zoomMin;
    private long zoomMax;

    private String loyStatus, loyPoints, loyMember, loyCustName;
    private boolean selectable = true;
    private boolean zoomable = true;
    private boolean moveable = true;
    private boolean snapEvents = true;
    private boolean stackEvents = true;
    private String eventStyle = "box";
    private boolean axisOnTop;
    private boolean showCurrentTime = true;
    private boolean showNavigation = true;

    public String getLoyCustName() {
        return loyCustName;
    }

    public void setLoyCustName(String loyCustName) {
        this.loyCustName = loyCustName;
    }

    public String getLoyStatus() {
        return loyStatus;
    }

    public void setLoyStatus(String loyStatus) {
        this.loyStatus = loyStatus;
    }

    public String getLoyPoints() {
        return loyPoints;
    }

    public void setLoyPoints(String loyPoints) {
        this.loyPoints = loyPoints;
    }

    public String getLoyMember() {
        return loyMember;
    }

    public void setLoyMember(String loyMember) {
        this.loyMember = loyMember;
    }

    protected void TLinitialize() throws SQLException {
        model = new TimelineModel();
        Calendar cal = getInstance();
        Connection con = null;
        try {
            model.clear();
            con = getMISServerGOLDConnection();
            PreparedStatement ps = con.prepareStatement("select regh_date,REGH_SUBDOC,REGH_NETTOTAL from kposreghead where regh_tele = ? group by regh_date,REGH_SUBDOC,REGH_NETTOTAL");
            ps.setString(1, mobNo);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                cal.setTime(rs.getDate(1));
                int Year = cal.get(YEAR);
                int Month = cal.get(MONTH);
                int Date = cal.get(DATE);
                int Hour = cal.get(HOUR);
                int Min = cal.get(MINUTE);
                int Sec = cal.get(SECOND);
                cal.set(Year, Month, Date, Hour, Min, Sec);
                model.add(new TimelineEvent(rs.getString(2) + " - " + rs.getString(3), cal.getTime()));
            }
        } catch (Exception e) {

        }
        try {
            PreparedStatement ps = con.prepareStatement("select min(regh_date),max(regh_date) from kposreghead where regh_tele = ?");
            ps.setString(1, mobNo);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                cal.setTime(rs.getDate(1));
                int Year = cal.get(YEAR);
                int Month = cal.get(MONTH);
                int Date = cal.get(DATE);
                int Hour = cal.get(HOUR);
                int Min = cal.get(MINUTE);
                int Sec = cal.get(SECOND);
                cal.set(Year, Month, Date, Hour, Min, Sec);
                cal.add(DATE, -30);
                min = cal.getTime();
                cal.setTime(rs.getDate(2));
                int MAXYear = cal.get(YEAR);
                int MAXMonth = cal.get(MONTH);
                int MAXDate = cal.get(DATE);
                int MAXHour = cal.get(HOUR);
                int MAXMin = cal.get(MINUTE);
                int MAXSec = cal.get(SECOND);
                cal.set(MAXYear, MAXMonth, MAXDate, MAXHour, MAXMin, MAXSec);
                cal.add(DATE, 30);
                max = cal.getTime();
            }
        } catch (Exception e) {

        }
        try {
            con = getLDBConnection();
            PreparedStatement ps = con.prepareStatement("select ticketno,status,regdatetime from crm_cms_master where pricontactno = ? or seccontactno = ?");
            ps.setString(1, mobNo);
            ps.setString(2, mobNo);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                String status = rs.getString(2);
                if (status.equals("0")) {
                    status = "NEW";
                } else if (status.equals("1")) {
                    status = "PROCESSING";
                } else if (status.equals("2")) {
                    status = "CLOSED";
                }
                cal.setTime(rs.getDate(3));
                int Year = cal.get(YEAR);
                int Month = cal.get(MONTH);
                int Date = cal.get(DATE);
                int Hour = cal.get(HOUR);
                int Min = cal.get(MINUTE);
                int Sec = cal.get(SECOND);
                cal.set(Year, Month, Date, Hour, Min, Sec);
                model.add(new TimelineEvent(rs.getString(1) + " - " + status, cal.getTime()));
            }
        } catch (Exception e) {

        } finally {
            con.close();
        }
        zoomMin = 1000L * 60 * 60 * 24;
        zoomMax = 1000L * 60 * 60 * 24 * 31 * 3;
    }

    public void onSelect(TimelineSelectEvent e) {
        TimelineEvent timelineEvent = e.getTimelineEvent();

        FacesMessage msg = new FacesMessage(SEVERITY_INFO, "Selected event:", timelineEvent.getData().toString());
        getCurrentInstance().addMessage(null, msg);
    }

    public boolean isSnapEvents() {
        return snapEvents;
    }

    public void setSnapEvents(boolean snapEvents) {
        this.snapEvents = snapEvents;
    }

    public long getZoomMin() {
        return zoomMin;
    }

    public void setZoomMin(long zoomMin) {
        this.zoomMin = zoomMin;
    }

    public long getZoomMax() {
        return zoomMax;
    }

    public void setZoomMax(long zoomMax) {
        this.zoomMax = zoomMax;
    }

    public Date getMin() {
        return min;
    }

    public void setMin(Date min) {
        this.min = min;
    }

    public Date getMax() {
        return max;
    }

    public void setMax(Date max) {
        this.max = max;
    }

    public TimelineModel getModel() {
        return model;
    }

    public boolean isSelectable() {
        return selectable;
    }

    public void setSelectable(boolean selectable) {
        this.selectable = selectable;
    }

    public boolean isZoomable() {
        return zoomable;
    }

    public void setZoomable(boolean zoomable) {
        this.zoomable = zoomable;
    }

    public boolean isMoveable() {
        return moveable;
    }

    public void setMoveable(boolean moveable) {
        this.moveable = moveable;
    }

    public boolean isStackEvents() {
        return stackEvents;
    }

    public void setStackEvents(boolean stackEvents) {
        this.stackEvents = stackEvents;
    }

    public String getEventStyle() {
        return eventStyle;
    }

    public void setEventStyle(String eventStyle) {
        this.eventStyle = eventStyle;
    }

    public boolean isAxisOnTop() {
        return axisOnTop;
    }

    public void setAxisOnTop(boolean axisOnTop) {
        this.axisOnTop = axisOnTop;
    }

    public boolean isShowCurrentTime() {
        return showCurrentTime;
    }

    public void setShowCurrentTime(boolean showCurrentTime) {
        this.showCurrentTime = showCurrentTime;
    }

    public boolean isShowNavigation() {
        return showNavigation;
    }

    public void setShowNavigation(boolean showNavigation) {
        this.showNavigation = showNavigation;
    }

    public String getMobNo() {
        return mobNo;
    }

    public void setMobNo(String mobNo) {
        this.mobNo = mobNo;
    }

    public List fetchCustBillHistory() throws SQLException {
        TLinitialize();
        fetchCustLoyalty(mobNo);

        try {
            billHistory.clear();
            billItemDetail.clear();
            billSummary.clear();
            Connection con = getMISServerGOLDConnection();
            PreparedStatement ps = con.prepareStatement("SELECT regh_site, pksitdgene.get_sitedescription@gold (regh_site) sitename, regh_part, (SELECT csmgname FROM kcsmgroup@gold WHERE csmgid = regh_part),count(regh_part) FROM kposreghead WHERE regh_tele = ? GROUP BY regh_site, regh_part order by 4 asc");
            ps.setString(1, mobNo);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                billHistory.add(new custBillHistory(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5)));
            }
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
        }
        return billHistory;
    }

    public List<custBillHistory> getBillHistory() {
        return billHistory;
    }

    public List fetchCustBillSummaryPartnerwise() {
        FacesContext context = getCurrentInstance();
        HttpServletRequest myRequest = (HttpServletRequest) context.getExternalContext().getRequest();
        String siteID = myRequest.getParameter("siteID");
        String partnerID = myRequest.getParameter("partnerID");
        Connection con = null;
        try {
            billSummary.clear();
            con = getMISServerGOLDConnection();
            PreparedStatement ps = con.prepareStatement("SELECT reghc_name,regh_subdoc, regh_date, regh_total, regh_disc, regh_nettotal,ID FROM kposreghead WHERE regh_site = ? and regh_part = ? AND regh_tele = ?");
            ps.setString(1, siteID);
            ps.setString(2, partnerID);
            ps.setString(3, mobNo);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                billSummary.add(new custBillSummary(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7)));
            }
        } catch (Exception e) {
            out.println(e.getLocalizedMessage());
        }
        return billSummary;
    }

    public List<custBillSummary> getBillSummary() {
        return billSummary;
    }

    public List fetchItemDetails(String _id) {
        FacesContext context = getCurrentInstance();
        HttpServletRequest myRequest = (HttpServletRequest) context.getExternalContext().getRequest();
        String id = myRequest.getParameter("id");
        Connection con = null;
        billItemDetail.clear();
        try {
            con = getMISServerGOLDConnection();
            PreparedStatement ps = con.prepareStatement("select regi_artcod,pkstrucobj.get_desc@gold(regi_artcod,'GB') itemname, regi_issqty,regi_price from kposregitem where  id = ?");
            ps.setString(1, _id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                billItemDetail.add(new custBillItemDetail(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4)));
            }
        } catch (Exception e) {
            out.println(e.getLocalizedMessage());
        }
        return billItemDetail;
    }

    public List<custBillItemDetail> getBillItemDetail() {
        return billItemDetail;
    }

    public void fetchCustLoyalty(String _mob) {
        try {
            final String USER_AGENT = "Mozilla/5.0";
            //http://10.4.14.4:8015/OneApolloLoyal.aspx?mobileno=8056427651&reqby=Mobile&Action=BALANCECHECK
            final String GET_URL = "http://10.4.14.4:8015/OneApolloLoyal.aspx?"
                    + "Action=BALANCECHECK&&reqby=Mobile&"
                    + "mobileno=" + _mob;
            URL obj = new URL(GET_URL);
            HttpURLConnection smscon = (HttpURLConnection) obj.openConnection();
            smscon.setRequestMethod("GET");
            smscon.setRequestProperty("User-Agent", USER_AGENT);
            int responseCode = smscon.getResponseCode();
            out.println("GET Response Code :: " + responseCode);
            if (responseCode == HTTP_OK) { // success
                BufferedReader in = new BufferedReader(new InputStreamReader(
                        smscon.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
                String res = response.toString();
                res = res.replace("|", ",");
                String[] data;
                String delims = ",";
                data = res.split(delims);
                loyStatus = data[0];
                if (loyStatus.equalsIgnoreCase("true")) {
                    loyCustName = data[1];
                    loyPoints = data[3];
                    loyMember = data[4];
                } else {
                    loyCustName = "-";
                    loyPoints = "-";
                    loyMember = "-";
                }
            } else {
                out.println("GET request not worked");
            }
        } catch (IOException ex) {
            out.println(ex.getLocalizedMessage());
        }
    }
}
