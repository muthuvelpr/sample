/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.heterolikereport.ctlr;

import com.heterolikereport.model.lsrSummary;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Pitchu
 * @created Apr 21, 2015 3:57:48 PM
 */
public class lsrLP {

    List<lsrSummary> lss = new ArrayList<lsrSummary>();

    /* public List lsrSum() {
        Connection con = null;
        String monthString = "";
        lss.clear();
        try {
            con = getProdServerGOLDConnection();
            PreparedStatement ps = con.prepareStatement("SELECT   MAX (lastupdated) lastupdated, SUM (noofbills) noofbills, siteid,cccardno, MAX (customername) customername,MAX (customertelephone) customertelephone, SUM (jan) AS jan,SUM (feb) AS feb, SUM (mar) AS mar, SUM (apr) AS apr,SUM (may) AS may, SUM (jun) AS jun, SUM (jul) AS jul,SUM (aug) AS aug, SUM (sep) AS sep, SUM (oct) AS oct,SUM (nov) AS nov, SUM (DEC) AS DEC FROM (SELECT (SELECT MAX (TRUNC (regh_date))FROM goldprod.kposreghead i WHERE id = s.id) AS lastupdated,COUNT (DISTINCT (s.regh_billno)) noofbills, s.regh_site AS siteid,TRIM (MAX (regh_parttr)) cccardno,(SELECT MAX (reghc_name)FROM goldprod.kposreghead i WHERE id = s.id) AS customername,(SELECT MAX (regh_tele) FROM goldprod.kposreghead i WHERE id = s.id) AS customertelephone, NVL(SUM (DECODE (EXTRACT (MONTH FROM s.regh_date),1,regi_issqty*regi_price))-SUM (DECODE (EXTRACT (MONTH FROM s.regh_date),1,regi_discamt)),0)jan, NVL(SUM (DECODE (EXTRACT (MONTH FROM s.regh_date),2,regi_issqty*regi_price))-SUM (DECODE (EXTRACT (MONTH FROM s.regh_date),2,regi_discamt)),0)feb, NVL(SUM (DECODE (EXTRACT (MONTH FROM s.regh_date),3,regi_issqty*regi_price))-SUM (DECODE (EXTRACT (MONTH FROM s.regh_date),3,regi_discamt)),0)mar,  NVL(SUM (DECODE (EXTRACT (MONTH FROM s.regh_date),4,regi_issqty*regi_price))-SUM (DECODE (EXTRACT (MONTH FROM s.regh_date),4,regi_discamt)),0)apr, NVL(SUM (DECODE (EXTRACT (MONTH FROM s.regh_date),5,regi_issqty*regi_price))-SUM (DECODE (EXTRACT (MONTH FROM s.regh_date),5,regi_discamt)),0)may,NVL(SUM (DECODE (EXTRACT (MONTH FROM s.regh_date),6,regi_issqty*regi_price))-SUM (DECODE (EXTRACT (MONTH FROM s.regh_date),6,regi_discamt)),0)jun, NVL(SUM (DECODE (EXTRACT (MONTH FROM s.regh_date),7,regi_issqty*regi_price))-SUM (DECODE (EXTRACT (MONTH FROM s.regh_date),7,regi_discamt)),0)jul, NVL(SUM (DECODE (EXTRACT (MONTH FROM s.regh_date),8,regi_issqty*regi_price))-SUM (DECODE (EXTRACT (MONTH FROM s.regh_date),8,regi_discamt)),0)aug, NVL(SUM (DECODE (EXTRACT (MONTH FROM s.regh_date),9,regi_issqty*regi_price))-SUM (DECODE (EXTRACT (MONTH FROM s.regh_date),9,regi_discamt)),0)sep, NVL(SUM (DECODE (EXTRACT (MONTH FROM s.regh_date),10,regi_issqty*regi_price))-SUM (DECODE (EXTRACT (MONTH FROM s.regh_date),10,regi_discamt)),0)oct, NVL(SUM (DECODE (EXTRACT (MONTH FROM s.regh_date),11,regi_issqty*regi_price))-SUM (DECODE (EXTRACT (MONTH FROM s.regh_date),11,regi_discamt)),0)nov, NVL(SUM (DECODE (EXTRACT (MONTH FROM s.regh_date),12,regi_issqty*regi_price))-SUM (DECODE (EXTRACT (MONTH FROM s.regh_date),12,regi_discamt)),0)dec FROM goldprod.kposreghead s, goldprod.kposregitem i WHERE s.id=i.id and regh_site = 14438 and trunc(regh_date) between '31-OCT-15' and '01-Nov-15' AND regh_part = 102 AND regh_subdoc NOT LIKE '%GF%' AND regh_transtype IN (0)  and regh_parttr IS NOT NULL  GROUP BY s.id,s.regh_site) a WHERE (  jan > 0 OR feb > 0 OR mar > 0 OR apr > 0 OR may > 0 OR jun > 0 OR jul > 0 OR aug > 0  OR sep > 0 OR oct > 0 OR nov > 0 OR dec > 0) GROUP BY siteid, cccardno ORDER BY 4");
            ResultSet rs = ps.executeQuery();
            Calendar cal = getInstance();
            int month = cal.get(MONTH) + 1;

            while (rs.next()) {
                String data1 = "";

                for (int i = 7; i > 1;) {
                    switch (month - i) {
                        case -1:
                            monthString = "DEC";
                            break;
                        case -2:
                            monthString = "NOV";
                            break;
                        case -3:
                            monthString = "OCT";
                            break;
                        case -4:
                            monthString = "SEP";
                            break;
                        case -5:
                            monthString = "AUG";
                            break;
                        case -6:
                            monthString = "JUL";
                            break;
                        case 0:
                            monthString = "JAN";
                            break;
                        case 1:
                            monthString = "FEB";
                            break;
                        case 2:
                            monthString = "MAR";
                            break;
                        case 3:
                            monthString = "APR";
                            break;
                        case 4:
                            monthString = "MAY";
                            break;
                        case 5:
                            monthString = "JUN";
                            break;
                        case 6:
                            monthString = "JUL";
                            break;
                        case 7:
                            monthString = "AUG";
                            break;
                        case 8:
                            monthString = "SEP";
                            break;
                        case 9:
                            monthString = "OCT";
                            break;
                        case 10:
                            monthString = "NOV";
                            break;
                        case 11:
                            monthString = "DEC";
                            break;

                        default:
                            monthString = "Invalid month";
                            break;
                    }
                    String data = rs.getString(monthString).toString();
                    data1 = data1 + data + " ";
                    i--;
                }
                String[] columns = data1.split(" ");

                String d1 = columns[0];
                String d2 = columns[1];
                String d3 = columns[2];
                String d4 = columns[3];
                String d5 = columns[4];
                String d6 = columns[5];
                out.println(rs.getString(4) + " - " + rs.getString(5) + " - " + rs.getString(6)
                        + "M1-" + d1 + "   :M2-" + d2 + "   :M3-" + d3 + "   :M4-" + d4 + "   :M5-" + d5 + "   :M6-" + d6);
                lss.add(new lsrSummary(rs.getString(4), rs.getString(5), rs.getString(6), d1, d2, d3, d4, d5, d6));
            }
        } catch (Exception e) {
        } finally {
            close(con);
        }
        return lss;
    }*/
}
