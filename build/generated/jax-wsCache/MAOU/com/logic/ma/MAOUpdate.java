
package com.logic.ma;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MA_Orderid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AP_orderid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Siteid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "maOrderid",
    "apOrderid",
    "siteid"
})
@XmlRootElement(name = "MAOUpdate")
public class MAOUpdate {

    @XmlElement(name = "MA_Orderid")
    protected String maOrderid;
    @XmlElement(name = "AP_orderid")
    protected String apOrderid;
    @XmlElement(name = "Siteid")
    protected String siteid;

    /**
     * Gets the value of the maOrderid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMAOrderid() {
        return maOrderid;
    }

    /**
     * Sets the value of the maOrderid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMAOrderid(String value) {
        this.maOrderid = value;
    }

    /**
     * Gets the value of the apOrderid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAPOrderid() {
        return apOrderid;
    }

    /**
     * Sets the value of the apOrderid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAPOrderid(String value) {
        this.apOrderid = value;
    }

    /**
     * Gets the value of the siteid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSiteid() {
        return siteid;
    }

    /**
     * Sets the value of the siteid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSiteid(String value) {
        this.siteid = value;
    }

}
