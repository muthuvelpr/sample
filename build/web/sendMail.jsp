<%-- 
    Document   : sendMail
    Created on : 31 Dec, 2014, 3:08:56 PM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="ccm.dao.bean.connectivity,java.io.File,java.util.List,java.io.IOException"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Properties"%>
<%@page import="javax.mail.Message"%>
<%@page import="javax.mail.Session"%>
<%@page import="javax.mail.Transport"%>
<%@page import="javax.mail.internet.InternetAddress"%>
<%@page import="javax.mail.internet.MimeMessage"%>
<%@page import="javax.mail.*,javax.mail.internet.*" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%
            try {
                String host = "smtp.gmail.com",
                        user = "itd@apollopharmacy.org",
                        pass = "apollo@741";
                String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";
                String mailid = "pitchaiah@apollopharmacy.org";
                String mailidbcc = "pitchaiah@apollopharmacy.org";
                /*String mailid = "cgbalaji@apollopharmacy.org";
                        String mailidbcc = "balaji_c@apollohospitals.com";*/
                String[] to = mailid.split(",");
                String[] bcc = mailidbcc.split(",");

                String from = "itd@apollopharmacy.org";
                String subject = "CRM Call Escalation ";
                StringBuilder htmlBuilder = new StringBuilder();
                htmlBuilder.append("<html>");
                htmlBuilder.append("<head><title>CRM Escalation</title></head>");
                Connection con = null;

                htmlBuilder.append("<h4>CRM Ticket No : " + "-" + "</h4>");
                try {
                    con = connectivity.getLDBConnection();
//PreparedStatement ps = con.prepareStatement("SELECT b.ordforshopname,a.orderby, count(distinct(a.prescriptionid)) FROM CP_DTLPrescription a,CP_LoginMaster b  where a.orderby = b.ordforshopid and a.prescriptiondate <= getdate() and a.prescriptiondate >= getdate()-1 group by a.orderby,b.ordforshopname order by a.orderby desc");
                    PreparedStatement ps = con.prepareStatement("SELECT (SELECT PROBLEMTYPE FROM CRM_PROBLEMTYPEMASTER WHERE PROBLEMID = M.PROBLEMTYPE) AS PROBLEMTYPE,CUSTNAME,PRICONTACTNO,SECCONTACTNO, LOCATION,TRACKINGREFNO, ORDERSOURCE,PROBLEMDESCRIPTION,SITEID,(SELECT NAME FROM cc_sun_locationdetails WHERE SITEID = M.SITEID),REGDATETIME   FROM CRM_CMS_MASTER M WHERE TICKETNO = ?");
                    ps.setString(1, _ticketNo);
                    ResultSet rs = ps.executeQuery();
                    if (rs.next()) {

                        htmlBuilder.append("<table border=1 cellspacing=1 cellpadding=4>"
                                + "<tr style=\"background-color: #87ceeb;\">"
                                + "<td>QUERY TYPE</td><td><b>" + rs.getString(1) + "</b></td>"
                                + "</tr>");
                        htmlBuilder.append("<tr>"
                                + "<td>CUSTOMER NAME</td><td>" + rs.getString(2) + "</td>"
                                + "</tr>");
                        htmlBuilder.append("<tr>"
                                + "<td>CONTACT NUMBER</td><td>" + rs.getString(3) + " - " + rs.getString(4) + "</td>"
                                + "</tr>");
                        htmlBuilder.append("<tr>"
                                + "<td>LOCATION</td><td>" + rs.getString(5) + "</td>"
                                + "</tr>");
                        htmlBuilder.append("<tr>"
                                + "<td>TRACKING REF. NO.</td><td>" + rs.getString(6) + "</td>"
                                + "</tr>");
                        htmlBuilder.append("<tr>"
                                + "<td>SOURCE</td><td>" + rs.getString(7) + "</td>"
                                + "</tr>");
                        htmlBuilder.append("<tr>"
                                + "<td>PROBLEM DESC.</td><td>" + rs.getString(8) + "</td>"
                                + "</tr>");
                        htmlBuilder.append("<tr>"
                                + "<td>SITE</td><td>" + rs.getString(9) + " - " + rs.getString(10) + "</td>"
                                + "</tr>");
                        htmlBuilder.append("<tr>"
                                + "<td>REG. DATETIME</td><td>" + rs.getString(11) + "</td>"
                                + "</tr>");
                        htmlBuilder.append("</table>");

                        htmlBuilder.append("<br><hr><br>");
                        PreparedStatement ps1 = con.prepareStatement("select ticketno,(SELECT USERNAME FROM CCLOGIN WHERE id = processby),processdatetime,(select username from cclogin where usr = allottedto), remarks,action from CRM_CMS_DTL where TICKETNO = 'COM171017-100000003' order by PROCESSDATETIME asc");
                        ps1.setString(1, _ticketNo);
                        ResultSet rs1 = ps1.executeQuery();
                        htmlBuilder.append("<table border=1 cellspacing=1 cellpadding=4>"
                                + "<tr "
                                + "style=\"background-color: #87ceeb;"
                                + " font-color: #FFFFFF\">"
                                + "<td><b>PROCESS BY</b></td>"
                                + "<td><b>ALLOTTED or FORWADED</b></td>"
                                + "<td><b>PROCESSED DATETIME</b></td>"
                                + "<td><b>REMARKS</b></td>"
                                + "<td><b>ACTION</b></td>"
                                + "</tr>");

                        while (rs1.next()) {
                            htmlBuilder.append("<tr>"
                                    + "<td>" + rs1.getString(2) + "</td>"
                                    + "<td>" + rs1.getString(4) + "</td>"
                                    + "<td>" + rs1.getString(3) + "</td>"
                                    + "<td>" + rs1.getString(5) + "</td>"
                                    + "<td>" + rs1.getString(6) + "</td>"
                                    + "</tr>");
                        }
                        htmlBuilder.append("</table>");
                    } else {

                    }
                } catch (Exception e) {
                    System.out.println(e.getLocalizedMessage());
                } finally {
                    con.close();
                }

                htmlBuilder.append("</body>");
                htmlBuilder.append("</html>");
                String html = htmlBuilder.toString();
                String messageText = html;
                boolean sessionDebug = true;
                Properties props = System.getProperties();
                props.put("mail.host", host);
                props.put("mail.transport.protocol.", "smtp");
                props.put("mail.smtp.auth", "true");
                props.put("mail.smtp.", "true");
                props.put("mail.smtp.port", "465");
                props.put("mail.smtp.socketFactory.fallback", "false");
                props.put("mail.smtp.socketFactory.class", SSL_FACTORY);
                Session mailSession = Session.getDefaultInstance(props, null);
                mailSession.setDebug(sessionDebug);
                Message msg = new MimeMessage(mailSession);
                msg.setFrom(new InternetAddress(from));
                for (String item : to) {
                    InternetAddress toa = new InternetAddress(item);
                    msg.addRecipient(Message.RecipientType.TO, toa);
                }
                for (String item : bcc) {
                    InternetAddress toa = new InternetAddress(item);
                    msg.addRecipient(Message.RecipientType.BCC, toa);
                }
                msg.setSubject(subject);
                msg.setContent(messageText, "text/html");
                Transport transport = mailSession.getTransport("smtp");
                transport.connect(host, user, pass);
                try {
                    transport.sendMessage(msg, msg.getAllRecipients());
                } catch (Exception err) {
                    System.out.println("Error" + err.getMessage());
                }
                transport.close();
            } catch (Exception e) {
                System.out.println(e.getLocalizedMessage());
            }
        %>
    </body>
</html>
