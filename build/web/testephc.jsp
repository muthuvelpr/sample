<%-- 
    Document   : testephc
    Created on : May 22, 2015, 11:07:23 AM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="ccm.dao.bean.connectivity,java.io.File,java.util.List,java.io.IOException"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Properties"%>
<%@page import="javax.mail.Message"%>
<%@page import="javax.mail.Session"%>
<%@page import="javax.mail.Transport"%>
<%@page import="javax.mail.internet.InternetAddress"%>
<%@page import="javax.mail.internet.MimeMessage"%>
<%@page import="javax.mail.*,javax.mail.internet.*" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%
                    SimpleDateFormat dfrmt = new SimpleDateFormat("dd-MMM-yyyy");
                    SimpleDateFormat pdmfrmt = new SimpleDateFormat("dd-MMM-yy");
                    Calendar c = Calendar.getInstance();
                    c.setTime(new java.util.Date());
                    c.add(Calendar.DATE, -1);
                    Date date = c.getTime();
                    String pd = dfrmt.format(date);
                    String pdm = pdmfrmt.format(date);
                    DecimalFormat df = new DecimalFormat("#,##,###.##");

                    SimpleDateFormat dfrmt1 = new SimpleDateFormat("dd-MMM-yy");
                    SimpleDateFormat datealone = new SimpleDateFormat("dd");

                    Calendar cal = Calendar.getInstance();
                    Date date2 = cal.getTime();
                    cal.set(Calendar.DAY_OF_MONTH, 1);
                    int dateofmonth = Integer.parseInt(datealone.format(new java.util.Date()));
                    if (dateofmonth <= 1) {
                        cal.add(Calendar.MONTH, -1);
                        date2 = cal.getTime();
                    } else if (dateofmonth > 1) {
                        cal.add(Calendar.MONTH, 0);
                        date2 = cal.getTime();
                    }
                    String mfd = dfrmt1.format(date2);
        %>
        <%
                    Connection con = null;
                    try {
                        con = connectivity.getGDBConnection();
                        //PreparedStatement ps = con.prepareStatement("SELECT b.ordforshopname,a.orderby, count(distinct(a.prescriptionid)) FROM CP_DTLPrescription a,CP_LoginMaster b  where a.orderby = b.ordforshopid and a.prescriptiondate <= getdate() and a.prescriptiondate >= getdate()-1 group by a.orderby,b.ordforshopname order by a.orderby desc");
                        PreparedStatement ps = con.prepareStatement("SELECT b.ordforshopname,a.orderby, count(distinct(a.prescriptionid)),sum(qty*mrp) as mrpval FROM CP_DTLPrescription a,CP_LoginMaster b where a.orderby = b.ordforshopid and a.prescriptiondate > convert(varchar(100),getdate()-1,110) and a.prescriptiondate < convert(varchar(100),getdate(),110) group by a.orderby,b.ordforshopname order by a.orderby desc");
                        ResultSet rs = ps.executeQuery();
                        double i = 0.0;
                        if (rs.next()) {
                            out.println("<table border=5><tr><td>");
                            out.println("<table border=0 cellspacing=4 cellpadding=4><tr bor><th>SHOPNAME</th><th>NO. OF ORDERS</th><th>Order Value</th></tr>");
                            do {
                                out.println("<tr><td>" + rs.getString(1) + "(" + rs.getString(2) + ")" + "</td><td align=center>" + rs.getString(3) + "</td><td align=right>" + df.format(rs.getDouble(4)) + "</td></tr>");
                                out.println("" + rs.getString(1) + "(" + rs.getString(2) + ")" + "" + rs.getString(3) + "" + df.format(rs.getDouble(4)) + "");
                                i += rs.getDouble(4);
                            } while (rs.next());
                            out.println(df.format(i));
                            out.println("<tr><td colspan=2 align=right>Total</td><td align=right><b>" + df.format(i) + "</b></td></tr>");
                            out.println("</table>");
                            out.println("</td></tr></table>");
                        } else {
                            out.println("No Orders were raised yesterday");
                        }
                    } catch (Exception e) {
                        out.println(e.getLocalizedMessage());
                    } finally {
                        con.close();
                    }


                    out.println("<h4 color=navy  font-variant: small-caps>MTD e-PHC Help counter orders for the date between " + mfd + " to " + pdm + " </h4>");
                    try {
                        con = connectivity.getGDBConnection();
                        //PreparedStatement ps = con.prepareStatement("SELECT b.ordforshopname,a.orderby, count(distinct(a.prescriptionid)) FROM CP_DTLPrescription a,CP_LoginMaster b  where a.orderby = b.ordforshopid and a.prescriptiondate <= getdate() and a.prescriptiondate >= getdate()-1 group by a.orderby,b.ordforshopname order by a.orderby desc");
                        PreparedStatement ps = con.prepareStatement("select b.ordforshopname, a.orderby, count(distinct(a.prescriptionid)),sum(qty*mrp) from CP_DTLPrescription a,CP_LoginMaster b where a.orderby = b.ordforshopid and a.prescriptiondate between '" + mfd + "' and convert(varchar(100),getdate(),110) group by a.orderby,b.ordforshopname order by a.orderby desc");
                        ResultSet rs = ps.executeQuery();
                        double i = 0.0;
                        if (rs.next()) {
                            out.println("<table border=5><tr><td>");
                            out.println("<table border=0 cellspacing=4 cellpadding=4><tr bor><th>SHOPNAME</th><th>NO. OF ORDERS</th><th>Order Value</th></tr>");
                            do {
                                out.println("<tr><td>" + rs.getString(1) + "(" + rs.getString(2) + ")" + "</td><td align=center>" + rs.getString(3) + "</td><td align=right>" + df.format(rs.getDouble(4)) + "</td></tr>");
                                out.println("" + rs.getString(1) + "(" + rs.getString(2) + ")" + "" + rs.getString(3) + "" + df.format(rs.getDouble(4)) + "");
                                i += rs.getDouble(4);
                            } while (rs.next());
                            out.println(df.format(i));
                            out.println("<tr><td colspan=2 align=right>Total</td><td align=right><b>" + df.format(i) + "</b></td></tr>");
                            out.println("</table>");
                            out.println("</td></tr></table>");
                        } else {
                            out.println("No Orders were raised for the Period " + mfd + " to " + pd);
                        }
                    } catch (Exception e) {
                        out.println(e.getLocalizedMessage());
                    } finally {
                        con.close();
                    }

        %>
    </body>
</html>
